<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.transaction.controller.MemberResultUpdateController"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.EventMemberListViewBean"%>
<%@page import="org.hibernate.HibernateException"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.MemberRunViewBean"%>
<%@page import="org.hibernate.transform.Transformers"%>
<%@page import="com.uthkrushta.basis.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.EventResultBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.EventLapTimeResultBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.MemberRecentEventView"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.MemberUpcomingEventView"%>
<%@page import="com.uthkrushta.mybos.code.bean.TimeCodeBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeTimeMasterBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />
  <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />
 <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="../plugins/light-gallery/css/lightgallery.css" rel="stylesheet">
		 <link href="../css/themes/all-themes.css" rel="stylesheet" />
<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Bootstrap Datepicker Plugin Js -->
<!-- <script src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> -->


<!-- <link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
	rel="stylesheet" type="text/css"> -->


<!-- <!-- Bootstrap Material Datetime Picker Css  	-->
   <!--  <link href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" /> -->
  

<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<script type = "text/javascript">
    
   function onSave(){
	   
	   var bibNo = $("#<%=ILabelConstants.txtBibNo%>").val();
	   if(bibNo == ""){
		   alert("Please enter BIB No.");
		   return;
	   }
	   
	   var chip = $("#<%=ILabelConstants.txtCHIP%>").val();
	   var gun = $("#<%=ILabelConstants.txtGUN%>").val();
	   var pace = $("#<%=ILabelConstants.txtPACE%>").val(); 
	   
	   
		   if(!IsValidTime(chip)){
			   alert("Please Enter Valid CHIP Value");
			   return ;
		   }
		   if(!IsValidTime(gun)){
			   alert("Please Enter Valid GUN Value");
			   return ;
		   }
		   if(!IsValidTime(pace)){
			   alert("Please Enter Valid PACE Value");
			   return ;
		   }

	   
	   
	   
	   
	   document.forms[0].hdnUserAction.value = "SUBMIT"; 
       
    		 if(null!= document.forms[0].hdnAjaxSubmit && document.forms[0].hdnAjaxSubmit.value.toLowerCase()== 'true')	
    		{	
    			
    			ajaxAnywhere.submitAJAX();
    		}
    		else
    		{
    			document.forms[0].submit();
    		} 
	   
	   
	   
   }
   
   function isTimeValid(obj){
	   
	   if(!IsValidTime(obj)){
		   alert("Please enter time in hh:mm:ss format");
		   return;
	   }
   }
   
      
   
   
// Scroll up
   window.onscroll = function() {scrollFunction()};

   function scrollFunction() {
       if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
           document.getElementById("myBtn").style.display = "block";
       } else {
           document.getElementById("myBtn").style.display = "none";
       }
   }

   // When the user clicks on the button, scroll to the top of the document
   function topFunction() {
       document.body.scrollTop = 0;
       document.documentElement.scrollTop = 0;
   }

   // scroll up ends
   
   
   
   
   function IsValidTime(timeString)
   {
	  
       var pattern = /^(?:[01]?\d|2[0-3]):[0-5]\d:[0-5]\d$/;           // /^\d?\d:\d{2}:\d{2}$/;
		
       if (!timeString.match(pattern))
       { return false; }
       else{return true;}
   }

    </script>




</head>

<%
	int Index = 1;
	String strUserAction = "GET";
	String strStatusText = "";
	String strStatusStyle = "";
	String strbibNo = "";
	int intRowCounter = 1;
	/* long pastID = 0; */
	
	
	ArrayList<MessageBean> arrMsg = null; 
	MessageBean objMsg;
	
	String strImageContextPath = "/servlet1/";
	//To exclude the time from date
	
	
	//Ends
	MemberRunViewBean objMRVB = new MemberRunViewBean();
	UserMasterBean objUMB = new UserMasterBean();
	long userID = WebUtil.parseLong(request, ILabelConstants.UID);
	long eventID = WebUtil.parseLong(request, ILabelConstants.EID);
	long eventTypeID = WebUtil.parseLong(request, ILabelConstants.ETID);
	objUMB =(UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID ="+userID, "");
	
	EventResultBean objERB = new EventResultBean();
	
	MemberResultUpdateController objGC = new MemberResultUpdateController();
	
	
	EventPhotoUploadBean objEPUB2 = new EventPhotoUploadBean();
	EventTypeLapMasterBean objETLMB = new EventTypeLapMasterBean();
	EventMemberListViewBean objEMLVB = new EventMemberListViewBean();
	List lstMembers = null; 
	
	

		List lstRunData = null;
		MemberRunViewBean objSTOCTVB = new MemberRunViewBean();
			
	
	
		
		
		
		
		
		
	
	
	MemberRecentEventView objMREV = new MemberRecentEventView();
	objMREV = (MemberRecentEventView) DBUtil.getRecord( ILabelConstants.TR_MEMBER_RECENT_VIEW_BN, " lngUserID = "+userID, " dtEventStartDate DESC");
	if(objMREV == null){
		objMREV = new MemberRecentEventView();
	}
	
	long lngCertificateType = 1;
	String strCertificationRpt = "Rpt/eventCompletionCertificate.rpt";
	List lstLastEventPhotos = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_PHOTO_UPLOAD , IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID = "+objMREV.getLngEventID()+" AND FIND_IN_SET( '" + objMREV.getStrBIBNO() + "', strBibNoEntry) > 0",
			"");
	
	EventMasterBean objEMB = new EventMasterBean();
	EventTypeMasterBean objETMB = new EventTypeMasterBean(); 
	List lstEventTypeLap = null;
	
	
	
	
	if (AAUtils.isAjaxRequest(request)) {
		strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
		intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
		
		objGC.doAction(request, response);
		
		if(strUserAction.equalsIgnoreCase(ILabelConstants.DETAILS))
		{
			/* pastID= WebUtil.parseLong(request, ILabelConstants.hdnDetailID); */
			eventTypeID = WebUtil.parseLong(request, ILabelConstants.hdnEventTypeID);
			strbibNo = WebUtil.parseString(request, ILabelConstants.hdnBibNo);
			/* System.out.println(" pastID = "+pastID); */
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			objERB = (EventResultBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_RESULT, IUMCConstants.GET_ACTIVE_ROWS+
					" AND lngMemberID = "+userID+" AND lngEventID ="+eventID+" AND  lngEventTypeID="+eventTypeID, "");
			
			if(objERB == null){
				
				objERB = new EventResultBean();
			}
			 	lstEventTypeLap = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_LAP, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+eventTypeID, "lngID");
			 	if(lstEventTypeLap != null){
			 		intRowCounter = lstEventTypeLap.size();
			 	}
			 	
			 	objEMB = (EventMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_MASTER, " lngID = "+eventID, "");
	 				objETMB = (EventTypeMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_TYPE_MASTER, " lngID = "+eventTypeID, "");
            
	 				String strWhereClause = "SELECT i.* FROM (SELECT @p1/*'*/:=/*'*/ " + userID
	 						+ ") parm1,(SELECT @p2/*'*/:=/*'*/ " + eventID
	 						+ ") parm2,(SELECT @p3/*'*/:=/*'*/ " + eventTypeID
	 						+ ") parm3, tr_member_run_view i ";
	 				Session session1 = null;
	 				try {
	 				session1 = HibernateUtil.getSessionFactory().openSession();

	 				Query query = session1.createSQLQuery(strWhereClause);

	 				lstRunData = query.setResultTransformer(Transformers.aliasToBean(objSTOCTVB.getClass())).list();
	 				if(lstRunData != null && lstRunData.size() != 0 ){
	 					intRowCounter = lstRunData.size();
	 				}
	 				} catch (HibernateException e) {
	 				e.printStackTrace();
	 				} finally {
	 				if (null != session1) {
	 				session1.flush();
	 				session1.close();
	 				}
	 				}
	 				
			 		
			 		if(lngCertificateType == 1){
			 			strCertificationRpt = "Rpt/eventCompletionCertificate.rpt";
			 		}else{
			 			strCertificationRpt = "Rpt/eventCompletionCertificate.rpt";
			 		}
			 
			AAUtils.addZonesToRefresh(request, "List,Header,PastData");
		}
		
		
		
	
		
	}
		
	
	if(eventID > 0){
		
		if(eventTypeID >0){
			lstMembers = DBUtil.getRecords(ILabelConstants.BN_EVENT_MEMBER_LIST_VIEW, "lngMemberID ="+userID+" AND lngEventID ="+eventID+" AND lngEventTypeID = "+eventTypeID, "strFirstName");
		}else{
			lstMembers = DBUtil.getRecords(ILabelConstants.BN_EVENT_MEMBER_LIST_VIEW, "lngMemberID ="+userID+" AND lngEventID ="+eventID, "strFirstName");
		}
		
		}
	
	
	
	 arrMsg = objGC.getArrMsg();		//Getting the messages to display
		if(null!= arrMsg && arrMsg.size() > 0)
		{
			objMsg = MessageBean.getLeadingMessage(arrMsg);
			if(objMsg != null)
			{
		strStatusText = objMsg.getStrMsgText();
		strStatusStyle = objMsg.getStrMsgType();
			}
		}
	
	
		AAUtils.addZonesToRefresh(request, "List,Header");
	
	
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
  
	<!-- Page Loader -->


	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->


	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	



<form  method="post" > 
	<section class="content">
	
	
		
		<aa:zone name="Header" skipIfNotIncluded="true">  	
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
		</aa:zone>
		<aa:zone name="List" skipIfNotIncluded="true"> 			
		<div class="container-fluid">
		
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                    <div class="card">
                        <div class="header" >
							<!-- <h2 >Member Result Master</h2> -->
							<h2 style="color: #006699"><%=objUMB.getStrFirstName()%> <%=objUMB.getStrLastName() %></h2>
						</div>
                        <div class="body" style=" padding-top: 0px;">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                
                                
                                
                                      
                                            <div class="card" style="margin-bottom: 0px;">
                                             <h4 style="color: #006699;padding-top: 15px;padding-left: 25px;">
                                            Event Details
                                            </h4>
                                          <aa:zone name="Header" skipIfNotIncluded="true">   
                                            <div id="collapseTwo_19" class="panel-collapse" role="tabpanel" aria-labelledby="headingTwo_19">
                                              <div class="body table-responsive" style="padding-top: 0px;">
					                            <table class="table table-hover" >
					                                <thead>
					                                    <tr>
					                                        <th>#</th>
					                                        <th>Date</th>
					                                        <th>Event Name</th>
					                                        <th>Competition Name</th>
					                                         <th>BIB No.</th>
					                                        <th>Place</th>
					                                        <th>Details</th>
					                                    </tr>
					                                </thead>
					                                <% 	if(null!=lstMembers){
					               			 				for(int index =0; index<lstMembers.size(); index++)
					               			 				{
					               			 				objEMLVB = (EventMemberListViewBean) lstMembers.get(index);
					               			 				
					               			 				%>
					                                <tbody>
					                                    <tr>
					                                        <td scope="row"><%=index+1 %>
					                                        <input type ="hidden" name="<%=ILabelConstants.Past_Event_Hidden_ID%>" value="<%=objERB.getLngID() %>" >
					                                        
					                                        </td>
					                                        <td><%=WebUtil.formatDate(objEMLVB.getDtEventStartDate(), IUMCConstants.DATE_FORMAT) %></td>
					                                        <td><%=WebUtil.processString(objEMLVB.getStrEventName())%></td>
					                                       <td><%=WebUtil.processString(objEMLVB.getStrEventTypeName()) %></td>
					                                       <td><%=WebUtil.processString(objEMLVB.getStrBibNo()) %></td>
					                                        <td><%=WebUtil.processString(objEMLVB.getStrEventPlace()) %></td>
					                                        <td><input type="button" class="btn bg-teal waves-effect waves-effect m-r-20" value="Details" data-toggle="modal" onClick="details(<%=objEMLVB.getLngEventTypeID()%>,'<%=objEMLVB.getStrBibNo()%>' )" data-target="#largeModal"> </td>
					                                    </tr>
					                                    
					                                </tbody>
					                                 <%}}else{%>
					                                
					                                <td>No Records</td>
					                               <% }%> 
					                            </table>
					                            <%-- <input type="hidden" name="<%=ILabelConstants.hdnDetailID%>"  id="<%=ILabelConstants.hdnDetailID%>" > --%>
					                            <input type="hidden" name="<%=ILabelConstants.hdnEventTypeID%>"  id="<%=ILabelConstants.hdnEventTypeID%>" >
					                            <input type="hidden" name="<%=ILabelConstants.hdnBibNo%>"  id="<%=ILabelConstants.hdnBibNo%>" >
					                       		<%-- <input type="text" name="<%=ILabelConstants.hdnItemRowCounter%>" size="2"
												value="<%=intRowCounter%>" id="<%=ILabelConstants.hdnItemRowCounter%>"> --%>
					                       
					                        </div>
                                            </div>
                                            </aa:zone>
                                      </div>
                                        
                                        
                                        
                                         <aa:zone name="PastData" skipIfNotIncluded="true"> 
						 
						 <div class="body" id="hideDiv"  style="  padding-top: 0px; padding-left: 0px; padding-right: 0px;">
                            <!-- Nav tabs -->
                           
                           <div class="card" style="margin-bottom: 0px;">
							<!-- <h2 >Member Result Master</h2> -->
							<h4 style="color: #006699;padding-top: 15px;padding-left: 25px;">Time Data </h4>
						
                           
                           
                            <!-- <ul class="nav nav-tabs tab-nav-right" role="tablist" >
                                <li role="presentation" class="active" style="padding-left: 3px;"><a href="#home" data-toggle="tab" >Time data</a></li>
                                <li role="presentation" ><a href="#profile" data-toggle="tab">Certificate</a></li>
                                <li role="presentation" ><a href="#messages" data-toggle="tab">Photos</a></li>
                               
                            </ul> -->

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                   
                                   <div class="row"> 
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding-left: 30px;">Event Name :  <%=WebUtil.processString(objEMB.getStrName()) %>
                                    <input type="hidden" name="<%=ILabelConstants.hdnEventID%>" value="<%=objEMB.getLngID()%>">
                                    
                                    </div>  
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding-left: 30px;">Event Type Name : <%=WebUtil.processString(objETMB.getStrName()) %>
                                    <input type="hidden" name="<%=ILabelConstants.hdnEventTypeIDNew%>" value="<%=objETMB.getLngID()%>">
                                    </div> 
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="padding-left: 30px;">BIB No. : 
                                    <input type="text" name="<%=ILabelConstants.txtBibNo%>" id="<%=ILabelConstants.txtBibNo%>" value="<%=strbibNo%>">
                                    </div>
                                    </div>
                                    <div class="row"> 
                                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" style="padding-left: 30px;">Rank :#<%if(objERB.getLngRank() != 0){ %> 
                                    <input type="text" value="<%=objERB.getLngRank() %>" name="<%=ILabelConstants.txtRank%>" onkeypress="return isNumber(event)">
                                    
                                     <%} else{%>
                                       <input type="text" value="" name="<%=ILabelConstants.txtRank%>" onkeypress="return isNumber(event)">
                                     
                                     <%} %>
                                      </div>   
                                    
                                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" style="padding-left: 30px;">CHIP : 
                                    <input type="text" placeholder="hh:mm:ss" value= "<%=WebUtil.processString(objERB.getStrChipValue()) %>" id="<%=ILabelConstants.txtCHIP%>"
                                     name="<%=ILabelConstants.txtCHIP%>">
                                    <input type="hidden" value="<%=objERB.getLngID() %>" name="<%=ILabelConstants.hdnResultLngID%>">
                                    <input type="hidden" value="<%=objERB.getLngCreatedBy() %>" name="<%=ILabelConstants.hdnCreatedBy%>">
                                    <input type="hidden" value="<%=objERB.getDtCreatedOn() %>" name="<%=ILabelConstants.hdnCreatedOn%>">
                                    
                                   </div>  
                                   	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" style="padding-left: 30px;">GUN : 
                                   	<input type="text" placeholder="hh:mm:ss" value= "<%=WebUtil.processString(objERB.getStrGunValue())%>" id="<%=ILabelConstants.txtGUN%>" name="<%=ILabelConstants.txtGUN%>">
                                   	
                                   	</div>
                                   	<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" style="padding-left: 30px;">PACE : 
                                   	<input type="text" placeholder="hh:mm:ss" value= "<%=WebUtil.processString(objERB.getStrPacevalue()) %>" id="<%=ILabelConstants.txtPACE%>" name="<%=ILabelConstants.txtPACE%>">
                                   	
                                   	
                                   	</div>
                                   	</div>
                                   	<div class="row"> 
                                    <!-- <div class="body table-responsive"> -->
                           <div class="body table-responsive" style="padding-top: 0px;">
                            <table class="table table-hover" style="margin-left:15px; width:96%" >
                                <thead>
                                    <tr>
                                       <th>#</th>
                                        <th>Split</th>
                                        <th>Your Run Time</th>
                                        <th>Category Avg Time</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                
                               <%					
                               
                               if(lstRunData != null && lstRunData.size() > 1 ){
													for (int i = 0; i < lstRunData.size(); i++) {
																objMRVB = (MemberRunViewBean) lstRunData.get(i);

																if (objMRVB == null) {
																	objMRVB = new MemberRunViewBean();
																}

															%>
                                		
                              
                                
                                
                                   <tr>
													<td scope="row"><%=i + 1%></td>
													<td><%=WebUtil.processString(objMRVB.getLap_name())%>
													<input type="hidden"
													name="<%=ILabelConstants.hdnItemRowCounter%>" value="<%=intRowCounter%>">
													<input type="hidden"
													name="<%=ILabelConstants.hdnLapResID_ + i%>" value="<%=objMRVB.getLap_result_id()%>">
													<input type="hidden"
													name="<%=ILabelConstants.hdnLapID_ + i%>" value="<%=objMRVB.getLap_id()%>">
													
													</td>
													<td>
													<%if(i == lstRunData.size()-1) {%>
													<input type="text" value= "<%=objMRVB.getTime()%>" name="<%=ILabelConstants.txtTimeTaken_ + i%>" disabled onblur="isTimeValid(this.value)">
													<%}else{ %>
													<input type="text" value= "<%=objMRVB.getTime()%>" name="<%=ILabelConstants.txtTimeTaken_ + i%>" onblur="isTimeValid(this.value)">
													
													<%} %>
													
													</td>
													<td><%=objMRVB.getCategory_avg_time()%></td>
													<td></td>

												</tr>
                                    <%  		
                                	}
                                
                               }
                               
                               else if(lstEventTypeLap != null){
                            	   for (int i = 0; i < lstEventTypeLap.size(); i++) {
                            		   objETLMB = (EventTypeLapMasterBean) lstEventTypeLap.get(i);
                            	%>   
                            	<tr>
                            	  <td scope="row"><%=i + 1%></td>
                            	   <td><%=WebUtil.processString(objETLMB.getStrName()) %>
                            	   <input type="hidden"
												name="<%=ILabelConstants.hdnItemRowCounter%>" value="<%=intRowCounter%>">
                            	   <input type="hidden"
													name="<%=ILabelConstants.hdnLapID_ + i%>" value="<%=objETLMB.getLngID()%>">
													<input type="hidden"
													name="<%=ILabelConstants.hdnLapResID_ + i%>" value="0">
                            	   </td>
                            	   <td><input type="text" placeholder="hh:mm:ss" value= "<%=WebUtil.formatDate(objMRVB.getTime(), IUMCConstants.TIME_FORMAT)%>" name="<%=ILabelConstants.txtTimeTaken_ + i%>" onblur="isTimeValid(this.value)"></td>
                            	   <td></td>
                            	   
                            	  </tr> 
                            	   
                            	   
                            	   
                              <% }}
                               else{
                            	   %>
                            	<td>No Records</td>   
                            <%   }
                                %>
                                   
                                </tbody>
                            </table>
                           </div>
                           </div>
                       <!--  </div> -->
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="javascript:void(0);" class="thumbnail">
                                        <img src="../Images/pdfImage.png" class="img-responsive">
                                    </a>
                                    <input type="button" class="bg-teal waves-effect" value="Click here to download certificate" onClick="openCertificate('<%=objMREV.getLngUserID()%>','<%=objMREV.getLngEventID()%>','<%=objMREV.getLngEventTypeID()%>','<%=strCertificationRpt%>')" >
                                </div>
                                
                            </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                               
                            <div class="body">
		
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <% 
						if(lstLastEventPhotos != null){
							for(int i = 0; i<lstLastEventPhotos.size(); i++ ){
								objEPUB2 = (EventPhotoUploadBean) lstLastEventPhotos.get(i);
									%>	
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="<%=strImageContextPath%><%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" data-sub-html="<%=objEPUB2.getStrImageDescription()%>" download>
                                        <img class="img-responsive thumbnail" src="<%=strImageContextPath%><%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%> " style="
 										   width: 200px; height: 150px;margin-bottom: 5px;">
 										  
                                    </a>
                                </div>
                                
                                <%	}
								}
								%>
                            </div>
                        </div>
                            
                            
                            
                            
                                </div>
                                
                                
                                
                                 
                                
                                
                                
                            </div>
                           
                            <div class="modal-footer" style="border-top:0px solid #e5e5e5 ">
                            <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="Save" onClick="onSave(),topFunction()">
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="Close" onClick="hideDetail()">
                            
                            </div>
                             </div>
                        </div>
                           
                                
                        </aa:zone>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                 
                                </div>
                            </div>
                        </div>
                    </div> <!-- end of card -->
                    
                </div>
            </div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			
		</div>
		</aa:zone>
			
			<jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
					     <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
					     <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
					     <jsp:param name="IS_AJAX_SUBMIT" value="true" />
				 	</jsp:include>
	</section>
	
	</form>
	

	
	
	
	<%-- <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <aa:zone name="PastData" skipIfNotIncluded="true"> 
						 
						 <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist" >
                                <li role="presentation" class="active" style="padding-left: 3px;"><a href="#home" data-toggle="tab" >Time data</a></li>
                                <li role="presentation" ><a href="#profile" data-toggle="tab">Certificate</a></li>
                                <li role="presentation" ><a href="#messages" data-toggle="tab">Photos</a></li>
                               
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    
                                    <p>
                                    
                                    <div class="col-lg-4">Event Name :  <%=WebUtil.processString(objEMB.getStrName()) %></div>  
                                    <div class="col-lg-4">Event Type Name : <%=WebUtil.processString(objETMB.getStrName()) %></div> 
                                    <div class="col-lg-4">Rank :<%if(objERB.getLngRank() != 0){ %> #<%=objERB.getLngRank() %> <%} %> </div>   
                                    <br>
                                    <div class="col-lg-4">CHIP : <%=WebUtil.processString(objERB.getStrChipValue()) %></div>  
                                   	<div class="col-lg-4">GUN : <%=WebUtil.processString(objERB.getStrGunValue())%></div>
                                   	<div class="col-lg-4">PACE : <%=WebUtil.processString(objERB.getStrPacevalue()) %></div>
                                    </p>
                                    
                                    <!-- <div class="body table-responsive"> -->
                           
                            <table class="table table-hover" style="margin-left:15px; width:96%" >
                                <thead>
                                    <tr>
                                       <th>#</th>
                                        <th>Split</th>
                                        <th>Your Run Time</th>
                                        <th>Category Avg Time</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                
                               <%					if(lstRunData != null){
													for (int i = 0; i < lstRunData.size(); i++) {
																objMRVB = (MemberRunViewBean) lstRunData.get(i);

																if (objMRVB == null) {
																	objMRVB = new MemberRunViewBean();
																}

															%>
                                		
                              
                                
                                
                                   <tr>
													<td scope="row"><%=i + 1%></td>
													<td><%=WebUtil.processString(objMRVB.getLap_name())%></td>
													<td><%=objMRVB.getTime()%></td>
													<td><%=objMRVB.getCategory_avg_time()%></td>
													<td></td>

												</tr>
                                    <%  		
                                	}
                                
                               }
                                %>
                                   
                                </tbody>
                            </table>
                           
                       <!--  </div> -->
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="javascript:void(0);" class="thumbnail">
                                        <img src="../Images/pdfImage.png" class="img-responsive">
                                    </a>
                                    <input type="button" class="bg-teal waves-effect" value="Click here to download certificate" onClick="openCertificate('<%=objMREV.getLngUserID()%>','<%=objMREV.getLngEventID()%>','<%=objMREV.getLngEventTypeID()%>','<%=strCertificationRpt%>')" >
                                </div>
                                
                            </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                               
                            <div class="body">
		
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <% 
						if(lstLastEventPhotos != null){
							for(int i = 0; i<lstLastEventPhotos.size(); i++ ){
								objEPUB2 = (EventPhotoUploadBean) lstLastEventPhotos.get(i);
									%>	
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <a href="<%=strImageContextPath%><%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" data-sub-html="<%=objEPUB2.getStrImageDescription()%>" download>
                                        <img class="img-responsive thumbnail" src="<%=strImageContextPath%><%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%> " style="
 										   width: 200px; height: 150px;margin-bottom: 5px;">
 										  
                                    </a>
                                </div>
                                
                                <%	}
								}
								%>
                            </div>
                        </div>
                            
                            
                            
                            
                                </div>
                                
                                
                                
                                 
                                
                                
                                
                            </div>
                        </div>
                        </aa:zone>
                     
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="CLOSE">
                            
                            </div>
                    </div>
                </div>
            </div> --%>

	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 

	 <!-- Light Gallery Plugin Js -->
    <script src="../plugins/light-gallery/js/lightgallery-all.js"></script>

    <!-- Custom Js -->
    <script src="../js/pages/medias/image-gallery.js"></script>


	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	 <!-- <script src="../js/demo.js"></script>  -->
	
	
	
	 <script>
	 function isNumber(evt) {
		    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;
		}
	   

	 
	 
	 function details(value2,bib){
		
	// $('.details').show();	
		$('#hideDiv').show();
		 
		<%--  $("#<%=ILabelConstants.hdnDetailID%>").val(value1); --%>
	
		 $("#<%=ILabelConstants.hdnEventTypeID%>").val(value2);
		 $("#<%=ILabelConstants.hdnBibNo%>").val(bib);
		 
		 
		<%--  document.forms[0].hdnUserAction.value = "<%=ILabelConstants.DETAILS%>"; --%>
		$("#<%=ILabelConstants.hdnUserAction%>").val("<%=ILabelConstants.DETAILS%>");
		 
		
  			
  			ajaxAnywhere.submitAJAX();
  	
		 
		 
	 }
	 
	 function hideDetail(){
		 $('#hideDiv').hide();
	 }
	 
	 $(function(){
	//	 $('.details').hide();
		 $('#hideDiv').hide();
		 
	 });
	 
	 
	 function openCertificate(UserID,EvntID,TypeID,strCertificatePath){
			openPageInNewWin('../reportViewer/rptCertificateViewer.jsp?ID='+UserID+"&evnt="+EvntID+"&comp="+TypeID+"&RP="+strCertificatePath,'CERT_RPT');
		} 
	 
	 
    </script>
	
  
	<!--  </form>  -->
</body>

</html>
