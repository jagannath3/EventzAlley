<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.controller.UserMasterController"%>
<%@page import="com.uthkrushta.mybos.code.bean.SendToCodeBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.SmsTypeCodeBean"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.mybos.utility.controller.SMSController"%>
<%@page import="com.uthkrushta.mybos.utility.bean.SMSTemplateNotificationBean"%>
<%@page import="com.uthkrushta.mybos.utility.bean.SmsTemplateBean"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.mybos.utility.bean.SmsTraceBean"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.utility.bean.SMSTypeUtilityBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/" %>    

<html class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EventzAlley :: SMS Notification</title>
    <jsp:include page="../MyIncludes/incTopPageReferences.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH"/>
	</jsp:include>
</head>

<script src="../assets/js/nicEdit.js" type="text/javascript" ></script>
<script type="text/javascript">

bkLib.onDomLoaded(function() { 
	 new nicEditor({fullPanel : true, onSave : function(content, id, instance) {
		  //  alert('save button clicked for element '+id+' = '+content);
		    onSaveTextEditor(id,content,instance);
		  } }).panelInstance('myTextEditor');

	});
	
	function onSaveTextEditor(id,content,objRef)
	{
		document.getElementById("myTextEditor").value = content;
	}

function onsmsTypeChange(){
	
	$("#<%=ILabelConstants.hdnUserAction%>").val('<%=IUMCConstants.LOAD_SMSTEMPLATE%>');
	ajaxAnywhere.submitAJAX();
}


</script>
<%
	//Code related to superCentrum to save activity count
/*uncomment to work ActivityTrackController objATC=new ActivityTrackController();
objATC.doAction(request, response); */

String strPageHeader = "SMS Notification";
String strPageDescription = "This page is used to manage SMS Notification.";
String strStatusText = "";
String strStatusStyle = "";
String strUserAction = "GET";
String strPageType = "";
String strSelectedID=";";
String strUrl = IUMCConstants.LOOKUP_FILE_UPLOAD_URL;
String strUploadFolder = "Upload";
MessageBean  objMsg; 
ArrayList<MessageBean> arrMsg=null;

/* DBUtil DBUtil = new DBUtil(request); */
//SmsTemplateBean objSTBean = new SmsTemplateBean();
SMSTemplateNotificationBean objSTNB=new SMSTemplateNotificationBean();
SMSController objSMSCtrl=new SMSController(request);
String strSMSTemplate = "";
int intRowCounter = 1;
String strDisablepromoType = "";
long lngselSmsTemplate = 0;
//MessageController objSMS = new MessageController(request);
SMSTypeUtilityBean objSmsUtlType=null;
List lstSmsUtlType=null;
List lstSendTo=null;
List lstBatch=null;
String strIsEnable="";
long userID=0;

// changed by me
UserMasterBean objUMB = new UserMasterBean();
Long memberID = (Long) session.getAttribute("LOGIN_ID");
if(null != memberID ){
	userID = memberID.longValue();
}
objUMB = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, "lngID = "+userID, "");
if(objUMB == null){
	 objUMB = new UserMasterBean();
}


UserMasterController objSMC=new UserMasterController();
 //long lngRoleID=objSMC.getRoleTypeID(request);
long lngRoleID = objUMB.getLngRoleID();
 
 
/* long lngCompanyID=StaffMasterController.getCompanyID(request); */
if(AAUtils.isAjaxRequest(request)){
	strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
	
	
  	  objSMSCtrl.doAction(request, response);
  	  objSTNB=(SMSTemplateNotificationBean)objSMSCtrl.get(request);
    
  
	 
	      /* if(strUserAction.equalsIgnoreCase(IUBESPOKEConstants.LOAD_SMSTEMPLATE)){
	    	 /*  lngselSmsTemplate = WebUtil.parseLong(request, ILabelConstants.selSmsType);
	    	 objSTBean = (SmsTemplateBean)DBUtil.getRecord(IUBESPOKEConstants.BN_SMS_TEMPLATE_BEAN, "lngID = "+lngselSmsTemplate,""); 
	    	 strSMSTemplate = objSTBean.getStrDescription(); */
	    	// AAUtils.addZonesToRefresh(request, "smsTemplate,smscontent");
	  //   } 
	      if(strUserAction.equalsIgnoreCase(IUMCConstants.LOAD_SMS_TYPE))
	      {
	    	  long lngSelCatID=WebUtil.parseLong(request, ILabelConstants.selSmsCategory);
	    	  lstSmsUtlType=DBUtil.getRecords(IUMCConstants.BN_SMS_TYPE_UTILITY_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgCategoryID = "+lngSelCatID+" and lngID not in (8)", "");
	    	  //objSTNB =new SMSTemplateNotificationBean();
	    	  objSTNB.setStrContent("");
	    	  lstSendTo= DBUtil.getRecords(IUMCConstants.BN_SEND_TO_CODE_BEAN, IUMCConstants.GET_ACTIVE_ROWS, "");
	    	  if(lngSelCatID==1)
	    	  {
	    	  strIsEnable="";
	    	  }
	    	  else
	    	  {
	    		  strIsEnable="";
	    	  }
	    	  AAUtils.addZonesToRefresh(request, "SMSTYPE,smscontent,SEND_TO");
	      }
	      else if(strUserAction.equalsIgnoreCase(IUMCConstants.LOAD_SMSTEMPLATE_TXT)){
		    	 lngselSmsTemplate = WebUtil.parseLong(request, ILabelConstants.selSmsType);
		    	 objSTNB = (SMSTemplateNotificationBean)DBUtil.getRecord(IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "lngMsgTypeID = "+lngselSmsTemplate,"");
		    	if(null==objSTNB )
		    	{
		    	 objSTNB=new SMSTemplateNotificationBean();
		    	}
		    	
		    	//To disable the content field
		    	long lngSelCatID=WebUtil.parseLong(request, ILabelConstants.selSmsCategory);
		    	if(lngSelCatID==1)
		    	  {
		    	  strIsEnable="";
		    	  }
		    	  else
		    	  {
		    		  strIsEnable="";
		    	  }
		    	 lstSendTo=DBUtil.getRecords(IUMCConstants.BN_SEND_TO_CODE_BEAN, IUMCConstants.GET_ACTIVE_ROWS, "");//Added code for Notification to show Member,Staff
		    	 AAUtils.addZonesToRefresh(request, "smscontent,ID,SEND,SEND_TO");
		     } 
	      /* else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SHOW_BATCH))
	      {
	     	lstBatch = DBUtil.getRecords(IUMCConstants.BN_PLAN_BATCH_NAME_MASTER_BEAN_VIEW, IUMCConstants.GET_ACTIVE_ROWS+" and lngCompanyID="+lngCompanyID, ""); 
	         AAUtils.addZonesToRefreh(request, "SHOW_BATCH");
	      } */
	     /* if(strUserAction.equalsIgnoreCase(IPOSConstants.ACTION_SUBMIT)){
	    	 objSMS.doAction(request, response);
	    	 objSMSBean = (SmsTraceBean)objSMS.get(null);
	     }   */
	      
	}   
arrMsg=objSMSCtrl.getArrMsg();
if(null!=arrMsg && arrMsg.size()>0)
{
	objMsg=MessageBean.getLeadingMessage(arrMsg);
	strStatusText=objMsg.getStrMsgText();
	strStatusStyle=objMsg.getStrMsgType();
}
%>
<body class="no-js sidebar-left-mini">
   <form method="post" class="form-horizontal"> 
    <div class="bg-dark dk" id="wrap">
       <jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->



	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	   
	    <div id="content">
	      <div class="inner bg-light lter" style="min-height: 650px">
	         
	          <!-- Page Header -->
			
			<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
			    <jsp:param name="PAGE_HEADER" value="<%=strPageHeader%>" />
			    <jsp:param name="PAGE_DESC" value="<%=strPageDescription%>" />
			    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
			    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
			</jsp:include>
	      
		     <!-- Action Panel -->
			    <div id="ActionPanel" class="row no-margin-bottom default-margin" >
		              <div class="col-lg-6 col-xs-10 floatLeft">
		                <input type="button" value="Save"  class="btn btn-primary save" onClick="onSubmit()" />
		                <input type="button" value="Send"  class="btn btn-primary send" onClick="onSend()" />
		              </div>
		              <div class="col-lg-6 col-xs-2" style="float:right ">
		                   <input  type="button" value="Close" class="btn btn-default rightDefaultMargin alignRight" onClick="onCancel('../myDesktop.jsp')"/>
		              </div>
	             </div>
			 <!-- Action Panel End -->    
	         
	         <div class="row">
	           <div class="col-lg-12">
	            <div class="box" style="min-height:500px;">
	              <header>
			           <div class="icons">
			             <i class="fa fa-edit"></i>
			           </div>
			           <h5>SMS Notification</h5>
			           
					     <div class="toolbar">
				     	   <nav style="padding: 8px;">
				     	     <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
				     	 	   <i class="fa fa-minus"></i>
				     	     </a>
				     	     <a href="javascript:;" class="btn btn-default btn-xs full-box">
			                   <i class="fa fa-expand"></i>
			                 </a>
				     	   </nav>
					 	 </div><!-- toolbar -->
					 	 
					</header>
		
					<div class="body">
					
					<div class="col-lg-12"> 
					  <div class="col-lg-10"> 
					  
					<div class="form-group">
					   <div class="col-lg-2">SMS Category </div>
					   <div class="col-lg-4" >
					   <aa:zone name="ID"  skipIfNotIncluded="true">
					    <input type="hidden" name="<%=ILabelConstants.hdnID%>" value="<%=objSTNB.getLngID()%>">
					   </aa:zone>
					      <select size="1" class="validate[required] form-control chzn-select" name="<%=ILabelConstants.selSmsCategory%>" onchange="getSMSType(this)"  style="width:220">
					         <option value= "0" selected>---Select---</option>
					       	         
						               <%
					       	         						               	List lstSmsCategory = (List)DBUtil.getRecords(IUMCConstants.BN_SMS_TYPE, IUMCConstants.GET_ACTIVE_ROWS, "");
					       	         						               				                   SmsTypeCodeBean objSmsType = new SmsTypeCodeBean(); 
					       	         						               				                     
					       	         						               							           if(null != lstSmsCategory){
					       	         						               								          	for(int i = 1 ; i<= lstSmsCategory.size(); i++){          
					       	         						               								          		objSmsType = (SmsTypeCodeBean) lstSmsCategory.get(i-1);
					       	         						               %>
										      
										       <option <%if(objSmsType.getLngID() == objSTNB.getLngMsgTypeID()){%> selected <%}%>  value="<%=objSmsType.getLngID()%>"><%=objSmsType.getStrName()%></option>
										      <%
										      	}}
										      %>			
						           
						           
					         </select>	         
					   </div>
					   
					    
					    <div class="col-lg-1">ACTIVE</div>
					   <div class="col-lg-2" >
					    <aa:zone name="SEND" skipIfNotIncluded="true">
					      <input type="checkbox"   name="<%=ILabelConstants.chkSend%>" <%if(objSTNB.getIntSend() == 1){%> checked="checked" <%}%> value="1">        
					  
					   </aa:zone>
					      </div>
					   
					         
					   </div>
					
					
					 <div class="form-group">
					   <div class="col-lg-2">SMS Type* </div>
					   <div class="col-lg-4" >
					      <aa:zone name="SMSTYPE" skipIfNotIncluded="true">
						      <select size="1" class="validate[required] form-control chzn-select" name="<%=ILabelConstants.selSmsType%>" id="<%=ILabelConstants.selSmsType%>" FieldType="mandatory" FieldName="SMS Type" onchange="getSMSText(this)" style="width:220">
						         <option value= "0" selected>---Select---</option>
						       	         
							               <%
						       	         							               	if(null != lstSmsUtlType){
						       	         							               									          	for(int i = 1 ; i<= lstSmsUtlType.size(); i++){          
						       	         							               									          		objSmsUtlType = (SMSTypeUtilityBean) lstSmsUtlType.get(i-1);
						       	         							               									          		if(objSmsUtlType==null)
						       	         							               									          		{
						       	         							               									          			objSmsUtlType=new SMSTypeUtilityBean();
						       	         							               									          		}
						       	         							               %>
											      
											       <option <%if(objSmsUtlType.getLngID() == objSTNB.getLngMsgTypeID()){%> selected <%}%>  value="<%=objSmsUtlType.getLngID()%>"><%=objSmsUtlType.getStrName()%></option>
											      <%
											      	}}
											      %>			
							           
							           
						        </select>	  
					        </aa:zone>        
					   </div>
					   
					    <div class="col-lg-1 uploadTo">
					       <%-- <input  type="radio" name="<%=ILabelConstants.rdlSendTo%>" value="1" checked  onclick="sendTo(this)">From Excel
                            <input  type="radio" name="<%=ILabelConstants.rdlSendTo %>" value="2"  onclick="sendTo(this)">Enquiry Members --%>
                           Send To:
                         </div>
                         
                         <div class="col-lg-2 uploadTo">  
                           <aa:zone name="SEND_TO" skipIfNotIncluded="true">
                            <select size="1" class="validate[required] form-control chzn-select" name="<%=ILabelConstants.selSendTo%>" id="<%=ILabelConstants.selSendTo%>" style="width:150px;" onchange="sendTo(this)">
						         <option value= "0" selected>---Select---</option>
						       	         
							               <%
						       	         							               	if(null != lstSendTo){
						       	         							               								        	   SendToCodeBean objSTCB=new SendToCodeBean();
						       	         							               									          	for(int i = 1 ; i<= lstSendTo.size(); i++){          
						       	         							               									          		objSTCB = (SendToCodeBean) lstSendTo.get(i-1);
						       	         							               									          		if(objSTCB==null)
						       	         							               									          		{
						       	         							               									          			objSTCB=new SendToCodeBean();
						       	         							               									          		}
						       	         							               %>
											      
											       <option  value="<%=objSTCB.getLngID()%>"<%if(objSTCB.getLngID()==1){%>selected<%}%>><%=objSTCB.getStrName()%></option>
											      <%
											      	}}
											      %>			
							           
							           
						        </select> 
                            </aa:zone>
					    </div>
					   <%-- commented due to changes <div class="col-lg-1 showBatch">
					       <input  type="radio" name="<%=ILabelConstants.rdlSendTo%>" value="1" checked  onclick="sendTo(this)">From Excel
                            <input  type="radio" name="<%=ILabelConstants.rdlSendTo %>" value="2"  onclick="sendTo(this)">Enquiry Members
                           Batch:
                         </div>
                         
                         <div class="col-lg-2 showBatch">  
                           <aa:zone name="SHOW_BATCH" skipIfNotIncluded="true">
                            <select size="1" class="validate[required] form-control chzn-select" name="<%=ILabelConstants.selBatch%>" id="<%=ILabelConstants.selBatch %>" style="width:150px;">
						         <option value= "0" selected>---Select---</option>
						       	         
							               <%
							                  
							                     
										           if(null != lstBatch){
										        	   PlanBatchNameMasterBeanView objPBNMBV=new PlanBatchNameMasterBeanView();
											          	for(int i = 1 ; i<= lstBatch.size(); i++){          
											          		objPBNMBV = (PlanBatchNameMasterBeanView) lstBatch.get(i-1);
											          		if(objPBNMBV==null)
											          		{
											          			objPBNMBV=new PlanBatchNameMasterBeanView();
											          		}
											      %>
											      
											       <option  value="<%=objPBNMBV.getLngID() %>"><%=objPBNMBV.getStrPlanName()%>--<%=objPBNMBV.getStrName() %></option>
											      <%
											      	}}
											      %>			
							           
							           
						        </select> 
                            </aa:zone>
					    </div> commented due to changes --%>
					   
					 </div><!-- form-group -->
					 
					 <%--  <div class="form-group">
					   <div class="col-lg-2">SMS Template </div>
					   <div class="col-lg-4" >
					       <aa:zone name="smsTemplate" skipIfNotIncluded="true">
					          <select size="1" class="validate[required] form-control chzn-select" name="<%=ILabelConstants.selSmsTemplate%>"  style="width:220" onchange="smsTemplateChange()">
					          <option selected value="0">---select---</option>
					          
					           <%
					           
					                      lngSelSmsTemplate = WebUtil.parseLong(request, ILabelConstants.selSmsType);
					           
					                          if(lngSelSmsTemplate == 1)
					                          {
					                           lstSmsTemplate = (List)DBUtil.getRecords(IUBESPOKEConstants.BN_SMS_TEMPLATE_BEAN, "lngTransactionTypeID ="+1, "");
					                          }
					                          if(lngSelSmsTemplate == 2)
					                          {
					                           lstSmsTemplate = (List)DBUtil.getRecords(IUBESPOKEConstants.BN_SMS_TEMPLATE_BEAN, "lngTransactionTypeID ="+2, "");
					                          }
					                          
									           if(null != lstSmsTemplate && lstSmsTemplate.size()>0){
										          	for(int i = 1 ; i<= lstSmsTemplate.size(); i++){          
										          		objSTBean = (SmsTemplateBean) lstSmsTemplate.get(i-1);
										      %>
										      
										       <option <%if(objSTBean.getLngID() == objSMSBean.getLngsmsTempateID()){strSMSTemplate = objSTBean.getStrDescription() ;  %> selected <%} %> value="<%=objSTBean.getLngID() %>"><%=objSTBean.getStrDescription() %></option>
										      <%
										      	}}
										      %>	
					          
					          
					          </select>
					          </aa:zone>     
					   </div>
					 </div><!-- form-group --> --%>
					 
					  <div class="form-group">
					   <div class="col-lg-2">SMS TEXT </div>
					   <div class="col-lg-4" >
					        <aa:zone name="smscontent" skipIfNotIncluded="true">
						     <textarea rows="3"  name="<%=ILabelConstants.txaSmsContent%>" maxlength="700" id="<%=ILabelConstants.txaSmsContent%>" <%if(lngRoleID!=IUMCConstants.INT_ROLE_SUPERADMIN){%><%=strIsEnable %><%} %> class="<%=ILabelConstants.txaSmsContent%>" cols="40" onkeyup="calculateText()"><%=objSTNB.getStrContent()%></textarea> <!-- id="myTextEditor" -->
						     </aa:zone>  
					   </div>
					   
					   <div class="col-lg-6" id="uploadFile">
					   
				         <aa:zone name="MailLookup" skipIfNotIncluded="true">
				          <input type="button"  value="Upload Excel File with Phone Nos." name="btnUpload" Id="UploadDoc"  style="display:block; float:left" class="clickable" onClick="openFileUploader('<%=strUrl%>','<%=ILabelConstants.hdnFilePath%>','<%=strUploadFolder%>','<%=IUMCConstants.FILE_UPLOAD_SINGLE%>')"></aa:zone>
				          <input type="hidden" name="<%=ILabelConstants.hdnFilePath%>" size="20" value = "" FieldName="Mail Attachments">
				          
					   </div>
					   
					    <div class="col-lg-1 showBatch">
					       <%-- <input  type="radio" name="<%=ILabelConstants.rdlSendTo%>" value="1" checked  onclick="sendTo(this)">From Excel
                            <input  type="radio" name="<%=ILabelConstants.rdlSendTo %>" value="2"  onclick="sendTo(this)">Enquiry Members --%>
                           Batch:
                         </div>
                         
                         <div class="col-lg-2 showBatch">  
                            <aa:zone name="SHOW_BATCH" skipIfNotIncluded="true">
                           <%--  <select size="1" class="validate[required] form-control chzn-select" name="<%=ILabelConstants.selBatch%>" id="<%=ILabelConstants.selBatch%>" style="width:150px;">
						         <option value= "0" selected>---Select---</option>
						       	         
							               <%
						       	         							               	if(null != lstBatch){
						       	         							               								        	   PlanBatchNameMasterBeanView objPBNMBV=new PlanBatchNameMasterBeanView();
						       	         							               									          	for(int i = 1 ; i<= lstBatch.size(); i++){          
						       	         							               									          		objPBNMBV = (PlanBatchNameMasterBeanView) lstBatch.get(i-1);
						       	         							               									          		if(objPBNMBV==null)
						       	         							               									          		{
						       	         							               									          			objPBNMBV=new PlanBatchNameMasterBeanView();
						       	         							               									          		}
						       	         							               %>
											      
											       <option  value="<%=objPBNMBV.getLngID()%>"><%=objPBNMBV.getStrPlanName()%>--<%=objPBNMBV.getStrName()%></option>
											      <%
											      	}}
											      %>			
							           
							           
						        </select>  --%>
                            </aa:zone> 
					    </div>
					   
					 </div><!-- form-group -->
					    
					 </div><!-- col-lg-10 ends  -->
					 
					  <div class="col-lg-2">
					    <div class="form-group">
					    
					       <div id="collapse4" class="body" style="height:205px;overflow:auto;margin-top:-12px;border:1px solid #E8E8E8;background-color:#FFFEFE">
		                     <table  class="table table-bordered table-condensed table-hover table-striped">
					           <thead> 
						        <tr>
						        <th>TAGS</th>
						        </tr>
				                </thead> 
				                <tbody>
				                 <tr><td onclick="setWord(this)">{[CUST_NAME]}</td> </tr>
				                 <tr><td onclick="setWord(this)">{[CUST_ADDRESS]}</td></tr>
				                 <tr><td onclick="setWord(this)">{[CUST_PH_NO]}</td></tr>
				                 <tr><td onclick="setWord(this)">{[CUST_MOB_NO]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[CUST_EMAIL]}</td> </tr>
				                 <tr> <td onclick="setWord(this)">{[CENTER_NAME]}</td> </tr>
				                 <tr> <td onclick="setWord(this)">{[CENTER_NO]}</td> </tr>
				                 <tr> <td onclick="setWord(this)">{[CENTER_ADDRESS]}</td> </tr>
				                 <tr> <td onclick="setWord(this)">{[CENTER_ADMIN]}</td>  </tr>
				                 <tr> <td onclick="setWord(this)">{[DOB]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[DOM]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[DOC_NO]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[DOC_DATE]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[DOC_AMT]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[PAYMENT_SUMMARY]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[PAYMENT_COLLECTED]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[PAYMENT_DUE]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[NEWLY_JOINED]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[ENQUIRY]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[MEMER_ATTENDANCE]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[STAFF_ATTENDANCE]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[PLAN_TYPE]}</td></tr>
				                 <tr> <td onclick="setWord(this)">{[RENEWED]}</td></tr>
				                </tbody>
					        </table>
					        
					       </div><!-- collapse4 --> 
					       
					       <!-- <div class="col-lg-12">TAGS</div>
					      
					       <div class="col-lg-12" onclick="setWord(this)">{[CUST_NAME]}</div>
					       <div class="col-lg-12" onclick="setWord(this)">{[CUST_ADDRESS]}</div>
					        <div class="col-lg-12" onclick="setWord(this)">{[CUST_PH_NO]}</div>
					         <div class="col-lg-12" onclick="setWord(this)">{[CUST_MOB_NO]}</div>
					          <div class="col-lg-12" onclick="setWord(this)">{[CUST_EMAIL]}</div>
					           <div class="col-lg-12" onclick="setWord(this)">{[CENTER_NAME]}</div>
					            <div class="col-lg-12" onclick="setWord(this)">{[CENTER_NO]}</div>
					             <div class="col-lg-12" onclick="setWord(this)">{[CENTER_ADDRESS]}</div>
					              <div class="col-lg-12" onclick="setWord(this)">{[CENTER_ADMIN]}</div>
					               <div class="col-lg-12" onclick="setWord(this)">{[DOB]}</div>
					                <div class="col-lg-12" onclick="setWord(this)">{[DOM]}</div> -->
					                 
					                       
					       
					    </div><!-- form-group -->
					 </div><!-- col-lg-2 ends  -->
					 </div><!-- col-lg-12 ends-->
					 
					</div><!-- .body -->
	            </div><!-- box -->
	           </div><!-- .col-lg-12 -->
	         </div><!-- .row -->
  
                         <jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
						     <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
						    <jsp:param value="<%=strSelectedID%>" name="SEL_ID"/> 
						     <jsp:param name="IS_AJAX_SUBMIT" value="true" />
							</jsp:include>                      

                        <!-- Action Panel -->
			    <div id="ActionPanel" class="row no-margin-bottom default-margin" >
		              <div class="col-lg-6 col-xs-10 floatLeft">
		                <input type="button" value="Save" class="btn btn-primary save" onClick="onSubmit()" />
		                 <input type="button" value="Send" class="btn btn-primary send" onClick="onSend()" />
		              </div>
		              <div class="col-lg-6 col-xs-2" style="float:right ">
		                   <input  type="button" value="Close" class="btn btn-default rightDefaultMargin alignRight" onClick="onCancel('../myDesktop.jsp')"/>
		              </div>
	             </div>
			 <!-- Action Panel End --> 
	      </div><!-- .inner -->
		</div><!-- #content -->
    </div><!-- #wrap -->
    
    <jsp:include page="../MyIncludes/incBottomPageReferences.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH"/>
  </jsp:include>
    
   </form>
</body>
<script type="text/javascript">
$(function() {
    Metis.MetisTable();
    Metis.formGeneral();
    $(".dateField").datepicker();
    $('#<%=ILabelConstants.selActivityID%>_chosen').css({"color":"#000"});
    $('.send').attr('disabled',true);
    $('.save').attr('disabled',true);
    $('.uploadTo').addClass('hide');
    $('.showBatch').addClass('hide');
    $('#uploadFile').hide();
  });

function afterAjaxReload(){
	Metis.MetisTable();
    Metis.formGeneral();
    $(".dateField").datepicker();
    $('#<%=ILabelConstants.selActivityID%>_chosen').css({"color":"#000"});
    new nicEditor({fullPanel : true, onSave : function(content, id, instance) {
		  //  alert('save button clicked for element '+id+' = '+content);
		    onSaveTextEditor(id,content,instance);
		  } }).panelInstance('myTextEditor');
    
}

function getSMSText(objSMS)
{
var smsType = document.forms[0].<%=ILabelConstants.selSmsCategory%>.value;
   
	if(objSMS.value==12 || smsType ==<%=IUMCConstants.PROMOTION%>)
		{
		  $('.uploadTo').removeClass('hide');
	      $('#uploadFile').show();
		}
	else
		{
		 $('.uploadTo').addClass('hide');
         $('#uploadFile').hide();
		}
	$('.showBatch').addClass('hide');

    if(objSMS.value==12)
        {
    	  $('.send').removeAttr('disabled');
   	      $('.send').removeClass('hide');//
        }

	
	$("#<%=ILabelConstants.hdnUserAction%>").val('<%=IUMCConstants.LOAD_SMSTEMPLATE_TXT%>');
	ajaxAnywhere.submitAJAX();
}
<%-- function smsTemplateChange()
{
	$("#<%=ILabelConstants.hdnUserAction%>").val('<%=IUBESPOKEConstants.LOAD_SMSTEMPLATE_TXT%>');
	ajaxAnywhere.submitAJAX();
} --%>
function getSMSType(objSMSCategory)
{
	if(objSMSCategory.value==<%=IUMCConstants.TRANSACTIONAL%> || objSMSCategory.value==0)	
		{
	         $('.send').attr('disabled',true);
	         $('.send').addClass('hide');//
	         $('.save').removeAttr('disabled');
	         $('.uploadTo').addClass('hide');
	         $('#uploadFile').hide();
	        <%--  $('#<%=ILabelConstants.txaSmsContent%>').attr('disabled',true); --%>
	        
	        
		}
	else if(objSMSCategory.value==<%=IUMCConstants.PROMOTION%>)
		{
             $('.send').removeAttr('disabled');
             $('.send').removeClass('hide');//
             $('.save').removeAttr('disabled');
             $('.uploadTo').removeClass('hide');
             $('#uploadFile').show();
             <%-- $('#<%=ILabelConstants.txaSmsContent%>').removeAttr('disabled'); --%>
		}
	 $('.showBatch').addClass('hide');
  $("#<%=ILabelConstants.hdnUserAction%>").val('<%=IUMCConstants.LOAD_SMS_TYPE%>');
  ajaxAnywhere.submitAJAX();
}
function onSend()
{
     $('#<%=ILabelConstants.hdnUserAction%>').val('<%=IUMCConstants.ACTION_SEND_SMS%>');
     ajaxAnywhere.submitAJAX();
}

function sendTo(obj)
{
   if(obj.value==1)
	   {
        $('#uploadFile').show();
	   }
   else /* if(obj.value==2) */
	   {
	   $('#uploadFile').hide();
	   }
   if(obj.value==2 || obj.value==5 || obj.value==6)
   {
     
      $('.showBatch').removeClass('hide');
      
      $('#<%=ILabelConstants.hdnUserAction%>').val('<%=IUMCConstants.ACTION_SHOW_BATCH%>');
      ajaxAnywhere.submitAJAX();
   }
else
   {
      $('.showBatch').addClass('hide');
   }
}

function setWord(word)
{
  var strContent=$('textarea').val();
  /*   strContent +=" "+$(word).html();
   $('textarea').val(strContent); */
   
   var txaObj = document.getElementById('<%=ILabelConstants.txaSmsContent%>');
   var startCurPos = txaObj.selectionStart;
   /* var endPos = ctl.selectionEnd; */
   /* alert(startPos + ", " + endPos); */

   var textBefore = strContent.substring(0,  startCurPos);
   var textAfter  = strContent.substring(startCurPos, strContent.length);

   /* alert("alert "+textBefore + $(word).html() + textAfter); */
   
   $('textarea').val(textBefore + $(word).html() + textAfter);
   
   
  /*  document.getElementById("myTextEditor").value =strContent+" "+$(word).html(); */
}

<%-- function calculateText()
{
   var textOne=160;
 
       var textLength= $('#<%=ILabelConstants.txaSmsContent%>').val().length;   
      
       var smsCount=Math.floor(Number(textLength)/Number(textOne))+1;
      
   
 
} --%>

/* $(document).ready(function() {
    var text_max = 99;
    $('#textarea_feedback').html(text_max + ' characters remaining');

    $('#textarea').keyup(function() {
        var text_length = $('#textarea').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' characters remaining');
    });
}); */

</script>
</html>