<!DOCTYPE html>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.SendToCodeBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.SmsTypeCodeBean"%>
<%@page import="com.uthkrushta.mybos.utility.bean.SMSTypeUtilityBean"%>
<%@page import="com.uthkrushta.mybos.utility.controller.SMSController"%>
<%@page
	import="com.uthkrushta.mybos.utility.bean.SMSTemplateNotificationBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.CountryBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.RoleBean"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page
	import="com.uthkrushta.mybos.master.controller.UserMasterController"%>
<%@page import="com.uthkrushta.mybos.code.bean.SecurityQuestionBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.SecurityQuestionBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.BloodGroupBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.MyBosGender"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>
<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.code.bean.PaymentModeCodeBean"%>


<%@page import="com.uthkrushta.mybos.master.bean.AreaMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

<link href="../Style/default.css" rel="stylesheet" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<!-- Bootstrap Material Datetime Picker Css -->
<link
	href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
	rel="stylesheet" />


<!-- Wait Me Css -->
<link href="../plugins/waitme/waitMe.css" rel="stylesheet" />

<link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="../css/themes/all-themes.css" rel="stylesheet" />
<style type="text/css">
</style>

<script src="../assets/js/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">

bkLib.onDomLoaded(function() { 
	 new nicEditor({fullPanel : true, onSave : function(content, id, instance) {
		  //  alert('save button clicked for element '+id+' = '+content);
		    onSaveTextEditor(id,content,instance);
		  } }).panelInstance('myTextEditor');

	});
	
	function onSaveTextEditor(id,content,objRef)
	{
		document.getElementById("myTextEditor").value = content;
	}

function onsmsTypeChange(){
	
	$("#<%=ILabelConstants.hdnUserAction%>").val('<%=IUMCConstants.LOAD_SMSTEMPLATE%>');
	ajaxAnywhere.submitAJAX();
}

$("#txaSmsContent").keyup(function(){
	  $("#count").text($(this).val().length);
	});
	
	
function calculateText()
{
	
   var textOne=10;
 
       var textLength= $('#<%=ILabelConstants.txaSmsContent%>').val().length;   
      
      
      // var smsCount=Math.floor(Number(textLength)/Number(textOne))+1;
      $("#count").html(textLength);
   
 
}

</script>

</head>

<%
	String strPageHeader = "SMS Notification";
	String strPageDescription = "This page is used to manage SMS Notification.";
	String strStatusText = "";
	String strStatusStyle = "";
	String strUserAction = "GET";
	String strPageType = "";
	String strSelectedID = ";";
	String strUrl = IUMCConstants.LOOKUP_FILE_UPLOAD_URL;
	String strUploadFolder = "Upload";
	MessageBean objMsg;
	ArrayList<MessageBean> arrMsg = null;

	/* DBUtil DBUtil = new DBUtil(request); */
	//SmsTemplateBean objSTBean = new SmsTemplateBean();
	SMSTemplateNotificationBean objSTNB = new SMSTemplateNotificationBean();
	SMSController objSMSCtrl = new SMSController(request);
	String strSMSTemplate = "";
	int intRowCounter = 1;
	String strDisablepromoType = "";
	long lngselSmsTemplate = 0;
	//MessageController objSMS = new MessageController(request);
	SMSTypeUtilityBean objSmsUtlType = null;
	List lstSmsUtlType = null;
	List lstSendTo = null;
	List lstBatch = null;
	String strIsEnable = "";
	long userID = 0;
	long eventID = 0;

	List lstEventType = null;
	// changed by me
	UserMasterBean objUMB = new UserMasterBean();
	Long memberID = (Long) session.getAttribute("LOGIN_ID");
	if (null != memberID) {
		userID = memberID.longValue();
	}
	objUMB = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, "lngID = " + userID, "");
	if (objUMB == null) {
		objUMB = new UserMasterBean();
	}

	UserMasterController objSMC = new UserMasterController();
	//long lngRoleID=objSMC.getRoleTypeID(request);
	long lngRoleID = objUMB.getLngRoleID();

	List lstEventNames = DBUtil.getRecords(ILabelConstants.BN_EVENT_MASTER, IUMCConstants.GET_ACTIVE_ROWS,
			"dtStartDate DESC");

	/* long lngCompanyID=StaffMasterController.getCompanyID(request); */
	if (AAUtils.isAjaxRequest(request)) {
		strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);

		objSMSCtrl.doAction(request, response);
		objSTNB = (SMSTemplateNotificationBean) objSMSCtrl.get(request);

		if (strUserAction.equalsIgnoreCase(IUMCConstants.LOAD_SMS_TYPE)) {
			long lngSelCatID = WebUtil.parseLong(request, ILabelConstants.selSmsCategory);
			lstSmsUtlType = DBUtil.getRecords(IUMCConstants.BN_SMS_TYPE_UTILITY_BEAN,
					IUMCConstants.GET_ACTIVE_ROWS + " and lngMsgCategoryID = " + lngSelCatID
							+ " and lngID not in (8)",
					"");
			//objSTNB =new SMSTemplateNotificationBean();
			objSTNB.setStrContent("");
			lstSendTo = DBUtil.getRecords(IUMCConstants.BN_SEND_TO_CODE_BEAN, IUMCConstants.GET_ACTIVE_ROWS,
					"");
			if (lngSelCatID == 1) {
				strIsEnable = "";
			} else {
				strIsEnable = "";
			}
			AAUtils.addZonesToRefresh(request, "SMSTYPE,smscontent,SEND_TO");
		} else if (strUserAction.equalsIgnoreCase(IUMCConstants.LOAD_SMSTEMPLATE_TXT)) {
			lngselSmsTemplate = WebUtil.parseLong(request, ILabelConstants.selSmsType);
			objSTNB = (SMSTemplateNotificationBean) DBUtil.getRecord(IUMCConstants.BN_SMS_TEMP_NOTIFICATION,
					"lngMsgTypeID = " + lngselSmsTemplate, "");
			if (null == objSTNB) {
				objSTNB = new SMSTemplateNotificationBean();
			}

			//To disable the content field
			long lngSelCatID = WebUtil.parseLong(request, ILabelConstants.selSmsCategory);
			if (lngSelCatID == 1) {
				strIsEnable = "";
			} else {
				strIsEnable = "";
			}
			lstSendTo = DBUtil.getRecords(IUMCConstants.BN_SEND_TO_CODE_BEAN, IUMCConstants.GET_ACTIVE_ROWS,
					"");//Added code for Notification to show Member,Staff
			AAUtils.addZonesToRefresh(request, "smscontent,ID,SEND,SEND_TO");
		}
		if (strUserAction.equalsIgnoreCase(ILabelConstants.FindEventType)) {
			eventID = WebUtil.parseInt(request, ILabelConstants.selEventName);

			if (eventID > 0) {

				lstEventType = DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER,
						IUMCConstants.GET_ACTIVE_ROWS + " AND lngEventID =" + eventID, "strName");

				AAUtils.addZonesToRefresh(request, "smscontent,SEND_TO,EventTypeRefresh");
			}

		}

	}
	arrMsg = objSMSCtrl.getArrMsg();
	if (null != arrMsg && arrMsg.size() > 0) {
		objMsg = MessageBean.getLeadingMessage(arrMsg);
		strStatusText = objMsg.getStrMsgText();
		strStatusStyle = objMsg.getStrMsgType();
	}
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
	<form method="post">
		<!-- Page Loader -->
		<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
			<jsp:param value="../" name="CONTEXT_PATH" />

		</jsp:include>
		<!-- #Top Bar -->



		<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
			<jsp:param value="../" name="CONTEXT_PATH" />
		</jsp:include>

		<section class="content">




			<jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">

				<jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
				<jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
			</jsp:include>

			<div class="container-fluid">

				<div class="row clearfix ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card" style="margin-bottom: 0px;">
							<div class="header">
								<h2 style="color: #006699;">SMS Notification</h2>
							</div>
							<div id="ActionPanel"
								class="row no-margin-bottom default-margin "
								style="background-color: #006699; border: none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
								<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft"
									style="margin-top: 4px">
									<input type="button" value="Save" class="btn btn-primary save"
										onClick="onSubmit()" />
									<!-- <input type="button" value="Delete" class="btn btn-danger "
									 onClick="onDelete()" /> -->
								</div>
								<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
									style="float: right; margin-top: 4px">
									<input type="button" value="Close"
										class="btn btn-danger rightDefaultMargin alignRight"
										style="float: right; background-color: blue;"
										onClick="onCancel('../myHome.jsp')" />
								</div>
							</div>

							<div class="body"
								style="margin-bottom: 20px; padding-bottom: 0px">

								<div class="row">
									<div class="col-lg-12" style=" padding-left: 0px;padding-right: 0px;">
										<div class="box" style="min-height: 500px;">
											<div class="body">

												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" padding-left: 0px;padding-right: 0px;">
													
														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														<div class="form-group">
														
															<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
																SMS Category</div>
															<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="padding-right: 0px;">
																<aa:zone name="ID" skipIfNotIncluded="true">
																	<input type="hidden" name="<%=ILabelConstants.hdnID%>"
																		value="<%=objSTNB.getLngID()%>">
																</aa:zone>
																<select size="1"
																	class="validate[required] form-control chzn-select"
																	name="<%=ILabelConstants.selSmsCategory%>"
																	onchange="getSMSType(this)" style="width: 220">
																	<option value="0" selected>---Select---</option>

																	<%
																		List lstSmsCategory = (List) DBUtil.getRecords(IUMCConstants.BN_SMS_TYPE, IUMCConstants.GET_ACTIVE_ROWS,
																				"");
																		SmsTypeCodeBean objSmsType = new SmsTypeCodeBean();

																		if (null != lstSmsCategory) {
																			for (int i = 1; i <= lstSmsCategory.size(); i++) {
																				objSmsType = (SmsTypeCodeBean) lstSmsCategory.get(i - 1);
																	%>

																	<option
																		<%if (objSmsType.getLngID() == objSTNB.getLngMsgTypeID()) {%>
																		selected <%}%> value="<%=objSmsType.getLngID()%>"><%=objSmsType.getStrName()%></option>
																	<%
																		}
																		}
																	%>


																</select>
															</div>
															</div>
														</div>

														<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
														<div class="form-group">
															<div class="col-lg-5">SMS Type*</div>
															<div class="col-lg-7">
																<aa:zone name="SMSTYPE" skipIfNotIncluded="true">
																	<select size="1"
																		class="validate[required] form-control chzn-select"
																		name="<%=ILabelConstants.selSmsType%>"
																		id="<%=ILabelConstants.selSmsType%>"
																		FieldType="mandatory" FieldName="SMS Type"
																		onchange="getSMSText(this)" style="width: 220">
																		<option value="0" selected>---Select---</option>

																		<%
																			if (null != lstSmsUtlType) {
																					for (int i = 1; i <= lstSmsUtlType.size(); i++) {
																						objSmsUtlType = (SMSTypeUtilityBean) lstSmsUtlType.get(i - 1);
																						if (objSmsUtlType == null) {
																							objSmsUtlType = new SMSTypeUtilityBean();
																						}
																		%>

																		<option
																			<%if (objSmsUtlType.getLngID() == objSTNB.getLngMsgTypeID()) {%>
																			selected <%}%> value="<%=objSmsUtlType.getLngID()%>"><%=objSmsUtlType.getStrName()%></option>
																		<%
																			}
																				}
																		%>


																	</select>
																</aa:zone>
															</div>



														</div>
														</div>
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<div class="form-group">
															<aa:zone name="SHOW_EVENT" skipIfNotIncluded="true">
																<div class="col-lg-2 showBatch">Send To*</div>

																<div class="col-lg-4 showBatch">

																	<select class="form-control show-tick"
																		id="<%=ILabelConstants.selEventName%>"
																		name="<%=ILabelConstants.selEventName%>"
																		onChange="findEventType()">
																		<!-- onChange="findEventType()" -->
																		<option value="0">-- Select Event Name*--</option>
																		<%
																			if (null != lstEventNames) {
																					for (int i = 0; i < lstEventNames.size(); i++) {
																						EventMasterBean objEMB = (EventMasterBean) lstEventNames.get(i);
																		%>

																		<option value="<%=objEMB.getLngID()%>"><%=objEMB.getStrName()%></option>

																		<%
																			}

																				}
																		%>


																	</select>

																</div>
															</aa:zone>


															<div class="col-lg-4 showType style=" float:right;" >
																<aa:zone name="EventTypeRefresh"
																	skipIfNotIncluded="true">
																	<select class="form-control show-tick"
																		id="<%=ILabelConstants.selEventTypeName%>"
																		name="<%=ILabelConstants.selEventTypeName%>">
																		<option value="0">All Competitions</option>
																		<%
																			if (null != lstEventType) {
																					for (int i = 0; i < lstEventType.size(); i++) {
																						EventTypeMasterBean objETMB = (EventTypeMasterBean) lstEventType.get(i);
																		%>

																		<option value="<%=objETMB.getLngID()%>"><%=objETMB.getStrName()%></option>

																		<%
																			}

																				}
																		%>


																	</select>
																</aa:zone>
															</div>


															<div class="col-lg-3" style="padding-top: 15px;">SMS Text</div>
															<div class="col-lg-6" style="padding-top: 15px;">
																<aa:zone name="smscontent" skipIfNotIncluded="true">
																	<textarea rows="3"
																		name="<%=ILabelConstants.txaSmsContent%>"
																		maxlength="700"
																		id="<%=ILabelConstants.txaSmsContent%>"
																		<%if (lngRoleID != IUMCConstants.INT_ROLE_SUPERADMIN) {%>
																		<%=strIsEnable%> <%}%>
																		class="<%=ILabelConstants.txaSmsContent%>" cols="25"
																		onkeyup="calculateText()"><%=objSTNB.getStrContent()%></textarea>
																	<!-- id="myTextEditor" -->
																	Text Length :
																	<span id="count" ></span>
																</aa:zone>
															</div>

														</div>
														</div>
														<!-- form-group -->

													
													<!-- col-lg-10 ends  -->

													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<div class="form-group">

															<div id="collapse4" class="body"
																style="height: 205px; overflow: auto; margin-top: -12px; border: 1px solid #E8E8E8; background-color: #FFFEFE">
																<table
																	class="table table-bordered table-condensed table-hover table-striped">
																	<thead>
																		<tr>
																			<th>TAGS</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_FIRST_NAME]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_LAST_NAME]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_DOB]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_MOB_NO]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_EMAIL]}</td>
																		</tr>
																		
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_USER_NAME]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_PASSWORD]}</td>
																		</tr>
																		
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_ADDRESS]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_BIB]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_RANK]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_GUN]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_CHIP]}</td>
																		</tr>
																		<tr>
																			<td onclick="setWord(this)">{[MEMB_PACE]}</td>
																		</tr>
																	</tbody>
																</table>

															</div>
															<!-- collapse4 -->
														</div>
														<!-- form-group -->
													</div>
													<!-- col-lg-2 ends  -->
												</div>
												<!-- col-lg-12 ends-->

											</div>
											<!-- .body -->
										</div>
										<!-- box -->
										<div id="ActionPanel"
											class="row no-margin-bottom default-margin "
											style="background-color: #006699; border: none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
											<div
												class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft"
												style="margin-top: 4px">


												<input type="button" value="Save"
													class="btn btn-primary save" onClick="onSubmit()" /> <input
													type="button" value="Send" class="btn btn-primary send"
													onClick="onSend()" />

											</div>
											<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
												style="float: right; margin-top: 4px">
												<input type="button" value="Close"
													class="btn btn-danger rightDefaultMargin alignRight"
													style="float: right; background-color: blue;"
													onClick="onCancel('../myDesktop.jsp')" />
											</div>
										</div>
									</div>
									<!-- .col-lg-12 -->

								</div>
								<!-- end of row-clarifix -->

							</div>
							<!-- end of body -->

							<jsp:include page="../MyIncludes/incPageVariables.jsp"
								flush="true">
								<jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
								<jsp:param value="<%=strSelectedID%>" name="SEL_ID" />
								<jsp:param name="IS_AJAX_SUBMIT" value="true" />
							</jsp:include>

							<!-- Action Panel -->

						</div>
						<!-- end of class="card" -->


					</div>
					<!-- end of class="col-lg-12 col-md-12 col-sm-12 col-xs-12" -->
				</div>
				<!-- end of row clearfix -->

			</div>

		</section>


		<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
			flush="true">
			<jsp:param value="../" name="CONTEXT_PATH" />
		</jsp:include>

		<!-- Bootstrap Core Js -->
		<script src="../plugins/bootstrap/js/bootstrap.js"></script>

		<!-- Select Plugin Js -->
		<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

		<!-- Slimscroll Plugin Js -->
		<script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

		<!-- Waves Effect Plugin Js -->
		<script src="../plugins/node-waves/waves.js"></script>

		<!-- Jquery CountTo Plugin Js -->
		<script src="../plugins/jquery-countto/jquery.countTo.js"></script>

		<!-- Autosize Plugin Js -->
		<script src="../plugins/autosize/autosize.js"></script>

		<!-- Moment Plugin Js -->
		<script src="../plugins/momentjs/moment.js"></script>

		<!-- Sparkline Chart Plugin Js -->
		<script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

		<!-- Custom Js -->
		<script src="../js/admin.js"></script>
		<!-- <script src="../js/pages/index.js"></script> -->

		<!-- Demo Js -->
		<script src="../js/demo.js"></script>

		<script src="../js/vendor/jquery.ui.widget.js"></script>
		<script src="../js/jquery.iframe-transport.js"></script>
		<script src="../js/jquery.fileupload.js"></script>


		<script type="text/javascript">
    
	
	
	function findEventType(){
		var eventName = $("#<%=ILabelConstants.selEventName%> option:selected").val();
		if(eventName == 0){
			alert("Please Select Event Name")
			return;
		}
		 $('.showType').show();
		document.forms[0].hdnUserAction.value = "FindEventType";
		
		ajaxAnywhere.submitAJAX();
	}

	
	
	
	$(function() {
	    Metis.MetisTable();
	    Metis.formGeneral();
	    $(".dateField").datepicker();
	    $('#<%=ILabelConstants.selActivityID%>_chosen').css({"color":"#000"});
	    $('.send').attr('disabled',true);
	     $('.save').attr('disabled',true); 
	    $('.uploadTo').addClass('hide');
	   
	   // $('.showBatch').addClass('hide');
	    $('#uploadFile').hide();
	  });

	function afterAjaxReload(){
		Metis.MetisTable();
	    Metis.formGeneral();
	    $(".dateField").datepicker();
	    $('#<%=ILabelConstants.selActivityID%>_chosen').css({"color":"#000"});
	    new nicEditor({fullPanel : true, onSave : function(content, id, instance) {
			  //  alert('save button clicked for element '+id+' = '+content);
			    onSaveTextEditor(id,content,instance);
			  } }).panelInstance('myTextEditor');
	    
	}

	function getSMSText(objSMS)
	{
	var smsType = document.forms[0].<%=ILabelConstants.selSmsCategory%>.value;
	   
		if(objSMS.value==12 || smsType ==<%=IUMCConstants.PROMOTION%>)
			{
			  $('.uploadTo').removeClass('hide');
		      $('#uploadFile').show();
			}
		else
			{
			 $('.uploadTo').addClass('hide');
	         $('#uploadFile').hide();
			}
		//$('.showBatch').addClass('hide');

	    if(objSMS.value==12)
	        {
	    	  $('.send').removeAttr('disabled');
	   	      $('.send').removeClass('hide');//
	        }

		
		$("#<%=ILabelConstants.hdnUserAction%>").val('<%=IUMCConstants.LOAD_SMSTEMPLATE_TXT%>');
		ajaxAnywhere.submitAJAX();
	}
	
	function getSMSType(objSMSCategory)
	{
		if(objSMSCategory.value==<%=IUMCConstants.TRANSACTIONAL%> || objSMSCategory.value==0)	
			{
		         //$('.send').attr('disabled',true);
		        // $('.send').addClass('hide');//
		         $('.save').removeAttr('disabled');
		         $('.uploadTo').addClass('hide');
		         $('#uploadFile').hide();
			}
		else if(objSMSCategory.value==<%=IUMCConstants.PROMOTION%>)
			{
	             $('.send').removeAttr('disabled');
	             $('.send').removeClass('hide');//
	             $('.save').removeAttr('disabled');
	             $('.uploadTo').removeClass('hide');
	             $('#uploadFile').show();
	           
			}
		 //$('.showBatch').addClass('hide');
	  $("#<%=ILabelConstants.hdnUserAction%>").val('<%=IUMCConstants.LOAD_SMS_TYPE%>');
	  ajaxAnywhere.submitAJAX();
	}
	function onSend()
	{
		var eventID = $('#<%=ILabelConstants.selEventName%>').val();
		var eventTypeId = $("#<%=ILabelConstants.selEventTypeName%>").val();
		
		if(eventID == 0){
			alert("Please Select Event Name");
			return;
		}
		
	     $('#<%=ILabelConstants.hdnUserAction%>').val('<%=IUMCConstants.ACTION_SEND_SMS%>');
	     ajaxAnywhere.submitAJAX();
	}

	function sendTo(obj)
	{
	  
	      
	      $('#<%=ILabelConstants.hdnUserAction%>').val('<%=IUMCConstants.ACTION_SHOW_BATCH%>');
	      ajaxAnywhere.submitAJAX();
	 
	}

	function setWord(word)
	{
	  var strContent=$('textarea').val();
	  /*   strContent +=" "+$(word).html();
	   $('textarea').val(strContent); */
	   
	   var txaObj = document.getElementById('<%=ILabelConstants.txaSmsContent%>');
				var startCurPos = txaObj.selectionStart;
				/* var endPos = ctl.selectionEnd; */
				/* alert(startPos + ", " + endPos); */

				var textBefore = strContent.substring(0, startCurPos);
				var textAfter = strContent.substring(startCurPos,
						strContent.length);

				/* alert("alert "+textBefore + $(word).html() + textAfter); */

				$('textarea').val(textBefore + $(word).html() + textAfter);

				/*  document.getElementById("myTextEditor").value =strContent+" "+$(word).html(); */
			}
		</script>
	</form>
</body>

</html>
