<!--************************Copyright@Uthkrushta Technologies****************************
 --------------------------------------------------------------------------------------
 Change trace : 
************************************************************************************** -->
<!-- Java Imports -->

<%@page import="com.uthkrushta.utils.WebUtil"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>

<!-- common application Imports -->


<!-- Page specific Imports -->
<%@page import="com.jspsmart.upload.File"%>
<%@ page import="com.jspsmart.upload.*"%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" href="../MyStyle/defaultStyle.css" rel="stylesheet" />
<SCRIPT language="JavaScript" src="../JS/commonActions.js" type="text/javascript"></SCRIPT>
<SCRIPT language="JavaScript" src="../JS/applicationValidation.js" type="text/javascript"></SCRIPT>
<title>File Upload Utility</title>
</head>
<script type="text/javascript">

function returnFilePath(strHdnFilePath,strUploadredFilePath,strFunction){
	
	
	objReturn = eval("window.opener.document.forms[0]." + strHdnFilePath);
	objReturn.value = strUploadredFilePath;
	
	if(null != strFunction && strFunction.length>0){
	window.opener[strFunction]();
}
  	self.close();
}






function onUploadFile(strActionURL){
	
	var URL = ""+document.forms[0].action;

	 var fileName = document.forms[0].flFileUploader.value;

	 if(fileName.includes("'")){
	        alert('Excel Filename Is Invalid');
	    }
	 else
		 {
				if( document.forms[0].flFileUploader.value.length > 0){
						document.forms[0].action = strActionURL + "&Action=upload";
						document.forms[0].submit();
				}else{
					alert('Please choose a document to upload');
					document.forms[0].flFileUploader.focus();
				}
		 }
}
</script>
<%
	int intUploadType = 1;
	String strHdnFilePath = "";
	String strUploadredFilePath = "";
	String strFolderPath = "";
	String strActionURL = "";
	String strUserAction = "";
	String strCallBackFunction = "";
	String strStatus = "";
	strHdnFilePath = WebUtil.parseString(request, "ReturnTo");
	strFolderPath = WebUtil.parseString(request, "Location");
	intUploadType = WebUtil.parseInt(request, "UploadType");
	strUploadredFilePath = WebUtil.parseString(request, "hdnUploadedFilePath");
	strUserAction = WebUtil.parseString(request, "Action");
	strCallBackFunction = WebUtil.parseString(request, "CLB");
	
	SmartUpload mySmartUpload = new SmartUpload();
	File myFile = null;
	
	if(strUserAction.equalsIgnoreCase("Upload")){
		mySmartUpload.initialize(pageContext);
		mySmartUpload.upload();
		myFile = mySmartUpload.getFiles().getFile(0); 
		strUploadredFilePath = strFolderPath+"/" + myFile.getFileName();
			myFile.saveAs(strUploadredFilePath,mySmartUpload.SAVE_VIRTUAL);
strStatus = "Uploaded Successfully";
	}
	strActionURL = "?ReturnTo="+ strHdnFilePath +"&Location="+strFolderPath + "&UploadType="+intUploadType + "&CLB=" +strCallBackFunction;
%>



<body>
<form method="post" enctype="multipart/form-data">
<table align="center" cellpadding="0" cellspacing="0" width="98%" class="defaultMargin" style="border: 1px solid #54A3F6;background-color: #AFD1EE;border-radius: 8px 8px;">
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td width="33%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="37%">&nbsp;</td>
  </tr>
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="10%">File</td>
    <td width="33%"><input type="file" name="flFileUploader" size="32" ></td>
    <td width="9%">
	<input type="button" value="Upload" name="btnUpload" onClick="onUploadFile('lookupFileUploader.jsp<%=strActionURL %>')" ></td>
    <td width="9%"><input type="button" value="Close" name="btnClose" onClick="returnFilePath('<%=strHdnFilePath %>','<%=strUploadredFilePath%>','<%=strCallBackFunction%>')"></td>
    
    <td width="37%"><%=strStatus %></td>
  </tr>
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td width="33%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="37%">&nbsp;</td>
  </tr>
</table>
<br>

<input type="hidden" name="hdnUploadedFilePath" value="<%=strUploadredFilePath%>" size="20">&nbsp;
</form>
</body>
</html>