jQuery(function() {
	
	$('#banner ul.items li').each(function() {
		$('#banner')
	});
	
	$('#banner ul.items').jcarousel({
		'scroll': 1,
		'auto': 3,
		'wrap': 'both',
        initCallback: mycarousel_initCallback,
        buttonNextHTML: null,
        buttonPrevHTML: null,
        itemVisibleInCallback: {
			onAfterAnimation: function(c, o, i, s) {
				jQuery('#banner .nav li').removeClass('active');
				jQuery('#banner .nav li:eq('+ (i-1) +')').addClass('active');
			}
		}
	});
	
	if ( $.browser.msie && $.browser.version == 6 ) {
		DD_belatedPNG.fix('.btn-more, #header, h1#logo a, .slide-btn, #banner .nav a, #wrapper, #wrapper .inner, #bottom, #banner img, #three-cols img');
	}
});

function mycarousel_initCallback(carousel) {

	var i = 1;
	
	$('#banner .nav').append('<ul></ul>');
	$('#banner .items li').each(function(){
		$('#banner .nav ul').append('<li><a href="#">' + i + '</a></li>');
		i++;
	});
	
	$('#banner .nav').css('margin-left', function(){
		var width = $('#banner .nav').width() / 2;
		return -width + 'px';
	});

    $('#banner .nav a').bind('click', function() {
        carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
        return false;
    });

};