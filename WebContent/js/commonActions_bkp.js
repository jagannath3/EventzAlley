/**
 *  Scripts performing application common actions
 */

	function openFileInNewWin(strHdnFilePath,calledFrom){
		if(calledFrom == "1"){
			strURL = "MyLookup/openFile.jsp";
			strURL +="?ImgPath="+strHdnFilePath;
			win = window.open(strURL,"FileWin","titlebar=0,status=0,toolbar=0");
			win.focus();
		}else{
			strURL = "../MyLookup/openFile.jsp";
			objHdnFilePath = eval("document.forms[0]."+strHdnFilePath);
			
			if(objHdnFilePath != null){
				strURL +="?ImgPath="+objHdnFilePath.value;
				win = window.open(strURL,"FileWin","titlebar=0,status=0,toolbar=0");
				win.focus();
			}
		}
		
	}
	
	function onDivClose(strDiv){
    	objDiv = document.getElementById(strDiv);
    	if(null != objDiv){
    		objDiv.className = "hide";
    	}
    }
	
	function openPageInNewWin(strURL,strWin){
		
		win = window.open(strURL,strWin,"titlebar=0,status=0,toolbar=0");
		win.focus();
	}
	
	function openPageInNewTab(strURL,strWin){
		
		win = window.open(strURL,strWin,"");
		win.focus();
	}
	
	function toggleSections(objSection1,objSection2){
		
		if(objSection1.className.search("hide") >= 0 ){
			objSection1.className = objSection1.className.replace("hide","show");
			objSection2.className = objSection2.className.replace("show","hide");
		}else if(objSection1.className.search("show") >= 0 ){
			objSection2.className = objSection2.className.replace("hide","show");
			objSection1.className = objSection1.className.replace("show","hide");
		}
	}

	/*function toggleFilter(strFilter){
		
		objFilter = document.getElementById(strFilter);
		
		if(objFilter!= null){
		
			if(objFilter.className.search("hide") == -1){
				objFilter.className = objFilter.className.replace("show","hide");
			}else{
				objFilter.className = objFilter.className.replace("hide","show");
			}
		} 
		
	}*/
	

	function toggleFilter(strFilter){
		
		objFilter = document.getElementById(strFilter);
		
		if(objFilter!= null){
		
			/*if(objFilter.className.search("hide") == -1){
				objFilter.className = objFilter.className.replace("show","hide");
			}else{
				objFilter.className = objFilter.className.replace("hide","show");
			}*/
			
			if(objFilter.style.display == "none"){
				objFilter.style.display = "block";
			}else{
				objFilter.style.display = "none";
			}
				
		} 
		
	}
	
	
	function filterEntries(event)
	{
	
		if(event.keyCode == 13)	
		{
			searchFilterEntries();
		}
		
	}
	
	function checkEntry(event){
		alert(event.keyCode)
		
	}
	
	function searchFilterEntries()
	{
		
		document.forms[0].hdnUserAction.value = "GET"
			
		if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == "true")	
		{
			ajaxAnywhere.submitAJAX();
		}
		else
		{
			document.forms[0].submit();
		}
	}



//*******************************************************************************************
//Function   : CheckSelectDouble
//Description: Function for checkbox clicking with unselected object
//In         : checkBox,checkBox,textBox,textBox,textBox
//Out        : void
//*******************************************************************************************
 // document.oncontextmenu=new Function("alert('No Right Click, please....!!');return false")

	function CheckSelectDouble(chkObjSub,chkobjMain,hdnobjCount,hdnobjSelect,hdnobjUnselect) //on click of a checkbox
	{
		strMainSelectedString = hdnobjSelect.value ;
		strMainUnSelectedString = hdnobjUnselect.value ;
		intCounter = Number(hdnobjCount.value);
		alert(strMainUnSelectedString);
		strResultUnSelectedString="";
		strResultSelectedString="";
		
		strSubString="";
		intIndex=-1;
		blnAllSelected=true;
		if(!chkObjSub.checked)
		{
			strSubString="";
			chkobjMain.checked=false;
			strSubString=";"+chkObjSub.value+";";
			intSubLength=strSubString.length;
		
			intIndex=strMainSelectedString.search(strSubString);
			
			if(intIndex>=0)
			{
			
				strResultSelectedString=strMainSelectedString.substring(0,intIndex+1)+strMainSelectedString.substring(intIndex+intSubLength,strMainSelectedString.length)
				hdnobjSelect.value=strResultSelectedString;
			}
				hdnobjUnselect.value = hdnobjUnselect.value + chkObjSub.value + ";";
		}
		else
		{

			strMainSelectedString=strMainSelectedString+chkObjSub.value+";";
			hdnobjSelect.value=strMainSelectedString;

			intIndex=-1;
			strSubString=";"+chkObjSub.value+";";
			intSubLength=strSubString.length;
		
			intIndex=strMainUnSelectedString.search(strSubString);
			if(intIndex>=0)
			{
			
				strResultUnSelectedString=strMainUnSelectedString.substring(0,intIndex+1)+strMainUnSelectedString.substring(intIndex+intSubLength,strMainUnSelectedString.length)
				hdnobjUnselect.value=strResultUnSelectedString;
			}

			strChkTempName = chkObjSub.name;
			strChkName="";
			intIndex = 0;
		
			for(intIndex=0;intIndex<strChkTempName.length;intIndex++)
			{
				if(isNaN(strChkTempName.charAt(intIndex)))
				{
					strChkName += strChkTempName.charAt(intIndex);			
				}
			} 
			
			for(intIndex=1;intIndex<= intCounter;intIndex++)
			{
				chkChild = eval("document.forms[0]." + strChkName + intIndex);
				if(! chkChild.checked)
				{
					blnAllSelected=false;
					break;
				}
			}
			
			chkobjMain.checked=blnAllSelected;
		}
	}




//*******************************************************************************************
//Function   : CheckSelectSingle
//Description: Function for clicking the checkbox without unselected object
//In         : checkBox,checkBox,textBox,textBox
//Out        : void
//*******************************************************************************************

	function CheckSelectSingle(chkObjSub,chkobjMain,hdnobjCount,hdnobjSelect) //on click of a checkbox
	{ 
		strMainSelectedString = hdnobjSelect.value ;
		intCounter = Number(hdnobjCount.value);
        
		strResultSelectedString="";
		
		strSubString="";
		intIndex=-1;
		blnAllSelected=true;

		if(!chkObjSub.checked)
		{
			strSubString="";
			chkobjMain.checked=false;
			strSubString=";"+chkObjSub.value+";";
			intSubLength=strSubString.length;
		
			intIndex=strMainSelectedString.search(strSubString);
			if(intIndex>=0)
			{
			
				strResultSelectedString=strMainSelectedString.substring(0,intIndex+1)+strMainSelectedString.substring(intIndex+intSubLength,strMainSelectedString.length)
				hdnobjSelect.value=strResultSelectedString;

			}
		}
		else
		{
			if(Number(chkObjSub.value) > 0 ){strMainSelectedString=strMainSelectedString+chkObjSub.value+";";}
			hdnobjSelect.value=strMainSelectedString;
		
			strChkTempName = chkObjSub.name;
			strChkName="";
			intIndex = 0;
		
		
			for(intIndex=0;intIndex<strChkTempName.length;intIndex++)
			{
				if(isNaN(strChkTempName.charAt(intIndex)))
				{
					strChkName += strChkTempName.charAt(intIndex);			
				}
			} 
			for(intIndex=1;intIndex<= intCounter;intIndex++)
			{
				chkChild = eval("document.forms[0]." + strChkName + intIndex);
                              if(chkChild != null)
                              {

                                      if(! chkChild.checked)
                                      {
                                              blnAllSelected=false;
                                              break;
                                       }
                               }
			}
			
			chkobjMain.checked=blnAllSelected;
		}
	}

//*******************************************************************************************
//Function   : SelectAll
//Description: Function for select all checkbox with unselected object
//In         : checkBoxName,checkBox,textBox,textBox,textBox
//Out        : void
//*******************************************************************************************

	function SelectAll(chkSubName,chkobjMain,hdnobjCount,hdnobjSelect,hdnobjUnselect) //To select all checkbox
	{
			
			var intLength=Number(hdnobjCount.value);
			if(chkobjMain.checked)
			{
			
				for(intRowIndex=1;intRowIndex<=intLength;intRowIndex++)
				{
			               chkChild = eval("document.forms[0]." +chkSubName + intRowIndex);
                                    
					if(chkChild != null)
					{
						
                                              if(chkChild.checked == false)
						{
							chkChild.checked=true;
							if(hdnobjUnselect != null)
							{
								CheckSelectDouble(chkChild,chkobjMain,hdnobjCount,hdnobjSelect,hdnobjUnselect)
							}
							else
							{
							 
								CheckSelectSingle(chkChild,chkobjMain,hdnobjCount,hdnobjSelect)
							}	
						}	
					}
				
				}
			}
			else
			{
				for(intRowIndex=1;intRowIndex<=intLength;intRowIndex++)
				{
					chkChild = eval("document.forms[0]." + chkSubName + intRowIndex);
                                      if(chkChild != null)
                                      {
                                        /*  if(chkChild.disabled != true)
                                          {*/
                                                  chkChild.checked=false;
                                                  if(hdnobjUnselect != null)
                                                  {

                                                          CheckSelectDouble(chkChild,chkobjMain,hdnobjCount,hdnobjSelect,hdnobjUnselect)
                                                  }
                                                  else
                                                  {

                                                          CheckSelectSingle(chkChild,chkobjMain,hdnobjCount,hdnobjSelect)

                                                  }	
                                         // }
                                      }
				}
			}
		}
//========================================================================================================
	
	
    function onSubmit()
    {
        alert('onSubmit');
    	document.forms[0].hdnUserAction.value = "SUBMIT"
    	if(ValidateForm())
    	{
    		if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == 'true')	
    		{
    			ajaxAnywhere.submitAJAX();
    		}
    		else
    		{
    				document.forms[0].submit();
    		}
    	}
    }
    
    
    function onDelete()
    {    
    	
    	if(document.forms[0].hdnSelectedID.value == ";")
    	{
    		alert('Please select record/s to be deleted')
    		return;
    	}
    	if(confirm("Do you want to delete the selected records?"))
    	{   
    		document.forms[0].hdnUserAction.value = "DEL"
    		if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == 'true')	
    		{
    			ajaxAnywhere.submitAJAX();
    		}
    		else
    		{
    			document.forms[0].submit();
    		}
    	}
    }
    function onCancel(strPageURL)
    {
        document.forms[0].action = strPageURL;
        document.forms[0].submit();
    }
    
    function onClose()
    {
		if(confirm('Unsaved data will be lost. Do you really want to close this page?'))
		{
	    		self.close();
		}
    }
    
    function onCloseLookup()
    {
    		self.close();
    }
    
    function onAddRow()
    {
    	alert("HI BKP")
     	var intAddCount = 1;
    	if(document.forms[0].hdnAddCount != null)
    	{
    		intAddCount = Number(document.forms[0].hdnAddCount.value)
    	}
    	if(document.forms[0].hdnRowCounter != null)
    	{
    		document.forms[0].hdnRowCounter.value = Number(document.forms[0].hdnRowCounter.value) + Number(intAddCount);
    		document.forms[0].hdnUserAction.value = "ADD"
    		if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == "true")	
    		{
    			ajaxAnywhere.submitAJAX();
    		}
    		else
    		{
    			document.forms[0].submit();
    		}
    		
    	}
    	else
    	{
    		alert("HTML Error: hdnRowCounter is missing" )
    		
    	}
    }

    function onApprove(){
    	if(document.forms[0].hdnSelectedID.value == ";"){
    		alert('Please select record/s to be Approved');
    		return;
    	}
    	if(confirm('Do you want to Approve the selected entries?'))
		{
    		document.forms[0].hdnUserAction.value = "APPROVE";
        	ajaxAnywhere.submitAJAX();
		}
    }

    function onReject(){
    	if(document.forms[0].hdnSelectedID.value == ";"){
    		alert('Please select record/s to be Rejected');
    		return;
    	}
    	if(confirm('Are you sure you want to reject the selected entries?')){
    		objRejectionDiv = document.getElementById("RejectBox");
    		if(null != objRejectionDiv){
    			objRejectionDiv.className = "show";
    		}
    	}
    	
    }
    function onRejectConfirm(strDiv){
    	objDiv = document.getElementById(strDiv);
    	if(null != objDiv){
    		objDiv.className = "hide";
    	}
    	document.forms[0].hdnUserAction.value = "REJECT";
    	ajaxAnywhere.submitAJAX();
    	
    }
   
    
    /*function openFileUploader(LookupURL,ReturnTo,FolderLocation,UploadType,Delimiter){
    	LookupURL += "?ReturnTo="+ReturnTo + "&Location=" + FolderLocation + "&UploadType=" + UploadType + "&Delimiter="+Delimiter
    	FileWin = window.open(LookupURL,"FileWin","scrollbars=0,titlebar=0,status=0,toolbar=0,width=800,height=100");
    	FileWin.focus(); 
    }*/
    function openFileUploader(LookupURL,ReturnTo,FolderLocation,UploadType,CallBack,intCol){
		
    	LookupURL += "?ReturnTo="+ReturnTo + "&Location=" + FolderLocation + "&UploadType=" + UploadType + "&CLB="+CallBack + "&Col="+intCol;
    	FileWin = window.open(LookupURL,"FileWin","titlebar=1,status=0,toolbar=0,width=950,height=120,top=50,left=50");
    	FileWin.focus(); 
    }
    function gotoThisPage(strPageURL)
    { 
    	document.forms[0].action = strPageURL ;
    	document.forms[0].submit();
    }
    
 // Functions for handling Data Grid 

    function setFocus(objTD)
       {
               objTD.className = "FOCUSEDCELL"
               objPrevTD = document.getElementById(document.forms[0].FocusedCell.value)
               if(objPrevTD.id != objTD.id)
               {
                       objPrevTD.className = "TABLECELL";
               }
               document.forms[0].FocusedCell.value = objTD.id

       }

       function findNext(e,strRowState)
       {
               intCurrIndex = Number(document.forms[0].FocusedElement.value);
               intLastIndex = Number(document.forms[0].LastCell.value);
       
               if(e.keyCode == 13)
               {
                       
                       if(intCurrIndex != intLastIndex)
                       {
                           blurObject(document.forms[0].elements[intCurrIndex])
                       }
                       for(intIndex = intCurrIndex+1; intIndex <= intLastIndex ; intIndex++)
                       { 
                           if(document.forms[0].elements[intIndex].Edit == 'yes' )
                           {
                               document.forms[0].elements[intIndex].className = "FOCUSEDCELL"
                               document.forms[0].elements[intIndex].focus()
                               if(document.forms[0].elements[intIndex].type == "text")
                               {
                                   document.forms[0].elements[intIndex].select()
                                }
                               document.forms[0].FocusedElement.value = intIndex
                               return
                            }

                       }
                     
               }
               else
               {
                   objRowState = eval("document.forms[0]."+strRowState);
                   if(objRowState != null)
                   {
                       objRowState.value = 1;
                   }
                   
               }


       }
      
       function focusObject(objHTML,AddRow)
       {
           objHTML.className = "FOCUSEDCELL"
           objHTML.parentNode.className = "FocusedCell"
           findObjectIndex(objHTML)
           intCurrIndex = Number(document.forms[0].FocusedElement.value);
           intLastIndex = Number(document.forms[0].LastCell.value);
           if(intCurrIndex == intLastIndex && AddRow == 1)
           {
                onAddRow();
             
           }
       }

       function blurObject(objHTML)
       {
             objHTML.className = "TABLECELL";
             objHTML.parentNode.className = "TABLECELL"
             
       }



       function findObjectIndex(objHTML)
       {
       	
           for(intIndex = 0;intIndex < document.forms[0].elements.length;intIndex++)
           {   
               if(document.forms[0].elements[intIndex].Edit == 'yes'  )
               { 
                   if(objHTML.name == document.forms[0].elements[intIndex].name)
                   {
                       document.forms[0].FocusedElement.value = intIndex;
                       return;
                   }
               }
           }
       }

       function findFirstandLastObjects()
       {
           for(intIndex = 0;intIndex < document.forms[0].elements.length;intIndex++)
           {
               if(document.forms[0].elements[intIndex].Edit == 'yes' && document.forms[0].elements[intIndex].type!="hidden")
               {
                   document.forms[0].FocusedElement.value = intIndex;
                   findLastObject();
                   return;
               }
           }
       }

       function findLastObject()
       {
           for(intIndex = document.forms[0].elements.length -1 ;intIndex >= 0;intIndex--)
           {
               if(document.forms[0].elements[intIndex].Edit == 'yes'  )
               {
                   document.forms[0].LastCell.value = intIndex;
                   return;
               }
           }

       }
       function focusToProperObject()
       {
           findLastObject();
           
           if(Number(document.forms[0].FocusedElement.value) > 0)
           {
               document.forms[0].elements[Number(document.forms[0].FocusedElement.value)].focus();
               document.forms[0].elements[Number(document.forms[0].FocusedElement.value)].select()
           }
          // setID();
      }

       
/*-----------------------------------------------------*/
       
       function changeStyle(strObj,strStyle){
    	   obj = document.getElementById(strObj);
    	   if(obj != null){
    		   obj.className = strStyle;
    	   }else{
    		   alert(strObj + " doesnot exists.");
    	   }
    	  
    	   
       }

       
       
/*--------------------------------------------------------------------------*/

       
       function changeImgSrc(strImg){
    	   objImg = document.getElementById(strImg);
    	   
    	   if(objImg != null){
    		
    		   if(objImg.src.search("BlurStar.png") == -1){
    			   objImg.src = "../Icons/BlurStar.png";
    		   }else{
    			   objImg.src =  "../Icons/ActiveStar.png";
    		   }
    	   }
       }
       
      