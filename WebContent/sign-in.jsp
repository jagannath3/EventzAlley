<!DOCTYPE html>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="org.ajaxanywhere.AAUtils"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.mybos.master.controller.UserMasterController"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="com.uthkrushta.mybos.frameWork.bean.LoginBean"%>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | EventzAlley</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="assets/css/main.min.css">
     <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/lib/animate.css/animate.min.css">
    <script src="js/aa.js" type="text/javascript"></script>
     <script src="js/commonActions.js" type="text/javascript"></script>
     <script src="js/applicationValidation.js" type="text/javascript"></script>
</head>
<script type="text/javascript">

function checkMail(){
	if(ValidateForm()){
		document.forms[0].hdnUserAction.value = "MATCHING_EMAIL";
		ajaxAnywhere.submitAJAX();
	}
}


function EnterPress()
{
	
  if(ValidateForm())
  {
      document.forms[0].action = "sign-in.jsp?Action=SUBMIT";
      document.forms[0].submit();
  }
}  


function checkForSubmit(event)
{
      if(event.keyCode == 13)
      {
      	EnterPress();
      }
}










</script>
<%

long lngUserID = 0;
long lngRoleID = 0;


String strEmail = "";
String strUserName = "";
String strPassword = "";
String strIDName = "";
String strUserAction = "GET";
String strAction = "";
String strWhereClause = "";
String strStatus = "";
String strClassName = "";
String strStatusText = "";
String strStatusStyle = "";
int intShowMessage = 0;

LoginBean objLogin = new LoginBean();
UserMasterBean objUser = new UserMasterBean();
UserMasterController obj= new UserMasterController();

ArrayList<MessageBean> arrMsg = null; 
MessageBean objMsg;

int sessionOut = WebUtil.parseInt(request, "SO");

if(request.getParameter("txtUserName") != null)
{
	strUserName = request.getParameter("txtUserName");
}
if(request.getParameter("txtPassword") != null)
{
    strPassword = request.getParameter("txtPassword");
}

strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
strAction = WebUtil.processString(request.getParameter(ILabelConstants.Action));
if(AAUtils.isAjaxRequest(request)){
	if(strUserAction.equalsIgnoreCase("MATCHING_EMAIL")){
		obj.doAction(request, response);
	}
}


if(strAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
{
	strWhereClause = IUMCConstants.GET_ACTIVE_ROWS+" AND strUserName='"+strUserName+"' AND strPassword='"+strPassword+"'";
	
	
	objUser = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, strWhereClause, "");

	/*wrong user name error page  if(null != objUser ) */
	if(null != objUser ){
		
		session.setAttribute(ISessionAttributes.LOGIN_ID, objUser.getLngID());
		session.setAttribute(ISessionAttributes.LOGIN_USER, objUser.getStrFirstName()+" "+objUser.getStrLastName());
		session.setAttribute(ISessionAttributes.ROLE_ID,new Long(objUser.getLngRoleID()));
        /* session.setAttribute(ISessionAttributes.ROLE_TYPE,new Long(objUser.getLngRoleTypeID())); */ 		// enable in future
		/* session.setAttribute(ISessionAttributes.FIANCIAL_YEAR,new Long(objFinancial.getLngID())); */		// enable in future
		
		response.sendRedirect("myHome.jsp");
		
	}else{
        strStatus = "Invalid User Name or Password : Try again";
        strClassName = IUMCConstants.ERROR ;
        strIDName = "StatusBlock" ;
        objUser = new UserMasterBean();
  
		}	
	}else if(strAction.equalsIgnoreCase("sign-out")){
    
	 if(null != session){
  		session.invalidate();
  	}  
	} 
	
	 
	 arrMsg = obj.getArrMsg();
     if(null!= arrMsg && arrMsg.size() > 0)
     {
     	objMsg = MessageBean.getLeadingMessage(arrMsg);
     	if(objMsg != null)
     	{
     		strStatusText = objMsg.getStrMsgText();
     		strStatusStyle = objMsg.getStrMsgType();
     		
     	}
     }

%>


<body class="login-page">
 <form id="sign_in" method="POST">
    <div class="login-box" >
        <div class="logo" style=" margin-bottom: 0px;">
            <a href="javascript:void(0);"><img src="Images/EventzalleyLogo.png" width="70%"  ></a>
           <!--  <small>Admin BootStrap Based - Material Design</small> -->
        </div>
        <div class="card">
            <div class="body">
                <div class="row">
                <%
                	if(strAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT) && strStatus.trim().length() > 0)
                                                				{
                %>
						<div >
					         <div class="<%=strClassName%>" id="<%=strIDName%>">
					         <h5 style="text-align:center; color:red"><%=strStatus%></h5>
					         </div>
					     </div>
						
					<%
												}else if(sessionOut == 1){
											%>
					<div >
					         <div class=<%=IUMCConstants.ERROR%> id="StatusBlock">
					         <h4>You are automatically Logged out as the system was ideal for more than 30 mins.<br> Please re-login.</h4>
					         </div>
					     </div>
					
					<%} %>
                </div>
               
               
                    <div class="msg">Sign in here</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control"  placeholder="Username (Registered Mobile)" onkeypress="checkForSubmit(event)" id="<%=ILabelConstants.txtUserName%>" name="<%=ILabelConstants.txtUserName%>" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control"  placeholder="Password (Registered DOB in ddmmyyyy)" onkeypress="checkForSubmit(event)" id="<%= ILabelConstants.txtPassword %>" name="<%= ILabelConstants.txtPassword %>" required>
                        </div>
                    </div>
                    <div class="row">
                    <!-- <div class="col-xs-6 align-right">
                            <a href="forgot_password.jsp">Forgot Password?</a>
                        </div> -->
                        <!-- <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div> -->
                         <div class="col-xs-6 ">
                            <a href="forgot_password.jsp">Forgot Password?</a>
                        </div> 
                        <div class="col-xs-4" style="float:right">
                            <button class="btn btn-block bg-pink waves-effect" type="button" onClick="EnterPress()">SIGN IN</button>
                        </div>
                    </div>
                     <div class="row m-t-15 m-b--20">
                    <!--    <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div> -->
                        
                    </div> 
               
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/sign-in.js"></script>
     <input type="hidden" name="<%=ILabelConstants.hdnUserAction %>" id="<%=ILabelConstants.hdnUserAction %>" size="2" class="flattextbox" value="<%=strUserAction %>">
  <input type="hidden" name="hdnAjaxSubmit" value="<%=true %>" size="2">

	<footer class="legal" style="padding:8px;text-align:center; color:white">
      <span align="left" style="font-size:12px">EventzAlley. All Rights Reserved. Application Powered By <u><a href="http://www.uthkrushta.com" style="color:white"  target="_blank">Uthkrushta Technologies</a></u>, Bangalore.</span>
      <span align="right" style="padding:8px;">
     <%--  <img align="right"  class="media-object img-thumbnail user-img" alt="Logo" style="height:30px; margin-right: 10px" src="<%=strContextPath%>Images/LogoTrans.png"> --%>
    <%--  <img src="<%=strContextPath%>Images/LogoTrans.png" style="margin-left:30px;" width="80">  --%>
      </span>
      
    </footer>

 </form>
</body>

</html>