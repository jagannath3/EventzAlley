<%@page import="com.uthkrushta.qp.code.bean.QPGender"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.qp.master.controller.StudentMasterRegistrationController"%>
<%@page import="com.uthkrushta.qp.master.bean.StudentMasterBean"%>
<%@page import="com.uthkrushta.qp.configuration.bean.BatchConfiguration"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>   
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Registration Form</title>
<LINK rel="stylesheet" href="../Style/default.css" type="text/css">
<script src="../JS/jquery.min.js" type="text/javascript"></script>
<script src="../JS/applicationValidation.js" type="text/javascript"></script>
<script src="../JS/commonActions.js" type="text/javascript"></script>
<script src="../JS/aa.js" type="text/javascript"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<SCRIPT src="../JS/datetimepicker.js" type="text/javascript"></SCRIPT>



</head>
<script type="text/javascript">


$(document).ready(function(){
    $(".txtField").keypress(function(event){
    	
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) { 
            event.preventDefault(); 
        }
    });
});

function register(){
	
	var batch = $("#<%=ILabelConstants.selBatch%> option:selected").val();
	var sName=  $("#<%=ILabelConstants.StudentName%> ").val();  
	var fName=	$("#<%=ILabelConstants.FatherName%> ").val();	
	var	gender=	$("#<%=ILabelConstants.GENDER%> option:selected").val();
	var	dob=	$("#<%=ILabelConstants.txtDOB%>").val();
	
	var	mob=	$("#<%=ILabelConstants.Mob%> ").val();	
	var	email=	$("#<%=ILabelConstants.EMAILID%> ").val();	
	var	cName=	$("#<%=ILabelConstants.CLGNAME%> ").val();	
	var yoc=	$("#<%=ILabelConstants.YOC%> ").val();	
	var address=$("#<%=ILabelConstants.txaAddress%> ").val();		
	 var zip =	$("#<%=ILabelConstants.zip%> ").val();	
	
	
	var feeAmount = $("#<%=ILabelConstants.FeeAmount%> ").val();
	var txtPayRef =$("#<%=ILabelConstants.txtPayRef%> ").val();
	var bankName =$("#<%=ILabelConstants.BankName%> ").val();
	
	var termsCondition = document.getElementById("termsCondition").checked;
			
	var today = new Date();	
	var yyyy = today.getFullYear();	
	
			
	var phoneno = /^[0]?[789]\d{9}$/;  
	var atposition=email.indexOf("@");  
	var dotposition=email.lastIndexOf("."); 
	var decimal= /\d{1,10}(\.\d{1,2})?/;  
		
	var zipno = /^\d{6}(-\d{5})?(?!-)$/ ;
				
	if(batch == 0){
		alert("Choose The Batch!!")
		return;
	}
	
	
	
	else if(sName == "" || sName == null){
		alert("Please Enter Student Name!!")
		$('#<%=ILabelConstants.StudentName%>').focus();
		return;
	}
	
	else if(fName== ""){
	alert("Please Enter Father Name!!")
	return;
	}
	
	else if(gender == 0){
		alert("Please Select Gender!!"); 
		return
	}
	
	else if(null == dob || dob==""){
		
		return dateValidation();
	}
	
	else if (!mob.match(phoneno)) {
	        alert("Enter Valid Mobile Number!!");
	        document.forms["Mob"]["<%=ILabelConstants.Mob %>"].focus();
	        return ;
	    }
	
	 else if (atposition<1 || dotposition<(atposition+2) || dotposition+2>=email.length){  
	 	 alert("Please enter a valid e-mail address!!");  /* \n atpostion:"+atposition+"\n dotposition:"+dotposition */  
	 	document.forms["myForm"]["<%=ILabelConstants.strEmailId %>"].focus();
		  return ;
    }
	else if(cName== ""){
	alert("Please Enter College Name!!")
	return;
	
	}
	
	else if(!((yoc>1980)&&(yoc <= yyyy))){
	
	alert("Please Enter a valid Year!!")
	return;
	}
	
	else if(address== ""){
	
	alert("Please Enter Address!!")
	return;
	}
	
	
else if( !zip.match(zipno)){			//!zip.match(decimal)		!zip.match(zipno) &&
		
		alert("Please Enter a Valid Zip Code!!")
		return;
		}
	
	
	
	else if(!feeAmount.match(decimal) ) {
		alert("Please Enter Valid Fee Amount!!")
		return;
	}
	
	else if(txtPayRef == ""){
		alert("Please Enter Payment Reference!!")
		return;
	}
	
	else if(bankName == ""){
		alert("Please Enter Bank Name!!")
		return;
	}
	
	else if (termsCondition == false){
		alert("Please Accept terms and conditions!!")
		return;
		
	}
		
	else{	
		
		
	
		document.forms[0].hdnUserAction.value = "REGISTER";
 		ajaxAnywhere.submitAJAX();
	}
}







function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}










function dateValidation(){
	var	dob=	$("#<%=ILabelConstants.txtDOB%>").val();
	if(dob == ""){
		
		alert("Please enter a valid date"); 
		return false;
	}
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	var today = dd+'-'+mm+'-'+yyyy;
	
	var oneDay = 24*60*60*1000;
	dob = dob.split('-'); 
	today = today.split('-'); 
	dob = new Date(dob[2], dob[1]-1, dob[0]);
	today = new Date(today[2], today[1]-1, today[0]);
	
	
	 if(Math.round(  (today.getTime() - dob.getTime()) / (oneDay)  ) < 0  )  
	  {
    alert("Please enter a valid date"); 	
    $('#<%=ILabelConstants.txtDOB%>').val('');
    $('#<%=ILabelConstants.txtDOB%>').focus();
   
    return false;
	  } else{
		  return true;
	  }
	
}


window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

</script>


<%
String strPageHeader ="PRASTUTA ACADEMY FOR SPECIALITY STUDIES";
String strPageDesc = "(Academic wing of Prastuta Foundation)";
String strStatusText ="";
String strStatusStyle ="";
String strUserAction="";

BatchConfiguration objBC = new BatchConfiguration();
StudentMasterBean objSMB = new StudentMasterBean();
StudentMasterRegistrationController objSMRC = new StudentMasterRegistrationController();
ArrayList<MessageBean> arrMsg = null;
MessageBean objMsg;
QPGender  objQPG = new QPGender();
String where = IUMCConstants.GET_ACTIVE_ROWS+ " AND intIsForRegistration = 1";
List lstBatch = DBUtil.getRecords(IUMCConstants.BN_QP_BATCH_CONFIGURATION, where, "");
List lstGender = DBUtil.getRecords(ILabelConstants.BN_CD_QPGENDER, IUMCConstants.GET_ACTIVE_ROWS, "");
strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_REGISTER)){
	objSMRC.doAction(request, response);
	
}
arrMsg = objSMRC.getArrMsg();
if(null!= arrMsg && arrMsg.size() > 0)
{
	objMsg = MessageBean.getLeadingMessage(arrMsg);
	if(objMsg != null){
		strStatusText = objMsg.getStrMsgText();
		strStatusStyle = objMsg.getStrMsgType();
	 
	} 
}
%>


<body>



<form method="post" >
<jsp:include page="../MyIncludes/registrationIncPageHeader.jsp" flush="true">
	  <jsp:param name="HEADER"  value="<%=strPageHeader%>" />
	  <jsp:param name="DESC" value="<%=strPageDesc%>" /> 
	  <jsp:param name="STATUS" value="<%=strStatusText%>" />
	  <jsp:param name="STYLE" value="<%=strStatusStyle%>" />
	</jsp:include>
	
<table border='0'  width='80%' cellpadding='0' cellspacing='0' align='center'>
	<center><tr>
   <td style="text-align:center;"><h1>Student Registration</h1></td>
	</tr>
	<center>
 
<table border='0' width='530px' cellpadding='0' cellspacing='0' align='center'>
<tr style="margin-left:5%">
   <br> <td style="padding-left: 15%; font-size:11pt; " >Batch to join*:</td>
   <td><select style="width:145px"  name="<%=ILabelConstants.selBatch%>" id=<%=ILabelConstants.selBatch %>>
        <option value="0">--select--</option>
    	<%	if(lstBatch != null){
	for(int i = 0; i<lstBatch.size(); i++ ){
		objBC= (BatchConfiguration) lstBatch.get(i); %>
		<option value="<%=objBC.getLngID()%>"><%=objBC.getStrBatchName()%> </option>
	<%}
}
    	 %> 
          </select></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;"><strong>Student Details:</strong></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Student Full Name*:</td>
    <td ><input type='text' name='StudentName' class = "<%=ILabelConstants.txtField%>" id=<%=ILabelConstants.StudentName%>>
      <input type="hidden" name="<%=ILabelConstants.hdnID%>" value="<%=objSMB.getLngID() %>" size="3">
           
            <input type="hidden" name="<%=ILabelConstants.hdnCreatedOn%>" value="<%=WebUtil.formatDate(new Date(), IUMCConstants.DATE_FORMAT)%>">  
    </td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Father Name*:</td>
    <td><input type='text' name='FatherName' class = "<%=ILabelConstants.txtField%>" id=<%=ILabelConstants.FatherName%>></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Gender*:</td>
    			<td><select style="width:145px "name="<%=ILabelConstants.selGender%>" id=<%=ILabelConstants.GENDER%> FieldType="mandatory" FieldName="Gender">
      
    		<option value="0">--select--</option>
    		<%	if(lstGender != null){
	for(int i = 0; i<lstGender.size(); i++ ){
		objQPG= (QPGender) lstGender.get(i); %>
		<option value="<%=objQPG.getLngID()%>"><%=objQPG.getStrName()%> </option>
	<%}
}
    	 %></select>  </td>
   
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Date Of Birth*:</td>
   <td>
    <input type="text" name="<%=ILabelConstants.txtDOB%>" readonly="readonly" value="<%=WebUtil.formatDate(objSMB.getDtDOB(),IUMCConstants.DATE_FORMAT) %>"  size="18" style="float: left"  FieldType="mandatory" FieldName="DOB" id="<%=ILabelConstants.txtDOB%>" onclick="NewCssCal('txtDOB', 'ddMMyyyy')" onchange="dateValidation()"> <!-- onblur="onStartDate()" -->
        <img border="0" id="<%=ILabelConstants.txtDOB%>" name="txtDOB" src="../Images/calendar.png" width="15" height="16" class="clickable" onclick="javascript:NewCssCal('txtDOB','ddMMyyyy')" value="<%=WebUtil.formatDate(objSMB.getDtDOB(),IUMCConstants.DATE_FORMAT) %>">  
    </td>   
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Mobile No*:</td>
    <td><input type='text' class="phoneNo" name='Mob' id=<%=ILabelConstants.Mob %> onkeypress="return isNumber(event)"></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">E-Mail Id*:</td>
    <td><input type='text' name='EMAILID' id=<%=ILabelConstants.EMAILID %>></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">BAMS College Name*:</td>
    <td><input type='text' name='CLGNAME' class = "<%=ILabelConstants.txtField%>" id=<%=ILabelConstants.CLGNAME%>></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Year Of Completion*:</td>
    <td><input type='text' name='YOC' id=<%=ILabelConstants.YOC %> onkeypress="return isNumber(event)"></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Address*:</td>
     <td width="25%" height="60" rowspan="2">
          <textarea rows="4" name="<%=ILabelConstants.txaAddress %>" id=<%=ILabelConstants.txaAddress%> cols="25"></textarea>
          </td>
          <tr> <td>&nbsp;</td> </tr>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Zip*:</td>
    <td><input type='text' name='zip' id=<%=ILabelConstants.zip%> onkeypress="return isNumber(event)"></td>
</tr>
<tr> <td>&nbsp;</td> </tr>

<tr>
    <td style="padding-left: 15%;font-size:11pt;" ><strong>Fees Details:</strong></td>
</tr>
<tr> <td>&nbsp;</td> </tr>

<tr>
    <td style="padding-left: 15%;font-size:11pt;">Fee Amount*:</td>
    <td><input type='text' name='FeeAmount' id=<%=ILabelConstants.FeeAmount %> onkeypress="return isNumber(event)"></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Payment Reference*:</td>
    <td><input type='text' name='txtPayRef'  id=<%=ILabelConstants.txtPayRef %>></td>
</tr>
<tr> <td>&nbsp;</td> </tr>
<tr>
    <td style="padding-left: 15%;font-size:11pt;">Bank and Branch Name*:</td>
    <td><input type='text' name='BankName'  id=<%=ILabelConstants.BankName %>></td>
</tr>
<tr> <td>&nbsp;</td> </tr>



	
 <!-- I Agree to Terms and Conditions -->







<table border='0' cellpadding='0' cellspacing='0' width='100%' align='center'>
<tr>
<td style="padding-left: 41%;">

<input type="checkbox"  id="<%=ILabelConstants.termsCondition %>" value="1" >

I Agree to <a href="termsAndConditionForRegister.jsp" target="_blank">Terms and Conditions</a> 

</td></tr><tr> <td>&nbsp;</td> </tr>

<tr>

    <td align='center'><input type='button' id="scroll_up" name='REGISTER' value="Register" onClick="register(),topFunction()" ></td>
</tr>
</table>
</table>
 
</table>


<footer style="height:30px;margin-top:60px;  padding-top:10px" id="Footer"  class="Footer bg-dark dker">
      <p style="height:20px;">All rights reserved. Prastuta Academy, Bangalore. 	 <a href="http://www.prastuta.com/privacy-policies.html" class="label label-info" target="_blank">Privacy Policies</a> ,Bangalore.</p>
    </footer>
<jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
		   <jsp:param name="hdnUserAction" value="<%=strUserAction%>" />
		   
		  
  </jsp:include>
	
</form>

</body>

<script type="text/javascript">

</script>
</html>