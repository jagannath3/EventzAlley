<!DOCTYPE html>
<%@page import="com.uthkrushta.mybos.master.bean.RoleBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.basis.bean.NavigationBean"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.mybos.frameWork.bean.ApplicationMenuBean"%>
<%@page import="java.util.ArrayList"%>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);  
%>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

 
</head>
<%
	
	int intCurrMenu = 1;
	String strLogOutPath = "";
	String strActivityAction = "";
	strActivityAction = WebUtil.parseString(request, "activityTracker");
	List<ApplicationMenuBean> AMList = null;

	/* strContextPath = WebUtil.parseString(request, "CONTEXT_PATH"); */
	strLogOutPath = WebUtil.parseString(request, "LOGOUT_PATH");
	UserMasterBean objUMB = new UserMasterBean();
	long userID = ((Long) session.getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
	objUMB = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+userID , "");
	
	long lngRoleId = 0;
	long lngPrevMenuID = 0;
	String strAttributes = "";

	RoleBean objRB = new RoleBean();
	
	if (null != session.getAttribute("ROLE_ID")) {
		lngRoleId = ((Long) session.getAttribute("ROLE_ID")).longValue();
	}
	objRB = (RoleBean) DBUtil.getRecord(ILabelConstants.BN_ROLE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+lngRoleId, "");
	if(objRB == null){
		objRB = new RoleBean();
	}

	/* if (null != session.getAttribute(ISessionAttributes.LEFT_TREE_LIST)) {
		AMList = (List) session.getAttribute(ISessionAttributes.LEFT_TREE_LIST);
		System.out.println("LODED FROM SESSIOn");
	} else {
		
		AMList = (List<ApplicationMenuBean>) DBUtil.getRecords(IUMCConstants.BN_APPLICATION_MENU_BEAN, "lngRoleID ="+lngRoleId, ""); //"lngRoleID="+lngRoleId
		session.setAttribute(ISessionAttributes.LEFT_TREE_LIST, AMList);
		System.out.println("LODED FROM DB");
	} */ // original
	
	//Changed
		AMList = (List<ApplicationMenuBean>) DBUtil.getRecords(IUMCConstants.BN_APPLICATION_MENU_BEAN, "lngRoleID ="+lngRoleId, ""); //"lngRoleID="+lngRoleId
		session.setAttribute(ISessionAttributes.LEFT_TREE_LIST, AMList);
		System.out.println("LODED FROM DB");
	

	ApplicationMenuBean amMenuBean = new ApplicationMenuBean();
%>
<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <!-- <p>Please wait...</p> -->
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar" style="top: 60px;width: 245px; height:100%">
            <!-- User Info -->
            <div class="user-info" style="height:42px;background-color: #ffffff;">
                <%-- <div class="image">
                    <img src="<%=strContextPath%>Images/user.png" alt="User" 
                    style="margin-left:75px; width: 50%;" />
                </div>
                <div class="info-container" style="text-align:center;width: 200px;">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><%=objUMB.getStrFirstName()%> <%=objUMB.getStrLastName()%></div>
                    <div class="email"><%=objUMB.getStrEmailID()%></div>
                    <div class="btn-group user-helper-dropdown" style=" padding-bottom: 10px;">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<%=strContextPath%>Masters/userMaster.jsp;"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<%=strContextPath%>javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="<%=strContextPath%>javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="<%=strContextPath%>javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<%=strContextPath%>sign-in.jsp?Action=sign-out"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div> --%>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="font-size:14px"><span>Main Navigation</span></li>
                   	 <li >
                        <a href="<%=strContextPath%>myHome.jsp">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li >
                    	<%if(objRB.getIntRoleType() == IUMCConstants.SUPER_ADMIN_ROLE_TYPE || objRB.getIntRoleType() == IUMCConstants.ADMIN_TYPE) {%>
                        <a href="<%=strContextPath%>Masters/userMaster.jsp">
                            <i class="material-icons">person</i>
                            <span>My Profile</span>
                        </a>
                        <%} else if(objRB.getIntRoleType() == IUMCConstants.MEMBER_ROLE_TYPE) {%>
                        <a href="<%=strContextPath%>Masters/memberMaster.jsp">
                            <i class="material-icons">person</i>
                            <span>My Profile</span>
                        </a>
                         <%} else if(objRB.getIntRoleType() == IUMCConstants.STAFF_ROLE_TYPE) {%>
                         <a href="<%=strContextPath%>Masters/staffMaster.jsp">
                            <i class="material-icons">person</i>
                            <span>My Profile</span>
                        </a>
                        <%} %>
                        
                    </li>
                   	
                   	<%
						if (null == AMList) {
						out.print("Error while building Menu. Contact Administrator");
						} else {
					
					%>	 
                   <%
                   String strPageURL = "";
					for (int i = 0; i < AMList.size(); i++) {
						amMenuBean = AMList.get(i);
						strPageURL = amMenuBean.getStrPageUrl() + strAttributes;
						if (strPageURL.indexOf("?") > 0) {
							strPageURL += "&ACTIVITY_ID=" + amMenuBean.getLngActivityID() + "&MENU_ID="
									+ amMenuBean.getLngMenuID() + "&IMG=" + amMenuBean.getStrImagePath();
						} else {
							strPageURL += "?ACTIVITY_ID=" + amMenuBean.getLngActivityID() + "&MENU_ID="
									+ amMenuBean.getLngMenuID() + "&IMG=" + amMenuBean.getStrImagePath();
						}
						if (amMenuBean.getLngMenuID() != lngPrevMenuID) {

                   
                   %>
                    <script>
		 	      if(document.getElementById("SubMenu<%=lngPrevMenuID%>") != null) {
					document.write("</ul></li>");

				}
			     </script>
			     
                   
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons"><%=amMenuBean.getStrImagePath()%></i>
                            <span><%=amMenuBean.getStrMenuName()%></span>
                        </a>
                        <ul id="SubMenu<%=amMenuBean.getLngMenuID() %>" class="ml-menu">
                       <%
						}
						%>    
                           
                            <li>
                                <a href="<%=strContextPath%><%=strPageURL%>"><span><%=amMenuBean.getStrActivityName()%></span></a>
                            </li>
                           
                      <%
								lngPrevMenuID = amMenuBean.getLngMenuID();
					}
					
							%>
                        </ul>
                         <%} %>
                    </li>
                    
                     <li >
                        <a href="<%=strContextPath%>sign-in.jsp?Action=sign-out"">
                            <i class="material-icons">input</i>
                            <span>Sign out</span>
                        </a>
                    </li>
                    
                   
                    
                    
                    
                    
                   
                   
                    
                    
                   
                </ul>
            </div>
            <!-- #Menu -->
            
        </aside>
        <!-- #END# Left Sidebar -->
        
    </section>

    
<%-- 
    <!-- Jquery Core Js -->
    <script src="<%=strContextPath%>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<%=strContextPath%>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<%=strContextPath%>plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<%=strContextPath%>plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<%=strContextPath%>plugins/raphael/raphael.min.js"></script>
    <script src="<%=strContextPath%>plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<%=strContextPath%>plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<%=strContextPath%>js/admin.js"></script>
    <script src="<%=strContextPath%>js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="<%=strContextPath%>js/demo.js"></script> --%>
</body>

</html>
