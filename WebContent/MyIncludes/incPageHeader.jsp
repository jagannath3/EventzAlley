<!DOCTYPE html>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.frameWork.bean.ApplicationMenuBean"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>


<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/" %>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);  
%>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | EventzAlley</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- Bootstrap Core Css -->
    <link href="<%=strContextPath%>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

  
  
    <!-- Custom Css -->
    <link href="<%=strContextPath%>css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<%=strContextPath%>css/themes/all-themes.css" rel="stylesheet" />
    
    <style type="text/css">
    
    
    
    
    /* sign-out on hover text starts  */
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
/* end */
    
    
    
    
    
    
    .wrapper {
  padding: 100px;
}

.image--cover {
  width: 150px;
  height: 150px;
  border-radius: 50%;
  margin: 20px;

  object-fit: cover;
  object-position: center right;
}
    
    
    </style>
</head>
<%

 	
 	String strDisplayName = "GUEST";
	String strPageHeader = "";
	String strHomePage = "";
	String strCenterProfile="";
	long lngRoleID = 0;
	String strContaxtImagePath = "/EventzAlley/servlet1"; 
	
	List lstClientMsgs=null;
	List<ApplicationMenuBean> lstQuickMenuItems = null;
	ApplicationMenuBean objQuickMenuItems =new ApplicationMenuBean();
	 
	UserMasterBean objUMB = new UserMasterBean();
	long userID = ((Long) session.getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
	objUMB = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+userID , "");
	
	
	strHomePage = strContextPath + WebUtil.processString(session.getAttribute("HOME_PAGE_URL")+"");
	/* strCenterProfile=strContextPath+"Masters/masterCompanyProfile.jsp?COMPANY_TYPE=1&ACTIVITY_ID=15&MENU_ID=2";
	 */
 %>
<body class="theme-blue" >
     <!-- Page Loader -->
   <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div> 
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar" style="position:fixed; height:77px;  background-color:#4E4D4D; 
    height: 60px;
    margin-bottom: 0px;border-radius: 0px
    ">
        <div class="container-fluid">
        	 <%-- <div class="navbar-brand collapse navbar-collapse" href="<%=strContextPath%>myHome.jsp" style=" padding-top:0px; padding-bottom:0px; width:250px;height:82px"> <img src="<%=strContextPath%>Images/eventzalley_logo.png"  style="height: 70%;padding-left: 30%; margin-top:15px;" ></div> --%>
            <div class="navbar-header" style="padding-left:40px;padding-top: 0px;">
            
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
               <a class="navbar-brand" href="<%=strContextPath%>myHome.jsp" style="padding-top: 5px; padding-left: 0px; margin-left: 5px;" >
              <img src="<%=strContextPath%>Images/EventzalleyLogo.png" height="50px"  ></a>
                <!-- <a class="navbar-brand" href="myHome.html" >EventzAlley</a><br>   -->         
			
				
            </div>
           
           
           
            <div class="collapse navbar-collapse" id="navbar-collapse" >
                <ul class="nav navbar-nav navbar-right" style=" background-color:#4E4D4D;height: 58px;" >
                    <!-- Call Search -->
                  <li> <div class="image" style=" padding-top: 2px;" >
                  <a class="navbar-brand" href="<%=strContextPath%>Masters/userMaster.jsp" style="padding-top: 5px; padding-left: 0px; margin-left: 5px;" >
					<% if( (null!= objUMB.getStrImagePath() && objUMB.getStrImagePath().trim().length() > 0 )  ){
                  			%>
                    <img src="<%=strContaxtImagePath%>/MemberProfile/<%=objUMB.getStrImagePath()%>" class="image--cover" alt="User"    style=" width: 45px;height: 45px; padding-bottom: 0px;  margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;" />
					<%}else{%>
                        	<img src="<%=strContextPath%>Images/user.png" alt="" class="image--cover center" style=" width: 45px;height: 45px; padding-bottom: 0px;  margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;"/>
                     	<%} %> 
                 <%--    <img src="<%=strContextPath%>Images/EventLogo.png" class="image--cover" alt="User" 
>>>>>>> 5e24a75d3daefdcb1ee92a521582f048db555fb3
                   
                    
                    style=" width: 45px;height: 45px; padding-bottom: 0px;  margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px;" />  --%> </a>
                </div></li>
                   <li><div class="navbar-brand"  aria-haspopup="true" aria-expanded="false" style="color:white;padding-top: 14px;"> <a class="navbar-brand" href="<%=strContextPath%>Masters/userMaster.jsp" style="padding-top: 5px; padding-left: 0px; margin-left: 5px;" ><%=WebUtil.processString(objUMB.getStrFirstName())%> <%=WebUtil.processString(objUMB.getStrLastName())%></a></div></li>
                    <li ><a href="<%=strContextPath%>sign-in.jsp?Action=sign-out" style="padding-top: 0px;" title="Sign out">
          <span class="glyphicon glyphicon-log-out " ></span>
        </a>
                    </li>
                    
                    <!-- <li><a href="javascript:void(0);" class="js-search" style=" padding-top: 0px;" data-close="true"><i class="material-icons">search</i></a></li> -->
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                     <!-- <li class="dropdown"  >
                        <a href="javascript:void(0);" class="dropdown-toggle" style=" padding-top: 0px;"  data-toggle="dropdown" role="button">
                            <i class="material-icons" >notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu" >
                            <li class="header" data-close="true">NOTIFICATIONS</li>
                            <li class="body">
                              
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">No New Notifications</a>
                            </li>
                        </ul>
                    </li> -->
                   <!--  #END# Notifications
                    Tasks -->
                   <!--  <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" style=" padding-top: 0px;" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <span class="label-count">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">No Tasks</a>
                            </li>
                        </ul>
                    </li>  -->
                    <!-- #END# Tasks -->
                    
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        
    </section>

    
  
</body>

</html>
