<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>

<%
	String strContextPath = "";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%> 


   
   
    
     <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/bootstrap.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/main.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/lib/datatables/3/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/lib/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/lib/jquery.gritter/css/jquery.gritter.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/lib/validationEngine/css/validationEngine.jquery.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/jquery-ui.css">
    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="<%=strContextPath%>assets/lib/metismenu/metisMenu.min.css">
	<link rel="stylesheet" href="<%=strContextPath%>assets/css/chosen.min.css">    
   
    <link rel="stylesheet" href="<%=strContextPath%>assets/lib/inputlimiter/jquery.inputlimiter.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/lib/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/uniform.default.min.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/jquery.tagsinput.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/datepicker3.min.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/bootstrap-datetimepicker.min.css">
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="<%=strContextPath%>assets/lib/html5shiv/html5shiv.js"></script>
      <script src="<%=strContextPath%>assets/lib/respond/respond.min.js"></script>
      <![endif]-->


    <link rel="stylesheet/less" type="text/css" href="<%=strContextPath%>assets/less/theme.less">
    <script src="<%=strContextPath%>assets/js/less.min.js"></script>

    <!--Modernizr-->
     <script src="<%=strContextPath%>assets/lib/modernizr/modernizr.min.js"></script>
    
     <link rel="stylesheet" href="<%=strContextPath%>assets/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<%=strContextPath%>assets/css/style-switcher.css">
     <script src="<%=strContextPath%>assets/lib/less/less-1.7.5.min.js"></script>
    
    <!-- Uthkrushta  -->
  
    <script src="<%=strContextPath%>assets/js/aa.js"></script>
    <script src="<%=strContextPath%>assets/js/applicationValidation.js"></script>
    <script src="<%=strContextPath%>assets/js/commonActions.js"></script>