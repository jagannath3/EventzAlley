
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>

<%
	String strSelectedID = ";";
	String strUnSelectedID = ";";
	String strUserAction = "";
	int intRowCounter = 0;
	int intCurrentPage = 1;
	int intReviewCurrentPage = 1;
	int intSkippedCurrentPage = 1;
	int intUnRecordedCurrentPage = 1;
	boolean blnIsAjaxSubmit = true; 
	
	if( null != request.getParameter(ILabelConstants.SEL_ID)){
		strSelectedID = WebUtil.parseString(request, ILabelConstants.SEL_ID);
	}
	if( null != request.getParameter(ILabelConstants.UN_SEL_ID)){
		strUnSelectedID = WebUtil.parseString(request, ILabelConstants.UN_SEL_ID);
	}
	if( null != request.getParameter(ILabelConstants.ROW_COUNTER)){
		intRowCounter = WebUtil.parseInt(request, ILabelConstants.ROW_COUNTER);
	} else{
		intRowCounter = -1;
	}
	if( null != request.getParameter(ILabelConstants.USER_ACTION)){
		strUserAction = WebUtil.parseString(request, ILabelConstants.USER_ACTION);
	}
	if( null != request.getParameter(ILabelConstants.IS_AJAX_SUBMIT)){ 
		blnIsAjaxSubmit = WebUtil.parseBoolean(request, ILabelConstants.IS_AJAX_SUBMIT);
	} 
	if( null != request.getParameter(ILabelConstants.CURRENT_PAGE)){
		intCurrentPage = WebUtil.parseInt(request, ILabelConstants.CURRENT_PAGE); 
	}
	if( null != request.getParameter(ILabelConstants.REVIEW_CURRENT_PAGE)){
		intReviewCurrentPage = WebUtil.parseInt(request, ILabelConstants.REVIEW_CURRENT_PAGE); 
	}
	if( null != request.getParameter(ILabelConstants.SKIPPED_CURRENT_PAGE)){
		intSkippedCurrentPage = WebUtil.parseInt(request, ILabelConstants.SKIPPED_CURRENT_PAGE); 
	}
	if( null != request.getParameter(ILabelConstants.UNRECORDED_CURRENT_PAGE)){
		intUnRecordedCurrentPage = WebUtil.parseInt(request, ILabelConstants.UNRECORDED_CURRENT_PAGE); 
	}
%> 

<input type="hidden" name="<%=ILabelConstants.hdnUserAction %>" size="2" class="flattextbox" value="<%=strUserAction %>" id="<%=ILabelConstants.hdnUserAction %>">


<input type="hidden" name="<%=ILabelConstants.hdnRowCounter %>" size="2" class="flattextbox" value="<%=intRowCounter%>" id="<%=ILabelConstants.hdnRowCounter%>">
   
<input type="hidden" name="hdnAjaxSubmit" value="<%=blnIsAjaxSubmit %>" size="2">
<input type="hidden" name="hdnSelectedID" id="hdnSelectedID" value="<%=strSelectedID%>" size="5">
<input type="hidden" name="hdnUnSelectedID"  value="<%=strUnSelectedID %>" size="10">

<input type="hidden" name="hdnShowWait" value="1" size="2"> 
<input type="hidden" name="FocusedElement" size="2" class="flattextbox" value="">
<input type="hidden" name="LastCell" size="2" class="flattextbox" value=""> 
<input type="hidden" name="<%=ILabelConstants.hdnPageEdited%>" size="2" class="flattextbox" value="0">
<input type="hidden" name="<%=ILabelConstants.hdnCurrentPage %>" id="<%=ILabelConstants.hdnCurrentPage %>" size="2" class="flattextbox" value="<%=intCurrentPage%>">
<input type="hidden" name="<%=ILabelConstants.hdnReviewCurrentPage %>"  size="2" class="flattextbox" value="<%=intReviewCurrentPage%>">
<input type="hidden" name="<%=ILabelConstants.hdnSkippedCurrentPage %>" size="2" class="flattextbox" value="<%=intSkippedCurrentPage%>">
<input type="hidden" name="<%=ILabelConstants.hdnUnRecordedCurrentPage %>" size="2" class="flattextbox" value="<%=intUnRecordedCurrentPage%>">
