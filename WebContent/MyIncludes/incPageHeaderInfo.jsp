<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/" %>
<script>
function hideIt(obj)
{
  obj.style.display='none';
}

$(function(){
$('#timeOut').delay(3000).hide(500);
});

function afterAjaxReload(){
	
	$('#timeOut').delay(3000).hide(500);
}



</script>
 

<%
	String strPageHeader = "";
	String strPageDesc = "";
	String strStatusStyle = "";
	String strStatusText = "";
	String strImg="";
	//Added Here for Showing the Expiry Due;
	String strStatusDueStyle="";		
	String strStatusDueText="";
			
	if( null != request.getParameter("PAGE_HEADER")){
		strPageHeader = WebUtil.parseString(request, "PAGE_HEADER");
	}

	if( null != request.getParameter("PAGE_DESC")){
		strPageDesc = WebUtil.parseString(request, "PAGE_DESC");
	}
	
	if( null != request.getParameter("STATUS_STYLE")){
		strStatusStyle = WebUtil.parseString(request, "STATUS_STYLE");
	}
	
	if( null != request.getParameter("STATUS_TEXT")){
		strStatusText = WebUtil.parseString(request, "STATUS_TEXT");
	}
	
	//Added Here for Showing the Expiry Due;
	if( null != request.getParameter("STATUS_DUE_STYLE")){
		strStatusDueStyle = WebUtil.parseString(request, "STATUS_DUE_STYLE");
	}		
			
	if( null != request.getParameter("STATUS_DUE_TEXT")){
		strStatusDueText = WebUtil.parseString(request, "STATUS_DUE_TEXT");
	}
	
	if( null != request.getParameter("IMG")){
		strImg = WebUtil.parseString(request, "IMG");
		request.getSession().setAttribute("IMG", strImg); 
	}
	
	if(null!=strImg && strImg.trim().length()==0){
	 String strSessionImage=(String)request.getSession().getAttribute("IMG");
	      if(null!=strSessionImage && strSessionImage.trim().length()>0){
	    	  strImg=strSessionImage;
	      }
	}
	
	
	
	out.print(strStatusDueText);
  
    
	
%>

	<%-- <header class="head pageheaderbg">
		<div class="main-bar" style="padding-top:0px;padding-bottom:5px;">
            <h3>&nbsp; <%=strPageHeader %></h3>
            
          </div>
	</header>  --%>
	<!-- fa fa-file-text -->
	
	<%-- <div id="PageHeader">	<!-- Start of PageHeader -->

		<span class="headerText"><%=strPageHeader %></span> 
		<div class="clear"></div>
		<span class="PageUse"><%=strPageDesc %></span>
		
	</div>  <!-- End of PageHeader -->
 --%> 
	<aa:zone name="Status" skipIfNotIncluded="true"> 	

	<%	if(strStatusStyle.trim().length() > 0){ %>
	
		 <div title="Click to close Message" class="row" onclick="hideIt(this)" onmouseover=""> <!-- Action Panel -->
		  <div class="col-lg-12" >
		  	<div class="box" style="
    padding-left: 15px;
    padding-right: 15px;
">
		  	<header > 
			<div class="<%=strStatusStyle%> defaultRoundedBox defaultPadding"  id="timeOut" 
   			style="border-bottom-width: 1px;text-align:center;  height: 45px;padding-top:12px"><%=strStatusText %></div>
		</header>
		</div>
		</div>
		</div> 
		
	<% } %>
	
	<%	if(strStatusDueStyle.trim().length() > 0){ %>
	
		 <div title="Click to close Message" class="row" onclick="hideIt(this)" onmouseover=""> <!-- Action Panel -->
		  <div class="col-lg-12" >
		  	<div class="box" style="
    padding-left: 15px;
    padding-right: 15px;
">
		  	<header> 
			<div class="<%=strStatusDueStyle%> defaultRoundedBox defaultPadding"><%=strStatusDueText %></div>
		</header>
		</div>
		</div>
		</div> 
		
	<% } %>
	</aa:zone>