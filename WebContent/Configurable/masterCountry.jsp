
<%@page import="org.ajaxanywhere.AAUtils"%>
<%@page import="com.uthkrushta.mybos.master.controller.CountryMasterController"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>
<%@page import="com.uthkrushta.mybos.master.bean.CountryBean"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);   
%>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Eventzalley</title>
  
  
   
<!-- Bootstrap Select Css -->
    <link href="../plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />    
    <script>

      function afterAjaxReload(){
    	  Metis.MetisTable();
    	  Metis.formGeneral();
    	  $('#<%=ILabelConstants.selActivityID%>_chosen').css({"color":"#000"});
          /* Metis.metisSortable(); */
    	}
    </script>
</head>
 <%
	int Index = 1;
	int intRowCounter = 1;
	String strPageHeader = "Country Master";
	String strPageDescription = "This page is used to manage Country names.";
	String strStatusText = "";
	String strStatusStyle = "";
	String strUserAction = "GET";

	
	
	CountryBean objCountry = new  CountryBean();
	GenericController objGC=new CountryMasterController();

	List<Object> arrList = null;
	ArrayList<MessageBean> arrMsg = null; 
	MessageBean objMsg;

	if(AAUtils.isAjaxRequest(request)){
		strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
		intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
		objGC.doAction(request, response);		//Calling the controller to do actions
		
		arrList = objGC.getList(request);		//Getting the list to display  
		if(!strUserAction.equalsIgnoreCase("ADD") && null!=arrList){     
	intRowCounter = arrList.size();      
		}
		else if(!strUserAction.equalsIgnoreCase("ADD") && null==arrList){     
	intRowCounter = 1;     
		}
		else if(null ==arrList){    
	intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter)); 
		}
	}
	arrList = objGC.getList(request);		//Getting the list to display

	if(null == arrList){
		//arrList = new ArrayList<Object>();
		if(strUserAction.equals("GET")){
	intRowCounter = 0;
		}
		}else{
	if(!strUserAction.equalsIgnoreCase("ADD")){
		intRowCounter = arrList.size();      //row counter counts the row (check box)
	}
		}
	arrMsg = objGC.getArrMsg();		//Getting the messages to display
	if(null!= arrMsg && arrMsg.size() > 0)
	{
		objMsg = MessageBean.getLeadingMessage(arrMsg);
		if(objMsg != null)
		{
	strStatusText = objMsg.getStrMsgText();
	strStatusStyle = objMsg.getStrMsgType();
		}
	}
	AAUtils.addZonesToRefreh(request, "List");
%> 
<body class="theme-blue" style="background-color:#e9e9e9;">
<form method="post">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    
    
    
    <jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
	   <jsp:param value="../" name="CONTEXT_PATH"/>
   </jsp:include>
    <!-- #Top Bar -->
  
        
            
            <jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
	  <jsp:param value="../" name="CONTEXT_PATH"/>
   </jsp:include>
          
        

    <section class="content">
    			<jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
    
        <div class="container-fluid">
            	<!-- Start of editable table  -->
				<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" >
                            <h2>
                               Country
                               
                            </h2>
                          <!-- <img src="/EventzAlley/servlet1/4/157490.png">

<a href="/EventzAlley/servlet1/1/157490.png">click for photo</a>  -->
		    			</div>
                       
                        <div id="ActionPanel" class="row no-margin-bottom default-margin " style="background-color:#006699;height: 50px; margin-right:0px;margin-left:0px;">
              			<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft">
               			<input type="button" value="Save" class="btn btn-primary" style="margin-top:7px" onClick="onSubmit()" />
             			<input type="button" value="Add Row" class="btn btn-primary" style="margin-top:7px" onClick="onAddRow()" />
                		<input type="button" value="Delete" class="btn btn-danger "  style="margin-top:7px" onClick="onDelete()" />
              			</div>
              			<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight" style="float:right;" >
                		<input  type="button" value="Close" class="btn btn-danger rightDefaultMargin alignRight" style="float:right;background-color:blue;margin-top:7px " onClick="onCancel('../myHome.jsp')" />
              			</div>
		    			</div>
                        
                        
                        
                        <div class="body" style="padding-top: 0px;">
                        <aa:zone name="List" skipIfNotIncluded="true"> 
                            <table id="dataTable" class="table table-striped">
                             <!--    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Cost</th>
                                        <th>Profit</th>
                                        <th>Fun</th>
                                    </tr>
                                </thead> -->
                                <thead>
                      
                        <tr>
                          <th style="width:5px">
                          <input type="checkbox" id="md_checkbox_26" class="filled-in chk-col-blue" name="chkMain" value="1" onclick="SelectAll('chkChild_',this,hdnRowCounter,hdnSelectedID)" />
                          <label for="md_checkbox_26"></label>
                          </th>
                          <th style="vertical-align:top">Country</th>
                          
                        </tr>
                      </thead>
                                <tbody>
                                
                                 <%
                      	for(Index = 1; Index <= intRowCounter; Index++){
                      	    					if(!strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD)){
                      	    						if(null != arrList){
                      	    							objCountry = (CountryBean)arrList.get(Index-1);  
                      	    						}
                      	    						if(null == objCountry){
                      	    							objCountry = new CountryBean();
                      						}
                      	    					}else{
                      	    						objCountry = new CountryBean();
                      	    						objCountry.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + Index));
                      	    						objCountry.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + Index));
                      	    						
                      	    					}
                      %>
                                
                                
                                    <tr id="tableRow">
                                        <td class="edit">
                                        
                                        <input style="margin-left:12px;" type="checkbox" id="md_checkbox_26_<%=Index %>" class="filled-in chk-col-blue" name="<%=ILabelConstants.chkChild_ +Index %>" value="<%=objCountry.getLngID() %>" onclick="CheckSelectSingle(this,chkMain,hdnRowCounter,hdnSelectedID)" ><!-- style="margin-left:10px;" -->
                           				<label for="md_checkbox_26_<%=Index %>"></label>
                           				<input type="hidden" name="<%=ILabelConstants.hdnID_ + Index %>" value="<%=objCountry.getLngID() %>" SIZE="2" >  
						  				
						  				</td>
                          				<td>
                          				<input  class="validate[required] form-control" id="req" type="text" maxlength="75" FieldType="mandatory" FieldName="Country"  name="<%=ILabelConstants.txtName_ +Index %>" value="<%=objCountry.getStrName()%>" onkeypress="return isSpclChar(event)">
                           				<span style="display:none;"><%=objCountry.getStrName()%></span>
                                        </td>
                                    </tr>
                                    <%
                      
	    				}
                      
                      %>
                                </tbody>
                               <!--  <tfoot>
                                    <tr>
                                        <th><strong>TOTAL</strong></th>
                                        <th>1290</th>
                                        <th>1420</th>
                                        <th>5</th>
                                    </tr>
                                </tfoot> -->
                            </table>
                            
                             <jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
					     <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
					     <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
					     <jsp:param name="IS_AJAX_SUBMIT" value="true" />
				 	</jsp:include>
		  		</aa:zone>
                        </div>
                    </div>
                </div>
            </div>
			<!-- End od editable table  -->	
		</div>		
	</section>			
	
				
				
				
	
<jsp:include page="../MyIncludes/incBottomPageReferences.jsp" flush="true">
 <jsp:param value="../" name="CONTEXT_PATH"/>
   </jsp:include>
<!-- Jquery Core Js -->
    <script src="../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="../plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="../plugins/raphael/raphael.min.js"></script>
    <script src="../plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="../plugins/chartjs/Chart.bundle.js"></script>

   
   

  

    <!-- Custom Js -->
    <script src="../js/admin.js"></script>
    

   
  
    <script type="text/javascript">
    
    

function afterAjaxReload(){
/* 	  $('#dataTable').destroy(); */
	
	table = Metis.MetisTable();
    /* Metis.metisSortable(); */
	 Metis.formGeneral();
	 $('#<%=ILabelConstants.selActivityID %>_chosen').css({"color":"#000"});
	 $('#<%=ILabelConstants.selActivityID %>_chosen').trigger('chosen:updated');
}
$(function() {
	/* $('#dataTable').destroy();  */
	
	
    Metis.MetisTable();
    Metis.formGeneral();
//     /* Metis.metisSortable(); */
    $('#<%=ILabelConstants.selActivityID %>_chosen').css({"color":"#000"});
    $('#<%=ILabelConstants.selActivityID %>_chosen').trigger('chosen:updated');
  });

function isSpclChar(evt)
{
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	   if ((charCode >= 33 && charCode <= 43) || (charCode >= 58 && charCode <= 64) || (charCode >= 91 && charCode <= 94) || (charCode >= 123 && charCode <= 126)) 
	      return false;
	   return true;

}

</script>
</form>


</body>

</html>
