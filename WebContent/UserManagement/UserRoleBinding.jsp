<!DOCTYPE html>

<%@page
	import="com.uthkrushta.mybos.frameWork.bean.ActivityRoleBindingFrameWorkBean"%>
<%@page import="com.uthkrushta.mybos.frameWork.bean.ApplicationMenuBean"%>
<%@page
	import="com.uthkrushta.mybos.frameWork.bean.ApplicationMasterBean"%>
<%@page
	import="com.uthkrushta.mybos.frameWork.controller.ActivityRoleBindingController"%>
<%@page import="com.uthkrushta.mybos.code.bean.RoleTypeBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page
	import="com.uthkrushta.mybos.master.controller.RoleMasterController"%>
<%@page import="com.uthkrushta.mybos.master.bean.RoleBean"%>
<%@page
	import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page
	import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.code.bean.PaymentModeCodeBean"%>

<%@page import="com.uthkrushta.mybos.master.bean.AreaMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>EventzAlley :: Role Binding</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

<link href="../Style/default.css" rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Bootstrap Datepicker Plugin Js -->
<!-- <script src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> -->


<!-- <link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
	rel="stylesheet" type="text/css"> -->


<!-- <!-- Bootstrap Material Datetime Picker Css  	-->
<!--  <link href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" /> -->


<link href="../css/themes/all-themes.css" rel="stylesheet" />

<script type="text/javascript">
function checkIsActive(obj)
{
   
    var chkCount= $(".isActive:checked").length;
      
    if(chkCount>4)
        {
          alert("You can select only 4 Quick Menu option");
          $(obj).attr("checked",false);
        }
}

function checkHomePageLimit(obj)
{
     var chkCount=$(".isHomePage:checked").length;
     if(chkCount>1)
         {
   	  alert("You can select only 1 Home Menu option");
         $(obj).attr("checked",false);
         }
}

function SelectMasterPermission(objChild,objMaster,objActivityCounter){
intActivityCounter = objActivityCounter.value;
for(i=1 ; i<=intActivityCounter ; i++){
	chkChild = eval("document.forms[0]." + objChild + i);  
	
	if(objMaster.checked == true){
		chkChild.checked = true;
	}else if(objMaster.checked == false){
		chkChild.checked = false;
	}
}
}

function SelectChildPermission(objChild,objMaster,objActivityCounter){
intActivityCounter = objActivityCounter.value
blnAllSelected = true;
for(intIndex=1;intIndex<= intActivityCounter;intIndex++)
{
                if(objChild != null)
                {
                        if(objChild.checked == false)
                        {
                                blnAllSelected = false;
                                break;
                         }
                 }
}
chkMaster = eval("document.forms[0]." + objMaster);
chkMaster.checked=blnAllSelected;
}

   
    
    
    

    </script>

</head>

<style>
ul {
	list-style-type: none;
}
</style>

<%
	long lngMenuID = 1;
	long lngActivityID = WebUtil.parseLong(request, ILabelConstants.selActivityID);
	long lngRoleID = WebUtil.parseLong(request, ILabelConstants.ID);
	long lngUserID = 0;
	long lngPrevMenuId = 0;
	int intMenuCounter = 0;
	int intActivityCounter = 0;
	int intUserType = 2;
	int Index = 1;
	int intRowCounter = 1;

	String strPageHeader = "Assign Permissions";
	String strPageDescription = "This page is used to assign permissions to User Roles";
	String strStatusStyle = "";
	String strStatusText = "";
	String strWhereClause = "lngRoleId= " + lngRoleID + "AND lngActivityId=" + lngActivityID;
	String strSortClause = "";
	String strAttributes = "";
	String strSelected = ";";
	String strQuickSelected = ";";
	String strHomePageSelected = ";";
	String strUserAction = "GET";
	strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
	/* DBUtil DBUtil = new DBUtil(request); */

	ArrayList<MessageBean> arrMsg = null;
	MessageBean objMsg;

	GenericController objGC = new ActivityRoleBindingController();
	List<Object> arrList = null;

	if (AAUtils.isAjaxRequest(request)) {
		intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
		objGC.doAction(request, response);
		arrList = objGC.getList(request);

	}
	//out.println("ARRLIST = "+arrList);

	arrList = objGC.getList(request);
	if (null == arrList) {
		arrList = new ArrayList<Object>();
		if (strUserAction.equals("GET")) {
			intRowCounter = 0;
		}
	} else {
		if (!strUserAction.equalsIgnoreCase("ADD")) {
			intRowCounter = arrList.size();
		}
	}
	//out.println("ARRLIST = "+intRowCounter);
	arrMsg = objGC.getArrMsg();
	//out.println("ARRMSG = "+arrMsg);
	if (null != arrMsg && arrMsg.size() > 0) {
		objMsg = MessageBean.getLeadingMessage(arrMsg);
		if (objMsg != null) {
			strStatusText = objMsg.getStrMsgText();
			strStatusStyle = objMsg.getStrMsgType();
			request.setAttribute(ISessionAttributes.ERR_LIST, arrMsg);
			if ((strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL)
					|| strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
					&& strStatusStyle.equalsIgnoreCase(IUMCConstants.OK)) {
				arrList = objGC.getList(request);
				if (null == arrList) {
					arrList = new ArrayList<Object>();
				} else {
					intRowCounter = arrList.size();
				}
			}
		}
	}

	RoleTypeBean objRoleBean = new RoleTypeBean();
	objRoleBean = (RoleTypeBean) DBUtil.getRecord(IUMCConstants.BN_ROLE_TYPE, "lngID = " + lngRoleID, "");

	//List<ApplicationMasterBean> AppMenuBean =null;

	long lngRoleAccessID = 0;
	if (null != session.getAttribute("ROLE_ID")) {
		lngRoleAccessID = ((Long) session.getAttribute("ROLE_ID")).longValue();
	}

	RoleBean objRoleTypeAccessBean = new RoleBean();
	objRoleTypeAccessBean = (RoleBean) DBUtil.getRecord(IUMCConstants.BN_ROLE_BEAN,
			"lngID = " + lngRoleAccessID, "");

	List AppMenuBean = null;

	if (objRoleTypeAccessBean.getIntRoleType() == 1 || objRoleTypeAccessBean.getIntRoleType() == 2) {
		AppMenuBean = (List<ApplicationMasterBean>) DBUtil.getRecords(IUMCConstants.BN_APPLICATION_MASTER_MENU_BEAN, "", "");
	} else {
		AppMenuBean = (List<ApplicationMenuBean>) DBUtil.getRecords(IUMCConstants.BN_APPLICATION_MENU_BEAN,
				"lngRoleID=" + lngRoleAccessID, "");
	}

	ApplicationMasterBean objAMBean = new ApplicationMasterBean();
	ApplicationMenuBean objAMenB = new ApplicationMenuBean();

	int intMenuBeanType = 0;//1--Master Bean,2---Menu Bean 

	List<ApplicationMenuBean> AppMenuItems = (List<ApplicationMenuBean>) DBUtil
			.getRecords(IUMCConstants.BN_APPLICATION_MENU_BEAN, "lngRoleID=" + lngRoleID, "");

	ApplicationMenuBean objAppMenuItem = new ApplicationMenuBean();

	/* List<ActivityRoleBindingFrameWorkBean> AppMItems = (List<ActivityRoleBindingFrameWorkBean>) DBUtil.getRecord(IUMCConstants.BN_ACTIVITY_ROLE_BINDING_BEAN, strWhereClause, ""); */

	ActivityRoleBindingFrameWorkBean objAppMitems = new ActivityRoleBindingFrameWorkBean();

	arrMsg = objGC.getArrMsg();
	if (null != arrMsg && arrMsg.size() > 0) {
		objMsg = MessageBean.getLeadingMessage(arrMsg);
		if (objMsg != null) {
			strStatusText = objMsg.getStrMsgText();
			strStatusStyle = objMsg.getStrMsgType();
		}
	}
%>


<body class="theme-blue" style="background-color: #e9e9e9;">
	<form method="post">
		<!-- Page Loader -->




		<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
			<jsp:param value="../" name="CONTEXT_PATH" />

		</jsp:include>
		<!-- #Top Bar -->

		<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
			<jsp:param value="../" name="CONTEXT_PATH" />
		</jsp:include>

		<section class="content">
			<jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
				<jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
				<jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
			</jsp:include>



			<div class="container-fluid">
				<div id="content" class="row clearfix ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

						<div class="card" style="margin-bottom: 0px;">
							<div class="header">
								<h2 style="color: #006699;">Permission Assignment</h2>
							</div>





							<div id="ActionPanel"
								class="row no-margin-bottom default-margin "
								style="background-color: #006699; border: none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
								<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
									<input type="button" value="Save" class="btn btn-primary"
										onClick="onSubmit()" />

								</div>
								<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight" style="margin-top:4px">
									<input type="button" value="Close" name="btnCancel"
										class="btn btn-danger" style="float: right"
										onClick="onClose()">
								</div>
							</div>

							

									<%
										String strPermistions = ";";
											String strCreatePermision = "";
											String strUpdatePermision = "";
											String strDeletePermision = "";
											String strApprovePermision = "";

											if (null != AppMenuItems) {
												for (int j = 0; j < AppMenuItems.size(); j++) {
													objAppMenuItem = AppMenuItems.get(j);
													strSelected += objAppMenuItem.getLngActivityID() + ";";
													if (objAppMenuItem.getIntIsQuickMenu() == 1) {
														strQuickSelected += objAppMenuItem.getLngActivityID() + ";";
													}
													if (objAppMenuItem.getIntIsHomePage() == 1) {
														strHomePageSelected += objAppMenuItem.getLngActivityID() + ";";
													}
													strPermistions += objAppMenuItem.getLngActivityID() + "_"
															+ objAppMenuItem.getIntPermissionToCreate() + "_"
															+ objAppMenuItem.getIntPermissionToUpdate() + "_"
															+ objAppMenuItem.getIntPermissionToDelete() + "_"
															+ objAppMenuItem.getIntPermissionToApprove() + ";";
												}
											}
											//	out.print(strPermistions);
									%>

									<%
										if (null != AppMenuBean) {
									%>
									<table id="dataTable" class="table  table-condensed ">
										<tr>
											<td width="100%" style="padding-left: 20px;">
												<div class="clear"></div>
												
													<!-- Menu Start -->
													<%
														//out.print("MenuItems.size()" + MenuItems.size());
																for (int i = 1; i <= AppMenuBean.size(); i++) {

																	if (AppMenuBean.get(i - 1) instanceof ApplicationMasterBean) {
																		intMenuBeanType = 1;
																		objAMBean = (ApplicationMasterBean) AppMenuBean.get(i - 1);

																		intActivityCounter++;

																		if (objAMBean.getLngMenuId() != lngPrevMenuId) {
																			intMenuCounter++;
																			if (i > 1) {
													%>
													<input type="hidden" size="2"
														value="<%=intActivityCounter - 1%>"
														name="hdnActivityCounter_<%=intMenuCounter - 1%>">
													<%
														}
																			intActivityCounter = 1;
													%>
													<script>
				      if(document.getElementById("SubMenu<%=lngPrevMenuId%>") != null)
				      {
				      	document.write("</ul></div>");
				      }
	      			 </script>


													<div>
														<div
															style="padding-left: 5px; height: 25; width: 98%; padding-top: 5px; overflow: Auto;">
															<input type="checkbox"
																id="md_checkbox_29_<%=intMenuCounter%>"
																class="filled-in chk-col-teal"
																name="Main_Menu_<%=intMenuCounter%>"
																value="<%=objAMBean.getLngMenuId()%>"
																onclick="SelectAll('Child_Menu_<%=intMenuCounter%>_',this,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID)" />
															<label for="md_checkbox_29_<%=intMenuCounter%>">
																<span style="margin-left: 10px; font-weight: bold"><%=objAMBean.getStrMenuName()%>
															</span>
															</label>

															<%--  <input type="checkbox" value="<%=objAMBean.getLngMenuId()%>" name = "Main_Menu_<%=intMenuCounter%>" onclick="SelectAll('Child_Menu_<%=intMenuCounter%>_',this,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID)" > --%>

														</div>

													</div>
													

														<%
															}
														%>
														<ul>
															<li><input type="hidden" size="2"
																value="<%=objAMBean.getLngActivityId()%>"
																name="<%=ILabelConstants.hdnID_ + intMenuCounter + "_" + intActivityCounter%>">

																<div
																	style="padding-left: 10px; height: 30px; width: 98%; overflow: Auto;">
																	<input type="checkbox"
																		id="md_checkbox_29_<%=intMenuCounter%>_<%=intActivityCounter%>"
																		class="filled-in chk-col-teal"
																		name="Child_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>"
																		value="<%=objAMBean.getLngActivityId()%>"
																		<%if (strSelected.contains(";" + objAMBean.getLngActivityId() + ";")) {%>
																		checked <%}%>
																		onclick="CheckSelectSingle1(this,Main_Menu_<%=intMenuCounter%>,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID,hdnAssignedActivities,hdnDeletedActivities)" />
																	<label
																		for="md_checkbox_29_<%=intMenuCounter%>_<%=intActivityCounter%>">

																		<%-- <input type="checkbox" style="margin-right:20px" value="<%=objAMBean.getLngActivityId() %>" name = "Child_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>" <%if(strSelected.contains(";"+objAMBean.getLngActivityId()+";")){%>checked <%}%> onclick="CheckSelectSingle1(this,Main_Menu_<%=intMenuCounter%>,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID,hdnAssignedActivities,hdnDeletedActivities)"> --%>
																		<span style="margin-left: 5px"><%=objAMBean.getStrActivityName()%></span>
																	</label> <span style="position: absolute; left: 38%;"><input
																		class="isActive" type="checkbox" value="1"
																		name="Quick_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>"
																		<%if (strQuickSelected.contains(";" + objAMBean.getLngActivityId() + ";")) {%>
																		checked <%}%> onclick="checkIsActive(this)"></span> <span
																		style="position: absolute; left: 70%;"><input
																		class="isHomePage" type="checkbox" value="1"
																		name="Home_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>"
																		<%if (strHomePageSelected.contains(";" + objAMBean.getLngActivityId() + ";")) {%>
																		checked <%}%> onclick="checkHomePageLimit(this)"></span>
																</div></li>




														</ul>


														<%
															lngPrevMenuId = objAMBean.getLngMenuId();
																		} //Application Master Bean ends
																		else if (AppMenuBean.get(i - 1) instanceof ApplicationMenuBean) {
																			intMenuBeanType = 2;
																			objAMenB = (ApplicationMenuBean) AppMenuBean.get(i - 1);

																			intActivityCounter++;

																			if (objAMenB.getLngMenuID() != lngPrevMenuId) {
																				intMenuCounter++;
																				if (i > 1) {
														%>
														<input type="hidden" size="2"
															value="<%=intActivityCounter - 1%>"
															name="hdnActivityCounter_<%=intMenuCounter - 1%>">
														<%
															}
																				intActivityCounter = 1;
														%>
														<script>
				      if(document.getElementById("SubMenu<%=lngPrevMenuId%>") != null)
				      {
				      	document.write("</ul></div>");
				      }
	      			 </script>


														<div>
															<div
																style="padding-left: 5px; height: 25; width: 98%; padding-top: 5px; overflow: Auto;">
																<input type="checkbox"
																	id="md_checkbox_29_<%=intMenuCounter%>"
																	class="filled-in chk-col-teal"
																	name="Main_Menu_<%=intMenuCounter%>"
																	value="<%=objAMenB.getLngMenuID()%>"
																	onclick="SelectAll('Child_Menu_<%=intMenuCounter%>_',this,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID)" />
																<label for="md_checkbox_29_<%=intMenuCounter%>"></label>

																<%-- <input type="checkbox" value="<%=objAMenB.getLngMenuID()%>" name = "Main_Menu_<%=intMenuCounter%>" onclick="SelectAll('Child_Menu_<%=intMenuCounter%>_',this,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID)" > --%>
																<span style="margin-left: 10px; font-weight: bold"><%=objAMenB.getStrMenuName()%>
																</span>
															</div>

														</div>
														<div class="MenuItem"
															id="SubMenu<%=objAMenB.getLngMenuID()%>">

															<%
																}
															%>
															<ul>
																<li><input type="hidden" size="2"
																	value="<%=objAMenB.getLngActivityID()%>"
																	name="<%=ILabelConstants.hdnID_ + intMenuCounter + "_" + intActivityCounter%>">

																	<div
																		style="padding-left: 10px; height: 30px; width: 98%; overflow: Auto;">
																		<input type="checkbox"
																			id="md_checkbox_29_<%=intMenuCounter%>_<%=intActivityCounter%>"
																			class="filled-in chk-col-teal"
																			name="Child_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>"
																			value="<%=objAMenB.getLngActivityID()%>"
																			<%if (strSelected.contains(";" + objAMenB.getLngActivityID() + ";")) {%>
																			checked <%}%>
																			onclick="CheckSelectSingle1(this,Main_Menu_<%=intMenuCounter%>,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID,hdnAssignedActivities,hdnDeletedActivities)" />
																		<label
																			for="md_checkbox_29_<%=intMenuCounter%>_<%=intActivityCounter%>"></label>


																		<%--  <input type="checkbox" style="margin-right:20px" value="<%=objAMenB.getLngActivityID() %>" name = "Child_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>" <%if(strSelected.contains(";"+objAMenB.getLngActivityID()+";")){%>checked <%}%> onclick="CheckSelectSingle1(this,Main_Menu_<%=intMenuCounter%>,hdnActivityCounter_<%=intMenuCounter%>,hdnSelectedID,hdnAssignedActivities,hdnDeletedActivities)"> --%>
																		<span style="margin-left: 5px"><%=objAMenB.getStrActivityName()%></span>
																		<span style="position: absolute; left: 38%;"><input
																			class="isActive" type="checkbox" value="1"
																			name="Quick_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>"
																			<%if (strQuickSelected.contains(";" + objAMenB.getLngActivityID() + ";")) {%>
																			checked <%}%> onclick="checkIsActive(this)"></span>
																		<span style="position: absolute; left: 70%;"><input
																			class="isHomePage" type="checkbox" value="1"
																			name="Home_Menu_<%=intMenuCounter%>_<%=intActivityCounter%>"
																			<%if (strHomePageSelected.contains(";" + objAMenB.getLngActivityID() + ";")) {%>
																			checked <%}%> onclick="checkHomePageLimit(this)"></span>
																	</div></li>




															</ul>


															<%
																lngPrevMenuId = objAMenB.getLngMenuID();
																			} //Application Menu Bean ends

																		}
																	}
															%>
														</div>
														<!-- Menu End -->
											</td>

										</tr>
									</table>
									<input type="hidden" size="2" value="<%=intActivityCounter%>"
										name="hdnActivityCounter_<%=intMenuCounter%>"> <input
										type="hidden" size="2" value="<%=intMenuCounter%>"
										name="hdnMenuCounter"> <input type="hidden"
										name="hdnAssignedActivities" size="20" class="flattextbox"
										value="<%=strSelected%>"> <input type="hidden"
										name="hdnDeletedActivities" size="20" class="flattextbox"
										value=";">

									<jsp:include page="../MyIncludes/incPageVariables.jsp"
										flush="true">
										<jsp:param name="STATUS" value="<%=strStatusText%>" />
										<jsp:param name="STYLE" value="<%=strStatusStyle%>" />
										<jsp:param name="SEL_ID" value="<%=strSelected%>" />
										<jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
										<jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
									</jsp:include>

									<div>

										<span> <%
 											if (intMenuBeanType == 1) {
											 %> <input type="hidden" size="2"
											value="<%=objAMBean.getLngActivityId()%>"
											name="<%=ILabelConstants.hdnID_ + intMenuCounter + "_" + intActivityCounter%>">
											<%
												} else if (intMenuBeanType == 2) {
											%> <input type="hidden" size="2"
											value="<%=objAMenB.getLngActivityID()%>"
											name="<%=ILabelConstants.hdnID_ + intMenuCounter + "_" + intActivityCounter%>">
											<%
												}
											%>
										</span>
									</div>

								
									<%-- <jsp:include page="../MyIncludes/incPageVariables.jsp"
										flush="true">
										<jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
										<jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
										<jsp:param name="IS_AJAX_SUBMIT" value="true" />
									</jsp:include> --%>
								
								<div id="ActionPanel"
								class="row no-margin-bottom default-margin "
								style="background-color: #006699; border: none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%; margin-bottom: 25px;">
								<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
									<input type="button" value="Save" class="btn btn-primary"
										onClick="onSubmit()" />

								</div>
								<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight" style="margin-top:4px">
									<input type="button" value="Close" name="btnCancel"
										class="btn  btn-danger" style="float:right"
										onClick="onClose()">
								</div>
							</div>
								
								</div>
							<!-- card -->

							
						</div>
						<!-- col lg 12 -->



					</div><!-- #row clearfix -->
					
				</div>
				<!-- container-fluid -->
			
			

		</section>


	
		<%
			
		%>




		<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
			flush="true">
			<jsp:param value="../" name="CONTEXT_PATH" />
		</jsp:include>
		<!-- Jquery Core Js -->
		<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

		<!-- Bootstrap Core Js -->
		<script src="../plugins/bootstrap/js/bootstrap.js"></script>

		<!-- Select Plugin Js -->
		<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

		<!-- Slimscroll Plugin Js -->
		<script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

		<!-- Waves Effect Plugin Js -->
		<script src="../plugins/node-waves/waves.js"></script>

		<!-- Jquery CountTo Plugin Js -->
		<script src="../plugins/jquery-countto/jquery.countTo.js"></script>

		<!-- Morris Plugin Js -->
		<script src="../plugins/raphael/raphael.min.js"></script>
		<script src="../plugins/morrisjs/morris.js"></script>

		<!-- Bootstrap Material Datetime Picker Plugin Js -->
		<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

		<!--  Bootstrap Datepicker Plugin Js -->
		<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  -->

		<!-- dialogs Sweet Alert  -->
		<script src="../js/pages/ui/dialogs.js"></script>

		<!-- Sparkline Chart Plugin Js -->
		<script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

		<!-- Custom Js -->
		<script src="../js/admin.js"></script>
		<!-- <script src="../js/pages/index.js"></script> -->

		<!-- Demo Js -->
		<!-- <script src="../js/demo.js"></script> -->

		<!--  multiple file upload  -->

		<script src="../js/vendor/jquery.ui.widget.js"></script>
		<script src="../js/jquery.iframe-transport.js"></script>
		<script src="../js/jquery.fileupload.js"></script>

		<script>
     function afterAjaxReload(){
     	  Metis.MetisTable();
     	/* $('#dataTable').destroy(); */
           /* Metis.metisSortable(); */
           Metis.formGeneral();
           $('#<%=ILabelConstants.selActivityID%>_chosen').css({"color":"#000"});
     	}
     	
       $(function() {
           Metis.MetisTable();
          /*  $('#dataTable').destroy(); */
           Metis.formGeneral();
           $('#<%=ILabelConstants.selActivityID%>_chosen').css({"color":"#000"});
           $(".dateField").datepicker();
         });
       function openRolePermisions(ID){
     		strURL = "../UserManagement/UserRoleBinding.jsp?ID="+ID;
     		win = openPageInNewWin(strURL,"RoleBindingWin","scrollbars=yes, resizable=yes");
     		win.focus();
     	}
       function openDashBoardRolePermisions(ID){
     		strURL = "../UserManagement/UserDashBoardRoleBinding.jsp?ID="+ID;
     		win = openPageInNewWin(strURL,"RoleBindingWin","scrollbars=yes, resizable=yes");
     		win.focus();
     	}

    </script>


	</form>
</body>

</html>
