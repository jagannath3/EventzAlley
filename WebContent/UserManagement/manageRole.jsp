<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.code.bean.RoleTypeBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.controller.RoleMasterController"%>
<%@page import="com.uthkrushta.mybos.master.bean.RoleBean"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.code.bean.PaymentModeCodeBean"%>

<%@page import="com.uthkrushta.mybos.master.bean.AreaMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Bootstrap Datepicker Plugin Js -->
<!-- <script src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> -->


<!-- <link
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"
	rel="stylesheet" type="text/css"> -->


<!-- <!-- Bootstrap Material Datetime Picker Css  	-->
   <!--  <link href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" /> -->
  

<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<script type="text/javascript">
    
   
    
    
    

    </script>

</head>

<%
	
int Index = 1;
int intRowCounter = 1;

long lngRecordCount = 0;
long lngTotalPage = 1;
int intRecordsPerPage = 10;
long lngCompanyId = 0;

String strStatusText = "";
String strUserAction = "GET";         
String strStatusStyle = "";
String strNameFilter = "";



String strPageHeader = "Manage Role";
String strPageDescription = "This page is used to manage Role.";

RoleBean objRoleBean = new  RoleBean();
GenericController objRC =new RoleMasterController();

//Fetching the Login Staff from the session
	 long lngStaffType=0;
	 UserMasterBean objUMBFromSession = new UserMasterBean(); 
	long userID = ((Long) session.getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
	 
	objUMBFromSession = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+userID , "");
	 if(null!=objUMBFromSession)
	 {
		 RoleBean objRB=new RoleBean();
		 objRB=(RoleBean) DBUtil.getRecord(IUMCConstants.BN_ROLE_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objUMBFromSession.getLngRoleID(), "");
		 lngStaffType=objRB.getIntRoleType();
		 
	 }else
	 {
		 objUMBFromSession=new UserMasterBean();
	 }
	 
	

List<Object> arrList = null;
/* DBUtil DBUtil = new DBUtil(request); */
List arrRTBList = new ArrayList();  
if(lngStaffType == 1)
{
	arrRTBList =  DBUtil.getRecords(IUMCConstants.BN_ROLE_TYPE,IUMCConstants.GET_ACTIVE_ROWS,"strName");
}else
{
	arrRTBList =  DBUtil.getRecords(IUMCConstants.BN_ROLE_TYPE,IUMCConstants.GET_ACTIVE_ROWS+" and lngID >= "+IUMCConstants.ADMIN ,"strName");
}



ArrayList<MessageBean> arrMsg = null; 
MessageBean objMsg;

if(AAUtils.isAjaxRequest(request)){
	 strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
	intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
	objRC.doAction(request, response);	
	arrList = objRC.getList(request);
	
	arrList = objRC.getList(request);  
	if(!strUserAction.equalsIgnoreCase("ADD") && null!=arrList){     
		intRowCounter = arrList.size();      
	}else if(null ==arrList){    
		intRowCounter =  WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
	}
}

arrList = objRC.getList(request);

if(null == arrList){
	//arrList = new ArrayList<Object>();
	if(strUserAction.equals("GET")){
		intRowCounter = 0;
	}
	}else{
		if(!strUserAction.equalsIgnoreCase("ADD")){
	intRowCounter = arrList.size();      //row counter counts de row (check box)
		}
	}
	
arrMsg = objRC.getArrMsg();
if(null!= arrMsg && arrMsg.size() > 0)
{
	objMsg = MessageBean.getLeadingMessage(arrMsg);
	if(objMsg != null)
	{
		strStatusText = objMsg.getStrMsgText();
		strStatusStyle = objMsg.getStrMsgType();
	}
}

	
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
<form method="post">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

<section class="content">	
		<jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
		    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
		    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
		</jsp:include>
	


<div class="container-fluid">
	<div id="content" class="row clearfix ">
      	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      	
      	 	<div class="card" style="margin-bottom: 0px;">
            <div class="header" >
				<h2 style="color: #006699;">Manage Role</h2>
			</div>
	
	
	
	
		
		<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
           <div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" >
               <input type="button" value="Save" class="btn btn-primary" onClick="onSubmit()" />
               <input type="button" value="Add Row" class="btn btn-primary" onClick="onAddRow()" />
               <input type="button" value="Delete" class="btn btn-danger "  onClick="onDelete()" />
        	</div>
       	  	<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight">
          		<input  type="button" value="Close" class="btn btn-danger rightDefaultMargin alignRight" style="float: right; background-color: blue;" onClick="onCancel('../myHome.jsp?')" />
             </div>
        </div>
        
       <!--  <div class="box"> -->
        	
        	<!-- <header>
                  <div class="icons">
                    <i class="fa fa-table"></i>
                  </div>
                  <h5>Manage Role</h5>
                   <div class="toolbar">
                    <nav style="padding: 8px;">
                     <a href="javascript:;" class="btn btn-default btn-xs full-box">
                        <i class="fa fa-expand"></i>
                      </a> 
                     </nav>
                  </div>/.toolbar
            </header> -->
            
           <!--  <div id="collapse4" class="body" style="width: 1096; height: 1208"> -->
                  <aa:zone name="List" skipIfNotIncluded="true" > 
                   	<div class="body table-responsive" style="margin-bottom: 0px;">
                    <table id="dataTable" class="table table-hover">
                      <thead>
                        <tr>
                         <th style="padding-bottom: 0px;">  <!-- style="width:15px" align="center" --> 
                         <div align="center" class="">
                         	 <input type="checkbox" id="md_checkbox_29" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkMain%>" value="1" onclick="SelectAll('chkChild_',this,hdnRowCounter,hdnSelectedID)" />
                                <label for="md_checkbox_29"></label>
                          </div>
                         </th>
                          <th style=" padding-bottom: 15px;">Role Name</th>
                          <th style=" padding-bottom: 15px;">Role Type</th>
                          <th class="edit" style=" padding-bottom: 15px;">Assign Permissions</th>
                         
                           </tr>
                      </thead> 
            
                      <tbody >
                  <%
                  	for(Index = 1 ; Index <=intRowCounter ; Index++){
                  	    					if(!strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD)){
                  	    						if(null != arrList){
                  	    							objRoleBean = (RoleBean)arrList.get(Index-1);  
                  	    						}
                  	    						if(null == objRoleBean){
                  	    							objRoleBean = new RoleBean();
                  						}
                  	    					}else{
                  	    						objRoleBean = new RoleBean();
                  	    						objRoleBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + Index));
                  	    						objRoleBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtRole_ + Index));
                  	    						objRoleBean.setIntRoleType(WebUtil.parseInt(request, ILabelConstants.selRoleType_+ Index));
                  	    					}
                  %>
                      
                      
                        <tr id="tableRow" >
                         <td align="center" >
                         
                         <%if((objRoleBean.getIntRoleType() >= lngStaffType && objRoleBean.getIntRoleType() != IUMCConstants.ADMIN )|| objRoleBean.getIntRoleType()==0){ %>
                      		<input type="checkbox" id="md_checkbox_29<%= +Index %>" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkChild_+Index %>" value="<%=objRoleBean.getLngID() %>" onclick="CheckSelectSingle(this,chkMain,hdnRowCounter,hdnSelectedID)" />
                                <label for="md_checkbox_29<%= +Index %>"></label>
                      		<%-- <input type="checkbox" name="<%=ILabelConstants.chkChild_+Index %>" value="<%=objRoleBean.getLngID() %>" onclick="CheckSelectSingle(this,chkMain,hdnRowCounter,hdnSelectedID)"> --%>
                           <%} %>
                           	<input type="hidden" name="<%=ILabelConstants.hdnID_ + Index %>" value="<%=objRoleBean.getLngID() %>" SIZE="2" >   
                          </td>
                          <td> <input FieldType="mandatory" FieldName="Role Name"  class="validate[required] form-control" id="req" type="text"  name="<%= ILabelConstants.txtRole_+Index %>" style="background-color: #e2f5ff;" placeholder="Role Name.."  value="<%=objRoleBean.getStrName() %>"  <%if(objRoleBean.getIntRoleType() < lngStaffType && objRoleBean.getIntRoleType()!=0){ %>disabled<%}%>>
                          <span style="display:none;"><%out.print(objRoleBean.getStrName());%> </span>
                          </td>
                          <td>
                           <select style="width:130px"  class="validate[required] form-control chzn-select" FieldType="mandatory" FieldName="Role Type" name="<%=ILabelConstants.selRoleType_ + Index%>"  <%if(objRoleBean.getIntRoleType() < lngStaffType && objRoleBean.getIntRoleType() != 0){ %>disabled<%}%>>
                         <option selected value="0">--- Select ---</option>
			 			<%
			 			 	
			             	
		                  	if(null != arrRTBList){
		                  		RoleTypeBean objRTB = null; 
			                	for(int i=0;i<arrRTBList.size();i++){
			                			objRTB = (RoleTypeBean) arrRTBList.get(i);
			 			 %> 
		                    		<option <%if(objRTB.getLngID() < lngStaffType){ %>disabled<%}%>  <%if(objRoleBean.getIntRoleType() ==(int)objRTB.getLngID()) {%> selected <%}%> value="<%=objRTB.getLngID()%>" ><%= objRTB.getStrName() %></option>
		                   <% 	
		                    	}
		                 	} %>
                          </select>
                          </td>
                          <td>
                          <!-- <div class="edit"> -->
                          	<%-- <%if(objRoleBean.getLngID() > 0){%> --%>
                           <input  type="button" value="Assign Permissions" style="color: #333333;" class="btn btn-default btn-sm btn-line" 
                            <%if(objRoleBean.getIntRoleType() < lngStaffType && objRoleBean.getIntRoleType() != 0){ %>disabled   <%}%> onClick="openRolePermisions('<%=objRoleBean.getLngID()%>')" />
                           <%-- <input  type="button" value="DashBoard Permissions" class="btn btn-default btn-sm btn-line"  <%if(objRoleBean.getIntRoleType() < lngStaffType && objRoleBean.getIntRoleType() != 0){ %>disabled <%}%> onClick="openDashBoardRolePermisions('<%=objRoleBean.getLngID()%>')" /> --%>
                            	<%-- <%}%> --%>
                            <!-- </div> -->
                           </td>
                          
                           </tr>
               		  <%                         
	    				}
                      %> 
                      </tbody>
                       <jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>
                    </table>
                    </div>  <!-- table-responsive -->
            <%-- <input type="hidden" name="<%=ILabelConstants.selCompanyID %>" size="10" value="<%= lngCompanyId %>"> --%>        
     </aa:zone> 
      
    
  
    
    <div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%; margin-bottom: 25px;">
           <div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" >
               <input type="button" value="Save" class="btn btn-primary" onClick="onSubmit()" />
               <input type="button" value="Add Row" class="btn btn-primary" onClick="onAddRow()" />
               <input type="button" value="Delete" class="btn btn-danger "  onClick="onDelete()" />
        	</div>
       	  	<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight">
          		<input  type="button" value="Close" class="btn btn-danger rightDefaultMargin alignRight" style="float: right; background-color: blue;" onClick="onCancel('../myHome.jsp?')" />
             </div>
        </div>
			</div><!-- card -->
			
			
			
			</div><!-- col lg 12 -->
		</div><!-- #row clearfix -->
		</div> <!-- container-fluid -->

</section>


	<!--  <section class="content">
        <div class="container-fluid">
            <div class="row clearfix ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card" style="margin-bottom: 0px;">
						<div class="header" >
							<h2 style="color: #006699;">Manage Role</h2>
						</div>
						<div class="body" style="margin-bottom:20px; padding-bottom:0px">
						
							<div class="row clearfix" style=" height: 450px;  background-color:#e8f3e8;">
							
							<h3 style="text-align:center">Work in Progress..</h3>
								
							</div>
						</div>
						
						
            
            </div>
            </div>
		</div>
        </div>
    </section> -->

<%



%>




	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 

	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	 <script src="../js/pages/forms/basic-form-elements.js"></script>
	 
	<!-- Demo Js -->
	<!-- <script src="../js/demo.js"></script> -->
	
	<!--  multiple file upload  --> 
	
    <script src="../js/vendor/jquery.ui.widget.js"></script>
	<script src="../js/jquery.iframe-transport.js"></script>
	<script src="../js/jquery.fileupload.js"></script>
	
	<script>
     function afterAjaxReload(){
     	  Metis.MetisTable();
     	/* $('#dataTable').destroy(); */
           /* Metis.metisSortable(); */
           Metis.formGeneral();
           $('#<%=ILabelConstants.selActivityID %>_chosen').css({"color":"#000"});
     	}
     	
       $(function() {
           Metis.MetisTable();
          /*  $('#dataTable').destroy(); */
           Metis.formGeneral();
           $('#<%=ILabelConstants.selActivityID %>_chosen').css({"color":"#000"});
           $(".dateField").datepicker();
         });
       function openRolePermisions(ID){
     		strURL = "../UserManagement/UserRoleBinding.jsp?ID="+ID;
     		win = openPageInNewWin(strURL,"RoleBindingWin","scrollbars=yes, resizable=yes");
     		win.focus();
     	}
       function openDashBoardRolePermisions(ID){
     		strURL = "../UserManagement/UserDashBoardRoleBinding.jsp?ID="+ID;
     		win = openPageInNewWin(strURL,"RoleBindingWin","scrollbars=yes, resizable=yes");
     		win.focus();
     	}

    </script>
	
  
	</form>
</body>

</html>
