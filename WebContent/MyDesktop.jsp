<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><head>
    <meta charset="UTF-8">
     <LINK rel="stylesheet" href="Style/default.css" type="text/css">
    <title>School Asset Management :: My Desktop</title>
	<jsp:include page="MyIncludes/incPageHeader.jsp" flush="true">
	   <jsp:param value="" name="CONTEXT_PATH"/>
   </jsp:include>
	<link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	 
</head>

</head>
<%
String strDesktopImagePath="";


%>


<body class="sidebar-left-mini" style="background-image: url('<%=strDesktopImagePath%>');background-repeat: no-repeat;background-attachment: fixed;background-position: center;background-size:cover;">
 <form method="post">
<div class="" id="wrap">
 
	  <jsp:include page="MyIncludes/incLeftPanel.jsp" flush="true">
	  <jsp:param value="" name="CONTEXT_PATH"/>
   </jsp:include>
	      
	    
	  



</div> <!-- # div wrap  -->
 <jsp:include page="MyIncludes/incBottomPageReferences.jsp" flush="true">
 <jsp:param value="" name="CONTEXT_PATH"/>
   </jsp:include>
 
</form> 
 
</body>

</html>