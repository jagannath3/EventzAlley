<!DOCTYPE html>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.basis.bean.NavigationBean"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>

<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.mybos.frameWork.bean.ApplicationMenuBean"%>
<%@page import="java.util.ArrayList"%>
<html>

<head>
<meta charset="UTF-8">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Form Wizard | Bootstrap Based Admin Template - Material
	Design</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->
<link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="plugins/node-waves/waves.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="plugins/animate-css/animate.css" rel="stylesheet" />

<!-- Sweet Alert Css -->
<link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />

<!-- Custom Css -->
<link href="css/style.css" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="css/themes/all-themes.css" rel="stylesheet" />
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">

<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<LINK rel="stylesheet" href="Style/default.css" type="text/css">
<LINK rel="stylesheet" href="Style/defaultMenu.css" type="text/css">
<link rel="stylesheet" href="Style/jquery-ui.css">
<link rel="stylesheet" href="Style/jquery-ui.structure.css">
<link rel="stylesheet" href="Style/jquery-ui.theme.css">
<script type="text/javascript" src="JS/jquery.min.js"></script>
<script type="text/javascript" src="JS/ddaccordion.js"></script>
<script type="text/javascript" src="JS/leftmenu.js"></script>
<SCRIPT src="JS/jquery.min.js" type="text/javascript"></SCRIPT>
<SCRIPT src="JS/jquery-ui.min.js" type="text/javascript"></SCRIPT>
<script src="JS/chosen.jquery.js" type="text/javascript"></script>
<title>Issue Register :: Left Panel</title>
<base target="main">
</head>
<%
	String strContextPath = "";
	int intCurrMenu = 1;
	String strLogOutPath = "";
	String strActivityAction = "";
	strActivityAction = WebUtil.parseString(request, "activityTracker");
	List<ApplicationMenuBean> AMList = null;

	strContextPath = WebUtil.parseString(request, "CONTEXT_PATH");
	strLogOutPath = WebUtil.parseString(request, "LOGOUT_PATH");

	long lngRoleId = 0;
	long lngPrevMenuID = 0;
	String strAttributes = "";

	if (null != session.getAttribute("ROLE_ID")) {
		lngRoleId = ((Long) session.getAttribute("ROLE_ID")).longValue();
	}

	if (null != session.getAttribute(ISessionAttributes.LEFT_TREE_LIST)) {
		AMList = (List) session.getAttribute(ISessionAttributes.LEFT_TREE_LIST);
		System.out.println("LODED FROM SESSIOn");
	} else {
		/* AMList=(List<ApplicationMenuBean>) DBUtil.getRecords(IUMCConstants.BN_APPLICATION_MENU_BEAN, "lngRoleID="+lngRoleId, ""); */ //actual
		AMList = (List<ApplicationMenuBean>) DBUtil.getRecords(IUMCConstants.BN_APPLICATION_MENU_BEAN,
				"lngRoleID > 0", ""); //"lngRoleID="+lngRoleId
		session.setAttribute(ISessionAttributes.LEFT_TREE_LIST, AMList);
		System.out.println("LODED FROM DB");
	}

	ApplicationMenuBean amMenuBean = new ApplicationMenuBean();
%>
<body class="theme-red">
	<!-- Page Loader -->
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="preloader">
				<div class="spinner-layer pl-red">
					<div class="circle-clipper left">
						<div class="circle"></div>
					</div>
					<div class="circle-clipper right">
						<div class="circle"></div>
					</div>
				</div>
			</div>
			<p>Please wait...</p>
		</div>
	</div>
	<!-- #END# Page Loader -->
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #END# Overlay For Sidebars -->
	<!-- Search Bar -->
	<div class="search-bar">
		<div class="search-icon">
			<i class="material-icons">search</i>
		</div>
		<input type="text" placeholder="START TYPING...">
		<div class="close-search">
			<i class="material-icons">close</i>
		</div>
	</div>
	<!-- #END# Search Bar -->

	<section>
		<!-- Left Sidebar -->
		<aside id="leftsidebar" class="sidebar">
			<!-- User Info -->
			<div class="user-info">
				<div class="image">
					<img src="images/user.png" width="48" height="48" alt="User" />
				</div>
				<div class="info-container">
					<div class="name" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false">John Doe</div>
					<div class="email">john.doe@example.com</div>
					<div class="btn-group user-helper-dropdown">
						<i class="material-icons" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
						<ul class="dropdown-menu pull-right">
							<li><a href="javascript:void(0);"><i
									class="material-icons">person</i>Profile</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="javascript:void(0);"><i
									class="material-icons">group</i>Followers</a></li>
							<li><a href="javascript:void(0);"><i
									class="material-icons">shopping_cart</i>Sales</a></li>
							<li><a href="javascript:void(0);"><i
									class="material-icons">favorite</i>Likes</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="javascript:void(0);"><i
									class="material-icons">input</i>Sign Out</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- #User Info -->

			<!-- Menu -->
			
			  <div class="menu">
             
               <%
				if (null == AMList) {
					out.print("Error while building Menu. Contact Administrator");
				} else {
					
				%>	
				 <ul class="list">
               <div class="menu">
                   
                   
                 
                  <!-- need to iterated -->
                   
                   <%
                   String strPageURL = "";
					for (int i = 0; i < AMList.size(); i++) {
						amMenuBean = AMList.get(i);
						strPageURL = amMenuBean.getStrPageUrl() + strAttributes;
						if (strPageURL.indexOf("?") > 0) {
							strPageURL += "&ACTIVITY_ID=" + amMenuBean.getLngActivityID() + "&MENU_ID="
									+ amMenuBean.getLngMenuID() + "&IMG=" + amMenuBean.getStrImagePath();
						} else {
							strPageURL += "?ACTIVITY_ID=" + amMenuBean.getLngActivityID() + "&MENU_ID="
									+ amMenuBean.getLngMenuID() + "&IMG=" + amMenuBean.getStrImagePath();
						}
						if (amMenuBean.getLngMenuID() != lngPrevMenuID) {

                   
                   %>
                   
                 <script>
		 	      if(document.getElementById("SubMenu<%=lngPrevMenuID%>") != null) {
					document.write("</ul></li>");

				}
			     </script>
			     
                   
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span><%=amMenuBean.getStrMenuName()%></span>
                        </a>
                        <ul id="SubMenu<%=amMenuBean.getLngMenuID() %>" class="ml-menu">
                       <%
                       }
						%>    
                           
                            <li>
                                <a href="<%=strContextPath%><%=strPageURL%>"><span><%=amMenuBean.getStrActivityName()%></span></a>
                            </li>
                           
                      <%
								lngPrevMenuID = amMenuBean.getLngMenuID();
									}
								}
							%>
                        </ul>
                    </li>
                 
               <!-- need to iterated -->
                    
                   </div><!-- End div --> 
                </ul>
                </div><!-- End div -->
            
			<!-- #Menu -->
			<!-- Footer -->
			
			<!-- div class="legal">
                <div class="copyright">
                    &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div> -->
			<!-- #Footer -->
		</aside>
		<!-- #END# Left Sidebar -->

	</section>



	<!-- Jquery Core Js -->
	<script src="plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Js -->
	<script src="plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	<script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

	<!-- Jquery Validation Plugin Css -->
	<script src="plugins/jquery-validation/jquery.validate.js"></script>

	<!-- JQuery Steps Plugin Js -->
	<script src="plugins/jquery-steps/jquery.steps.js"></script>

	<!-- Sweet Alert Plugin Js -->
	<script src="plugins/sweetalert/sweetalert.min.js"></script>

	<!-- Waves Effect Plugin Js -->
	<script src="plugins/node-waves/waves.js"></script>

	<!-- Custom Js -->
	<script src="js/admin.js"></script>
	<script src="js/pages/forms/form-wizard.js"></script>

	<!-- Demo Js -->
	<script src="js/demo.js"></script>
</body>
</html>