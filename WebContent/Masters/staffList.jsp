<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.code.bean.StatusBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.MyBosGender"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.controller.UserMasterController"%>
<%@page import="com.uthkrushta.mybos.master.controller.EventListController"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />
  <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="../plugins/light-gallery/css/lightgallery.css" rel="stylesheet">

<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<script type = "text/javascript">
    
  
  
function currentpageUpdate(event){
	   var x = $("#<%=ILabelConstants.hdnTempCurrentPage %>").val();
	  
	   $("#<%=ILabelConstants.hdnCurrentPage %>").val(x);
	   
	   filterEntries(event);
	 
}

    </script>

</head>

<%

String strUserAction = "GET"; 
int intRowCounter = 1;
String strSelectedID = ";";
String strStatusText = "";
String strStatusStyle = "";


//Pagination
	int intCurrentPage = 1;
	long lngRecordCount = 0;
	long lngTotalPage = 1;
	int intRecordsPerPage = 10;
	boolean blnEnableNext = false;
	boolean blnEnablePrev = false; 

ArrayList<MessageBean> arrMsg = null; 
MessageBean objMsg;




//Pagination 
	int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
	intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
	intRecordsPerPage = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
	if(intRecordsPerPage <=0)
	   intRecordsPerPage = 10;
	if(intNumberOfRecords == 0){
	intNumberOfRecords = 10;
	}
	if(intCurrentPage == 0){
	intCurrentPage = 1;
	}
	intRowCounter=WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
	AAUtils.addZonesToRefresh(request, "Status,List,Footer"); 
		//Ends Pagination 



List lstGender = DBUtil.getRecords(ILabelConstants.CD_GENDER_BN, IUMCConstants.GET_ACTIVE_ROWS, "");


UserMasterController objGC = new UserMasterController();
UserMasterBean objUMB = new UserMasterBean();
MyBosGender objGen = new MyBosGender();
StatusBean objSB = new StatusBean();
intRowCounter=WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));	
String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS+" AND lngRoleID = "+IUMCConstants.STAFF_ROLE ;
List lstStaff = null;
List lstGen = null;




lstGen = (List) DBUtil.getRecords(ILabelConstants.CD_GENDER_BN, IUMCConstants.GET_ACTIVE_ROWS, "");



lstStaff = (List) objGC.getStaffList(request);


if(null!= lstStaff)
{
	intRowCounter = lstStaff.size(); 
}
AAUtils.addZonesToRefresh(request, "List");





if(lstStaff != null){
	intRowCounter = lstStaff.size();
}else{
	intRowCounter =0;
}

if (AAUtils.isAjaxRequest(request)) {
	strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
	intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));	
	objGC.doAction(request, response);
	
	lstStaff = (List) objGC.getStaffList(request);
	if(lstStaff != null){
		intRowCounter = lstStaff.size();
		
	}else{
		intRowCounter=0;
	}
	
	if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_GET))
	{
		lstStaff=objGC.getStaffList(request);
		objSB = (StatusBean)objGC.get(request);
		if(null!=lstStaff)
		{
			intRowCounter=lstStaff.size();
			
		}else{
			intRowCounter=0;
		}
		
	}
}


lngRecordCount =  objGC.getRecordsCount(request); 
if(lngRecordCount > 0 && intRecordsPerPage > 0){
	lngTotalPage = Math.round(lngRecordCount / intRecordsPerPage);
	
	if(lngRecordCount % intRecordsPerPage > 0){
lngTotalPage = lngTotalPage+1;
	}
}

if(intCurrentPage == lngTotalPage){
	blnEnableNext = false;
}else{
	blnEnableNext = true;
}
if(intCurrentPage ==1){
	blnEnablePrev = false;
}else {
	blnEnablePrev = true;
}





arrMsg = objGC.getArrMsg();		//Getting the messages to display
if(null!= arrMsg && arrMsg.size() > 0)
{
	objMsg = MessageBean.getLeadingMessage(arrMsg);
	if(objMsg != null)
	{
strStatusText = objMsg.getStrMsgText();
strStatusStyle = objMsg.getStrMsgType();
	}
}
AAUtils.addZonesToRefresh(request, "List");
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
<form method="post">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

<style type="text/css">
		.bar {
		    height: 18px;
		    background: green;
		}
	</style>

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	



	<aa:zone name="List" skipIfNotIncluded="true"> 
	<section class="content">
	
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
		<div class="container-fluid">
		
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" >
							<h2 >Staff List</h2>
						</div>
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							<div class="col-xs-8 col-md-8 col-sm-6 col-lg-6 floatleft" style="margin-top:4px">
								 <input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('staffMaster.jsp')"/> 
									 
							
							</div>
							<div class="col-xs-4 col-md-4 col-sm-6 col-lg-6 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;margin-left: 10px;"
									onClick="onCancel('../myHome.jsp')" />
								
								<input type="button" value="Delete"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right;  background-color: blue;" 
									onClick="onDelete()"/>
							</div>
						</div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                                       
                                            
                                            <div id="collapseOne_19" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_19">
                                               
                                                  
                                                    <div class="body table-responsive" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
                            <table class="table table-hover" >
                                <thead>
                                    <tr>
                                       <th style=" padding-bottom: 0px; border-bottom-width: 0px;">
                         	 <input type="checkbox" id="md_checkbox_29" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkMain%>" value="1" onclick="SelectAll('chkChild_',this,hdnRowCounter,hdnSelectedID)" />
                                <label for="md_checkbox_29" style=" margin-bottom: 0px;"></label>
                          </th>
                                        <th>Name</th>
                                        <th>DOB</th>
                                        <th>Gender</th>
                                        <th>Mobile No.</th>
                                        <th>Place</th> 
                                       <th></th>
                                       
                                       
                                       <!-- <img border="0" src="../Images/Filter.gif" width="16" height="16" ></th> -->
                                        
                                       	
                                        
                                    </tr>
                                   <tr id="tableFilter"  >
                                    	<td></td>
                                    	<td><input  type="text" name="<%=ILabelConstants.txtNameFilter%>" value="<%=WebUtil.processString(objSB.getStrName()) %>" onkeypress="filterEntries(event)" ></td>
                                        <td></td>
                                        <td>  <select name="<%=ILabelConstants.txtGenFilter%>" onchange="filterEntries(event)" >                                        
                                        	<option value=0>-- All --</option>
                                        	<%
												if (null != lstGender) {
													for (int i = 0; i < lstGender.size(); i++) {
														MyBosGender objGCB = (MyBosGender) lstGender.get(i);
											%>

											<option  value="<%=objGCB.getLngID()%>" <%if(objSB.getGenID() == objGCB.getLngID()){ %> selected <% }%>><%=objGCB.getStrName()%></option>

											<%
												}

												}
											%>
                                        
                                        </select></td>
                                        <td><input  type="text" name="<%=ILabelConstants.txtMobFilter%>" onkeypress="filterEntries(event)" value="<%=WebUtil.processString(objSB.getStrMob()) %>">
                                        
                                      
                                        
                                        
                                        </td>
                                        <td><input  type="text" name="<%=ILabelConstants.txtPlaceFilter%>" onkeypress="filterEntries(event)" value="<%=WebUtil.processString(objSB.getStrPlace())%>"></td> 
                                   		<td style="padding-bottom: 0px; padding-top: 0px;" class="clickable">
                                   		
                                   		<input type="button" value="Search"
									
									style="float: right; background-color: #1f91f3;"
									onClick="searchFilterEntries()" />
                                   		
                                   		
                                   		
                                   		
                                   		
                                   		
                                   		</td>
                                   </tr>
                                    
                                    
                                    
                                </thead>
                                <tbody>
                                <% 	if(null!=lstStaff){
               			 				for(int index =1; index<=lstStaff.size(); index++)
               			 				{
               			 				objUMB = (UserMasterBean) lstStaff.get(index-1);
               			 				
               			 				
               			 				%>
                                    <tr>
                                         <td style="padding-bottom: 1px;border-bottom-width: 1px;"><input type="checkbox" id="md_checkbox_29<%= +index %>" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkChild_+index %>" value="<%=objUMB.getLngID() %>" onclick="CheckSelectSingle(this,chkMain,hdnRowCounter,hdnSelectedID)" />
                                		<label style=" margin-bottom: 0px;" for="md_checkbox_29<%= +index %>"></label></td> 
                                        
                                       
                                       
                                        <td class="clickable" onClick="gotoThisPage('staffMaster.jsp?<%=ILabelConstants.PAGE_TYPE %>=Edit&<%=ILabelConstants.ID %>=<%=objUMB.getLngID() %>')">
                                         <input type="hidden"
									name="<%=ILabelConstants.hdnItemRowCounter%>" size="2"
									value="<%=intRowCounter%>"
									id="<%=ILabelConstants.hdnItemRowCounter%>">
                                        
                                        
                                        <%=WebUtil.processString(objUMB.getStrFirstName())%> <%=WebUtil.processString(objUMB.getStrLastName())%></td>
                                        <td><%=WebUtil.formatDate(objUMB.getDtDOB(), IUMCConstants.DATE_FORMAT)%></td>
                                        <td> <% if(lstGen != null){ 
                                        	for(int i= 0; i<lstGen.size(); i++){
                                        		objGen = (MyBosGender)lstGen.get(i);
                                        		if(objGen.getLngID() == objUMB.getLngGenderID() ){ %>
                                        	<%=objGen.getStrName() %>
                                        <%} }}%> </td>
                                        <td><%=WebUtil.processString(objUMB.getStrPrimaryContactNumber()) %></td>
                                        <td><%=WebUtil.processString(objUMB.getStrCity()) %></td>
                                       <td></td>
                                       
                                       
                                    </tr>
                                     	<% }}
                                     	else{%>
                                     	
                                     	<td style="border-bottom-width:0px; font-size:16px; align:center">No Records</td>
                                     	
                                     	
                                     	
                                     	<%}%>
                                    
                                    
                                    
                                    <table align="center" id="tableFooter" class="defaultRoundedBoxBottom" width="100%" style="border-collapse: collapse; background-color:#ffffff;" cellpadding="0" cellspacing="0">
							        
							        
							        
							        <tr>
							          <td width="40%" align="left" class="footerText">
							          
							          <div class="col-xs-3" style=" width: 55px;margin-top: 10px;padding-right: 0px;"> Page :</div> 
							          <div class="col-xs-3" style="padding-right: 0px;width: 85px;">
							          
							          
							          
							          
							          
							           <input type="text" class="form-control"  style="text-align:center;width: 86px;"
							           name="<%=ILabelConstants.hdnTempCurrentPage%>" id="<%=ILabelConstants.hdnTempCurrentPage%>" value="<%=intCurrentPage%>" 
							           SIZE="2" DataType ="positivenumericinteger"  onkeypress="currentpageUpdate(event)"></div> <!-- filterEntries(event) -->
							            <div class="col-xs-5" style="margin-top: 10px;padding-right: 0px;padding-left: 20px;"> of <%=lngTotalPage%></div>
							            
							            
							            
							            </td>
							          
							           <td width="25%">
							          	<div class="col-xs-5"> Records per Page :</div> <div class="col-xs-3"><input type="text" class="form-control"  style="text-align:center;width:50px;" name="<%=ILabelConstants.txtRecordsPerPage%>" value="<%=intRecordsPerPage%>" SIZE="2" DataType ="positivenumericinteger"  onkeypress="filterEntries(event)"></div>
							          </td>  
							          
							          <td width="43%" align="center">
							         	 <input type="button" value=" Prev " class="btn btn-primary" <%if(!blnEnablePrev){%> style="display:none;" <%}%> onClick="onPrev('<%=ILabelConstants.hdnCurrentPage%>','<%=ILabelConstants.hdnPageEdited%>')" name="btnPrev" >
							         	 <input type="button" value=" Next " class="btn btn-primary" <%if(!blnEnableNext){%>style="display:none;"<%}%> name="btnNext" onClick="onNext('<%=ILabelConstants.hdnCurrentPage%>','<%=ILabelConstants.hdnPageEdited%>')">
							          </td>
							          <td width="18%" align="right" >&nbsp;Total Records : <%=lngRecordCount%></td>
							        </tr>
							      </table>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </tbody>
                           <jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
                           <jsp:param name="CURRENT_PAGE" value="<%=intCurrentPage%>" />
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>
                           
                            </table>
                           
                        </div>
                                             
                                            </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none;height: 50px;margin-right: 0px;margin-left: 0px;width: 100%;margin-bottom: 25px;">
							<div class="col-xs-8 col-md-8 col-sm-6 col-lg-6 floatleft" style="margin-top:4px">
								 <input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('staffMaster.jsp')"/> 
									 
							
							</div>
							<div class="col-xs-4 col-md-4 col-sm-6 col-lg-6 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;margin-left: 10px;"
									onClick="onCancel('../myHome.jsp')" />
									
								<input type="button" value="Delete"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right;  background-color: blue;" 
									onClick="onDelete()" />
									
								
									
							</div>
						</div>
                    </div>
                </div>
            </div>
		
		
			
		</div>
			
			
	</section>
 
 </aa:zone>  

	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 

	 <!-- Light Gallery Plugin Js -->
    <script src="../plugins/light-gallery/js/lightgallery-all.js"></script>

    <!-- Custom Js -->
    <script src="../js/pages/medias/image-gallery.js"></script>


	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	<!-- <script src="../js/demo.js"></script> -->
	
	
	
	 <script>
 	
	
	 
    </script>
	
  
	</form>
</body>

</html>
