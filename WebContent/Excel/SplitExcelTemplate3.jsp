<%@page import="com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
long lngEventTypeID=WebUtil.parseLong(request,"TYPEID");
List lstEventTypeSplit = null;
EventTypeLapMasterBean objETLMB = new EventTypeLapMasterBean();
EventTypeMasterBean objETMB = (EventTypeMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_TYPE_MASTER,
		IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+lngEventTypeID, "");
if(objETMB == null){
	objETMB = new EventTypeMasterBean();
}
else{
	lstEventTypeSplit = (List)DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_LAP, 
			IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+objETMB.getLngID(), "lngLapOrder");
}
response.setContentType("application/vnd.ms-excel"); 
response.setHeader("Content-Disposition", "attachment; filename=\""+objETMB.getStrName()+".csv\""); 
%>
Rank,BIB,CHIP,GUN,PACE,<%if(lstEventTypeSplit != null){
	for(int i = 0; i<lstEventTypeSplit.size(); i++){
		objETLMB = (EventTypeLapMasterBean) lstEventTypeSplit.get(i);
		if(i==lstEventTypeSplit.size()-1){out.print(objETLMB.getStrName());}else{out.print(objETLMB.getStrName()+",");}	}} %>

