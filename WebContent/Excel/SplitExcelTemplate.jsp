<%@page import="com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%


long lngEventTypeID=WebUtil.parseLong(request,"TYPEID");

List lstEventTypeSplit = null;
EventTypeLapMasterBean objETLMB = new EventTypeLapMasterBean();



EventTypeMasterBean objETMB = (EventTypeMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_TYPE_MASTER,
		IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+lngEventTypeID, "");
if(objETMB == null){
	objETMB = new EventTypeMasterBean();
}
else{
	lstEventTypeSplit = (List)DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_LAP, 
			IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+objETMB.getLngID(), "lngLapOrder");
}










response.setContentType("application/vnd.ms-excel"); 
response.setHeader("Content-Disposition", "attachment; filename=\""+objETMB.getStrName()+".xls\""); 

%>


<body>

<table align="center" border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="98%" id="AutoNumber1">
  <tr>

<th align="center">Rank</th>
<th align="center">BIB</th>
<th align="center">CHIP</th>
<th align="center">GUN</th>
<th align="center">PACE</th>
<%if(lstEventTypeSplit != null){
	for(int i = 0; i<lstEventTypeSplit.size(); i++){
		objETLMB = (EventTypeLapMasterBean) lstEventTypeSplit.get(i);
	%>
	
<th align="center"><%=objETLMB.getStrName()%></th>
	
	<% }} %>



</tr>
</table>


</body>
</html>