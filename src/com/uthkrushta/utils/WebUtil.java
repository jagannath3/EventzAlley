package com.uthkrushta.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.hibernate.Query;
import org.hibernate.Session;

import com.jspsmart.upload.File;
import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.constants.IUMCConstants;


public class WebUtil {

	public static String formatDate(Date dtDate, String strDateFormat) {
		SimpleDateFormat objSimDateFormat = new SimpleDateFormat(strDateFormat);
		if (dtDate == null || dtDate.toString().trim().length() == 0) {
			return "";
		} else {
			return objSimDateFormat.format(dtDate);
		}

	}

	public static Date parseDate(HttpServletRequest request, String strLabel, String strDateFormat) {
		String strValue = request.getParameter(strLabel);
		try {
			if (null == strValue) {
				return new Date();
			}else if(strValue.trim().length() == 0){
				return null;
			}
			SimpleDateFormat objSimDateFormat = new SimpleDateFormat(strDateFormat);
			return objSimDateFormat.parse(strValue);

		} catch (ParseException EX) {
			EX.printStackTrace();
		}
		return new Date();
	}
	
	
	
	public static String processString(String strMyInputString) {
		String strMyOutputString = "";
		String strEscapedString = "";

		if (strMyInputString == null) {
			return "";
		}
		char myCharArray[] = strMyInputString.toCharArray();
		for (int i = 0; i < myCharArray.length; i++) {
			switch (myCharArray[i]) {
			case 37: // '%'
				strEscapedString = "&#37;";
				break;

			case 60: // '<'
				strEscapedString = "&#60;";
				break;

			case 62: // '>'
				strEscapedString = "&#62;";
				break;

			case 34: // '"'
				strEscapedString = "&#34;";
				break;

			case 39: // '\''
				strEscapedString = "&#39;";
				break;

			case 92: // '\\'
				strEscapedString = "&#47;";
				break;

			default:
				strEscapedString = (new StringBuilder()).append("").append(myCharArray[i]).toString();
				break;
			}
			strMyOutputString = (new StringBuilder()).append(strMyOutputString).append(strEscapedString).toString();
		}

		System.out.println(strMyOutputString);
		return strMyOutputString;
	}

	

	public static int parseInt(HttpServletRequest request, String strLabel) {
		String strValue = request.getParameter(strLabel);
		if (strValue == null || strValue.trim().length() == 0) {
			return 0;
		} else {
			return Integer.parseInt(strValue);
		}
	}

	public static boolean parseBoolean(HttpServletRequest request, String strLabel) {
		String strValue = request.getParameter(strLabel);
		if (isStringOK(strValue)) {
			return Boolean.parseBoolean(strValue);
		} else {
			return false;
		}
	}
	
	public static int parseInt(String strText) {
		if (strText == null || strText.trim().length() == 0) {
			return 0;
		} else {
			return Integer.parseInt(strText);
		}
	}
	public static long parseLong(String strText) {
		if (strText == null || strText.trim().length() == 0) {
			return 0;
		} else {
			return Long.parseLong(strText);
		}
	}

	public static Long parseLong(HttpServletRequest request, String strLabel) {
		String strText = request.getParameter(strLabel);
		if (strText == null || strText.trim().length() == 0) {
			return Long.parseLong("0");
		} else {
			return Long.parseLong(strText);
		}
	}

	public static Double parseDouble(HttpServletRequest request, String strLabel) {
		String strValue = request.getParameter(strLabel);
		if (strValue == null || strValue.trim().length() == 0) {
			return 0.0;
		} else {
			return Double.parseDouble(strValue);
		}
	}

	public static Float parseFloat(HttpServletRequest request, String strLabel) {
		String strValue = request.getParameter(strLabel);
		if (strValue == null || strValue.trim().length() == 0) {
			return Float.parseFloat("0");
		} else {
			return Float.parseFloat(strValue);
		}
	}

	public static boolean isStringOK(String strString) {
		if (null != strString && (strString.trim().length() > 0 || strString.equals(""))) {
			return true;
		} else if (null == strString) {
			return false;
		} else {
			return false;
		}

	}

	public static String parseString(HttpServletRequest request, String strLabel) {
		String strValue = request.getParameter(strLabel);
		if (isStringOK(strValue)) {
			return strValue.trim();
		} else {
			return "";
		}
	}
	
	
	public static String processStringForDB(String strInput) {
		String strOutput = strInput;
		String strDelimeter = "!";
		if (isStringOK(strInput)) {
			if(strInput.contains("\'")){
				strOutput = strInput.substring(0, strInput.indexOf("\'")) + strDelimeter+ strInput.substring(strInput.indexOf("\'")) ; 
			}
			System.out.print(strOutput);
			return strOutput;
		} else {
			return "";
		}
	}

	
	
	@SuppressWarnings("rawtypes")
	public static boolean checkIsUnique(String strBeanName, String strAliasName, ArrayList<NameValueBean> arrNameValueList) {
		String strSQL = "";
		NameValueBean objNameValue = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		strSQL = " From " + strBeanName + " " + strAliasName + " WHERE ";

		for (int i = 0; i < arrNameValueList.size(); i++) {
			objNameValue = arrNameValueList.get(i);
			if (objNameValue.isBlnKey() && WebUtil.parseInt(objNameValue.getStrBeanFieldValue()) > 0) {
				strSQL += strAliasName + "." + objNameValue.getStrBeanFieldName() + " != " + objNameValue.getStrBeanFieldValue() + " AND ";
			} else if (!objNameValue.isBlnKey()) {
				strSQL += strAliasName + "." + objNameValue.getStrBeanFieldName() + " = " + objNameValue.getStrBeanFieldValue() + " AND ";
			}
		}
		if (strSQL.contains("AND")) {
			strSQL = strSQL.substring(0, strSQL.lastIndexOf("AND"));
		} else {
			strSQL = strSQL.substring(0, strSQL.lastIndexOf("WHERE"));
		}
		strSQL += " AND intStatus = " + IUMCConstants.STATUS_ACTIVE;
		Query query = session.createQuery(strSQL);
		List queryResult = query.list();
		if (queryResult != null && queryResult.size() == 0) {
			return true;
		}
		return false;
	}

	public static void main(String s[]) {

		/*ArrayList<NameValueBean> arrTest = new ArrayList<NameValueBean>();
		NameValueBean objNV = null;
		objNV = new NameValueBean("ID", "1", true);
		arrTest.add(objNV);
		objNV = new NameValueBean("Name", "Test", false);
		arrTest.add(objNV);
		WebUtil.checkIsUnique("test1", "TS", arrTest);*/
		String strInput = "san's";
		processStringForDB(strInput);
		
	}

	public static String removePKID(String QueryString) {

		if (QueryString == null)
			return "";

		String[] strArray = QueryString.split("&");
		String finalString = "";
		for (String objText : strArray) {
			if (objText.indexOf("StudentID") == -1) {
				finalString = finalString + "&" + objText;
			}
		}
		return finalString;
	}

	public static String buildSearchString(String strSearchString) {
		int intIndex = -1;
		if (null != strSearchString && strSearchString.trim().length() > 0) {
			while (strSearchString.indexOf("*") >= 0) {
				intIndex = strSearchString.indexOf("*");
				strSearchString = strSearchString.substring(0, intIndex) + "%" + strSearchString.substring(intIndex + 1);
			}
		}
		return strSearchString;
	}

	public static String getReableStatus(int intStatus) {
		switch (intStatus) {
		case IUMCConstants.STATUS_ACTIVE:
			return "Active";
		case IUMCConstants.STATUS_INACTIVE:
			return "InActive";
		default:
			return "";
		}
	}
	
	public static String uploadFile(HttpServletRequest request, PageContext pagecontext){
		String strUploadedFile = "COMING";
		SmartUpload mySmartUpload = new SmartUpload();
		File myFile = null;
		
		try {
			mySmartUpload.initialize(pagecontext);
			mySmartUpload.upload();
		} catch ( ServletException e){
		
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch( SmartUploadException e){
			e.printStackTrace();
		}catch ( IOException e) {
			e.printStackTrace();
		}
		myFile = mySmartUpload.getFiles().getFile(0);
		strUploadedFile = myFile.getFileName();
		return strUploadedFile;
		
	}
	
	
	
}