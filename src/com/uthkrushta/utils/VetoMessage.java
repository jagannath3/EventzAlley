package com.uthkrushta.utils;

public class VetoMessage {
	
	private String strids;
	private boolean blnIsdel;
	private String strUnselIds;
	
	public boolean isBlnIsdel() {
		return blnIsdel;
	}
	public void setBlnIsdel(boolean blnIsdel) {
		this.blnIsdel = blnIsdel;
	}
	public String getStrids() {
		return strids;
	}
	public void setStrids(String strids) {
		this.strids = strids;
	}
	public String getStrUnselIds() {
		return strUnselIds;
	}
	public void setStrUnselIds(String strUnselIds) {
		this.strUnselIds = strUnselIds;
	}
	
}
