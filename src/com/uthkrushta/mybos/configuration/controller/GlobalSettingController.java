package com.uthkrushta.mybos.configuration.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.ibm.icu.text.SimpleDateFormat;
import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.configuration.bean.GlobalSettingBean;
import com.uthkrushta.mybos.configuration.bean.RewardPointsConfigurationBean;
import com.uthkrushta.mybos.configuration.bean.TransactionSettingBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.WebUtil;

public class GlobalSettingController extends GenericController {

	GlobalSettingBean objGSB  = new GlobalSettingBean();
	TransactionSettingBean objTSB = new TransactionSettingBean();
	RewardPointsConfigurationBean objRPCB=new RewardPointsConfigurationBean();
	
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		String strUserAction=WebUtil.parseString(request, ILabelConstants.hdnUserAction);
		if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
		{
			/*modifyRecord(request);
			modifyRecords(request);
			modifyRewardPoints(request);*/
		}
		
	}

	

	/*@Override*/
	/*protected boolean modifyRecord(HttpServletRequest request) {
		boolean blnModified=false;
		objGSB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID));
		objGSB.setLngFinancialYear(WebUtil.parseLong(request, ILabelConstants.selFinancialyear));
		objGSB.setLngTimeZone(WebUtil.parseLong(request, ILabelConstants.selTimeZone));
		objGSB.setLngLanguage(WebUtil.parseLong(request, ILabelConstants.selLanguage));
		objGSB.setLngCurrency(WebUtil.parseLong(request, ILabelConstants.selCurrency));
		objGSB.setLngFreezeLimit(WebUtil.parseLong(request, ILabelConstants.txtFreezeNo));
		objGSB.setLngExtionLimit(WebUtil.parseLong(request, ILabelConstants.txtExtensionNo));
		objGSB.setLngCompanyID(StaffMasterController.getCompanyID(request));
		objGSB.setIntIsDiffComp(WebUtil.parseInt(request, ILabelConstants.chkDiffComp));
		objGSB.setIntIsStrictStock(WebUtil.parseInt(request, ILabelConstants.chkStock));
		objGSB.setIntIsInclusive(WebUtil.parseInt(request, ILabelConstants.chkTaxInclusive));
		objGSB.setLngWeightID(WebUtil.parseLong(request, ILabelConstants.selUomForWeight));
		objGSB.setLngHeightID(WebUtil.parseLong(request, ILabelConstants.selUomForHeight));
		objGSB.setStrPhotoPath(WebUtil.parseString(request, ILabelConstants.hdnMemberLogo));
		//Added on 23DEC
		objGSB.setIntIsEmail(WebUtil.parseInt(request, ILabelConstants.chkEmailNotification));
		objGSB.setIntIsSMS(WebUtil.parseInt(request, ILabelConstants.chkSMSNotification));
		
		//Added on MARCH25
		objGSB.setIntContinuousAbsent(WebUtil.parseInt(request, ILabelConstants.txtContinuousAbsentDays));
		objGSB.setIntNoOfAlertSMS(WebUtil.parseInt(request, ILabelConstants.txtNoOfAlertSMS));
		objGSB.setIntSMSIntervalDays(WebUtil.parseInt(request, ILabelConstants.txtSMSIntervalDays));
		
		//Added for auto-logout
		objGSB.setIntAutoLogout(WebUtil.parseInt(request, ILabelConstants.chkAutoLogout));
		objGSB.setIntAssessmentMaxDays(WebUtil.parseInt(request, ILabelConstants.txtAssessmentMaximumDays));
		objGSB.setIntExport(WebUtil.parseInt(request, ILabelConstants.chkExport));
		
		//added for feedback frequency 
		objGSB.setLngFeedbackFrequencyDays(WebUtil.parseLong(request, ILabelConstants.txtFeedbackFrequency));
		
		//GST Changes
		objGSB.setIntGSTEnabled(WebUtil.parseInt(request, ILabelConstants.chkGSTEnabled));
		objGSB.setDtGSTEnabledDate(WebUtil.parseDate(request, ILabelConstants.txtGSTDate, IUMCConstants.DATE_FORMAT)); 
		objGSB.setDblCGSTDistributionPercent(WebUtil.parseDouble(request, ILabelConstants.hdnCGSTDistributionPercent));
		objGSB.setDblSGSTDistributionPercent(WebUtil.parseDouble(request, ILabelConstants.hdnSGSTDistributionPercent));
		
		
		//New Changes
		objGSB.setIntAutoSendMailAfterSave(WebUtil.parseInt(request, ILabelConstants.chkAutoSendEmailAftrSave));
		objGSB.setIntEnqRefferedByMandatory(WebUtil.parseInt(request, ILabelConstants.chkEnqRefMandatory));
		objGSB.setIntConsiderPaymentForIncentive(WebUtil.parseInt(request, ILabelConstants.chkConsiderPayForIncent));
		
		if(objGSB.getLngID() > 0)
		{
			objGSB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));
			objGSB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy));
		}
		else
		{
			objGSB.setDtCreatedOn(WebUtil.getCurrentDate(request));//WebUtil.getCurrentDate(request)
			objGSB.setLngCreatedBy(StaffMasterController.getLoginID(request));
		}
		objGSB.setDtUpdatedOn(WebUtil.getCurrentDate(request));//WebUtil.getCurrentDate(request)
		objGSB.setLngUpdatedBy(StaffMasterController.getLoginID(request));
		objGSB.setLngFinancialYearID(StaffMasterController.getFinancialYearID(request));
		objGSB.setIntStatus(IUMCConstants.STATUS_ACTIVE);
		
		//Added for May28 Reward points
				objGSB.setIntIsRewards(WebUtil.parseInt(request,ILabelConstants.chkRewards ));
				objGSB.setDblRedeemedAmtPoints(WebUtil.parseDouble(request, ILabelConstants.txtRedemedAmtPoints));
				objGSB.setDblRedeemedAmt(WebUtil.parseDouble(request, ILabelConstants.txtRedemedAmt));
				objGSB.setDblRedeemedDaysPoints(WebUtil.parseDouble(request, ILabelConstants.txtRedemedDaysPoints));
				objGSB.setLngRedeemedDays(WebUtil.parseLong(request, ILabelConstants.txtRedemedDays));
				objGSB.setDblMinimalRedeemedPoints(WebUtil.parseDouble(request, ILabelConstants.txtMinRedemedPoints)); 
				objGSB.setIntRedemptionType(WebUtil.parseInt(request, ILabelConstants.selRedemptionType));
				
	  //Added for print 27Jun16
				 objGSB.setIntPrintType(WebUtil.parseInt(request, ILabelConstants.selPrintType));
				 
	  //Added for selecting week start days
				 objGSB.setLngWeekDayStart(WebUtil.parseLong(request, ILabelConstants.selWeekDay));
 		
	  //Added for duration per session
				  objGSB.setLngDurationPerSession(WebUtil.parseLong(request, ILabelConstants.txtDurationPerSession));
				  
				  objGSB.setIntAutoSendSMSAfterSave(WebUtil.parseInt(request, ILabelConstants.chkAutoSendSMSAfterSave));
				  objGSB.setIntMemProfPicMand(WebUtil.parseInt(request, ILabelConstants.chkMemPicMandatory));
				  objGSB.setIntStaffProfPicMand(WebUtil.parseInt(request, ILabelConstants.chkStaffPicMandatory));

				  objGSB.setIntPDFAutoGenerate(WebUtil.parseInt(request, ILabelConstants.chkPDFAutoGenerate));
				  
				  objGSB.setIntExpiredMemSMSDay(WebUtil.parseInt(request, ILabelConstants.txtPlanExpirySendSMSDays));
				  objGSB.setIntRestrictHolidaySignIn(WebUtil.parseInt(request, ILabelConstants.chkRestrictHolidaySignin));
		          objGSB.setIntPlanExpiryNotificationDays(WebUtil.parseInt(request, ILabelConstants.txtPlanExpiryNotificationDays));
		          
		          //Enhancement
		          objGSB.setDblMinBalanceRemainderAmt(WebUtil.parseDouble(request, ILabelConstants.txtMinimumBalanceRemainder));
		try {
			blnModified=new DBUtil(request).modifyRecord(objGSB);
			
			if(blnModified)
			{
				List lstCurrency = (List) new DBUtil(request).getRecords(IUMCConstants.BN_CURRENCY,IUMCConstants.GET_ACTIVE_ROWS ,"");
		         String strCurrencySymbol="";
		         if(null != lstCurrency)
		         {
		      	   for(int i = 0; i<lstCurrency.size(); i++)
		      	   {
		      		   CurrencyConfigurationBean  objCCB = (CurrencyConfigurationBean)lstCurrency.get(i);
		      		        if(null!=objCCB && objCCB.getLngID()==objGSB.getLngCurrency())
		      		        {
		      		        	if(null!=objCCB.getStrSymbol())
		      		        	{
		      		        		strCurrencySymbol=objCCB.getStrSymbol();
		      		        	}
		      		        	else if(null!=objCCB.getStrCurrency())
		      		        	{
		      		        		strCurrencySymbol=objCCB.getStrCurrency();
		      		        	}
		      		        	
		      		        }
		      	   }
		         }
				
		        request.getSession().setAttribute(ISessionAttributes.CURRENCY_SYMBOL, strCurrencySymbol);
		        
		        List lstTimeZone=(List)new DBUtil(request).getRecords(IUMCConstants.BN_TIME_ZONE, IUMCConstants.GET_ACTIVE_ROWS, "");
		         
		         String strTimeGMT="";
		         if(null!=lstTimeZone)
		         {
		        	 
		        	 for(int i=0;i<lstTimeZone.size();i++)
		        	 {
		        	    TimeZoneConfigurationBean objTZCB=(TimeZoneConfigurationBean)lstTimeZone.get(i);
		        	    if(null!=objTZCB && objTZCB.getLngID()==objGSB.getLngTimeZone())
		        	    {
		        	    	if(null!=objTZCB.getStrTimeZone())
		        	    	{
		        	    		strTimeGMT=objTZCB.getStrTimeZone();
		        	    	}
		        	    }
		        	 }
		         }
		         request.getSession().setAttribute(ISessionAttributes.CURRENT_TIME_ZONE, strTimeGMT);
				
		         request.getSession().setAttribute(ISessionAttributes.WEEK_START_DAY, (int)objGSB.getLngWeekDayStart());
		         
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefreh(request, "Status,ID");
				return true;
			}
			else
			{
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefreh(request, "Status");
				return false;
			}
		} catch (UMBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			msg=new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			return false;
		}

	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		List lstTransSetting=new DBUtil(request).getRecords(IUMCConstants.BN_TRANSACTION_SETTING, IUMCConstants.GET_ACTIVE_ROWS, "");
		ArrayList arrLstTrans=new ArrayList();
		boolean blnModifed=false;
		for(int i=1;i<=lstTransSetting.size();i++)
		{
			objTSB=new TransactionSettingBean();
			objTSB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_+i));
			objTSB.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_+i));
			objTSB.setStrPrefix(WebUtil.parseString(request, ILabelConstants.txtPrefix_+i));
			objTSB.setLngStartNumber(WebUtil.parseLong(request, ILabelConstants.txtStartNumber_+i));
			objTSB.setIntAutoNo(WebUtil.parseInt(request, ILabelConstants.chkAutoNo_+i));
			objTSB.setIntAutoNoReset(WebUtil.parseInt(request, ILabelConstants.chkResetAutoNo_+i));
            objTSB.setIntStatus(IUMCConstants.STATUS_ACTIVE);
		    arrLstTrans.add(objTSB);
		    
		}
		
		try {
			blnModifed=new DBUtil(request).modifyRecords(arrLstTrans);
			if(blnModifed)
			{
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefreh(request, "List");
				return false;
			}
			else
			{
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefreh(request, "Status");
				return false;
			}
		} catch (UMBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			msg=new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefreh(request, "Status");
			return false;
		}
		
	
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	private void modifyRewardPoints(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean blnModified=false;
		try
		{
		objRPCB=new RewardPointsConfigurationBean();
		objRPCB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnRewardID));
		objRPCB.setDblReferalPoints(WebUtil.parseDouble(request, ILabelConstants.txtReferalRewardPoints));
		objRPCB.setDblAnivBDayPoints(WebUtil.parseDouble(request, ILabelConstants.txtBARewardPoints));
		objRPCB.setDblAttendancePoints(WebUtil.parseDouble(request, ILabelConstants.txtAttendanceRewardPoints));
		objRPCB.setLngPresentDays(WebUtil.parseLong(request, ILabelConstants.txtAttendanceRewardDays));
		objRPCB.setDblSalesPoints(WebUtil.parseDouble(request, ILabelConstants.txtSalesRewardPoints));
		objRPCB.setDblSalesAmount(WebUtil.parseDouble(request, ILabelConstants.txtSalesRewardAmount));
		
		//Added for check box;
		objRPCB.setIntPerReferal(WebUtil.parseInt(request, ILabelConstants.chkPerReferal));
		objRPCB.setIntBA(WebUtil.parseInt(request, ILabelConstants.chkBA));
		objRPCB.setIntAttendance(WebUtil.parseInt(request, ILabelConstants.chkAttend));
		objRPCB.setIntSales(WebUtil.parseInt(request, ILabelConstants.chkSales));
		//End for check box;
		
		objRPCB.setIntStatus(IUMCConstants.STATUS_ACTIVE);
		blnModified=new DBUtil(request).modifyRecord(objRPCB);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	//check whether to enable reward point
	public boolean checkEnableRewardEntity(HttpServletRequest request) {
		long lngNumberOfActiveMembers = 0;
		EntityPlanMasterView objEntity =  StaffMasterController.getEntityPlan(request);
		if(null!=objEntity && objEntity.getIntEnableRewardSystem()==1)
		{
			return true;
		}
		else
		{
		return false;
		}
		
	}
	
	public boolean isGSTEnabled(HttpServletRequest request){
		boolean blnEnabled=false;
		GlobalSettingBean objGSB=(GlobalSettingBean)new DBUtil(request).getRecord(IUMCConstants.BN_GLOBAL_SETTING, IUMCConstants.GET_ACTIVE_ROWS, "");
		Date dtCurrentDate=new Date();
		String strCurrentDate=WebUtil.formatDate(dtCurrentDate, IUMCConstants.DATE_FORMAT);
		SimpleDateFormat sdf=new SimpleDateFormat(IUMCConstants.DATE_FORMAT);
		
		//
		
				try {
					dtCurrentDate=sdf.parse(strCurrentDate);
					if(null!=objGSB && objGSB.getIntGSTEnabled()==1 && null!=objGSB.getDtGSTEnabledDate()){
						String strGSTDate=WebUtil.formatDate(objGSB.getDtGSTEnabledDate(), IUMCConstants.DATE_FORMAT);
						Date dtGSTEnDate=sdf.parse(strGSTDate);
						System.out.println(dtCurrentDate.compareTo(objGSB.getDtGSTEnabledDate()));
						if(dtCurrentDate.compareTo(dtGSTEnDate) >= 0)
						{
						  blnEnabled=true;
						  
						}
					}else{
						blnEnabled=false;
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				
				
			
			
			return blnEnabled;
		
	}
*/


	@Override
	protected boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
