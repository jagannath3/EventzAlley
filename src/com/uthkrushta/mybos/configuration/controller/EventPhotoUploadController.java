package com.uthkrushta.mybos.configuration.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;

public class EventPhotoUploadController extends GenericController {
	EventPhotoUploadBean objEPUB = new EventPhotoUploadBean();
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);//Getting User Action
		if (null != strUserAction) {
				if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
				{//Save objects to database
					modifyRecords(request);
				}
				
			}
		
	}

	@Override
	public boolean modifyRecord(HttpServletRequest request) {
		long EventID = Long.parseLong((String) request.getSession().getAttribute(ISessionAttributes.EventPhotoID));
		String strImagePath =  (String) request.getSession().getAttribute(ISessionAttributes.PHOTO_UPLOAD_PATH);
		// TODO Auto-generated method stub
		System.out.println("strImagePath "+strImagePath);
		/*String strImagePath = fileImagePath.getPath();*/
		
		
		boolean modified = false;
		
		objEPUB = new EventPhotoUploadBean();
		objEPUB.setLngEventID(EventID);
		objEPUB.setStrBibNoEntry("");
		objEPUB.setStrImageDescription("");
		objEPUB.setStrImagePath(strImagePath);
		objEPUB.setIntStatus(2);
		try {
			modified = DBUtil.modifyRecord(objEPUB);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return modified;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String EventPhotoPath = (String) request.getSession().getAttribute(ISessionAttributes.PHOTO_UPLOAD_PATH);
		
		boolean blnModified = false;
		
		
		return false;
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
}
