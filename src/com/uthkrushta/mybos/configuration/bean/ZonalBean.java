package com.uthkrushta.mybos.configuration.bean;
public class ZonalBean {

	private long lngID;
	private String strZonalName;
	private long lngCityID;
	private String strAreas;
	private int intStatus;
	
	public ZonalBean(){
		this.strAreas = strZonalName = "";
	}
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrZonalName() {
		return strZonalName;
	}
	public void setStrZonalName(String strZonalName) {
		this.strZonalName = strZonalName;
	}
	public long getLngCityID() {
		return lngCityID;
	}
	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}
	public String getStrAreas() {
		return strAreas;
	}
	public void setStrAreas(String strAreas) {
		this.strAreas = strAreas;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
}
