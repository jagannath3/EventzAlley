package com.uthkrushta.mybos.configuration.bean;

import java.util.Date;

public class GlobalSettingBean {
	 long lngID;
	 long lngFinancialYear;
	 long lngTimeZone;
	 long lngLanguage;
	 long lngCurrency;
	 int intStatus;
	 long lngFreezeLimit;
	 long lngExtionLimit;
	  long lngCreatedBy;
	 Date dtCreatedOn;
	 long lngUpdatedBy;
	 Date dtUpdatedOn;
	long lngFinancialYearID;
	long lngCompanyID;
	int intIsDiffComp;
	long lngWeightID;
	long lngHeightID;
	int intIsStrictStock;
	String strPhotoPath;
	int intIsInclusive;
	//Added on 22DEC
	int intIsEmail;
	int intIsSMS;
	//Added fields
	int intContinuousAbsent;
	int intNoOfAlertSMS;
	int intSMSIntervalDays;
	//Added Fields
	int intAutoLogout;
	int intAutoLogoutTime;
	
	//Added on 28May16 for Reward points
		int intIsRewards;
		double dblRedeemedAmtPoints;
		double dblRedeemedAmt;
		double dblRedeemedDaysPoints;
		long lngRedeemedDays;
		double dblMinimalRedeemedPoints;
		int intRedemptionType; 
		//Added to know last trigger date
		Date dtLastAttendanceTrigger;
		Date dtLastBATrigger;
	 
		//Added for print
		int intPrintType;
		
		int intAssessmentMaxDays;
		int intExport;
	   
		//Added for feedbackFrequency
		long lngFeedbackFrequencyDays;
		long lngWeekDayStart;
		
		long lngDurationPerSession;
		
		int intAutoSendSMSAfterSave;
		
		int intMemProfPicMand;
		int intStaffProfPicMand;
		
		int intPDFAutoGenerate;
		
		//GST Changes;
		int intGSTEnabled;
		Date dtGSTEnabledDate;
		double dblCGSTDistributionPercent;
		double dblSGSTDistributionPercent;
		
		int intAutoSendMailAfterSave;
		int intEnqRefferedByMandatory;
		int intConsiderPaymentForIncentive;
		
		private int intRenewalAdmission;//For plan expiry member to collect admission fee
		private int intExpiredMemSMSDay; //Send sms to the plan expired member after the given days
		private int intRestrictHolidaySignIn;//restricting from signing during weekOff and holidays
	    private int intPlanExpiryNotificationDays;//To notify before the plan expires
	    
	    private double dblMinBalanceRemainderAmt;//To send sms for members who balace is more than the given amount
	   
	public int getIntAutoLogoutTime() {
		return intAutoLogoutTime;
	}
	public void setIntAutoLogoutTime(int intAutoLogoutTime) {
		this.intAutoLogoutTime = intAutoLogoutTime;
	}
	 
	public int getIntAutoLogout() {
		return intAutoLogout;
	}
	public void setIntAutoLogout(int intAutoLogout) {
		this.intAutoLogout = intAutoLogout;
	}
	public long getLngWeightID() {
		return lngWeightID;
	}
	public void setLngWeightID(long lngWeightID) {
		this.lngWeightID = lngWeightID;
	}
	public long getLngHeightID() {
		return lngHeightID;
	}
	public void setLngHeightID(long lngHeightID) {
		this.lngHeightID = lngHeightID;
	}
	public int getIntIsDiffComp() {
		return intIsDiffComp;
	}
	public void setIntIsDiffComp(int intIsDiffComp) {
		this.intIsDiffComp = intIsDiffComp;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}
	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngFinancialYear() {
		return lngFinancialYear;
	}
	public void setLngFinancialYear(long lngFinancialYear) {
		this.lngFinancialYear = lngFinancialYear;
	}
	public long getLngTimeZone() {
		return lngTimeZone;
	}
	public void setLngTimeZone(long lngTimeZone) {
		this.lngTimeZone = lngTimeZone;
	}
	public long getLngLanguage() {
		return lngLanguage;
	}
	public void setLngLanguage(long lngLanguage) {
		this.lngLanguage = lngLanguage;
	}
	public long getLngCurrency() {
		return lngCurrency;
	}
	public void setLngCurrency(long lngCurrency) {
		this.lngCurrency = lngCurrency;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngFreezeLimit() {
		return lngFreezeLimit;
	}
	public void setLngFreezeLimit(long lngFreezeLimit) {
		this.lngFreezeLimit = lngFreezeLimit;
	}
	public long getLngExtionLimit() {
		return lngExtionLimit;
	}
	public void setLngExtionLimit(long lngExtionLimit) {
		this.lngExtionLimit = lngExtionLimit;
	}
	public String getStrPhotoPath() {
		return strPhotoPath;
	}
	public void setStrPhotoPath(String strPhotoPath) {
		this.strPhotoPath = strPhotoPath;
	}
	public int getIntIsStrictStock() {
		return intIsStrictStock;
	}
	public void setIntIsStrictStock(int intIsStrictStock) {
		this.intIsStrictStock = intIsStrictStock;
	}
	public int getIntIsInclusive() {
		return intIsInclusive;
	}
	public void setIntIsInclusive(int intIsInclusive) {
		this.intIsInclusive = intIsInclusive;
	}
	public int getIntIsEmail() {
		return intIsEmail;
	}
	public void setIntIsEmail(int intIsEmail) {
		this.intIsEmail = intIsEmail;
	}
	public int getIntIsSMS() {
		return intIsSMS;
	}
	public void setIntIsSMS(int intIsSMS) {
		this.intIsSMS = intIsSMS;
	}
	public int getIntContinuousAbsent() {
		return intContinuousAbsent;
	}
	public void setIntContinuousAbsent(int intContinuousAbsent) {
		this.intContinuousAbsent = intContinuousAbsent;
	}
	public int getIntNoOfAlertSMS() {
		return intNoOfAlertSMS;
	}
	public void setIntNoOfAlertSMS(int intNoOfAlertSMS) {
		this.intNoOfAlertSMS = intNoOfAlertSMS;
	}
	public int getIntSMSIntervalDays() {
		return intSMSIntervalDays;
	}
	public void setIntSMSIntervalDays(int intSMSIntervalDays) {
		this.intSMSIntervalDays = intSMSIntervalDays;
	}
	public int getIntIsRewards() {
		return intIsRewards;
	}
	public void setIntIsRewards(int intIsRewards) {
		this.intIsRewards = intIsRewards;
	}
	public double getDblRedeemedAmtPoints() {
		return dblRedeemedAmtPoints;
	}
	public void setDblRedeemedAmtPoints(double dblRedeemedAmtPoints) {
		this.dblRedeemedAmtPoints = dblRedeemedAmtPoints;
	}
	public double getDblRedeemedAmt() {
		return dblRedeemedAmt;
	}
	public void setDblRedeemedAmt(double dblRedeemedAmt) {
		this.dblRedeemedAmt = dblRedeemedAmt;
	}
	public double getDblRedeemedDaysPoints() {
		return dblRedeemedDaysPoints;
	}
	public void setDblRedeemedDaysPoints(double dblRedeemedDaysPoints) {
		this.dblRedeemedDaysPoints = dblRedeemedDaysPoints;
	}
	public long getLngRedeemedDays() {
		return lngRedeemedDays;
	}
	public void setLngRedeemedDays(long lngRedeemedDays) {
		this.lngRedeemedDays = lngRedeemedDays;
	}
	public double getDblMinimalRedeemedPoints() {
		return dblMinimalRedeemedPoints;
	}
	public void setDblMinimalRedeemedPoints(double dblMinimalRedeemedPoints) {
		this.dblMinimalRedeemedPoints = dblMinimalRedeemedPoints;
	}
	public int getIntRedemptionType() {
		return intRedemptionType;
	}
	public void setIntRedemptionType(int intRedemptionType) {
		this.intRedemptionType = intRedemptionType;
	}
	public Date getDtLastAttendanceTrigger() {
		return dtLastAttendanceTrigger;
	}
	public void setDtLastAttendanceTrigger(Date dtLastAttendanceTrigger) {
		this.dtLastAttendanceTrigger = dtLastAttendanceTrigger;
	}
	public Date getDtLastBATrigger() {
		return dtLastBATrigger;
	}
	public void setDtLastBATrigger(Date dtLastBATrigger) {
		this.dtLastBATrigger = dtLastBATrigger;
	}
	public int getIntPrintType() {
		return intPrintType;
	}
	public void setIntPrintType(int intPrintType) {
		this.intPrintType = intPrintType;
	}
	public int getIntAssessmentMaxDays() {
		return intAssessmentMaxDays;
	}
	public void setIntAssessmentMaxDays(int intAssessmentMaxDays) {
		this.intAssessmentMaxDays = intAssessmentMaxDays;
	}
	public int getIntExport() {
		return intExport;
	}
	public void setIntExport(int intExport) {
		this.intExport = intExport;
	}
	public long getLngFeedbackFrequencyDays() {
		return lngFeedbackFrequencyDays;
	}
	public void setLngFeedbackFrequencyDays(long lngFeedbackFrequencyDays) {
		this.lngFeedbackFrequencyDays = lngFeedbackFrequencyDays;
	}
	public long getLngWeekDayStart() {
		return lngWeekDayStart;
	}
	public void setLngWeekDayStart(long lngWeekDayStart) {
		this.lngWeekDayStart = lngWeekDayStart;
	}
	public long getLngDurationPerSession() {
		return lngDurationPerSession;
	}
	public void setLngDurationPerSession(long lngDurationPerSession) {
		this.lngDurationPerSession = lngDurationPerSession;
	}
	public int getIntAutoSendSMSAfterSave() {
		return intAutoSendSMSAfterSave;
	}
	public void setIntAutoSendSMSAfterSave(int intAutoSendSMSAfterSave) {
		this.intAutoSendSMSAfterSave = intAutoSendSMSAfterSave;
	}
	public int getIntMemProfPicMand() {
		return intMemProfPicMand;
	}
	public void setIntMemProfPicMand(int intMemProfPicMand) {
		this.intMemProfPicMand = intMemProfPicMand;
	}
	public int getIntStaffProfPicMand() {
		return intStaffProfPicMand;
	}
	public void setIntStaffProfPicMand(int intStaffProfPicMand) {
		this.intStaffProfPicMand = intStaffProfPicMand;
	}
	public int getIntPDFAutoGenerate() {
		return intPDFAutoGenerate;
	}
	public void setIntPDFAutoGenerate(int intPDFAutoGenerate) {
		this.intPDFAutoGenerate = intPDFAutoGenerate;
	}
	public int getIntGSTEnabled() {
		return intGSTEnabled;
	}
	public void setIntGSTEnabled(int intGSTEnabled) {
		this.intGSTEnabled = intGSTEnabled;
	}
	public Date getDtGSTEnabledDate() {
		return dtGSTEnabledDate;
	}
	public void setDtGSTEnabledDate(Date dtGSTEnabledDate) {
		this.dtGSTEnabledDate = dtGSTEnabledDate;
	}
	public double getDblCGSTDistributionPercent() {
		return dblCGSTDistributionPercent;
	}
	public void setDblCGSTDistributionPercent(double dblCGSTDistributionPercent) {
		this.dblCGSTDistributionPercent = dblCGSTDistributionPercent;
	}
	public double getDblSGSTDistributionPercent() {
		return dblSGSTDistributionPercent;
	}
	public void setDblSGSTDistributionPercent(double dblSGSTDistributionPercent) {
		this.dblSGSTDistributionPercent = dblSGSTDistributionPercent;
	}
	public int getIntAutoSendMailAfterSave() {
		return intAutoSendMailAfterSave;
	}
	public void setIntAutoSendMailAfterSave(int intAutoSendMailAfterSave) {
		this.intAutoSendMailAfterSave = intAutoSendMailAfterSave;
	}
	
	public int getIntEnqRefferedByMandatory() {
		return intEnqRefferedByMandatory;
	}
	public void setIntEnqRefferedByMandatory(int intEnqRefferedByMandatory) {
		this.intEnqRefferedByMandatory = intEnqRefferedByMandatory;
	}
	public int getIntConsiderPaymentForIncentive() {
		return intConsiderPaymentForIncentive;
	}
	public void setIntConsiderPaymentForIncentive(int intConsiderPaymentForIncentive) {
		this.intConsiderPaymentForIncentive = intConsiderPaymentForIncentive;
	}
	public int getIntRenewalAdmission() {
		return intRenewalAdmission;
	}
	public void setIntRenewalAdmission(int intRenewalAdmission) {
		this.intRenewalAdmission = intRenewalAdmission;
	}
	public int getIntExpiredMemSMSDay() {
		return intExpiredMemSMSDay;
	}
	public void setIntExpiredMemSMSDay(int intExpiredMemSMSDay) {
		this.intExpiredMemSMSDay = intExpiredMemSMSDay;
	}
	public int getIntRestrictHolidaySignIn() {
		return intRestrictHolidaySignIn;
	}
	public void setIntRestrictHolidaySignIn(int intRestrictHolidaySignIn) {
		this.intRestrictHolidaySignIn = intRestrictHolidaySignIn;
	}
	public int getIntPlanExpiryNotificationDays() {
		return intPlanExpiryNotificationDays;
	}
	public void setIntPlanExpiryNotificationDays(int intPlanExpiryNotificationDays) {
		this.intPlanExpiryNotificationDays = intPlanExpiryNotificationDays;
	}
	public double getDblMinBalanceRemainderAmt() {
		return dblMinBalanceRemainderAmt;
	}
	public void setDblMinBalanceRemainderAmt(double dblMinBalanceRemainderAmt) {
		this.dblMinBalanceRemainderAmt = dblMinBalanceRemainderAmt;
	}
	
	
	 
	 

}
