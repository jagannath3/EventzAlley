package com.uthkrushta.mybos.configuration.bean;

public class LedgerViewConfigurationBean {

	private long lngID;
	private String strLedgerName;
	private Double dblAmount;
	private Double dblUsedAmount;
	private Double dblBalanceAmount;
	private long lngCashFlowID;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrLedgerName() {
		return strLedgerName;
	}
	public void setStrLedgerName(String strLedgerName) {
		this.strLedgerName = strLedgerName;
	}
	public double getDblAmount() {
		if(null == dblAmount) return 0;
		return dblAmount;
	}
	public void setDblAmount(double dblAmount) {
		this.dblAmount = dblAmount;
	}
	public long getLngCashFlowID() {
		return lngCashFlowID;
	}
	public void setLngCashFlowID(long lngCashFlowID) {
		this.lngCashFlowID = lngCashFlowID;
	}

	public double getDblUsedAmount() {
		if(null == dblUsedAmount) return 0;
		return dblUsedAmount;
	}
	public void setDblUsedAmount(double dblUsedAmount) {		
		this.dblUsedAmount = dblUsedAmount;
	}
	public double getDblBalanceAmount() {
		if(null == dblBalanceAmount) return 0;
		return dblBalanceAmount;
	}
	public void setDblBalanceAmount(double dblBalanceAmount) {
		this.dblBalanceAmount = dblBalanceAmount;
	}
	
	
}
