package com.uthkrushta.mybos.utility.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;
import org.apache.axis.types.Entities;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Query;
import org.hibernate.Session;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.code.bean.SMSVariableCodeBean;
import com.uthkrushta.mybos.configuration.bean.GlobalSettingBean;
import com.uthkrushta.mybos.master.bean.UserMasterBean;
import com.uthkrushta.mybos.transaction.bean.view.EventMemberListViewBean;
/*import com.uthkrushta.mm.master.bean.CompanyMasterBean;
import com.uthkrushta.mm.master.bean.MemberMasterBean;
import com.uthkrushta.mm.master.bean.PlanBatchNameMasterBeanView;
import com.uthkrushta.mm.master.bean.StaffMasterBean;
import com.uthkrushta.mm.master.controller.StaffMasterController;
import com.uthkrushta.mm.product.bean.OutsidersProductBean;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.transaction.bean.EnquiryHeaderTransaction;
import com.uthkrushta.mm.transaction.bean.PlanSubsPaidMemNameTransactionViewBean;*/
import com.uthkrushta.mybos.utility.bean.SMSTemplateNotificationBean;
import com.uthkrushta.mybos.utility.bean.SmsTemplateBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.HibernateUtil;
import com.uthkrushta.utils.WebUtil;

public class SMSController extends GenericController {

	SMSTemplateNotificationBean objSTNB;
	HttpServletRequest request = null;
	DBUtil DBUtil = new DBUtil();
	boolean blnModified = false;

	// To check whether the sms option is available or not in the global setting
	// GlobalSettingBean objGSB = (GlobalSettingBean)new
	// DBUtil(request).getRecord(IUBESPOKEConstants.BN_GLOBAL_SETTING,
	// IUBESPOKEConstants.GET_ACTIVE_ROWS, "");
	/*
	 * if(null == objGSB)
	 * 
	 * objGSB = new GlobalSettingBean();
	 */

	public SMSController(HttpServletRequest request) {
		// TODO Auto-generated constructor stub
		super();
		this.request = request;
		DBUtil = new DBUtil();
	}

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
		if (strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT)) {
			modifyRecord(request);
		} else if (strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SEND_SMS)) {
			sendPromotionalMessage(request);
		}

	}

	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean blnModified = false;
		try {

			// long lngCompID=StaffMasterController.getCompanyID(request);
			long lngCompID = 1;
			objSTNB = new SMSTemplateNotificationBean();
			objSTNB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID));
			objSTNB.setLngMsgTypeID(WebUtil.parseLong(request, ILabelConstants.selSmsType));
			objSTNB.setStrContent(WebUtil.parseString(request, ILabelConstants.txaSmsContent));
			objSTNB.setIntSend(WebUtil.parseInt(request, ILabelConstants.chkSend));
			objSTNB.setLngCompanyID(lngCompID);
			objSTNB.setLngMsgCategoryID(WebUtil.parseLong(request, ILabelConstants.selSmsCategory));
			objSTNB.setIntStatus(IUMCConstants.STATUS_ACTIVE);
			/* Added on JAN18 for ExcelUpload */
			objSTNB.setStrFileUploadExcel(WebUtil.parseString(request, ILabelConstants.hdnFilePath));

			if (validate(request)) {
				blnModified = DBUtil.modifyRecord(objSTNB);

			}
			if (blnModified) {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,ID,Footer");
			} else {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
			}

		} catch (Exception e) {
			e.printStackTrace();
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
		}

		return blnModified;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		NameValueBean objNameValueBean = null;
		ArrayList<NameValueBean> arrNameValueList = new ArrayList<NameValueBean>();
		if (null != objSTNB) {
			objNameValueBean = new NameValueBean("lngID", "" + objSTNB.getLngID(), true);
			arrNameValueList.add(objNameValueBean);

			objNameValueBean = new NameValueBean("lngMsgTypeID", "" + objSTNB.getLngMsgTypeID(), false);
			arrNameValueList.add(objNameValueBean);

			return DBUtil.checkIsUnique(IUMCConstants.BN_SMS_TEMP_NOTIFICATION, IUMCConstants.BN_SMS_TEMP_NOTIFICATION,
					arrNameValueList);

		} else {
			return true;
		}
	}

	/*
	 * public List<Object> getList(EntityPlanMasterView objEntity) { // TODO
	 * Auto-generated method stub String strWhere =
	 * IUMCConstants.GET_ACTIVE_ROWS+" and intSend = 1";buildQueryString(request);
	 * 
	 * return (List) getRecords(IUMCConstants.BN_SMS_TEMP_NOTIFICATION, strWhere,
	 * "",objEntity);
	 * 
	 * }
	 */

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		if (null != objSTNB) {
			return objSTNB;
		} else {
			return new SMSTemplateNotificationBean();
		}
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String strWhere = IUMCConstants.GET_ACTIVE_ROWS;
		return strWhere;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean sendPromotionalMessage(HttpServletRequest request) {
		List lstTemplate = getList(request);
		SMSTemplateNotificationBean objTNB = null;

		List lstMem = null;

		

		long lngSelectEvent = WebUtil.parseLong(request, ILabelConstants.selEventName);
		long lngSelectEventType = WebUtil.parseLong(request, ILabelConstants.selEventTypeName);
		
		if(lngSelectEventType >0) {
			lstMem = DBUtil.getRecords(ILabelConstants.BN_EVENT_MEMBER_LIST_VIEW,
					" lngEventID =" + lngSelectEvent + " AND lngEventTypeID = " + lngSelectEventType, "strFirstName");
		}else {
			lstMem = DBUtil.getRecords(ILabelConstants.BN_EVENT_MEMBER_LIST_VIEW,
					" lngEventID =" + lngSelectEvent , "strFirstName");
		}
		
		if (null != lstMem && lstMem.size() > 1) {
			blnModified = sendPromoMessage(request, lstMem);
		}

		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SENT_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			arrMsg.add(msg);
			AAUtils.addZonesToRefreh(request, "Status");
		} else {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_ACTION_FAILED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			arrMsg.add(msg);
			AAUtils.addZonesToRefreh(request, "Status");
		}

		return blnModified;

	}

	public boolean sendPromoMessage(HttpServletRequest request, List<EventMemberListViewBean> lstMem) {
		String strContent = "";
		String strSpecificContent = "";
		long lngCompanyID = 0;
		EventMemberListViewBean objEMLVB = new EventMemberListViewBean();
		UserMasterBean objUMB = null;
		StringBuilder builder = new StringBuilder();

		if (WebUtil.parseString(request, ILabelConstants.txaSmsContent).trim().length() > 0) {

			strContent = WebUtil.parseString(request, ILabelConstants.txaSmsContent);
		}

		strSpecificContent = strContent;
		String strWhereSMS = IUMCConstants.GET_ACTIVE_ROWS + " AND intSMSType = " + IUMCConstants.TRANSACTIONAL;
		SMSVariableCodeBean objSMSVar = (SMSVariableCodeBean) DBUtil.getRecord(IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN,
				strWhereSMS, "");

		// String strSMSAPI =
		// objCMB.getStrSMSAPI();//"http://login.bulksmsgateway.in/sendmessage.php?user=myhomeindustries&password=myhome@2015&mobile={[SEND_TO]}&message={[SMS_TEXT]}&sender=MAHACM&type=3";
		String strSMSAPI = "";
		if (null != objSMSVar && objSMSVar.getStrName().trim().length() > 0) {
			strSMSAPI = objSMSVar.getStrName();
		}
		
		URL url = null;
		HttpURLConnection uc;

		if (null != lstMem && lstMem.size() > 0) {
			for (int i = 0; i < lstMem.size(); i++) {
				String strSMSAPIForCust = strSMSAPI;
				strSpecificContent = strContent;
				objEMLVB = (EventMemberListViewBean) lstMem.get(i);
				builder = new StringBuilder();

				if (null != objEMLVB && objEMLVB.getStrMemberMobNumber() != null) {
					if (null != objEMLVB.getStrFirstName() && objEMLVB.getStrFirstName().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_FIRST_NAME,
								WebUtil.processString(objEMLVB.getStrFirstName()));
										
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_FIRST_NAME, "");
					}
					if (null != objEMLVB.getStrLastName() && objEMLVB.getStrLastName().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_LAST_NAME,
								WebUtil.processString(WebUtil.processString(objEMLVB.getStrLastName())));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_LAST_NAME, "");
					}
					if (null != objEMLVB.getStrFirstName() && objEMLVB.getStrFirstName().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_DOB,
								WebUtil.formatDate(objEMLVB.getDtMemberDOB(), IUMCConstants.DATE_FORMAT));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_DOB, "");
					}

					if (null != objEMLVB.getStrMemberMobNumber()
							&& objEMLVB.getStrMemberMobNumber().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_MOB_NO,
								WebUtil.processString(objEMLVB.getStrMemberMobNumber()));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_MOB_NO, "");
					}

					if (null != objEMLVB.getStrMemberEmail() && objEMLVB.getStrMemberEmail().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_EMAIL,
								WebUtil.processString(objEMLVB.getStrMemberEmail()));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_EMAIL, "");
					}
					if (null != objEMLVB.getStrMemberPlace() && objEMLVB.getStrMemberPlace().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_ADDRESS,
								WebUtil.processString(objEMLVB.getStrMemberPlace()));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_ADDRESS, "");
					}
					if (null != objEMLVB.getStrUserName() && objEMLVB.getStrUserName().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_USER_NAME,
								WebUtil.processString(objEMLVB.getStrUserName()));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_USER_NAME, "");
					}
					if (null != objEMLVB.getStrPassword() && objEMLVB.getStrPassword().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_PASSWORD,
								WebUtil.processString(objEMLVB.getStrPassword()));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_PASSWORD, "");
					}
					
					
					if (null != objEMLVB.getStrBibNo() && objEMLVB.getStrBibNo().trim().length() > 0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_BIB,
								WebUtil.processString(objEMLVB.getStrBibNo()));
					} else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_BIB, "");
					}
					if (null != objEMLVB.getStrRank() && objEMLVB.getStrRank().trim().length() >0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_RANK, 
								WebUtil.processString(objEMLVB.getStrRank()));
					}else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_RANK, "");
					}if (null != objEMLVB.getStrChip() && objEMLVB.getStrChip().trim().length() >0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_CHIP, 
								WebUtil.processString(objEMLVB.getStrChip()));
					}else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_CHIP, "");
					}if (null != objEMLVB.getStrGun() && objEMLVB.getStrGun().trim().length() >0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_GUN, 
								WebUtil.processString(objEMLVB.getStrGun()));
					}else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_GUN, "");
					}if (null != objEMLVB.getStrPace() && objEMLVB.getStrPace().trim().length() >0) {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_PACE, 
								WebUtil.processString(objEMLVB.getStrPace()));
					}else {
						strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_MEMB_PACE, "");
					}

					System.out.println("before = " + strSpecificContent);
					//strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					
					try {
						strSpecificContent = URLEncoder.encode(strSpecificContent, "UTF-8");
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					System.out.println("Final = " + strSpecificContent);
					strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,
							objEMLVB.getStrMemberMobNumber());
					System.out.println("strSMSAPIForCust send to ----" + strSMSAPIForCust);
					strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT,
							builder.append(strSpecificContent).toString());
					System.out.println("strSMSAPIForCust sms text -----" + strSMSAPIForCust);
					
					  try { 
						  url = new URL(strSMSAPIForCust); //working,commented due to sending of sms
						  System.out.println(strSMSAPIForCust); 
						  uc = (HttpURLConnection)url.openConnection(); 
						  uc.getResponseMessage();
						  uc.disconnect(); System.out.print(strSMSAPIForCust); 
						  blnModified=true; 
						 }
					  catch (MalformedURLException e) { 
						  // TODO Auto-generated catch block
					 	e.printStackTrace(); } 
					  catch (IOException e) 
					  { 
						  // TODO Auto-generated catch block
					   e.printStackTrace(); 
					   }
					 
				}
			}
		}

		return blnModified;

	}
	/*
	 * public List sendBAPRecords() { String
	 * strWhere="strDbName not in ('super_centrum')"; List
	 * lstRecords=getRecords(null,IUMCConstants.BN_ENTITY_VIEW,strWhere,"");
	 * if(null!=lstRecords) { for(int i=0;i<lstRecords.size();i++) {
	 * EntityPlanMasterView objEPMV=new EntityPlanMasterView();
	 * objEPMV=(EntityPlanMasterView)lstRecords.get(i); if(null!=objEPMV) {
	 * sendMessageTemplate(objEPMV); } } } return arrMsg;
	 * 
	 * }
	 * 
	 * 
	 * //To query from super_centrum database public List getRecords(String
	 * strSelectList, String strTableName, String strWhereClause, String
	 * strSortClause) { Session session = new
	 * HibernateUtil().getSessionFactorySupreme().openSession(); String SQL_QUERY =
	 * null; if (null == strSelectList || strSelectList.equals("")) { SQL_QUERY =
	 * ""; } else { SQL_QUERY = "select " + strSelectList; } SQL_QUERY = SQL_QUERY +
	 * " from " + strTableName; if (null != strWhereClause &&
	 * !strWhereClause.equalsIgnoreCase("")) { SQL_QUERY = SQL_QUERY + "  where " +
	 * strWhereClause + " "; } if (null != strSortClause &&
	 * !strSortClause.equalsIgnoreCase("")) { SQL_QUERY = SQL_QUERY + " order by " +
	 * strSortClause; } Query query = session.createQuery(SQL_QUERY);
	 * System.out.println(" Before pagination : " + query.list().size());
	 * 
	 * query.setMaxResults(5); System.out.println(" After pagination : " +
	 * query.list().size()); if (null != query.list() && query.list().size() > 0) {
	 * return query.list(); } return null; }
	 * 
	 * 
	 * public boolean sendWishMessage(SMSTemplateNotificationBean
	 * objSTNB,List<MemberMasterBean> lstMem,EntityPlanMasterView objEntity) {
	 * String strContent = ""; String strSpecificContent = ""; long lngCompanyID=0;
	 * MemberMasterBean objMMB=null;
	 * 
	 * if(null!=lstMem && lstMem.size()>0) { objMMB=(MemberMasterBean)lstMem.get(0);
	 * if(null!=objMMB) { lngCompanyID=objMMB.getLngCompanyID(); } }
	 * 
	 * if (null != request.getSession().getAttribute("COMPANY_ID")) { lngCompanyID =
	 * ((Long) request.getSession().getAttribute("COMPANY_ID")).longValue(); }
	 * 
	 * List lstCompany = getRecords(null, IUMCConstants.BN_COMPANY_MASTER_BEAN,
	 * IUMCConstants.GET_ACTIVE_ROWS+" and lngCompanyTypeID in (1,2)",
	 * "",objEntity); if(null!=lstCompany) { for(int i=0;i<lstCompany.size();i++) {
	 * CompanyMasterBean objCMB=new CompanyMasterBean();
	 * objCMB=(CompanyMasterBean)lstCompany.get(i);
	 * 
	 * 
	 * // CompanyMasterBean objCMB =
	 * (CompanyMasterBean)getRecord(IUBESPOKEConstants.BN_COMPANY_MASTER_BEAN,
	 * IUBESPOKEConstants.GET_ACTIVE_ROWS+" AND lngID = "+lngCompanyID, ""); String
	 * strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and lngCompanyID = "+objCMB.
	 * getLngID()+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;//lngCompanyID
	 * SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean)
	 * getRecord(IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN,strWhereSMS,"",objEntity);
	 * StringBuilder builder = new StringBuilder(); //String strSMSAPI =
	 * objCMB.getStrSMSAPI();//
	 * "http://login.bulksmsgateway.in/sendmessage.php?user=myhomeindustries&password=myhome@2015&mobile={[SEND_TO]}&message={[SMS_TEXT]}&sender=MAHACM&type=3";
	 * String strSMSAPI = ""; if(null!=objSMSVar &&
	 * objSMSVar.getStrName().trim().length()>0) { strSMSAPI=objSMSVar.getStrName();
	 * } String strSMSAPIForCust = strSMSAPI ; URL url = null; HttpURLConnection uc;
	 * 
	 * if(null!=objSTNB) { // strContent =WebUtil.parseString(request,
	 * ILabelConstants.txaSmsContent); commented due to request; strContent
	 * =objSTNB.getStrContent(); strContent = strContent.replaceAll(" ", "%20");
	 * strSpecificContent=strContent;
	 * 
	 * //To get the Admin Name of the center StaffMasterBean
	 * objAdminStaff=(StaffMasterBean) getRecord(IUMCConstants.BN_STAFF_MASTER_BEAN,
	 * IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID() ,
	 * "",objEntity); String strAdminName=""; if(null!=objAdminStaff &&
	 * objAdminStaff.getStrName().trim().length()>0) {
	 * strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName(); }
	 * 
	 * if(null!=lstMem && lstMem.size()>0) { for(int i1=0;i1<lstMem.size();i1++) {
	 * objMMB=new MemberMasterBean(); objMMB = (MemberMasterBean)lstMem.get(i1);
	 * builder = new StringBuilder();
	 * 
	 * if(null!= objMMB && objMMB.getStrMobileNumber()!=null) {
	 * if(objMMB.getStrMobileNumber().trim().length()==0)continue;
	 * strSpecificContent =
	 * strSpecificContent.replace(IUBESPOKEConstants.ST_COMP_NAME,
	 * objCMB.getStrName()); strSpecificContent =
	 * strSpecificContent.replace(IUBESPOKEConstants.ST_COMP_ADDRESS,
	 * objCMB.getStrAddress()); strSpecificContent =
	 * strSpecificContent.replace(IUBESPOKEConstants.ST_COMP_EMAIL,
	 * objCMB.getStrAdminEmail());
	 * strSpecificContent=strSpecificContent.replace(IUBESPOKEConstants.ST_NAME,
	 * objMMB.getStrName()+" "+objMMB.getStrLastName()); String
	 * strName=objMMB.getStrName()+" "+objMMB.getStrLastName();
	 * 
	 * if(null!=strName && strName.trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,
	 * strName); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
	 * } if(null!=objMMB.getStrAddress() &&
	 * objMMB.getStrAddress().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS,
	 * objMMB.getStrAddress()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS,
	 * ""); }
	 * 
	 * if(null!=objMMB.getStrLandlineNumber() &&
	 * objMMB.getStrLandlineNumber().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO,
	 * objMMB.getStrLandlineNumber()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO,
	 * ""); } if(null!=objMMB.getStrMobileNumber() &&
	 * objMMB.getStrMobileNumber().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,
	 * objMMB.getStrMobileNumber()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,""
	 * ); } if(null!=objMMB.getStrEmailID() &&
	 * objMMB.getStrEmailID().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,
	 * objMMB.getStrEmailID()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"")
	 * ; } if(null!=objMMB.getDtDOB()) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,
	 * objMMB.getDtDOB()+""); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,""); }
	 * if(null!=objMMB.getDtDOM() ) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,
	 * objMMB.getDtDOM()+""); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,""); }
	 * if(null!=objCMB.getStrName() && objCMB.getStrName().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME,
	 * objCMB.getStrName()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME,
	 * ""); } if(null!=objCMB.getStrPrimaryMobileNumber() &&
	 * objCMB.getStrPrimaryMobileNumber().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,
	 * objCMB.getStrPrimaryMobileNumber()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
	 * } if(null!=objCMB.getStrAddress() &&
	 * objCMB.getStrAddress().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.
	 * ST_CENTER_ADDRESS, objCMB.getStrAddress()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS
	 * ,""); } if(null!=strAdminName && strAdminName.trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN,
	 * strAdminName); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN,
	 * ""); } if(null!=objCMB.getStrContactEmail() &&
	 * objCMB.getStrContactEmail().trim().length()>0) {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL,
	 * objCMB.getStrContactEmail()); } else {
	 * strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL,
	 * ""); }
	 * 
	 * 
	 * 
	 * strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
	 * strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objMMB.
	 * getStrMobileNumber()); strSMSAPIForCust =
	 * strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT,
	 * builder.append(strSpecificContent).toString());
	 * 
	 * try { url = new URL(strSMSAPIForCust); System.out.println(strSMSAPIForCust);
	 * uc = (HttpURLConnection)url.openConnection(); uc.getResponseMessage();
	 * uc.disconnect(); System.out.print(strSMSAPIForCust); blnModified= true; }
	 * catch (MalformedURLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); blnModified= false; } catch (IOException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); blnModified= false; } } } } }
	 * 
	 * } } return blnModified;
	 * 
	 * 
	 * }
	 * 
	 * 
	 * //To query from given client database public Object getRecords(String
	 * strTableName, String strWhereClause, String
	 * strSortClause,EntityPlanMasterView objEPMV) { return getRecords(null,
	 * strTableName, strWhereClause, strSortClause,objEPMV); }
	 * 
	 * public List getRecords(String strSelectList, String strTableName, String
	 * strWhereClause, String strSortClause,EntityPlanMasterView objEntity) {
	 * Session session = new
	 * HibernateUtil().getSessionFactoryForCenter(objEntity).openSession(); String
	 * SQL_QUERY = null; if (null == strSelectList || strSelectList.equals("")) {
	 * SQL_QUERY = ""; } else { SQL_QUERY = "select " + strSelectList; } SQL_QUERY =
	 * SQL_QUERY + " from " + strTableName; if (null != strWhereClause &&
	 * !strWhereClause.equalsIgnoreCase("")) { SQL_QUERY = SQL_QUERY + "  where " +
	 * strWhereClause + " "; } if (null != strSortClause &&
	 * !strSortClause.equalsIgnoreCase("")) { SQL_QUERY = SQL_QUERY + " order by " +
	 * strSortClause; } Query query = session.createQuery(SQL_QUERY);
	 * System.out.println(" Before pagination : " + query.list().size());
	 * 
	 * query.setMaxResults(5); System.out.println(" After pagination : " +
	 * query.list().size()); if (null != query.list() && query.list().size() > 0) {
	 * return query.list(); } return null; }
	 * 
	 * 
	 * public Object getRecord(String strTableName, String strWhereClause, String
	 * strSortClause,EntityPlanMasterView objEntity) { return getRecord(null,
	 * strTableName, strWhereClause, strSortClause,objEntity); } public Object
	 * getRecord(String strSelectList, String strTableName, String strWhereClause,
	 * String strSortClause,EntityPlanMasterView objEntity) { Session session = new
	 * HibernateUtil().getSessionFactoryForCenter(objEntity).openSession(); String
	 * SQL_QUERY = null; if (null == strSelectList || strSelectList.equals("")) {
	 * SQL_QUERY = ""; } else { SQL_QUERY = "select " + strSelectList; } SQL_QUERY =
	 * SQL_QUERY + " from " + strTableName; if (null != strWhereClause &&
	 * !strWhereClause.equalsIgnoreCase("")) { SQL_QUERY = SQL_QUERY + "  where " +
	 * strWhereClause + " "; } if (null != strSortClause &&
	 * !strSortClause.equalsIgnoreCase("")) { SQL_QUERY = SQL_QUERY + " order by " +
	 * strSortClause; } Query query = session.createQuery(SQL_QUERY); if (null !=
	 * query.list() && query.list().size() > 0) { return query.list().get(0); }
	 * return null; }
	 * 
	 * @Override public List<Object> getList(HttpServletRequest request) { // TODO
	 * Auto-generated method stub return null; }
	 */

}
