package com.uthkrushta.mybos.utility.bean;

public class SmsTraceBean {
	
	private int intID;
	private long lngAlertCatID;	
	private long lngExhID;
	private long lngAreaID;
	private String strActiveAreas;
	private String strContent;
	private int intStatus;
	private long lngSmsTypeID;
	private long lngPromoTypeID;
	private long lngsmsTempateID;
	private long lngCityID;
	private long lngZoneID;
	
	
	
	
	
	public SmsTraceBean() {
		this.strActiveAreas = strContent ="";
	}
	
	public long getLngAlertCatID() {
		return lngAlertCatID;
	}
	public int getIntID() {
		return intID;
	}
	public void setIntID(int intID) {
		this.intID = intID;
	}
	public void setLngAlertCatID(long lngAlertCatID) {
		this.lngAlertCatID = lngAlertCatID;
	}
	public long getLngExhID() {
		return lngExhID;
	}
	public void setLngExhID(long lngExhID) {
		this.lngExhID = lngExhID;
	}
	public long getLngAreaID() {
		return lngAreaID;
	}
	public void setLngAreaID(long lngAreaID) {
		this.lngAreaID = lngAreaID;
	}
	public String getStrActiveAreas() {
		return strActiveAreas;
	}
	public void setStrActiveAreas(String strActiveAreas) {
		this.strActiveAreas = strActiveAreas;
	}
	public String getStrContent() {
		return strContent;
	}
	public void setStrContent(String strContent) {
		this.strContent = strContent;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}

	public long getLngSmsTypeID() {
		return lngSmsTypeID;
	}

	public void setLngSmsTypeID(long lngSmsTypeID) {
		this.lngSmsTypeID = lngSmsTypeID;
	}

	public long getLngPromoTypeID() {
		return lngPromoTypeID;
	}

	public void setLngPromoTypeID(long lngPromoTypeID) {
		this.lngPromoTypeID = lngPromoTypeID;
	}

	public long getLngsmsTempateID() {
		return lngsmsTempateID;
	}

	public void setLngsmsTempateID(long lngsmsTempateID) {
		this.lngsmsTempateID = lngsmsTempateID;
	}

	public long getLngCityID() {
		return lngCityID;
	}

	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}

	public long getLngZoneID() {
		return lngZoneID;
	}
}
