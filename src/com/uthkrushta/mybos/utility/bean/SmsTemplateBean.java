package com.uthkrushta.mybos.utility.bean;

import java.util.Date;

public class SmsTemplateBean {
	
		
		private long lngID;
		private long lngMsgTypeID;
		private String strDescription;
		private int intStatus;
		private long lngCreatedBy;
		private long lngUpdatedBy;
		private Date dtUpdatedOn;
		private Date dtCreatedOn;
		
		
		
		
		public SmsTemplateBean() {
			
			
			strDescription="";
		}
		
		
		
		public long getLngID() {
			return lngID;
		}
		public void setLngID(long lngID) {
			this.lngID = lngID;
		}
		
		public long getLngMsgTypeID() {
			return lngMsgTypeID;
		}



		public void setLngMsgTypeID(long lngMsgTypeID) {
			this.lngMsgTypeID = lngMsgTypeID;
		}



		public String getStrDescription() {
			return strDescription;
		}
		public void setStrDescription(String strDescription) {
			this.strDescription = strDescription;
		}
		public int getIntStatus() {
			return intStatus;
		}
		public void setIntStatus(int intStatus) {
			this.intStatus = intStatus;
		}
		public long getLngCreatedBy() {
			return lngCreatedBy;
		}
		public void setLngCreatedBy(long lngCreatedBy) {
			this.lngCreatedBy = lngCreatedBy;
		}
		public long getLngUpdatedBy() {
			return lngUpdatedBy;
		}
		public void setLngUpdatedBy(long lngUpdatedBy) {
			this.lngUpdatedBy = lngUpdatedBy;
		}
		
		public Date getDtCreatedOn() {
			return dtCreatedOn;
		}
		public void setDtCreatedOn(Date dtCreatedOn) {
			this.dtCreatedOn = dtCreatedOn;
		}
		public Date getDtUpdatedOn() {
			return dtUpdatedOn;
		}
		public void setDtUpdatedOn(Date dtUpdatedOn) {
			this.dtUpdatedOn = dtUpdatedOn;
		}
}
