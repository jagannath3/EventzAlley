package com.uthkrushta.mybos.frameWork.bean;

public class ApplicationMasterBean {

	private long lngActivityId;
	private String strMenuName;
	private String strActivityName;
	private long lngMenuId;
	
	
	public long getLngActivityId() {
		return lngActivityId;
	}
	public void setLngActivityId(long lngActivityId) {
		this.lngActivityId = lngActivityId;
	}
	public String getStrMenuName() {
		return strMenuName;
	}
	public void setStrMenuName(String strMenuName) {
		this.strMenuName = strMenuName;
	}
	public String getStrActivityName() {
		return strActivityName;
	}
	public void setStrActivityName(String strActivityName) {
		this.strActivityName = strActivityName;
	}
	public long getLngMenuId() {
		return lngMenuId;
	}
	public void setLngMenuId(long lngMenuId) {
		this.lngMenuId = lngMenuId;
	}
	
	
}