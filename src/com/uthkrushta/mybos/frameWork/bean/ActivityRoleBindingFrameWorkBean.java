package com.uthkrushta.mybos.frameWork.bean;

public class ActivityRoleBindingFrameWorkBean {
	private long lngID;
	private long lngActivityId;
	private long lngRoleId;
	private long lngUserId;
	private int intStatus;
	private long lngCompanyId;
	private int intPermissionToCreate;
	private int intPermissionToUpdate;
	private int intPermissionToDelete;
	private int intPermissionToApprove;
	private int intIsQuickMenu;
	private int intIsHomePage;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngActivityId() {
		return lngActivityId;
	}
	public void setLngActivityId(long lngActivityId) {
		this.lngActivityId = lngActivityId;
	}
	public long getLngRoleId() {
		return lngRoleId;
	}
	public void setLngRoleId(long lngRoleId) {
		this.lngRoleId = lngRoleId;
	}
	public long getLngUserId() {
		return lngUserId;
	}
	public void setLngUserId(long lngUserId) {
		this.lngUserId = lngUserId;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngCompanyId() {
		return lngCompanyId;
	}
	public void setLngCompanyId(long lngCompanyId) {
		this.lngCompanyId = lngCompanyId;
	}
	public int getIntPermissionToCreate() {
		return intPermissionToCreate;
	}
	public void setIntPermissionToCreate(int intPermissionToCreate) {
		this.intPermissionToCreate = intPermissionToCreate;
	}
	public int getIntPermissionToUpdate() {
		return intPermissionToUpdate;
	}
	public void setIntPermissionToUpdate(int intPermissionToUpdate) {
		this.intPermissionToUpdate = intPermissionToUpdate;
	}
	public int getIntPermissionToDelete() {
		return intPermissionToDelete;
	}
	public void setIntPermissionToDelete(int intPermissionToDelete) {
		this.intPermissionToDelete = intPermissionToDelete;
	}
	public int getIntPermissionToApprove() {
		return intPermissionToApprove;
	}
	public void setIntPermissionToApprove(int intPermissionToApprove) {
		this.intPermissionToApprove = intPermissionToApprove;
	}
	public int getIntIsQuickMenu() {
		return intIsQuickMenu;
	}
	public void setIntIsQuickMenu(int intIsQuickMenu) {
		this.intIsQuickMenu = intIsQuickMenu;
	}
	public int getIntIsHomePage() {
		return intIsHomePage;
	}
	public void setIntIsHomePage(int intIsHomePage) {
		this.intIsHomePage = intIsHomePage;
	}
	
}

