package com.uthkrushta.mybos.frameWork.bean;

import java.util.Date;

public class LoginBean {

	private long lngID;
	private long lngUserId;
	private long lngClientId;
	private String strUserName;
	private String strPassword;
	private int intIsFirstLogin;
	private long lngRoleId;
	private long lngRoleType;
	private Date dtLastLoginDate;
	private long lngSecurityQuestion;
	private String strSecurityAnswer;
	private String strActivationCode;
	private String strEmail;
	private int intStatus;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private long lngCompanyId;
	private String strDisplyName;
	
	public LoginBean() {
		this.strUserName = strPassword =  strSecurityAnswer = strActivationCode = strEmail = strDisplyName = "" ;
	}

	public long getLngID() {
		return lngID;
	}

	public void setLngID(long lngID) {
		this.lngID = lngID;
	}

	public long getLngUserId() {
		return lngUserId;
	}

	public void setLngUserId(long lngUserId) {
		this.lngUserId = lngUserId;
	}

	public long getLngClientId() {
		return lngClientId;
	}

	public void setLngClientId(long lngClientId) {
		this.lngClientId = lngClientId;
	}

	public String getStrUserName() {
		return strUserName;
	}

	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}

	public String getStrPassword() {
		return strPassword;
	}

	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}

	public int getIntIsFirstLogin() {
		return intIsFirstLogin;
	}

	public void setIntIsFirstLogin(int intIsFirstLogin) {
		this.intIsFirstLogin = intIsFirstLogin;
	}

	public long getLngRoleId() {
		return lngRoleId;
	}

	public void setLngRoleId(long lngRoleId) {
		this.lngRoleId = lngRoleId;
	}

	public long getLngRoleType() {
		return lngRoleType;
	}

	public void setLngRoleType(long lngRoleType) {
		this.lngRoleType = lngRoleType;
	}

	public Date getDtLastLoginDate() {
		return dtLastLoginDate;
	}

	public void setDtLastLoginDate(Date dtLastLoginDate) {
		this.dtLastLoginDate = dtLastLoginDate;
	}

	public long getLngSecurityQuestion() {
		return lngSecurityQuestion;
	}

	public void setLngSecurityQuestion(long lngSecurityQuestion) {
		this.lngSecurityQuestion = lngSecurityQuestion;
	}

	public String getStrSecurityAnswer() {
		return strSecurityAnswer;
	}

	public void setStrSecurityAnswer(String strSecurityAnswer) {
		this.strSecurityAnswer = strSecurityAnswer;
	}

	public String getStrActivationCode() {
		return strActivationCode;
	}

	public void setStrActivationCode(String strActivationCode) {
		this.strActivationCode = strActivationCode;
	}

	public String getStrEmail() {
		return strEmail;
	}

	public void setStrEmail(String strEmail) {
		this.strEmail = strEmail;
	}

	public int getIntStatus() {
		return intStatus;
	}

	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}

	public long getLngCreatedBy() {
		return lngCreatedBy;
	}

	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}

	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}

	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}

	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}

	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}

	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}

	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}

	public long getLngCompanyId() {
		return lngCompanyId;
	}

	public void setLngCompanyId(long lngCompanyId) {
		this.lngCompanyId = lngCompanyId;
	}

	public String getStrDisplyName() {
		return strDisplyName;
	}

	public void setStrDisplyName(String strDisplyName) {
		this.strDisplyName = strDisplyName;
	}
	
}
