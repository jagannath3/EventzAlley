package com.uthkrushta.mybos.frameWork.bean;

public class ApplicationMenuBean {
	
	private long lngMenuID;
	private String strMenuName;
	private long lngActivityID;
	private String strActivityName;
	private String strPageUrl;
	private String strImagePath;
	private String strDescription;
	private long lngRoleID;
	private long lngUserID;
	private long lngID;
	private int intPermissionToCreate;
	private int intPermissionToUpdate;
	private int intPermissionToDelete;
	private int intPermissionToApprove;
	private int intIsQuickMenu;
	private int intIsHomePage;
	
	public int getIntPermissionToCreate() {
		return intPermissionToCreate;
	}
	public void setIntPermissionToCreate(int intPermissionToCreate) {
		this.intPermissionToCreate = intPermissionToCreate;
	}
	public int getIntPermissionToUpdate() {
		return intPermissionToUpdate;
	}
	public void setIntPermissionToUpdate(int intPermissionToUpdate) {
		this.intPermissionToUpdate = intPermissionToUpdate;
	}
	public int getIntPermissionToDelete() {
		return intPermissionToDelete;
	}
	public void setIntPermissionToDelete(int intPermissionToDelete) {
		this.intPermissionToDelete = intPermissionToDelete;
	}
	public long getLngMenuID() {
		return lngMenuID;
	}
	public void setLngMenuID(long lngMenuID) {
		this.lngMenuID = lngMenuID;
	}
	public String getStrMenuName() {
		return strMenuName;
	}
	public void setStrMenuName(String strMenuName) {
		this.strMenuName = strMenuName;
	}
	public long getLngActivityID() {
		return lngActivityID;
	}
	public void setLngActivityID(long lngActivityID) {
		this.lngActivityID = lngActivityID;
	}
	public String getStrActivityName() {
		return strActivityName;
	}
	public void setStrActivityName(String strActivityName) {
		this.strActivityName = strActivityName;
	}
	public String getStrPageUrl() {
		return strPageUrl;
	}
	public void setStrPageUrl(String strPageUrl) {
		this.strPageUrl = strPageUrl;
	}
	public String getStrImagePath() {
		return strImagePath;
	}
	public void setStrImagePath(String strImagePath) {
		this.strImagePath = strImagePath;
	}
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	public long getLngRoleID() {
		return lngRoleID;
	}
	public void setLngRoleID(long lngRoleID) {
		this.lngRoleID = lngRoleID;
	}
	public long getLngUserID() {
		return lngUserID;
	}
	public void setLngUserID(long lngUserID) {
		this.lngUserID = lngUserID;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public int getIntPermissionToApprove() {
		return intPermissionToApprove;
	}
	public void setIntPermissionToApprove(int intPermissionToApprove) {
		this.intPermissionToApprove = intPermissionToApprove;
	}
	public int getIntIsQuickMenu() {
		return intIsQuickMenu;
	}
	public void setIntIsQuickMenu(int intIsQuickMenu) {
		this.intIsQuickMenu = intIsQuickMenu;
	}
	public int getIntIsHomePage() {
		return intIsHomePage;
	}
	public void setIntIsHomePage(int intIsHomePage) {
		this.intIsHomePage = intIsHomePage;
	}
		
   	
}
