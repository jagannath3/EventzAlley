package com.uthkrushta.mybos.frameWork.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.frameWork.bean.ActivityRoleBindingFrameWorkBean;
import com.uthkrushta.mybos.frameWork.bean.ApplicationMenuBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;





public class ActivityRoleBindingController extends GenericController{
	private ActivityRoleBindingFrameWorkBean objRoleActivityBinding = null;
	private long lngCompanyId =0;
	
	public ActivityRoleBindingController(){
		
		super();
	}
	
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
		if (null != strUserAction) {
			if (strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT)) {
				try {
					try {
						modify(request, response);
					} catch (UTHException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (UMBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		}
	}

	private void modify(HttpServletRequest request, HttpServletResponse response) throws UMBException, UTHException {
		if(null != request.getSession().getAttribute("COMP_ID")){
			lngCompanyId = ((Long)request.getSession().getAttribute("COMP_ID")).longValue();
		}
		ArrayList<Object> arrActivityList = new ArrayList<Object>();
		int intMenuCounter = WebUtil.parseInt(request, ILabelConstants.hdnMenuCounter);
		int intActivityCounter = 0;
		long lngRoleID = WebUtil.parseLong(request, ILabelConstants.ID);
		
		ActivityRoleBindingFrameWorkBean objARB = new ActivityRoleBindingFrameWorkBean();
		String strWhereClause = "";
		String strID = WebUtil.parseString(request, "hdnAssignedActivities");
		strWhereClause = "lngRoleId="+lngRoleID;
		DBUtil.deleteRecordsFromTable(IUMCConstants.BN_ACTIVITY_ROLE_BINDING_BEAN, strWhereClause);
		
		for (int i = 1; i <= intMenuCounter; i++) {
			intActivityCounter = WebUtil.parseInt(request, ILabelConstants.hdnActivityCounter_+i);
			for(int j = 1; j<= intActivityCounter; j++){
				if(WebUtil.parseLong(request, "Child_Menu_"+i+"_"+j) > 0){
					
					objRoleActivityBinding = new ActivityRoleBindingFrameWorkBean(); 
					objRoleActivityBinding.setLngActivityId(WebUtil.parseLong(request, "Child_Menu_"+i+"_"+j));
					objRoleActivityBinding.setIntIsQuickMenu(WebUtil.parseInt(request, "Quick_Menu_"+i+"_"+j));
					objRoleActivityBinding.setIntIsHomePage(WebUtil.parseInt(request, "Home_Menu_"+i+"_"+j));
					objRoleActivityBinding.setLngRoleId(WebUtil.parseLong(request, ILabelConstants.ID));
					objRoleActivityBinding.setIntPermissionToCreate(WebUtil.parseInt(request, "Child_Create_"+i+"_"+j));
					objRoleActivityBinding.setIntPermissionToUpdate(WebUtil.parseInt(request, "Child_Update_"+i+"_"+j));
					objRoleActivityBinding.setIntPermissionToDelete(WebUtil.parseInt(request, "Child_Delete_"+i+"_"+j));
					objRoleActivityBinding.setIntPermissionToApprove(WebUtil.parseInt(request, "Child_Approve_"+i+"_"+j));
					strWhereClause = "lngActivityId="+objRoleActivityBinding.getLngActivityId()+" AND lngRoleId="+objRoleActivityBinding.getLngRoleId();
					objARB = (ActivityRoleBindingFrameWorkBean) DBUtil.getRecord(IUMCConstants.BN_ACTIVITY_ROLE_BINDING_BEAN, strWhereClause, "");	
					//objRoleActivityBinding.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_));
					if(null != objARB){
						objRoleActivityBinding.setLngID(objARB.getLngID());
					}
					//objRoleActivityBinding.setLngUserId(WebUtil.parseLong(request, ILabelConstants.txtUserId_));
					objRoleActivityBinding.setLngCompanyId(lngCompanyId);
					objRoleActivityBinding.setIntStatus(IUMCConstants.STATUS_ACTIVE);
					if (validate(request)) {
						arrActivityList.add(objRoleActivityBinding);
					
				}
			}
			
}
		}	
		boolean blnModified = false;
		try {
			blnModified = DBUtil.modifyRecords(arrActivityList);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Setting left menu to session after saving activity role-binding to reflect the assignment changes
		long lngRoleId=0;
		if(null != request.getSession().getAttribute("ROLE_ID")){
			lngRoleId= ((Long)request.getSession().getAttribute("ROLE_ID")).longValue();
		} 
		List <ApplicationMenuBean> AMList= null;
		AMList=(List<ApplicationMenuBean>) DBUtil.getRecords(IUMCConstants.BN_APPLICATION_MENU_BEAN, "lngRoleID="+lngRoleId, ""); //"lngRoleID="+lngRoleId
		request.getSession().setAttribute(ISessionAttributes.LEFT_TREE_LIST,AMList);
		//Setting left menu to session after saving activity role-binding to reflect the assignment changes
		
		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			AAUtils.addZonesToRefresh(request, "Status,List");
			this.arrMsg.add(msg);
		}
	}

	private void delete(HttpServletRequest request, HttpServletResponse response) throws UMBException {
		boolean blnDeleted = false;
		try {
			blnDeleted = DBUtil.deleteRecords(IUMCConstants.BN_ACTIVITY_ROLE_BINDING_BEAN, WebUtil.parseString(request, ILabelConstants.hdnSelectedID));
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(blnDeleted){
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_ERROR);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
		}
	}
	
	public List<Object> getList(HttpServletRequest request){
		String strWhere = "intStatus="+IUMCConstants.STATUS_ACTIVE;
		Long lngRoleFilter = WebUtil.parseLong(request, ILabelConstants.selRoleFilter);
		int intStatusFilter = 0;
		
		if(null != request.getParameter(ILabelConstants.selStatusFilter)){
			intStatusFilter = WebUtil.parseInt(request, ILabelConstants.selStatusFilter);
		}
		if(intStatusFilter > 0){
			strWhere = "intStatus = " + intStatusFilter;
		}
	
	
		if(lngRoleFilter>0){
			if(strWhere.trim().length() > 0){
				strWhere += " AND lngRoleId = " + lngRoleFilter ;
			}else{ 
				strWhere += " lngRoleId = " + lngRoleFilter;
			}
		}
		
	
		
		
		strWhere = strWhere.replace("*", "%");
		AAUtils.addZonesToRefresh(request, "List");
		return DBUtil.getRecords(IUMCConstants.BN_ACTIVITY_ROLE_BINDING_BEAN,strWhere,"intStatus,lngRoleId");
	}


	

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean deleteRecords() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
}
