package com.uthkrushta.mybos.code.bean;

public class StatusBean {

	
	private String strName;
	private long GenID;
	private String strMob;
	private String strPlace;
	
	private String strCategory;
	private String strBibNo;
	
	
	
	
	
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public long getGenID() {
		return GenID;
	}
	public void setGenID(long genID) {
		GenID = genID;
	}
	public String getStrMob() {
		return strMob;
	}
	public void setStrMob(String strMob) {
		this.strMob = strMob;
	}
	public String getStrPlace() {
		return strPlace;
	}
	public void setStrPlace(String strPlace) {
		this.strPlace = strPlace;
	}
	public String getStrCategory() {
		return strCategory;
	}
	public void setStrCategory(String strCategory) {
		this.strCategory = strCategory;
	}
	public String getStrBibNo() {
		return strBibNo;
	}
	public void setStrBibNo(String strBibNo) {
		this.strBibNo = strBibNo;
	}
	
	
	
	
	
	
	
}
