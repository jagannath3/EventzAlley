package com.uthkrushta.mybos.code.bean;

public class CertificateTypeBean {

	long lngID;
	String strName;
	String strCertificatePath;
	int intStatus;
	
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	
	public String getStrCertificatePath() {
		return strCertificatePath;
	}
	public void setStrCertificatePath(String strCertificatePath) {
		this.strCertificatePath = strCertificatePath;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
	
	
	
}
