package com.uthkrushta.mybos.transaction.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.master.bean.EventTypeTimeMasterBean;
import com.uthkrushta.mybos.transaction.bean.EventLapTimeResultBean;
import com.uthkrushta.mybos.transaction.bean.EventResultBean;
import com.uthkrushta.mybos.transaction.bean.EventTypeRegistrationBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;

public class MemberResultUpdateController extends GenericController {
	EventResultBean objERB = new EventResultBean(); 
	EventLapTimeResultBean objELTRB = new EventLapTimeResultBean();
	EventTypeTimeMasterBean objETTMB = new EventTypeTimeMasterBean();
	EventTypeRegistrationBean objETRB = new EventTypeRegistrationBean();
	
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);//Getting User Action
		if (null != strUserAction) {
			if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
			{//Save objects to database
				modifyRecords(request);
			}
			
		}
		
		
		
		
		
		
		
	}

	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean blnModified = false;
		long userID =  ((Long)  request.getSession().getAttribute("LOGIN_ID")).longValue();
		long memberID = WebUtil.parseLong(request, ILabelConstants.UID);
		long eventID = WebUtil.parseLong(request, ILabelConstants.hdnEventID);
		long eventTypeID = WebUtil.parseLong(request, ILabelConstants.hdnEventTypeIDNew);
		int intRowCounter = 0;
		objETTMB = (EventTypeTimeMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_TYPE_TIME,IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID ="+eventTypeID , "");
		objETRB = (EventTypeRegistrationBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_TYPE_REGISTRATION,IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID ="+eventTypeID+" AND lngMemberID = "+memberID , ""); 
		if(objETTMB == null) {
			objETTMB = new EventTypeTimeMasterBean();
		}
		if(objETRB == null) {
			objETRB = new EventTypeRegistrationBean();
		}
		
		objERB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnResultLngID));
		objERB.setLngEventID(eventID);
		objERB.setLngEventTypeID(eventTypeID);
		objERB.setLngEventTimeID(objETTMB.getLngID());
		objERB.setLngMemberID(memberID);
		objERB.setStrBIBNo(WebUtil.parseString(request, ILabelConstants.txtBibNo));
		objERB.setLngRank(WebUtil.parseLong(request, ILabelConstants.txtRank));
		objERB.setStrChipValue(WebUtil.parseString(request, ILabelConstants.txtCHIP));
		objERB.setStrGunValue(WebUtil.parseString(request, ILabelConstants.txtGUN));
		objERB.setStrPacevalue(WebUtil.parseString(request, ILabelConstants.txtPACE));
		
		
		if(objERB.getLngID() > 0)
		{
			objERB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));      
			objERB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy));
			objERB.setDtUpdatedOn(new Date());
			objERB.setLngUpdatedBy(userID);
		}
		else
		{	
			objERB.setDtCreatedOn(new Date());
			objERB.setLngCreatedBy(userID);
			
		}
		
		
		objERB.setIntStatus(2);
		try {
			blnModified = DBUtil.modifyRecord(objERB);
			if(blnModified) {
				
				blnModified = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_REGISTRATION,
				"strBIBNo = "+objERB.getStrBIBNo(), IUMCConstants.GET_ACTIVE_ROWS+" AND lngMemberID ="+memberID+" AND lngEventTypeID ="+eventTypeID);
				
			}
			
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnItemRowCounter);
		objERB = (EventResultBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_RESULT, IUMCConstants.GET_ACTIVE_ROWS
				+" AND lngMemberID ="+memberID+" AND lngEventTypeID ="+eventTypeID, "");
		for (int intIndex = 0; intIndex < intRowCounter; intIndex++) {
			
			objELTRB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnLapResID_ + intIndex));
			objELTRB.setLngEventresultID(objERB.getLngID());
			objELTRB.setLngEventlapID(WebUtil.parseLong(request, ILabelConstants.hdnLapID_ + intIndex));     
			objELTRB.setStrTime(WebUtil.parseString(request, ILabelConstants.txtTimeTaken_ + intIndex));
			if(objELTRB.getLngID() > 0)
			{
				objELTRB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));  
				objELTRB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy));
				objELTRB.setDtUpdatedOn(new Date());
				objELTRB.setLngUpdatedBy(userID);
			}
			else
			{	
				objELTRB.setDtCreatedOn(new Date());
				objELTRB.setLngCreatedBy(userID);
				
			}
			objELTRB.setIntStatus(2);
			try {
				blnModified = DBUtil.modifyRecord(objELTRB);
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Header,Footer");
			blnModified = true;
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			blnModified =  false;
		}
		return blnModified;
		
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}

}
