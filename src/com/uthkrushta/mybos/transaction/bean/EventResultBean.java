package com.uthkrushta.mybos.transaction.bean;

import java.util.Date;

public class EventResultBean {
	
	private long lngID;
	private long lngEventID;
	private long lngEventTypeID;
	private long lngEventTimeID;
	private long lngMemberID;
	private String strBIBNo;
	private long lngRank;
	private String strChipValue;
	private String strGunValue;
	private String strPacevalue;
	private long lngGroupLeaderID;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public long getLngEventTypeID() {
		return lngEventTypeID;
	}
	public void setLngEventTypeID(long lngEventTypeID) {
		this.lngEventTypeID = lngEventTypeID;
	}
	public long getLngEventTimeID() {
		return lngEventTimeID;
	}
	public void setLngEventTimeID(long lngEventTimeID) {
		this.lngEventTimeID = lngEventTimeID;
	}
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	public String getStrBIBNo() {
		return strBIBNo;
	}
	public void setStrBIBNo(String strBIBNo) {
		this.strBIBNo = strBIBNo;
	}
	public long getLngRank() {
		return lngRank;
	}
	public void setLngRank(long lngRank) {
		this.lngRank = lngRank;
	}
	public String getStrChipValue() {
		return strChipValue;
	}
	public void setStrChipValue(String strChipValue) {
		this.strChipValue = strChipValue;
	}
	public String getStrGunValue() {
		return strGunValue;
	}
	public void setStrGunValue(String strGunValue) {
		this.strGunValue = strGunValue;
	}
	public String getStrPacevalue() {
		return strPacevalue;
	}
	public void setStrPacevalue(String strPacevalue) {
		this.strPacevalue = strPacevalue;
	}
	public long getLngGroupLeaderID() {
		return lngGroupLeaderID;
	}
	public void setLngGroupLeaderID(long lngGroupLeaderID) {
		this.lngGroupLeaderID = lngGroupLeaderID;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
	
	
	
	
	
	

}
