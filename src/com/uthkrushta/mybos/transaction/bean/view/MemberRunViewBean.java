package com.uthkrushta.mybos.transaction.bean.view;


import java.util.Date;

public class MemberRunViewBean {

	
	private long pk_id;
	private long fk_event_id;
	private long fk_event_type_id;
	private long member_id;
	private String lap_name;
	private String lap_order;
	private String distance;
	private long lap_id;
	private String category_name;
	private long lap_result_id;
	private Date time;
	private String category_avg_time;

	public long getPk_id() {
		return pk_id;
	}
	public void setPk_id(long pk_id) {
		this.pk_id = pk_id;
	}
	public long getFk_event_id() {
		return fk_event_id;
	}
	public void setFk_event_id(long fk_event_id) {
		this.fk_event_id = fk_event_id;
	}
	public long getFk_event_type_id() {
		return fk_event_type_id;
	}
	public void setFk_event_type_id(long fk_event_type_id) {
		this.fk_event_type_id = fk_event_type_id;
	}
	public long getMember_id() {
		return member_id;
	}
	public void setMember_id(long member_id) {
		this.member_id = member_id;
	}
	
	
	
	public String getLap_order() {
		return lap_order;
	}
	public void setLap_order(String lap_order) {
		this.lap_order = lap_order;
	}
	
	public long getLap_result_id() {
		return lap_result_id;
	}
	public void setLap_result_id(long lap_result_id) {
		this.lap_result_id = lap_result_id;
	}
	public long getLap_id() {
		return lap_id;
	}
	public void setLap_id(long lap_id) {
		this.lap_id = lap_id;
	}
	public String getLap_name() {
		return lap_name;
	}
	public void setLap_name(String lap_name) {
		this.lap_name = lap_name;
	}
	
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getCategory_name() {
		return category_name;
	}
	
	
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getCategory_avg_time() {
		return category_avg_time;
	}
	public void setCategory_avg_time(String category_avg_time) {
		this.category_avg_time = category_avg_time;
	}
	
}
