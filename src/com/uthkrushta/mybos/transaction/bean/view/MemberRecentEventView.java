package com.uthkrushta.mybos.transaction.bean.view;

import java.util.Date;

public class MemberRecentEventView {

	private long lngID;
	private long lngEventID;
	private long lngEventTypeID;
	private long lngUserID;
	private String strEventName;
	private long lngIsTimed;
	
	
	
	
	private String strEventTypeName;
	private long lngCertificateID;
	private long lngEventTypeStartTime;
	private Date dtEventTypeStartDate;
	
	
	
	
	
	
	private Date dtEventStartDate;
	private Date dtEventEndDate;
	private String strBIBNO;
	private long lngRank;
	private String strChip;
	private String strGun;
	private String strPace;
	private String strPlace;
	
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public long getLngEventTypeID() {
		return lngEventTypeID;
	}
	public void setLngEventTypeID(long lngEventTypeID) {
		this.lngEventTypeID = lngEventTypeID;
	}
	public long getLngUserID() {
		return lngUserID;
	}
	public void setLngUserID(long lngUserID) {
		this.lngUserID = lngUserID;
	}
	public String getStrEventName() {
		return strEventName;
	}
	public void setStrEventName(String strEventName) {
		this.strEventName = strEventName;
	}
	
	public long getLngIsTimed() {
		return lngIsTimed;
	}
	public void setLngIsTimed(long lngIsTimed) {
		this.lngIsTimed = lngIsTimed;
	}
	
	public String getStrEventTypeName() {
		return strEventTypeName;
	}
	public void setStrEventTypeName(String strEventTypeName) {
		this.strEventTypeName = strEventTypeName;
	}
	public long getLngCertificateID() {
		return lngCertificateID;
	}
	public void setLngCertificateID(long lngCertificateID) {
		this.lngCertificateID = lngCertificateID;
	}
	public long getLngEventTypeStartTime() {
		return lngEventTypeStartTime;
	}
	public void setLngEventTypeStartTime(long lngEventTypeStartTime) {
		this.lngEventTypeStartTime = lngEventTypeStartTime;
	}
	public Date getDtEventTypeStartDate() {
		return dtEventTypeStartDate;
	}
	public void setDtEventTypeStartDate(Date dtEventTypeStartDate) {
		this.dtEventTypeStartDate = dtEventTypeStartDate;
	}
	public Date getDtEventStartDate() {
		return dtEventStartDate;
	}
	public void setDtEventStartDate(Date dtEventStartDate) {
		this.dtEventStartDate = dtEventStartDate;
	}
	public Date getDtEventEndDate() {
		return dtEventEndDate;
	}
	public void setDtEventEndDate(Date dtEventEndDate) {
		this.dtEventEndDate = dtEventEndDate;
	}
	public String getStrBIBNO() {
		return strBIBNO;
	}
	public void setStrBIBNO(String strBIBNO) {
		this.strBIBNO = strBIBNO;
	}
	public long getLngRank() {
		return lngRank;
	}
	public void setLngRank(long lngRank) {
		this.lngRank = lngRank;
	}
	public String getStrChip() {
		return strChip;
	}
	public void setStrChip(String strChip) {
		this.strChip = strChip;
	}
	public String getStrGun() {
		return strGun;
	}
	public void setStrGun(String strGun) {
		this.strGun = strGun;
	}
	public String getStrPace() {
		return strPace;
	}
	public void setStrPace(String strPace) {
		this.strPace = strPace;
	}
	public String getStrPlace() {
		return strPlace;
	}
	public void setStrPlace(String strPlace) {
		this.strPlace = strPlace;
	}
	
	
	
	
	
	
	
	
}
