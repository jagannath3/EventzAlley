package com.uthkrushta.mybos.transaction.bean.view;

import java.util.Date;

public class EventMemberListViewBean {

	private long lngID;
	private long lngMemberID;
	private String strFirstName;
	private String strLastName;
	private long lngEventID;
	private String strEventName;
	private String strMemberMobNumber;
	private String strMemberPlace;
	private Date dtMemberDOB;
	private String strMemberEmail;
	private String strUserName;
	private String strPassword;
	
	private String strCategoryName;
	private long lngEventTypeID;
	private String strEventTypeName;
	private Date dtEventStartDate;
	private Date dtEventEndDate; 
	private String strEventPlace;
	private String strBibNo;
	
	private String strRank;
	private String strChip;
	private String strGun;
	private String strPace;
	
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	public String getStrFirstName() {
		return strFirstName;
	}
	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}
	public String getStrLastName() {
		return strLastName;
	}
	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public String getStrEventName() {
		return strEventName;
	}
	public void setStrEventName(String strEventName) {
		this.strEventName = strEventName;
	}
	
	public String getStrMemberMobNumber() {
		return strMemberMobNumber;
	}
	public void setStrMemberMobNumber(String strMemberMobNumber) {
		this.strMemberMobNumber = strMemberMobNumber;
	}
	
	
	public String getStrMemberEmail() {
		return strMemberEmail;
	}
	public void setStrMemberEmail(String strMemberEmail) {
		this.strMemberEmail = strMemberEmail;
	}
	
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public String getStrMemberPlace() {
		return strMemberPlace;
	}
	public void setStrMemberPlace(String strMemberPlace) {
		this.strMemberPlace = strMemberPlace;
	}
	public Date getDtMemberDOB() {
		return dtMemberDOB;
	}
	public void setDtMemberDOB(Date dtMemberDOB) {
		this.dtMemberDOB = dtMemberDOB;
	}
	
	public String getStrCategoryName() {
		return strCategoryName;
	}
	public void setStrCategoryName(String strCategoryName) {
		this.strCategoryName = strCategoryName;
	}
	public long getLngEventTypeID() {
		return lngEventTypeID;
	}
	public void setLngEventTypeID(long lngEventTypeID) {
		this.lngEventTypeID = lngEventTypeID;
	}
	public String getStrEventTypeName() {
		return strEventTypeName;
	}
	public void setStrEventTypeName(String strEventTypeName) {
		this.strEventTypeName = strEventTypeName;
	}
	public Date getDtEventStartDate() {
		return dtEventStartDate;
	}
	public void setDtEventStartDate(Date dtEventStartDate) {
		this.dtEventStartDate = dtEventStartDate;
	}
	public Date getDtEventEndDate() {
		return dtEventEndDate;
	}
	public void setDtEventEndDate(Date dtEventEndDate) {
		this.dtEventEndDate = dtEventEndDate;
	}
	public String getStrEventPlace() {
		return strEventPlace;
	}
	public void setStrEventPlace(String strEventPlace) {
		this.strEventPlace = strEventPlace;
	}
	public String getStrBibNo() {
		return strBibNo;
	}
	public void setStrBibNo(String strBibNo) {
		this.strBibNo = strBibNo;
	}
	public String getStrRank() {
		return strRank;
	}
	public void setStrRank(String strRank) {
		this.strRank = strRank;
	}
	public String getStrChip() {
		return strChip;
	}
	public void setStrChip(String strChip) {
		this.strChip = strChip;
	}
	public String getStrGun() {
		return strGun;
	}
	public void setStrGun(String strGun) {
		this.strGun = strGun;
	}
	public String getStrPace() {
		return strPace;
	}
	public void setStrPace(String strPace) {
		this.strPace = strPace;
	}
	
	
	
}
