package com.uthkrushta.mybos.transaction.bean.view;

import java.util.Date;

public class MemberUpcomingEventView {

	private long lngID;
	private long lngUserID;
	private String strBIBNO;
	private String strBIBName;
	private long lngEventTypeID;
	private long lngEventID;
	private String strEventName;
	private Date dtStartDate;
	private Date dtEndDate;
	private String strPlace;
	
	private String strEventTypeName;
	private Date dtEventTypeStartDate;
	private long lngEventTimeID;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngUserID() {
		return lngUserID;
	}
	public void setLngUserID(long lngUserID) {
		this.lngUserID = lngUserID;
	}
	public String getStrBIBNO() {
		return strBIBNO;
	}
	public void setStrBIBNO(String strBIBNO) {
		this.strBIBNO = strBIBNO;
	}
	public String getStrBIBName() {
		return strBIBName;
	}
	public void setStrBIBName(String strBIBName) {
		this.strBIBName = strBIBName;
	}
	public long getLngEventTypeID() {
		return lngEventTypeID;
	}
	public void setLngEventTypeID(long lngEventTypeID) {
		this.lngEventTypeID = lngEventTypeID;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public String getStrEventName() {
		return strEventName;
	}
	public void setStrEventName(String strEventName) {
		this.strEventName = strEventName;
	}
	public Date getDtStartDate() {
		return dtStartDate;
	}
	public void setDtStartDate(Date dtStartDate) {
		this.dtStartDate = dtStartDate;
	}
	public Date getDtEndDate() {
		return dtEndDate;
	}
	public void setDtEndDate(Date dtEndDate) {
		this.dtEndDate = dtEndDate;
	}
	public String getStrPlace() {
		return strPlace;
	}
	public void setStrPlace(String strPlace) {
		this.strPlace = strPlace;
	}
	public String getStrEventTypeName() {
		return strEventTypeName;
	}
	public void setStrEventTypeName(String strEventTypeName) {
		this.strEventTypeName = strEventTypeName;
	}
	public Date getDtEventTypeStartDate() {
		return dtEventTypeStartDate;
	}
	public void setDtEventTypeStartDate(Date dtEventTypeStartDate) {
		this.dtEventTypeStartDate = dtEventTypeStartDate;
	}
	public long getLngEventTimeID() {
		return lngEventTimeID;
	}
	public void setLngEventTimeID(long lngEventTimeID) {
		this.lngEventTimeID = lngEventTimeID;
	}
	
	
	
	
	
	
	
	
	
	
	
}
