package com.uthkrushta.mybos.transaction.bean;

import java.util.Date;

public class EventLapTimeResultBean {

	private long lngID;
	private long lngEventresultID;
	private long lngEventlapID;
	private String strTime;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngEventresultID() {
		return lngEventresultID;
	}
	public void setLngEventresultID(long lngEventresultID) {
		this.lngEventresultID = lngEventresultID;
	}
	public long getLngEventlapID() {
		return lngEventlapID;
	}
	public void setLngEventlapID(long lngEventlapID) {
		this.lngEventlapID = lngEventlapID;
	}
	
	public String getStrTime() {
		return strTime;
	}
	public void setStrTime(String strTime) {
		this.strTime = strTime;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
	
	
	
	
}
