package com.uthkrushta.mybos.supreme.control;

import java.util.Date;

public class EntityMasterSupremeControl {
	long lngID;
	String strGymCode;
	String strUrl;
	String strDriverClass;
	String strUserName;
	String strPassword;
	long lngPoolSize;
	long lngPlanID; 
	Date dtPlanStartDate;
	Date dtPlanEndDate;
	String strContactNumber;
	String strContactPersonName;
	String strEmailID;
	int intIsActive;
	long lngGracePeriod;
	String strMessage;
	long lngMessageType;//(error,alert,success)
	String strDbName;
	int intIsLogout;
	
	
	//Added for GenericMsg;
	int intGenericMsg;
	int intSpecificMsg;
	
	public String getStrDbName() {
		return strDbName;
	}
	public void setStrDbName(String strDbName) {
		this.strDbName = strDbName;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrGymCode() {
		return strGymCode;
	}
	public void setStrGymCode(String strGymCode) {
		this.strGymCode = strGymCode;
	}
	public String getStrUrl() {
		return strUrl;
	}
	public void setStrUrl(String strUrl) {
		this.strUrl = strUrl;
	}
	public String getStrDriverClass() {
		return strDriverClass;
	}
	public void setStrDriverClass(String strDriverClass) {
		this.strDriverClass = strDriverClass;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public long getLngPoolSize() {
		return lngPoolSize;
	}
	public void setLngPoolSize(long lngPoolSize) {
		this.lngPoolSize = lngPoolSize;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public Date getDtPlanStartDate() {
		return dtPlanStartDate;
	}
	public void setDtPlanStartDate(Date dtPlanStartDate) {
		this.dtPlanStartDate = dtPlanStartDate;
	}
	public Date getDtPlanEndDate() {
		return dtPlanEndDate;
	}
	public void setDtPlanEndDate(Date dtPlanEndDate) {
		this.dtPlanEndDate = dtPlanEndDate;
	}
	public String getStrContactNumber() {
		return strContactNumber;
	}
	public void setStrContactNumber(String strContactNumber) {
		this.strContactNumber = strContactNumber;
	}
	public String getStrContactPersonName() {
		return strContactPersonName;
	}
	public void setStrContactPersonName(String strContactPersonName) {
		this.strContactPersonName = strContactPersonName;
	}
	public String getStrEmailID() {
		return strEmailID;
	}
	public void setStrEmailID(String strEmailID) {
		this.strEmailID = strEmailID;
	}
	public int getIntIsActive() {
		return intIsActive;
	}
	public void setIntIsActive(int intIsActive) {
		this.intIsActive = intIsActive;
	}
	public long getLngGracePeriod() {
		return lngGracePeriod;
	}
	public void setLngGracePeriod(long lngGracePeriod) {
		this.lngGracePeriod = lngGracePeriod;
	}
	public String getStrMessage() {
		return strMessage;
	}
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	public long getLngMessageType() {
		return lngMessageType;
	}
	public void setLngMessageType(long lngMessageType) {
		this.lngMessageType = lngMessageType;
	}
	public int getIntIsLogout() {
		return intIsLogout;
	}
	public void setIntIsLogout(int intIsLogout) {
		this.intIsLogout = intIsLogout;
	}
	public int getIntGenericMsg() {
		return intGenericMsg;
	}
	public void setIntGenericMsg(int intGenericMsg) {
		this.intGenericMsg = intGenericMsg;
	}
	public int getIntSpecificMsg() {
		return intSpecificMsg;
	}
	public void setIntSpecificMsg(int intSpecificMsg) {
		this.intSpecificMsg = intSpecificMsg;
	}
	
	
	

}
