package com.uthkrushta.mybos.master.bean;

public class CityMasterBean {
	
	
	private long lngID;
	private long lngStateID;
	private String strName;
	private int intStatus;
	
	public CityMasterBean() {
		super();
		// TODO Auto-generated constructor stub
		strName = "";
	}
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngStateID() {
		return lngStateID;
	}
	public void setLngStateID(long lngStateID) {
		this.lngStateID = lngStateID;
	}
	
	
	

}
