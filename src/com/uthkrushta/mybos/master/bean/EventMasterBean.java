package com.uthkrushta.mybos.master.bean;

import java.util.Date;

public class EventMasterBean {

	private long lngID;
	private String strName;
	private Date dtStartDate;
	private Date dtEndDate; 
	private String strPlace;
	private long lngCountryID;
	private long lngStateID;
	private long lngAreaID;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	private int intIsCompleted;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public Date getDtStartDate() {
		return dtStartDate;
	}
	public void setDtStartDate(Date dtStartDate) {
		this.dtStartDate = dtStartDate;
	}
	
	public Date getDtEndDate() {
		return dtEndDate;
	}
	public void setDtEndDate(Date dtEndDate) {
		this.dtEndDate = dtEndDate;
	}
	public String getStrPlace() {
		return strPlace;
	}
	public void setStrPlace(String strPlace) {
		this.strPlace = strPlace;
	}
	public long getLngCountryID() {
		return lngCountryID;
	}
	public void setLngCountryID(long lngCountryID) {
		this.lngCountryID = lngCountryID;
	}
	public long getLngStateID() {
		return lngStateID;
	}
	public void setLngStateID(long lngStateID) {
		this.lngStateID = lngStateID;
	}
	public long getLngAreaID() {
		return lngAreaID;
	}
	public void setLngAreaID(long lngAreaID) {
		this.lngAreaID = lngAreaID;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public int getIntIsCompleted() {
		return intIsCompleted;
	}
	public void setIntIsCompleted(int intIsCompleted) {
		this.intIsCompleted = intIsCompleted;
	}
	
	
	
	
	
	
	
	
}
