package com.uthkrushta.mybos.master.bean;

public class AreaMasterBean {
 
	private long lngID;
	private long lngCityID;
	private String strName;
	private String strPincode;
	private int intStatus;
	
	
	public AreaMasterBean() {
		// TODO Auto-generated constructor stub
	   this.strName=this.strPincode="" ;
	}
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	
	
	
	public long getLngCityID() {
		return lngCityID;
	}
	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}

	public String getStrPincode() {
		return strPincode;
	}

	public void setStrPincode(String strPincode) {
		this.strPincode = strPincode;
	}
	
	
}
