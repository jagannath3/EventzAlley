package com.uthkrushta.mybos.master.bean;

import java.util.Date;

public class EventTypeMasterBean {
	
	private long lngID;
	private String strName;
	private String strIdentificationName;
	private long lngEventID;
	private long lngCertificateTypeID;
	private long lngIsTimed;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	private long lngTotalDistance;
	
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	
	public String getStrIdentificationName() {
		return strIdentificationName;
	}
	public void setStrIdentificationName(String strIdentificationName) {
		this.strIdentificationName = strIdentificationName;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	
	public long getLngCertificateTypeID() {
		return lngCertificateTypeID;
	}
	public void setLngCertificateTypeID(long lngCertificateTypeID) {
		this.lngCertificateTypeID = lngCertificateTypeID;
	}
	
	public long getLngIsTimed() {
		return lngIsTimed;
	}
	public void setLngIsTimed(long lngIsTimed) {
		this.lngIsTimed = lngIsTimed;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngTotalDistance() {
		return lngTotalDistance;
	}
	public void setLngTotalDistance(long lngTotalDistance) {
		this.lngTotalDistance = lngTotalDistance;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
