package com.uthkrushta.mybos.master.bean;

public class StateMasterBean {

	private long lngID;
	private String strName;
	private long lngCountryID;
	private int intStatus;
	private String strStateCode;
	
	
	
	
	
	public StateMasterBean() {
		super();
		// TODO Auto-generated constructor stub
		strName = strStateCode= "";
	}
	
	
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public long getLngCountryID() {
		return lngCountryID;
	}
	public void setLngCountryID(long lngCountryID) {
		this.lngCountryID = lngCountryID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}



	public String getStrStateCode() {
		return strStateCode;
	}



	public void setStrStateCode(String strStateCode) {
		this.strStateCode = strStateCode;
	}
	
	
	
}
