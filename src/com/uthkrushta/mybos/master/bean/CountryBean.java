package com.uthkrushta.mybos.master.bean;

public class CountryBean {
	private long lngID;
	private String strName;
	private int intStatus;
	
	
	public CountryBean() {
		super();
		// TODO Auto-generated constructor stub
		strName = "";
		
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
}
