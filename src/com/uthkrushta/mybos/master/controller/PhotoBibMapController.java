package com.uthkrushta.mybos.master.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.PaginationBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;

public class PhotoBibMapController extends GenericController {
 
	EventPhotoUploadBean objEPUB = new EventPhotoUploadBean();
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);//Getting User Action
		if (null != strUserAction) {
			if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT) || strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SAVE_AND_NEXT))
			{//Save objects to database
				modifyRecords(request);
			}else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_PUBLISH))
			{//Save objects to database
				publishRecords(request);
			}
			
		}
		
	}

	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean modified = false;
		int intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnRowCounter);
		long eventID = WebUtil.parseLong(request, ILabelConstants.selEventName);
		
		for(int i = 1; i <= intRowCounter; i++) {
		
		long lngID = WebUtil.parseLong(request, ILabelConstants.hdnID_ + i);
		String imgBibNo = WebUtil.parseString(request, ILabelConstants.txtIMAGE_BIB_NUM_ + i);
		
		
		
		String imgDescription =  WebUtil.parseString(request, ILabelConstants.txtIMAGE_DESCRIPTION_ + i);
		String updateClause = " strImageDescription = '"+imgDescription+"', strBibNoEntry = '"+imgBibNo+"'";
		
			try {
				modified = DBUtil.updateRecords(ILabelConstants.BN_EVENT_PHOTO_UPLOAD, updateClause , "lngID = "+lngID);
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (modified) {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer");
				modified = true;
			}else{
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				modified =  false;
			}
		
		}
		return false;
	}
	
	
	protected boolean publishRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean modified = false;
		int intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnRowCounter);
		long eventID = WebUtil.parseLong(request, ILabelConstants.selEventName);
		
		for(int i = 1; i <= intRowCounter; i++) {
		
		long lngID = WebUtil.parseLong(request, ILabelConstants.hdnID_ + i);
		String imgBibNo = WebUtil.parseString(request, ILabelConstants.txtIMAGE_BIB_NUM_ + i);
		String imgDescription =  WebUtil.parseString(request, ILabelConstants.txtIMAGE_DESCRIPTION_ + i);
		
		/*if(imgBibNo.trim().length() > 0) {
			imgBibNo = "," + imgBibNo + ",";
		}
		
		imgBibNo = imgBibNo.replace(",,",",");*/
		String updateClause = " strImageDescription = '"+imgDescription+"', strBibNoEntry = '"+imgBibNo+"'";
		
		
			try {
				modified = DBUtil.updateRecords(ILabelConstants.BN_EVENT_PHOTO_UPLOAD, updateClause , "lngID = "+lngID);
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
		}
		if(modified) {
			String updateClause = " intPublish = 1";
			String where = " lngEventID = "+eventID+" AND strBibNoEntry <> '' ";
			try {
				modified = DBUtil.updateRecords(ILabelConstants.BN_EVENT_PHOTO_UPLOAD, updateClause , where);
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		if (modified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_PUBLISHED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			modified = true;
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_PUBLISHED_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			modified =  false;
		}
		return false;
	}
	
	
	
	
	
	
	

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		String strWhere = buildQueryString(request);
		int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
        int intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
        
        if(intNumberOfRecords == 0){
            intNumberOfRecords = 10;
        }
        if(intCurrentPage == 0){
            intCurrentPage = 1;
        }
	
        PaginationBean objPaginationBean = new PaginationBean();
        objPaginationBean.setIntNumberOfRecords(intNumberOfRecords);
        objPaginationBean.setIntStartingCount((intCurrentPage-1)*intNumberOfRecords);
       long  eventID = WebUtil.parseLong(request, ILabelConstants.selEventName);
		if(eventID > 0){
			/*String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS + " AND (strBibNoEntry = '' OR strBibNoEntry IS NULL ) AND lngEventID = "+eventID;*/
			String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS + " AND lngEventID = "+eventID;
			
			return DBUtil.getRecords(ILabelConstants.BN_EVENT_PHOTO_UPLOAD, strWhereClause, "", objPaginationBean);
		}else {
			return null;
		}
        
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		 long  eventID = WebUtil.parseLong(request, ILabelConstants.selEventName);
		 String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS + " AND lngEventID = "+eventID;
		
		return DBUtil.getRecordsCount(ILabelConstants.BN_EVENT_PHOTO_UPLOAD, strWhereClause);
	}

}
