package com.uthkrushta.mybos.master.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.master.bean.EventMasterBean;
import com.uthkrushta.mybos.master.bean.EventTypeMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;

public class EventListController extends GenericController {
 
	EventMasterBean objEMB = new EventMasterBean();
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
		if (null != strUserAction) {
			if (strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL)) {
				deleteRecords(request);
			
			
		}
	}

	}	
		
		
		protected boolean deleteRecords(HttpServletRequest request) {
			EventTypeMasterBean objETMB = new EventTypeMasterBean();
			List lstTypeID=null;
			boolean blnDeleted = false;
			boolean blnUpdated = false;
			String strSelectedTypeID = ";";
			
			String strSelectedID = WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
			String selectedID = strSelectedID.replace(";", ",");
			selectedID = selectedID.substring(1, selectedID.length()-1);
			
			lstTypeID = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, " lngEventID IN ("+selectedID+")", "");
			if(lstTypeID != null && lstTypeID.size()>0) {
				for(int i = 1; i<= lstTypeID.size(); i++) {
					objETMB = (EventTypeMasterBean) lstTypeID.get(i-1);
					strSelectedTypeID = strSelectedTypeID+objETMB.getLngID()+";";
				}
				strSelectedTypeID = strSelectedTypeID.replace(";", ",");
				strSelectedTypeID = strSelectedTypeID.substring(1, strSelectedTypeID.length()-1);	
				
			}
			
					
			
			try {
				blnDeleted = DBUtil.updateRecords(ILabelConstants.BN_EVENT_MASTER, "intStatus = 4" ,"lngID IN ("+selectedID+")" );
				if(blnDeleted) {
					try {
						blnUpdated = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, "intStatus = 4" ,"lngEventID IN ("+selectedID+")" );
					if(blnUpdated) {
						try {
							blnUpdated = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_TIME, "intStatus = 4" ,"lngEventID IN ("+selectedID+")");
						if(blnUpdated) {
							








							try {
								blnUpdated = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_LAP,  "intStatus = 4", 
										"lngEventTypeID IN ("+strSelectedTypeID+")");
									if(blnDeleted) {


							
							msg = new MessageBean();
							msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
							msg.setStrMsgType(IUMCConstants.OK);
							msg.setBlnIsLeadingMessage(true);
							this.arrMsg.add(msg);
							AAUtils.addZonesToRefresh(request, "Status,List,Footer,AmountSection");
							return true;
						}
						
						
						}catch (UTHException e) {
						// TODO Auto-generated catch block
						msg = new MessageBean();
						msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
						msg.setStrMsgType(IUMCConstants.ERROR);
						msg.setBlnIsLeadingMessage(true);
						this.arrMsg.add(msg);
						AAUtils.addZonesToRefresh(request, "Status");
						e.printStackTrace();
						return false;
					}
					}
					
					}catch (UTHException e) {
						// TODO Auto-generated catch block
						msg = new MessageBean();
						msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
						msg.setStrMsgType(IUMCConstants.ERROR);
						msg.setBlnIsLeadingMessage(true);
						this.arrMsg.add(msg);
						AAUtils.addZonesToRefresh(request, "Status");
						e.printStackTrace();
						return false;
					}
					}}catch (UTHException e) {
						// TODO Auto-generated catch block
						msg = new MessageBean();
						msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
						msg.setStrMsgType(IUMCConstants.ERROR);
						msg.setBlnIsLeadingMessage(true);
						this.arrMsg.add(msg);
						AAUtils.addZonesToRefresh(request, "Status");
						e.printStackTrace();
						return false;
					}
				
					
					if(blnDeleted) {


						
						msg = new MessageBean();
						msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
						msg.setStrMsgType(IUMCConstants.OK);
						msg.setBlnIsLeadingMessage(true);
						this.arrMsg.add(msg);
						AAUtils.addZonesToRefresh(request, "Status,List,Footer,AmountSection");
						return true;
					}
					
				
				}
				
			
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				e.printStackTrace();
				return false;
			}
			
			return blnDeleted;
		}
		
		
		
		
		
		
		
		
		
	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		List lstEvents =(List) DBUtil.getRecords(ILabelConstants.BN_EVENT_MASTER, IUMCConstants.GET_ACTIVE_ROWS, "");
		return lstEvents;
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		
		return null;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}

}
