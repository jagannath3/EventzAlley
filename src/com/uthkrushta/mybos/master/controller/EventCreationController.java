package com.uthkrushta.mybos.master.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.mybos.master.bean.EventMasterBean;
import com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean;
import com.uthkrushta.mybos.master.bean.EventTypeMasterBean;
import com.uthkrushta.mybos.master.bean.EventTypeTimeMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;

public class EventCreationController extends GenericController{
	int intRowCounter = 0;
	EventMasterBean objEMB = new EventMasterBean();
	EventTypeMasterBean objETMB = new EventTypeMasterBean();
	EventTypeTimeMasterBean objETTMB = new EventTypeTimeMasterBean();
	EventTypeLapMasterBean objETLMB = new EventTypeLapMasterBean();
	
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
		if (null != strUserAction) {
			if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
			{//Save objects to database
				modifyRecord(request);
			}
			else if (strUserAction.equalsIgnoreCase(ILabelConstants.SAVE_SPLIT)) {
				modifySplitRecord(request);
			}else if (strUserAction.equalsIgnoreCase(ILabelConstants.ACTION_DELETE_SPLIT_ITEM)) {
				deleteSplitItems(request, ILabelConstants.hdnSelectedItemID);
			}
			
			
			else if (strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DELETE_ITEM)) {
				deleteItems(request, ILabelConstants.hdnSelectedItemID);
			} else if (strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL)) {
				deleteRecords(request);
			}
			
		}
		
		
		
		
		
	}

	
	
	
	
	
	
	
	private boolean deleteSplitItems(HttpServletRequest request, String hdnSelectedID) {
		boolean blnDeleted;
	

			try {
				String strClause = WebUtil.parseString(request, ILabelConstants.hdnSelectedItemIDSplit);
				blnDeleted = DBUtil.deleteRecords(ILabelConstants.BN_EVENT_TYPE_LAP,strClause
						);
				if (blnDeleted) {
					msg = new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
					msg.setStrMsgType(IUMCConstants.OK);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status,List,Footer,AmountSection");
					return true;
			
				}} catch (UTHException e) {
				// TODO Auto-generated catch block
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				e.printStackTrace();
				return false;
				}
			
		return blnDeleted;

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	protected boolean modifySplitRecord(HttpServletRequest request) {
		boolean blnModified = false;
		long userID = ((Long) request.getSession().getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
		intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnItemRowCounterSplit);
		
		
		for (int intIndex = 1; intIndex <= intRowCounter; intIndex++) {
		
			if(WebUtil.parseLong(request, ILabelConstants.SPLIT_ORDER_ +intIndex) == 0 ) {
				continue;
			}
			
			
			objETLMB.setLngID(WebUtil.parseLong(request, ILabelConstants.lngSplitID_ +intIndex));
			objETLMB.setLngEventTypeID(WebUtil.parseLong(request, ILabelConstants.hdnEventTypeID));
			objETLMB.setStrName(WebUtil.parseString(request, ILabelConstants.SPLIT_NAME_ +intIndex));
			objETLMB.setStrdistance(WebUtil.parseString(request, ILabelConstants.SPLIT_LABEL_NAME_ +intIndex));
			objETLMB.setLngLapOrder(WebUtil.parseLong(request, ILabelConstants.SPLIT_ORDER_ +intIndex));
			objETLMB.setIntStatus(2);
			
			
		if(objETLMB.getLngID() > 0)
		{	
			objETLMB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy));
			objETLMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DATE_FORMAT));
			objETLMB.setDtUpdatedOn(new Date());
			objETLMB.setLngUpdatedBy(userID);
		}
		else
		{	
			objETLMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));
			objETLMB.setLngCreatedBy(userID);
			objETLMB.setDtUpdatedOn(new Date());
			objETLMB.setLngUpdatedBy(userID);
		}
		
		if(validate(request)) {
		try {
			blnModified = DBUtil.modifyRecord(objETLMB);
			
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_ERROR);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			blnModified=false;
			break;
		}
		
		}else {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED+"\" Lap order "+objETLMB.getLngLapOrder()+"\""+ ITextConstants.MSG_DUPLICATE_ENTRY);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			blnModified=false;
			break;
			
		}
		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,SplitList,List,Footer");
		} else {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_ERROR);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);

		}
		
		
		
		
		}
		
		
		return blnModified;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		boolean blnModified = false;
		long userID = ((Long) request.getSession().getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
		intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnItemRowCounter);
		objEMB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID));
		objEMB.setStrName(WebUtil.parseString(request, ILabelConstants.EventName));
		objEMB.setIntIsCompleted(WebUtil.parseInt(request, ILabelConstants.IsEventCompleted));
		objEMB.setStrPlace(WebUtil.parseString(request, ILabelConstants.EventPlaceName));
		objEMB.setDtStartDate(WebUtil.parseDate(request, ILabelConstants.START_DATE, IUMCConstants.DATE_FORMAT));
		objEMB.setDtEndDate(WebUtil.parseDate(request, ILabelConstants.END_DATE, IUMCConstants.DATE_FORMAT));
		objEMB.setIntStatus(2);
		if(objEMB.getLngID() > 0)
		{	objEMB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy));
			objEMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DATE_FORMAT));
			objEMB.setDtUpdatedOn(new Date());
			objEMB.setLngUpdatedBy(userID);
		}
		else
		{	
			objEMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));
			objEMB.setLngCreatedBy(userID);
			objEMB.setDtUpdatedOn(new Date());
			objEMB.setLngUpdatedBy(userID);
		}
		
		try {
			blnModified = DBUtil.modifyRecord(objEMB);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int intIndex = 1; intIndex <= intRowCounter; intIndex++) {
		
		String strIdentificationNmae = WebUtil.parseString(request, ILabelConstants.EventName)+"-"+WebUtil.parseString(request, ILabelConstants.txtEventTypeName_+ intIndex);
			
		objETMB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + intIndex));
		objETMB.setLngEventID(objEMB.getLngID());
		objETMB.setStrName(WebUtil.parseString(request, ILabelConstants.txtEventTypeName_+ intIndex));
		objETMB.setStrIdentificationName(strIdentificationNmae);
		
		objETMB.setLngTotalDistance(WebUtil.parseLong(request, ILabelConstants.TotalDistance_+ intIndex));
		objETMB.setLngCertificateTypeID(WebUtil.parseLong(request, ILabelConstants.txtCertificateType_+ intIndex));
		objETMB.setLngIsTimed(WebUtil.parseLong(request, ILabelConstants.chkIsTimed_ +intIndex));
		if(objETMB.getLngIsTimed() == 0 && objETMB.getLngID() > 0) {
			try {
				DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_LAP, "intStatus = 4", "lngEventTypeID = "+objETMB.getLngID());
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		objETMB.setIntStatus(2);
		if(objETMB.getLngID() > 0)
		{	
			objETMB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy));
			objETMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DATE_FORMAT));
			objETMB.setDtUpdatedOn(new Date());
			objETMB.setLngUpdatedBy(userID);
		}
		else
		{	
			objETMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));
			objETMB.setLngCreatedBy(userID);
			objETMB.setDtUpdatedOn(new Date());
			objETMB.setLngUpdatedBy(userID);
		}
		
		
		try {
			blnModified = DBUtil.modifyRecord(objETMB);
			objETTMB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnTimeID_+intIndex));
			objETTMB.setLngEventTypeID(objETMB.getLngID());
			objETTMB.setLngEventID(objEMB.getLngID());
			objETTMB.setLngStartTimeID(WebUtil.parseLong(request, ILabelConstants.START_TIME_+ intIndex));
			objETTMB.setDtStartDate(WebUtil.parseDate(request, ILabelConstants.START_DATE_+ intIndex, IUMCConstants.DATE_FORMAT));
			objETTMB.setIntStatus(2);
			if(objETTMB.getLngID() > 0)
			{
				objETTMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon_+ intIndex, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));
				objETTMB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy_+ intIndex));
				objETTMB.setDtUpdatedOn(new Date());
				objETTMB.setLngUpdatedBy(userID);
			}
			else
			{	
				objETTMB.setDtCreatedOn(new Date());
				objETTMB.setLngCreatedBy(userID);
				objETTMB.setDtUpdatedOn(new Date());
				objETTMB.setLngUpdatedBy(userID);
			}
			
			
			blnModified = DBUtil.modifyRecord(objETTMB);
		
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		}
		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,header,List,Footer");
		} else {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_ERROR);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,header,List,Footer");

		}
		
		return blnModified;
	}
	
	private boolean deleteItems(HttpServletRequest request, String hdnSelectedID) {
		boolean blnDeleted;
		try {
			
			long eventID = WebUtil.parseLong(request, ILabelConstants.hdnID);
			String eventTypeID = WebUtil.parseString(request, ILabelConstants.hdnSelectedItemID);
			eventTypeID = eventTypeID.substring(1, eventTypeID.length()-1);
			try {
			blnDeleted = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_TIME, "intStatus = 4" ,"lngEventID = "+eventID+" AND lngEventTypeID = "+eventTypeID);
			}catch (UTHException ex) {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				ex.printStackTrace();
				return false;

			}
			if(blnDeleted) {
				blnDeleted = DBUtil.deleteRecords(ILabelConstants.BN_EVENT_TYPE_MASTER,
						WebUtil.parseString(request, ILabelConstants.hdnSelectedItemID));
			
			}
			
			if(blnDeleted) {
				
				blnDeleted = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_LAP, "intStatus = 4", " lngEventTypeID = "+eventTypeID );
			}
			
			if (blnDeleted) {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer,AmountSection");
				return true;
			}
		} catch (UTHException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			ex.printStackTrace();
			return false;

		}
		return blnDeleted;

	}

	protected boolean deleteRecords(HttpServletRequest request) {
		boolean blnDeleted = false;
		String strSelectedID = WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
		
		long eventID = WebUtil.parseLong(request, ILabelConstants.hdnID);
	
		String selectedID = strSelectedID.replace(";", ",");
		selectedID = selectedID.substring(1, selectedID.length()-1);
		
		try {
			blnDeleted = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_TIME, "intStatus = 4" ,"lngEventID = "+eventID+" AND lngEventTypeID IN ("+selectedID+")" );
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			e.printStackTrace();
			return false;
		}
		
		
		
		
		try {

			blnDeleted = DBUtil.deleteRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, strSelectedID);
			if (blnDeleted) {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer");
				return true;
			}
		} catch (UTHException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			ex.printStackTrace();
			return false;

		}
		
		return blnDeleted;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	protected boolean validate(HttpServletRequest request) {
		NameValueBean objNameValueBean = null;
		ArrayList<NameValueBean> arrNameValueList=new ArrayList<NameValueBean>();
		
		if(null!=objETLMB)
		{
			objNameValueBean=new NameValueBean("lngID", ""+objETLMB.getLngID(), true);
			arrNameValueList.add(objNameValueBean);
			
			objNameValueBean=new NameValueBean("lngEventTypeID", ""+objETLMB.getLngEventTypeID(), false);
			arrNameValueList.add(objNameValueBean);
			
			objNameValueBean=new NameValueBean("lngLapOrder", ""+objETLMB.getLngLapOrder(), false);
			arrNameValueList.add(objNameValueBean);
			
			return DBUtil.checkIsUnique(ILabelConstants.BN_EVENT_TYPE_LAP, ILabelConstants.BN_EVENT_TYPE_LAP, arrNameValueList);
		}
		else
		{
		return true;
		}
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		if(null == request) {
			return objEMB;
		}else {
			//take hdnID
			long eventID = WebUtil.parseLong(request, ILabelConstants.hdnID);
			//get the obj from db using hdnID
			objEMB =(EventMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_MASTER, " lngID = "+eventID, "");
			return objEMB;
		}
		
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	
	
	
}
