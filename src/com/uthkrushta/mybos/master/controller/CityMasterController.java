package com.uthkrushta.mm.master.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ajaxanywhere.AAUtils;
import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mm.master.bean.AreaMasterBean;
import com.uthkrushta.mm.master.bean.CityMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.VetoMessage;
import com.uthkrushta.utils.WebUtil;

public class  CityMasterController	 extends GenericController {
 CityMasterBean objCityBean = new CityMasterBean();
	long lngCityID= 0;
	   boolean blnModified = false;
	   public boolean modifyUser(HttpServletRequest request){
			// logic to store the values
		   int Index = 1;
			// Fill object from UI
			objCityBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + Index));
			objCityBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + Index));
			objCityBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
			
			// delegate the call to framework
			try {
				blnModified = new DBUtil(request).modifyRecord(objCityBean);
			} catch (UMBException e) {
				// TODO Auto-generated catch block
				((Throwable) e).printStackTrace();
				blnModified = false;
			}
			return blnModified;
		}

		@SuppressWarnings("unchecked")
		public List<Object> getList(HttpServletRequest request)
		{
			String strWhere = buildQueryString(request);
			int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
		/*	int intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
			
			if(intNumberOfRecords == 0){
				intNumberOfRecords = 10;
			}
			if(intCurrentPage == 0){
				intCurrentPage = 1;
			}
			
			PaginationBean objPaginationBean = new PaginationBean ();
			objPaginationBean.setIntNumberOfRecords(intNumberOfRecords);
			objPaginationBean.setIntStartingCount((intCurrentPage-1)*intNumberOfRecords);*/
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			return new DBUtil(request).getRecords(IUMCConstants.BN_CITY_MASTER,strWhere, "strName");
		}

		@Override
		public void doAction(HttpServletRequest request, HttpServletResponse response) {
			String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
			if (null != strUserAction) {
					if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT)){
						modifyRecords(request);}  //
					else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL)){ 
						deleteRecords1(request);}
					else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD)){ 
						AAUtils.addZonesToRefresh(request, "List,Footer");}
				}
			}

		@Override
		protected boolean modifyRecord(HttpServletRequest request) {
			// TODO Auto-generated method stub
		boolean blnModified = false;
		int Index = 1;
			
			objCityBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + Index));
			objCityBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + Index));
			// delegate the call to framework
				try {
					blnModified = new DBUtil(request).modifyRecord(objCityBean);
				} catch (UMBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return blnModified;
		}

		@Override
		protected boolean modifyRecords(HttpServletRequest request) {
			// TODO Auto-generated method stub
			boolean blnModified = false;
			ArrayList<Object> arrCityMasterList = new ArrayList<Object>();
			int intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnRowCounter);
			
			for (int i = 1; i <= intRowCounter; i++) {
				objCityBean = new CityMasterBean(); 
				objCityBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + i));
				objCityBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + i));
				objCityBean.setLngStateID(WebUtil.parseLong(request, ILabelConstants.selState_ + i));
				objCityBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
				if(objCityBean.getStrName().trim().length() == 0) continue;
				if (validate(request,objCityBean)) {
					
				try {
						blnModified = new DBUtil(request).modifyRecord(objCityBean);
						
				} catch (UMBException e) {
					e.printStackTrace();
					msg = new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status");
					blnModified = false;
					break;
				}
					
				}else{
					msg = new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED + "\"" + objCityBean.getStrName() + "\""+ ITextConstants.MSG_DUPLICATE_ENTRY);
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status");
					blnModified = false;
					break;
				}
			}	
					
			if (blnModified) {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer");
				blnModified = true;
			}else{
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				blnModified =  false;
			}
			return blnModified;
		}

		protected boolean deleteRecords(HttpServletRequest request) {
			boolean blnDeleted;
			try{
				
				blnDeleted = new DBUtil(request).deleteRecords(IUMCConstants.BN_CITY_MASTER, WebUtil.parseString(request, ILabelConstants.hdnSelectedID));
				if(blnDeleted){
					msg = new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
					msg.setStrMsgType(IUMCConstants.OK);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status,List,Footer");
					return true;
				}
			}catch(UMBException ex){
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED );
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				ex.printStackTrace();
				return false;
				
			}
			return blnDeleted;
		}

		protected boolean validate(HttpServletRequest request,CityMasterBean objCityBean) {
			NameValueBean objNameValueBean = null;
			ArrayList<NameValueBean> arrNameValueList = new ArrayList<NameValueBean>();
			
			if(objCityBean!= null){
				
				objNameValueBean = new NameValueBean("lngID", ""+objCityBean.getLngID(), true);
				arrNameValueList.add(objNameValueBean);
				
				objNameValueBean = new NameValueBean("strName",objCityBean.getStrName(),false);
				arrNameValueList.add(objNameValueBean);
				
				objNameValueBean = new NameValueBean("lngStateID",""+objCityBean.getLngStateID(),false);
				arrNameValueList.add(objNameValueBean);
				return new DBUtil(request).checkIsUnique(IUMCConstants.BN_CITY_MASTER, IUMCConstants.BN_CITY_MASTER, arrNameValueList);
			}else{
				return true;
			}
			}

		@Override
		public Object get(HttpServletRequest request) {
			// TODO Auto-generated method stub
			return null;
		}


		public String buildQueryString(HttpServletRequest request) {
			String strWhereClause = "intStatus="+IUMCConstants.STATUS_ACTIVE;
			
			if(WebUtil.parseString(request, ILabelConstants.txtNameFilter).length() > 0){
				strWhereClause += " AND strName LIKE '"+ WebUtil.parseString(request, ILabelConstants.txtNameFilter) + "'";
			}
			if(WebUtil.parseLong(request, ILabelConstants.selStateFilter) > 0){
				strWhereClause += " AND lngStateID ="+ WebUtil.parseLong(request, ILabelConstants.selStateFilter);
			}
			
			strWhereClause = strWhereClause.replace("*", "%");
			return strWhereClause;
		}

		@Override
		public long getRecordsCount(HttpServletRequest request) {
			String strWhereClause = buildQueryString(request);
			return new DBUtil(request).getRecordsCount(IUMCConstants.BN_CITY_MASTER, strWhereClause);
		}
		@Override
		public List<Object> getItemList(HttpServletRequest request) {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		protected boolean validate(HttpServletRequest request) {
			// TODO Auto-generated method stub
			return false;
		}
		
		
		protected boolean deleteRecords1(HttpServletRequest request) {
			boolean blnDeleted = false;
			boolean blnIsDeletable = false;
			String strItems = "";
			String strDelete = "";
			int intUserAction = 2;
			long lngActivityID = WebUtil.parseLong(request,IUMCConstants.ACTIVITY_ID);
			String strSelectedID=WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
			try{
				
				//Write generalized code to restrict the used data
				VetoMessage objVeto =  (VetoMessage) new DBUtil(request).vetoCheck(request, lngActivityID, strSelectedID,intUserAction);
				if(null == objVeto)
				{
					objVeto = new VetoMessage();
				}
				if(!objVeto.getStrids().equals("") && !objVeto.getStrids().equalsIgnoreCase(";"))
				{	
					blnDeleted = new DBUtil(request).deleteRecords(IUMCConstants.BN_CITY_MASTER,objVeto.getStrids() );
				  	
					//deleting material batches
				  	String strID="";
					strID = objVeto.getStrids().replaceAll(";", ",");
					strID = strID.substring(1,strID.length()-1);
					/*if(blnDeleted)
					{
						blnDeleted=new DBUtil(request).updateRecords(IUMCConstants.BN_MATERIAL_BATCH, "intStatus="+IUMCConstants.STATUS_DELETED, "lngMaterialID IN ("+strID+")");
					}*/
				}
				if(!objVeto.getStrUnselIds().equals("") &&  !objVeto.getStrUnselIds().equalsIgnoreCase(";"))
				  {
					  String strMyIds = objVeto.getStrUnselIds().substring(1,objVeto.getStrUnselIds().length()-1);
					  strMyIds = strMyIds.replace(";", ",");
					  List lstCity = (List)new DBUtil(request).getRecords(IUMCConstants.BN_CITY_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" and lngID IN ("+strMyIds+")","");
					  if(null != lstCity)
					  {
						  for(int m=0;m<lstCity.size();m++)
						  {
							  CityMasterBean  objCMB = (CityMasterBean)lstCity.get(m);
							  strItems += objCMB.getStrName()+",";
						  }
						  strItems = strItems.substring(0, strItems.length()-1);
					  }
				  }	
				
				
				if(blnDeleted){
					msg = new MessageBean();
					if(objVeto.isBlnIsdel() == true)
					{
						msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS+" '"+strItems+"' Used");
					}
					else
					{
						msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
					}
					msg.setStrMsgType(IUMCConstants.OK);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status,List,Footer");
					return true;
				}
				else
				{
					msg = new MessageBean();
					msg.setStrMsgText("'"+strItems+"' "+ITextConstants.MSG_USED);
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status,List,Footer");
					return false;
				}
			}catch(UMBException ex){
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED );
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				ex.printStackTrace();
				return false;
				
			}
		}
	
}