package com.uthkrushta.mm.master.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ajaxanywhere.AAUtils;
import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mm.master.bean.CityMasterBean;
import com.uthkrushta.mm.master.bean.StateMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.VetoMessage;
import com.uthkrushta.utils.WebUtil;

public class StateMasterController extends GenericController{ 
	StateMasterBean  objStateMasterBean = new StateMasterBean();
	long lngStateId = 0;
	   boolean blnModified = false;
	   public boolean modifyUser(HttpServletRequest request){
			// logic to store the values
		   int Index = 1;
			// Fill object from UI
			objStateMasterBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + Index));
			objStateMasterBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + Index));
			objStateMasterBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
			// delegate the call to framework
			try {
				blnModified = new DBUtil(request).modifyRecord(objStateMasterBean);
			} catch (UMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				blnModified = false;
			}
			return blnModified;
		} 

		@SuppressWarnings("unchecked")
		public List<Object> getList(HttpServletRequest request)
		{
			
			String strWhere = buildQueryString(request);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			return new DBUtil(request).getRecords(IUMCConstants.BN_STATE_MASTER,strWhere, "strName");
		}

		@Override
		public void doAction(HttpServletRequest request, HttpServletResponse response) {
			String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
			if (null != strUserAction) {
					if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
					{
						modifyRecords(request);
					}  //
					else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL))
					{ 
						deleteRecords1(request);
					}
					else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD))
					{ 
						AAUtils.addZonesToRefresh(request, "List,Footer");
					}
				}
			}

		@Override
		protected boolean modifyRecord(HttpServletRequest request) {
			// TODO Auto-generated method stub
		boolean blnModified = false;
		int Index = 1;
			objStateMasterBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + Index));
			objStateMasterBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + Index));
			objStateMasterBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
			// delegate the call to framework
				try {
					blnModified = new DBUtil(request).modifyRecord(objStateMasterBean);
				} catch (UMBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return blnModified;
		}

		@Override
		protected boolean modifyRecords(HttpServletRequest request) {
			// TODO Auto-generated method stub
			boolean blnModified = false;
			ArrayList<Object> arrCityMasterList = new ArrayList<Object>();
			int intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnRowCounter);
			
			for (int i = 1; i <= intRowCounter; i++) {
				objStateMasterBean = new StateMasterBean(); 
				objStateMasterBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + i));
				objStateMasterBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + i));
				objStateMasterBean.setLngCountryID(WebUtil.parseLong(request, ILabelConstants.selCountry_ + i));  //set value in the datab n dspl in dd
				objStateMasterBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
				objStateMasterBean.setStrStateCode(WebUtil.parseString(request, ILabelConstants.txtStateCode_+i));
				if(objStateMasterBean.getStrName().trim().length() == 0) continue;
				
				if (validate (request)) {
					try {
						blnModified = new DBUtil(request).modifyRecord(objStateMasterBean);
						
				} catch (UMBException e) {
					e.printStackTrace();
					msg = new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status");
					blnModified = false;
					break;
				}
					
				}else{
					msg = new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED + "\"" + objStateMasterBean.getStrName() + "\""+ ITextConstants.MSG_DUPLICATE_ENTRY);
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status");
					blnModified = false;
					break;
				}
			}	
					
			if (blnModified) {
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer");
				blnModified = true;
			}else{
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				blnModified =  false;
			}
			return blnModified;
		}

		protected boolean deleteRecords(HttpServletRequest request) {
			boolean blnDeleted;
			String strSelectedID=WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
			try{
				
				blnDeleted = new DBUtil(request).deleteRecords(IUMCConstants.BN_STATE_MASTER,strSelectedID );
				if(blnDeleted){
					msg = new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
					msg.setStrMsgType(IUMCConstants.OK);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status,List,Footer");
					return true;
				}
			}catch(UMBException ex){
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS );
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				ex.printStackTrace();
				return false;
			}
			return blnDeleted;
		}

		protected boolean validate(HttpServletRequest request) {
			NameValueBean objNameValueBean = null;
			ArrayList<NameValueBean> arrNameValueList = new ArrayList<NameValueBean>();
			
			if(null != request.getSession().getAttribute("COMP_ID")){
				lngStateId = ((Long)request.getSession().getAttribute("COMP_ID")).longValue();
			}
			
			if(objStateMasterBean!= null){
				objNameValueBean = new NameValueBean("lngID", ""+objStateMasterBean.getLngID(), true);
				arrNameValueList.add(objNameValueBean);
				
				objNameValueBean = new NameValueBean("strName",objStateMasterBean.getStrName(),false);
				arrNameValueList.add(objNameValueBean);
				
				
				objNameValueBean = new NameValueBean("lngCountryID",""+objStateMasterBean.getLngCountryID(),false);
				arrNameValueList.add(objNameValueBean);
				return new DBUtil(request).checkIsUnique(IUMCConstants.BN_STATE_MASTER, IUMCConstants.BN_STATE_MASTER, arrNameValueList);
			}else{
				return true;
			}
		}
			
		public Object get(HttpServletRequest request) {
			// TODO Auto-generated method stub
			return null;
		}

		public String buildQueryString(HttpServletRequest request) {
			String strWhereClause = "intStatus="+IUMCConstants.STATUS_ACTIVE;
			
			if(WebUtil.parseString(request, ILabelConstants.txtNameFilter).length() > 0){
				strWhereClause += " AND strName LIKE '"+ WebUtil.parseString(request, ILabelConstants.txtNameFilter) + "'";
			}
			if(WebUtil.parseLong(request, ILabelConstants.selCountryFilter) > 0){
				strWhereClause += " AND lngCountryID ="+ WebUtil.parseLong(request, ILabelConstants.selCountryFilter);
			}
			
			strWhereClause = strWhereClause.replace("*", "%");
			return strWhereClause;
		}




		public long getRecordsCount(HttpServletRequest request) {
			String strWhereClause = buildQueryString(request);
			return new DBUtil(request).getRecordsCount(IUMCConstants.BN_STATE_MASTER, strWhereClause);
		}

		public List<Object> getItemList(HttpServletRequest request) {
			// TODO Auto-generated method stub
			return null;
		}
		
		
		protected boolean deleteRecords1(HttpServletRequest request) {
			boolean blnDeleted = false;
			boolean blnIsDeletable = false;
			String strItems = "";
			String strDelete = "";
			int intUserAction = 2;
			long lngActivityID = WebUtil.parseLong(request,IUMCConstants.ACTIVITY_ID);
			String strSelectedID=WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
			try{
				
				//Write generalized code to restrict the used data
				VetoMessage objVeto =  (VetoMessage) new DBUtil(request).vetoCheck(request, lngActivityID, strSelectedID,intUserAction);
				if(null == objVeto)
				{
					objVeto = new VetoMessage();
				}
				if(!objVeto.getStrids().equals("") && !objVeto.getStrids().equalsIgnoreCase(";"))
				{	
					blnDeleted = new DBUtil(request).deleteRecords(IUMCConstants.BN_STATE_MASTER,objVeto.getStrids() );
				  	
					//deleting material batches
				  	String strID="";
					strID = objVeto.getStrids().replaceAll(";", ",");
					strID = strID.substring(1,strID.length()-1);
					/*if(blnDeleted)
					{
						blnDeleted=new DBUtil(request).updateRecords(IUMCConstants.BN_MATERIAL_BATCH, "intStatus="+IUMCConstants.STATUS_DELETED, "lngMaterialID IN ("+strID+")");
					}*/
				}
				if(!objVeto.getStrUnselIds().equals("") &&  !objVeto.getStrUnselIds().equalsIgnoreCase(";"))
				  {
					  String strMyIds = objVeto.getStrUnselIds().substring(1,objVeto.getStrUnselIds().length()-1);
					  strMyIds = strMyIds.replace(";", ",");
					  List lstState = (List)new DBUtil(request).getRecords(IUMCConstants.BN_STATE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" and lngID IN ("+strMyIds+")","");
					  if(null != lstState)
					  {
						  for(int m=0;m<lstState.size();m++)
						  {
							  StateMasterBean  objSMB = (StateMasterBean)lstState.get(m);
							  strItems += objSMB.getStrName()+",";
						  }
						  strItems = strItems.substring(0, strItems.length()-1);
					  }
				  }	
				
				
				if(blnDeleted){
					msg = new MessageBean();
					if(objVeto.isBlnIsdel() == true)
					{
						msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS+" '"+strItems+"' Used");
					}
					else
					{
						msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
					}
					msg.setStrMsgType(IUMCConstants.OK);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status,List,Footer");
					return true;
				}
				else
				{
					msg = new MessageBean();
					msg.setStrMsgText("'"+strItems+"' "+ITextConstants.MSG_USED);
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefresh(request, "Status,List,Footer");
					return false;
				}
			}catch(UMBException ex){
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED );
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				ex.printStackTrace();
				return false;
				
			}
		}
	}