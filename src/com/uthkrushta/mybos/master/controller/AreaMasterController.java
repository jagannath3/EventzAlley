package com.uthkrushta.mybos.master.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.master.bean.AreaMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.VetoMessage;
import com.uthkrushta.utils.WebUtil;

public class AreaMasterController extends GenericController{

	AreaMasterBean objAMB=new AreaMasterBean();

	@Override
	public void doAction(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		String strUserAction=WebUtil.parseString(request, ILabelConstants.hdnUserAction);
		if(null!=strUserAction)
		{
			if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT)){
				modifyRecords(request);
			}
			else
				if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL)){
					deleteRecords1(request);
				}
				else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD))
				{
				  AAUtils.addZonesToRefresh(request, "List,Footer");	
				}
		}
	}
	private boolean deleteRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean blnDeleted;
		try {
			blnDeleted=DBUtil.deleteRecords(IUMCConstants.BN_AREA_BEAN, WebUtil.parseString(request, ILabelConstants.hdnSelectedID));
		     if(blnDeleted)
		     {
		    	 msg=new MessageBean();
		    	 msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
		    	 msg.setStrMsgType(IUMCConstants.OK);
		    	 msg.setBlnIsLeadingMessage(true);
		    	 this.arrMsg.add(msg);
		    	 AAUtils.addZonesToRefreh(request, "Status,List,Footer");
		    	 return true;
		     }
		   
		} catch (UMBException e) {
			// TODO Auto-generated catch block
			msg=new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefreh(request, "Status");
			e.printStackTrace();
			return false;
		}
		return blnDeleted;
	}

	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean blnModified=false;
		ArrayList<Object> arrAreaList=new ArrayList<Object>();
		int intRowCounter=WebUtil.parseInt(request,ILabelConstants.hdnRowCounter);
		
		for(int i=1;i<=intRowCounter;i++)
		{
			objAMB=new AreaMasterBean();
			objAMB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_+i));
			objAMB.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_+i));
			objAMB.setStrPincode(WebUtil.parseString(request, ILabelConstants.txtPincode_+i));
			objAMB.setLngCityID(WebUtil.parseLong(request, ILabelConstants.selCity_+i));
			objAMB.setIntStatus(IUMCConstants.STATUS_ACTIVE);
			if(objAMB.getStrName().trim().length()==0)continue;
			if(validate(request))
			{
				try {
					blnModified=new DBUtil(request).modifyRecord(objAMB);
				} 
				catch (UMBException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					msg=new MessageBean();
					msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setBlnIsLeadingMessage(true);
					this.arrMsg.add(msg);
					AAUtils.addZonesToRefreh(request, "Status");
					blnModified=false;
					break;
				}
			}
			else
			{
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED+"\""+objAMB.getStrName()+"\""+ ITextConstants.MSG_DUPLICATE_ENTRY);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefreh(request, "Status");
				blnModified=false;
				break;
			}
		}
		
		if(blnModified)
		{
			msg=new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefreh(request, "Status,List,Footer");
			blnModified=true;
			
		}
		else
		{
			msg=new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefreh(request, "Status");
			blnModified=false;
		}
		return blnModified;
	}
     
	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		NameValueBean objNameValueBean=null;
		ArrayList<NameValueBean> arrNameValueList=new ArrayList<NameValueBean>();
		
		if(null!=objAMB)
		{
			objNameValueBean=new NameValueBean("lngID", ""+objAMB.getLngID(), true);
			arrNameValueList.add(objNameValueBean);
			
			objNameValueBean=new NameValueBean("lngCityID", ""+objAMB.getLngCityID(), false);
			arrNameValueList.add(objNameValueBean);
			
			objNameValueBean=new NameValueBean("strName", objAMB.getStrName(), false);
			arrNameValueList.add(objNameValueBean);
			
			return new DBUtil(request).checkIsUnique(IUMCConstants.BN_AREA_BEAN, IUMCConstants.BN_AREA_BEAN, arrNameValueList);
		}
		else
		{
		return true;
		}
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		String strWhere=buildQueryString(request);
		int intNumberOfRecords=WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
		AAUtils.addZonesToRefreh(request, "Status,List,Footer");
		return new DBUtil(request).getRecords(IUMCConstants.BN_AREA_BEAN, strWhere, "strName");
		
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		String strWhereClause="intStatus="+IUMCConstants.STATUS_ACTIVE;
		return strWhereClause;
	}
	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	protected boolean deleteRecords1(HttpServletRequest request) {
		boolean blnDeleted = false;
		boolean blnIsDeletable = false;
		String strItems = "";
		String strDelete = "";
		int intUserAction = 2;
		long lngActivityID = WebUtil.parseLong(request,IUMCConstants.ACTIVITY_ID);
		String strSelectedID=WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
		try{
			
			//Write generalized code to restrict the used data
			VetoMessage objVeto =  (VetoMessage) new DBUtil(request).vetoCheck(request, lngActivityID, strSelectedID,intUserAction);
			if(null == objVeto)
			{
				objVeto = new VetoMessage();
			}
			if(!objVeto.getStrids().equals("") && !objVeto.getStrids().equalsIgnoreCase(";"))
			{	
				blnDeleted = new DBUtil(request).deleteRecords(IUMCConstants.BN_AREA_BEAN,objVeto.getStrids() );
			  	
				//deleting material batches
			  	String strID="";
				strID = objVeto.getStrids().replaceAll(";", ",");
				strID = strID.substring(1,strID.length()-1);
				/*if(blnDeleted)
				{
					blnDeleted=new DBUtil(request).updateRecords(IUMCConstants.BN_MATERIAL_BATCH, "intStatus="+IUMCConstants.STATUS_DELETED, "lngMaterialID IN ("+strID+")");
				}*/
			}
			if(!objVeto.getStrUnselIds().equals("") &&  !objVeto.getStrUnselIds().equalsIgnoreCase(";"))
			  {
				  String strMyIds = objVeto.getStrUnselIds().substring(1,objVeto.getStrUnselIds().length()-1);
				  strMyIds = strMyIds.replace(";", ",");
				  List lstArea = (List)new DBUtil(request).getRecords(IUMCConstants.BN_AREA_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" and lngID IN ("+strMyIds+")","");
				  if(null != lstArea)
				  {
					  for(int m=0;m<lstArea.size();m++)
					  {
						  AreaMasterBean  objAMB = (AreaMasterBean)lstArea.get(m);
						  strItems += objAMB.getStrName()+",";
					  }
					  strItems = strItems.substring(0, strItems.length()-1);
				  }
			  }	
			
			
			if(blnDeleted){
				msg = new MessageBean();
				if(objVeto.isBlnIsdel() == true)
				{
					msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS+" '"+strItems+"' Used");
				}
				else
				{
					msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
				}
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer");
				return true;
			}
			else
			{
				msg = new MessageBean();
				msg.setStrMsgText("'"+strItems+"' "+ITextConstants.MSG_USED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer");
				return false;
			}
		}catch(UMBException ex){
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			ex.printStackTrace();
			return false;
			
		}
	}
	@Override
	protected boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}
}
