package com.uthkrushta.mybos.master.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.bean.PaginationBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.master.bean.RoleBean;
import com.uthkrushta.mybos.master.bean.UserMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;



public class RoleMasterController extends GenericController{

	private RoleBean objRoleBean = new RoleBean();
	long lngCompanyId =0;

	@Override
	public void doAction(HttpServletRequest request,HttpServletResponse response) 
	{
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);
		if(null != strUserAction)
		{
			if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
			{
				modify(request,response);
			}
			else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL))
			{
				delete(request, response);
			}
			else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD))
			{
				AAUtils.addZonesToRefresh(request, "List,Footer");
			}
		}
	}

	private void modify(HttpServletRequest request, HttpServletResponse response) {
		if(null != request.getSession().getAttribute("COMP_ID")){
			lngCompanyId = ((Long)request.getSession().getAttribute("COMP_ID")).longValue();
		}
		ArrayList<Object> arrRoleList = new ArrayList<Object>();
		int intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnRowCounter);
		for (int i = 1; i <= intRowCounter; i++) {
			objRoleBean = new RoleBean(); 
			objRoleBean.setLngID(WebUtil.parseInt(request, ILabelConstants.hdnID_ + i));
			objRoleBean.setLngCompanyId(lngCompanyId);
			objRoleBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtRole_+i));
			objRoleBean.setIntRoleType(WebUtil.parseInt(request, ILabelConstants.selRoleType_+i));
			objRoleBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
			if (validate(request)) {
				arrRoleList.add(objRoleBean);
			}else{
				msg = new MessageBean();
				msg.setStrMsgText(objRoleBean.getStrName()  + ITextConstants.MSG_DUPLICATE_ENTRY);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
				return;
			}
		}
		boolean blnModified = false;
		try {
			blnModified = DBUtil.modifyRecords(arrRoleList);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			this.arrMsg.add(msg);
		}
	}
	
	private void delete(HttpServletRequest request, HttpServletResponse response) {
		boolean blnDeleted = false;
		//To check whether this role id is assigned or not,if assigned do not allow it to delete
		//Check whether the item to be deleted contained in the WorkOrderItem
		String strSelectedID = WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
		String strID = strSelectedID.replaceAll(";", ",");
		strID = strID.substring(1,strID.length()-1);
		List lstStaffRole = DBUtil.getRecords(ILabelConstants.BN_USER_MASTER_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" and lngRoleID in ("+strID+")", "");
		
		if(null==lstStaffRole)
		{
			try {
				blnDeleted = DBUtil.deleteRecords(IUMCConstants.BN_ROLE_BEAN, WebUtil.parseString(request, ILabelConstants.hdnSelectedID));
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(blnDeleted){
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			}else{
				msg = new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETE_ERROR);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefresh(request, "Status");
			}
		}else
		{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_ERROR);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
		}
	}

	@Override
	public boolean validate(HttpServletRequest request) {
		NameValueBean objNameValueBean = null;
		ArrayList<NameValueBean> arrNameValueList = new ArrayList<NameValueBean>();
		
		if(objRoleBean != null){
			objNameValueBean = new NameValueBean("lngID", ""+objRoleBean.getLngID(), true);
			arrNameValueList.add(objNameValueBean);
			
			objNameValueBean = new NameValueBean("strName",objRoleBean.getStrName(),false);
			arrNameValueList.add(objNameValueBean);
			
			return DBUtil.checkIsUnique(IUMCConstants.BN_ROLE_BEAN, IUMCConstants.BN_ROLE_BEAN, arrNameValueList);
			
		}else{
			return true;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Object> getList(HttpServletRequest request){
		
		//Fetching the Login Staff from the session
		 long lngStaffType=0;
		 
		
		 
		 
		 /*UserMasterBean objSMBFromSession=(UserMasterBean)request.getSession().getAttribute(ISessionAttributes.LOGIN_USER);*/
		 
		 
		 
		 UserMasterBean objSMBFromSession = new UserMasterBean(); 
			long userID = ((Long) request.getSession().getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
			 
			objSMBFromSession = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+userID , "");
			
		 
		 
		 if(null!=objSMBFromSession)
		 {
			 RoleBean objRB=new RoleBean();
			 objRB=(RoleBean)DBUtil.getRecord(IUMCConstants.BN_ROLE_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSMBFromSession.getLngRoleID(), "");
			 lngStaffType=objRB.getIntRoleType();
			 
		 }else
		 {
			 objSMBFromSession=new UserMasterBean();
		 }
		
		 
		 
		String strWhere = buildQueryString(request);
		/*int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
		int intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
		
		if(intNumberOfRecords == 0){
			intNumberOfRecords = 10;
		}
		if(intCurrentPage == 0){
			intCurrentPage = 1;
		}
		
		PaginationBean objPaginationBean = new PaginationBean();
		objPaginationBean.setIntNumberOfRecords(intNumberOfRecords);
		objPaginationBean.setIntStartingCount((intCurrentPage-1)*intNumberOfRecords);*/
		if(lngStaffType!=1)
		 {
			strWhere +=" and intRoleType > 1"; 
		 }
		
		AAUtils.addZonesToRefresh(request, "ID,Status,List,Footer");
		return DBUtil.getRecords(IUMCConstants.BN_ROLE_BEAN,strWhere,"strName");
	}
	
	protected String buildQueryString(HttpServletRequest request) {

		String strWhere = "intStatus = "+IUMCConstants.STATUS_ACTIVE ;
		
		strWhere = strWhere.replace("*", "%");
	 return strWhere;	
	}

	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean deleteRecords() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

}
