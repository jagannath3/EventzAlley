package com.uthkrushta.basis.util;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mybos.code.bean.MailVariableCodeBean;
import com.uthkrushta.utils.DBUtil;






public class SendMail {
	
	static MailVariableCodeBean objMVU =(MailVariableCodeBean) DBUtil.getRecord(ILabelConstants.BN_MAIL_VARIABLES_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngCompanyID = 1", "");

	public static boolean sendMail(String strTo,String strFrom,String strCC,String strBCC,String subject,String content,boolean blnAttachement,String strFilePath)  
    {
		
		if(null == objMVU)
		{
			objMVU= new MailVariableCodeBean();
		}
		
	 	boolean blnMailSent = false;
        String host = objMVU.getStrHostName() ; 
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        SMTPAuthenticator auth = new SMTPAuthenticator();
        String arrTo[] = null;
        String arrCC[] = null;
        String arrBCC[] = null;
        
        try {
        Session session = Session.getDefaultInstance(props, auth);
        //BodyPart bp2 = getFileBodyPart("MailTemplates/mailTempECSConfirmation.jsp", "text/html");

        Message msg = new MimeMessage(session);
        
			msg.setFrom(new InternetAddress(strFrom));
		
        if(null != strTo && strTo.trim().length() > 0){
        	arrTo = strTo.split(";");
        	InternetAddress[] addressTo = new InternetAddress[arrTo.length];
        	for(int i=0;i<arrTo.length;i++){
	        	addressTo[i] =  new InternetAddress(arrTo[i]) ;
	        }
        	if(null != addressTo && addressTo.length > 0){
	        	msg.setRecipients(Message.RecipientType.TO, addressTo);
	        }
        }
        
        
        if(null != strCC && strCC.trim().length() > 0){
	        arrCC = strCC.split(";");
	        InternetAddress[] addressCC = new InternetAddress[arrCC.length];
	        for(int i=0;i<arrCC.length;i++){
	        	addressCC[i] =  new InternetAddress(arrCC[i]) ;
	        }
	        if(null != addressCC && addressCC.length > 0){
	        	msg.setRecipients(Message.RecipientType.CC, addressCC);
	        }
        }
	     
        if(null != strBCC && strBCC.trim().length() > 0){
        	arrBCC = strBCC.split(";");
	        InternetAddress[] addressBCC = new InternetAddress[arrBCC.length];
	        for(int i=0;i<arrBCC.length;i++){
	        	addressBCC[i] =  new InternetAddress(arrBCC[i]) ;
	        }
	        if(null != addressBCC && addressBCC.length > 0){
	        	msg.setRecipients(Message.RecipientType.BCC, addressBCC);
	        }
        }
        
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        msg.setText(content);
        if(blnAttachement){
        	
       /*	 // Create the message part 
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText(content);
            
            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = strFilePath; 
           // System.out.println("filename" + filename);
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);*/
        	
        	 // Create the message part 
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setContent(content, "text/html");
            
            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);
                     		 
            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename =  strFilePath; 
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            msg.setContent(multipart);
            
       }else{
       	  msg.setContent(content, "text/html");
       }
     
       
      
        
        
        
        
        Transport.send(msg);
        blnMailSent = true;
        System.out.print(" Mail Sent");
        } catch (AddressException e) {
			e.printStackTrace();
			blnMailSent = false;
		} catch (MessagingException e) {
			e.printStackTrace();
			blnMailSent = false;
		}
        return blnMailSent;
    }
	 public static boolean sendMail(String strTo,String strFrom,String strCC,String strBCC,String subject,String content)  
	    {
		 	boolean blnMailSent = false;
		 	String host =objMVU.getStrHostName();
		 	Properties props = new Properties();
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.port", "465");
            props.put("mail.smtp.starttls.enable","true");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        SMTPAuthenticator auth = new SMTPAuthenticator();
	        String arrTo[] = null;
	        String arrCC[] = null;
	        String arrBCC[] = null;
	        
	        try {
	        Session session = Session.getDefaultInstance(props, auth);
	        //BodyPart bp2 = getFileBodyPart("MailTemplates/mailTempECSConfirmation.jsp", "text/html");

	        Message msg = new MimeMessage(session);
	        
				msg.setFrom(new InternetAddress(strFrom));
			
	        if(null != strTo && strTo.trim().length() > 0){
	        	arrTo = strTo.split(";");
	        	InternetAddress[] addressTo = new InternetAddress[arrTo.length];
	        	for(int i=0;i<arrTo.length;i++){
		        	addressTo[i] =  new InternetAddress(arrTo[i]) ;
		        }
	        	if(null != addressTo && addressTo.length > 0){
		        	msg.setRecipients(Message.RecipientType.TO, addressTo);
		        }
	        }
	        
	        
	        if(null != strCC && strCC.trim().length() > 0){
		        arrCC = strCC.split(";");
		        InternetAddress[] addressCC = new InternetAddress[arrCC.length];
		        for(int i=0;i<arrCC.length;i++){
		        	addressCC[i] =  new InternetAddress(arrCC[i]) ;
		        }
		        if(null != addressCC && addressCC.length > 0){
		        	msg.setRecipients(Message.RecipientType.CC, addressCC);
		        }
	        }
		     
	        if(null != strBCC && strBCC.trim().length() > 0){
	        	arrBCC = strBCC.split(";");
		        InternetAddress[] addressBCC = new InternetAddress[arrBCC.length];
		        for(int i=0;i<arrBCC.length;i++){
		        	addressBCC[i] =  new InternetAddress(arrBCC[i]) ;
		        }
		        if(null != addressBCC && addressBCC.length > 0){
		        	msg.setRecipients(Message.RecipientType.BCC, addressBCC);
		        }
	        }
	        
	        msg.setSubject(subject);
	        msg.setSentDate(new Date());
	        //msg.setText(content);
	        msg.setContent(content, "text/html");
	        Transport.send(msg);
	        blnMailSent = true;
	        System.out.print(" Mail Sent");
	        } catch (AddressException e) {
				e.printStackTrace();
				blnMailSent = false;
			} catch (MessagingException e) {
				e.printStackTrace();
				blnMailSent = false;
			}
	        return blnMailSent;
	    }

	    private static class SMTPAuthenticator extends javax.mail.Authenticator 
	    {
	        public PasswordAuthentication getPasswordAuthentication() 
	        {
	        	  return new PasswordAuthentication(objMVU.getStrUserName(), objMVU.getStrPassword()); //1.info@swasstik.com,swasstik@2015  2."info@swasstik.com", "mail@2015" curr
	        }
	    }
	    

	    public static void main(String[] args) throws Exception 
	    {
	    	String strContent = "<!DOCTYPE html>" +
	    						"<html><head><meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>" +
	    						"<title>Hello </title></head>" +
	    						"<body>" +
	    						"<form method='post'>" +
	    						"<center><img border='0' src='http://localhost:8080/myBOSPOS/Images/close.png' ><center><font color='blue'> This is the content Mr  {[cust_name]} </font>" +
	    						"</form> " +
	    						"</body> </html>";
	    	//String strFileContent = ReadFile.readThisFile("C:\\Uthkrushta\\Project\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp1\\wtpwebapps\\uLoanOffice\\MailTemplates\\test.txt");
	    	//strContent = strContent.replace("{[cust_name]}", strFileContent);
	    			//"<center><img border='0' src='http://localhost:8080/uPackNMove/Icons/Layout/logo.png'><center><font color='blue'> This is the content </font>";*/
	    	SendMail sendMail = new SendMail();
	    	sendMail.sendMail("j.meti1447@gmail.com",objMVU.getStrHostName(),null,null, "Hi krusha temple", "test msg",true,"C:\\MYBOS_SWASTIK_AUTO_PDF\\INV_AUTO_BACKUP\\CI-88(16-10-2015).pdf");
	    }   

}

