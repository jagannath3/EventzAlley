/**
 * 
 */
package com.uthkrushta.basis.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller Interface
 * 
 */
public interface IController {
	public void doAction(HttpServletRequest request, HttpServletResponse response);

	public List<Object> getList(HttpServletRequest request);

	public Object get(HttpServletRequest request);
}
