package com.uthkrushta.basis.constants;

public interface ITextConstants {
	
	public static final String MSG_QUESTION_RESET_SUCCESS = "The questions related to the chapters have been reset successfully!!.";
	public static final String MSG_QUESTION_RESET_FAILED = "Reset Failed, Try Again!!.";
	public static final String MSG_NO_QUESTIONS = "No Question for Now, Come Back After a While!!.";
	public static final String MSG_SENT_SUCCESS="Message Sent Successfully";
	
	public static final String STR_MOD = " Modified.";
	public static final String STR_DEL = " deleted.";
	public static final String STR_TWO_COLONS = " :: ";
	public static final String STR_SEL_IDS = " selected ID's " + STR_TWO_COLONS;
	public static final String MSG_DELETE_SUCCESS = "Deleted Successfully";
	public static final String MSG_MAIL_SENT_SUCCESS = "Mail Sent Successfully";
	public static final String MSG_MAIL_SENT_FAILED = "Mail Sending Failed";
	public static final String MSG_SAVED_SUCCESS = "Saved Successfully";
	public static final String MSG_PUBLISHED_SUCCESS = "Published Successfully";
	public static final String MSG_DELETED_SUCCESS = "Deleted Successfully";
	public static final String MSG_UNBLOCK_SUCCESS = "Unblocked Successfully";
	public static final String MSG_DELETED_ACTIVE_ROW = "Deleted Successfully Excluding Active Batch.";
	public static final String ANSWER_SENT_SUCCESSFULLY="Answer Sent Successfully";
	public static final String ANSWER_SENDING_FAILED="Answer Sending Failed";
	public static final String MSG_EMAIL_MATCHING_FAILED = "Email is Not Matching";
	public static final String MSG_MAIL_ERROR = " Mail sending failed";
	public static final String MSG_RESET_EMAIL = "Reset Link sent to your email";
	public static final String MSG_DELETE_ERROR = " Delete Error";
	
	public static final String MSG_SAVED_ERROR = "Error occured while Saving, please try again.";
	public static final String MSG_EXCEL_UPLOADED_SUCCESSFULLY = "Excel Uploaded Successfully";
	public static final String MSG_EXCEL_UPLOADED_FAILED = "Excel Upload Failed";
	
	public static final String MSG_DELETE_FAILED = "Delete Failed";
	public static final String MSG_QUESTION_GENERATED_SUCCESS = "Questions Generated Successfully";
	public static final String MSG_USED ="been used";
	public static final String MSG_UNBLOCK_FAILED = "Unblocked Failed, Try Again!";
	public static final String MSG_SAVED_FAILED = "Save Failed!";
	public static final String MSG_PUBLISHED_FAILED = "Publish Failed!";
	public static final String MSG_DELETED_FAILED = "Delete Failed!";
	public static final String MSG_DELETED_ACTIVE = "Active Batches can't be deleted";
	public static final String MSG_GET_FAILED = "Action Failed! Try Again.";
	public static final String MSG_ACTION_FAILED = "Action Failed! Try Again.";
	public static final String MSG_QUESTION_GENERATION_FAILED = "Questions Generation Failed!";
	
	public static final String MSG_DUPLICATE_ORDER_ENTRY = "split order number already exists. ";
	public static final String MSG_DUPLICATE_ENTRY = " already exists. ";
	public static final String MSG_REJECTED_SUCCESS = " Selected records Rejected Successfully";
	public static final String MSG_APPROVED_SUCCESSFULLY = " Selected records Approved Successfully";
	public static final String MSG_DUPLICATE_USERNAME = " User Name already exists";
	public static final String MSG_MAIL_SENT = " Mail Sent Successfully to ";
	
	public static final String MSG_INVALID_LOGIN = "Invalid User Name or Password";
	public static final String MSG_INVALID_USER = "Invalid User Name";
	public static final String MSG_INVALID_QUESTION = "Not Enough Questions Exists In this Chapter/Complexity";
	
}
