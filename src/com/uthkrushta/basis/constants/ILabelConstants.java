package com.uthkrushta.basis.constants;

public interface ILabelConstants {
	
	
	String BN_CERTIFICATE_TYPE = "CertificateTypeBean";
	String newMember = "newMember";
	String GroupMember = "GroupMember";
	String BN_GROUP_LEADER_VIEW ="GroupLeaderViewBean";
	String hdnUploadingMonth = "hdnUploadingMonth";
	String chkIsTimed_ = "chkIsTimed_";
	String hdnInvItemAgeID = "hdnInvItemAgeID";
	String hdnInvItemAgeID_ = "hdnInvItemAgeID_";
	String hdnSelectedItemIDAge = "hdnSelectedItemIDAge";
	String hdnRowCounterAge = "hdnRowCounterAge";
	String hdnItemRowCounterAge = "hdnItemRowCounterAge";
	String txtEventType = "txtEventType";
	String txtMaxAge = "txtMaxAge";
	String txtMinAge = "txtMinAge";
	String txtSelectgender = "txtSelectgender";
	String textAgeCategoryName = "textAgeCategoryName";
	String mobileNumber = "mobileNumber";
	String otpNumber = "otpNumber";
	String newPassword = "newPassword";
	
	String txtEventType_ = "txtEventType_";
	String txtMaxAge_ = "txtMaxAge_";
	String txtMinAge_ = "txtMinAge_";
	String IsEventCompleted = "IsEventCompleted";
	String txtSelectgender_ = "txtSelectgender_";
	String textAgeCategoryName_ = "textAgeCategoryName_";
	String hdnAgeRowItemID_ = "hdnAgeRowItemID_";
	String lngAgeID_ = "lngAgeID_";
	String AgeCategoryCreation = "AgeCategoryCreation";
	String hdnSelectedSplitItemID = "hdnSelectedSplitItemID";
	String hdnInvItemSplitID_ = "hdnInvItemSplitID_";
	String hdnInvItemSplitID = "hdnInvItemSplitID";
	String hdnSplitSelectedItemID = "hdnSplitSelectedItemID";
	String ACTION_DELETE_SPLIT_ITEM = "DELETE_SPLIT_ITEM";
	String ACTION_DELETE_AGE_ITEM = "DELETE_AGE_ITEM";
	String SAVE_SPLIT ="SAVE_SPLIT";
	 String selSmsCategory="selSmsCategory";
	 String selSmsType="selSmsType";
	 String chkSend="chkSend";
	 String selSendTo="selSendTo";
	 String txaSmsContent="txaSmsContent";
	String hdnRunDataDetails ="hdnRunDataDetails";
	String hdnSplitRowItemID_ = "hdnSplitRowItemID_";
	String hdnRowCounterSplit = "hdnRowCounterSplit";
	String hdnItemRowCounterSplit = "hdnItemRowCounterSplit";
	String hdnSelectedItemIDSplit ="hdnSelectedItemIDSplit";
	String ADD_SPLIT = "ADD_SPLIT";
	String BN_AGE_CATEGORY_CONFIG = "AgeCategoryConfigurationBean";
	
	String MemberPhoto ="MemberPhoto";
	String SplitCreation = "SplitCreation";
	String SPLIT_ORDER = "SPLIT_ORDER";
	String SPLIT_NAME ="SPLIT_NAME";
	String SPLIT_LABEL_NAME = "SPLIT_LABEL_NAME";
	
	String SPLIT_ORDER_ = "SPLIT_ORDER_";
	String SPLIT_NAME_ ="SPLIT_NAME_";
	String SPLIT_LABEL_NAME_ = "SPLIT_LABEL_NAME_";
	
	String hdnLapResID_ = "hdnLapResID_";
	String hdnLapID_ = "hdnLapID_";
	String txtBibNo = "txtBibNo";
	String hdnBibNo = "hdnBibNo";
	String hdnResultLngID = "hdnResultLngID";
	String txtRank = "txtRank";
	String hdnEventID = "hdnEventID";
	String hdnEventTypeIDNew = "hdnEventTypeIDNew";
	String hdnEventTypeID ="hdnEventTypeID";
	String hdnDetailID="hdnDetailID";
	//For QuestApp Mobile
		String strRegistrationToken = "registrationToken";
		
	//for quest app 
	String hdnStudentID = "hdnStudentID";
	String hdnCompanyLogo = "hdnCompanyLogo";
	String txtDocumentPath = "txtDocumentPath";
	String hdnFileUpload = "hdnFileUpload";
	String fileUpload = "fileUpload";
	String hdnCorrectAnswer = "hdnCorrectAnswer";
	String hdnGroupId_="hdnGroupId_";
	String hdnGroupId="hdnGroupId";
	String hdnItemRowCounter="hdnItemRowCounter";
	String fileLogo = "fileLogo";
	String file_="file_";
	String hdnInvItemID = "hdnInvItemID";
	String hdnInvItemID_ = "hdnInvItemID_";
	String lngID="lngID";
	String lngID_="lngID_";
	String lngSplitID_ = "lngSplitID_";
	String hdnRowItemID_="hdnRowItemID_";
	String GET = "GET";
	String hdnRowItemID = "hdnRowItemID";
	String address= "address";
	String contactPersonNumber = "contactPersonNumber";
	String contactPersonName = "contactPersonName";
	String propInformation = "propInformation";
	String propDimension = "propDimension";
	String propNumber="propNumber";
	String floorNumber = "floorNumber";
	String propName = "propName";
	String find_area ="find_area";
	String find_city = "find_city";
	String selArea = "selArea";
	String TotalRent ="TotalRent";
	String find_rent = "find_rent";
	String find_month = "find_month";
	String find_rent_amount="find_rent_amount";
	String selMonth = "selMonth";
	String TRANSACTION_MODE = "TRANSACTION_MODE";
	String TENANT_NAME ="TENANT_NAME";
	String RENT_MONTH = "RENT_MONTH";
	String CURRENT_DATE = "CURRENT_DATE";
	String balance = "balance";
	String TotalRentToPay = "TotalRentToPay";
	String find_tenant = "find_tenant";
	String find_property="find_property";
	String area = "area";
	String lngQuestionPaperID = "lngQuestionPaperID";
	String property = "property";
	String selActivityID ="selActivityID";
	String txtField = "txtField";
	String txtPayRef = "txtPayRef";
	String strBAMSClgName ="strBAMSClgName";
	String lngGenderID ="lngGenderID";
	String txtFatherName = "txtFatherName";
	String BN_CD_QPGENDER = "QPGender";
	String termsCondition="termsCondition";
	String FeeAmount="FeeAmount";
	String DDNum="DDNum";
	String BankName = "BankName";
	String StudentName = "StudentName";
	String FatherName = "FatherName";
	String GENDER="GENDER";
	String Mob="Mob";
	String EMAILID="EMAILID";
	String CLGNAME="CLGNAME";
	String YOC="YOC";
	String aadress="address";
	String zip="zip";
	String selGender="selGender";
	String NUM_COUNT = "NUM_COUNT";
	String hdnUnRecordedCurrentPage = "hdnUnRecordedCurrentPage";
	String StudentNameID = "StudentNameID";
	String TestNameID = "TestNameID";
	String txtPaperNameFilter = "txtPaperNameFilter";
	String txtTemplateNameFilter = "txtTemplateNameFilter";
	String totalMarksObtained = "totalMarksObtained";
	String totalMarks = "totalMarks";
	String txtTestStartDate = "txtTestStartDate";
	String txtTestEndDate = "txtTestEndDate";
	String RPT = "RPT";
	String testQuestionID = "testQuestionID";
	String END_TEST_ID = "END_TEST_ID";
	String startDate ="startDate";
	String studentID = "studentID";
	String hdnCount= "hdnCount";
	String hdnQuestionID_= "hdnQuestionID_";
	String hdnChoosedOption_ = "hdnChoosedOption_";
	String hdnQuestionID= "hdnQuestionID";
	String hdnIsReviewed= "hdnIsReviewed";
	String hdnTestID= "hdnTestID";
	String hdnQuestionDetailsID= "hdnQuestionDetailsID";
	String txtAnswer_= "txtAnswer_";
	String DATE= "DATE";
	String selStudent = "selStudent"; 
	String strAdminName = "strAdminName";
	String lngAdminNumber = "lngAdminNumber";
	String strAdminEmail = "strAdminEmail";
	String strWebsite = "strWebsite";
	String lngPhoneNumber = "lngPhoneNumber";
	String lngMobileNumber = "lngMobileNumber";
	String strEmailId = "strEmailId";
	String strAddress = "strAddress";
	String txtQualification="txtQualification";
	String ACTION = "ACTION";
	String selStudentStatus = "selStudentStatus";
	String selBatch = "selBatch";
	String txtAnswer="txtAnswer";
	String hdnbatchModified_ = "hdnbatchModified_";
	String checkStarDate="checkStarDate";
	String checkEndDate="checkEndDate";
	String txtRecordsPerPage="txtRecordsPerPage";
	String test_attempt_count = "test_attempt_count";
	String ChapterName = "ChapterName";
	
	String BN_CITY_MASTER ="CityMasterBean";
	String BN_STATE_MASTER ="StateMasterBean";
	String BN_AREA_MASTER ="AreaMasterBean";
	
	String SecondaryEmailID = "SecondaryEmailID";
	String lngBatchID = "lngBatchID";
	String lngChapterID = "lngChapterID";
	String lngEducationalID = "lngEducationalID";
	String lngStudentStatusID = "lngStudentStatusID";
	String strChapterName = "strChapterName";
	String hiddenSelectedID = "hiddenSelectedID";
	String selChapter = "selChapter";
	String selAnswer = "selAnswer";
	String selComplexity = "selComplexity";
	String selTestName = "selTestName";
	String ACTION_GET_LIST ="ACTION_GET_LIST";
	String DELETE_RECORD = "DELETE_RECORD";
	
	
	String FindEventType = "FindEventType";
	String FindMembers = "FindMembers";
	
	String txtQuestionPaperName = "txtQuestionPaperName";
	String selBatchName = "selBatchName";
	
	String  hdnDesignSheet_index ="hdnDesignSheet_index";
	String txaQuestion = "txaQuestion";
	String txaAnswer = "txaAnswer";
	String txtMobileNo = "txtMobileNo";
	String selRole = "selRole";
	String selSecurityQuestions = "selSecurityQuestions";
	String txtSecurityAnswer = "txtSecurityAnswer";
	
	String dtStartDate = "dtStartDate";
	String dtEndDate = "dtEndDate";
	
	String fileLogo_= "fileLogo_";
	String TEST_TYPE = "TEST_TYPE";
	String TotalRentPaid= "TotalRentPaid";
	String txtOption1 = "txtOption1";
	String txtOption2 = "txtOption2";
	String txtOption3 = "txtOption3";
	String txtOption4 = "txtOption4";
	
	
	
	/*-------EventzAlley------------*/
	
	String eventID = "eventID";
	String UID = "UID";
	String EID = "EID";
	String ETID = "ETID";
	String TR_MEMBER_RUN_VIEW_BN = "MemberRunViewBean";
	String avgTime_ = "avgTime_";
	String hdnTempCurrentPage = "hdnTempCurrentPage";
	String Past_Event_Hidden_ID = "Past_Event_Hidden_ID";
	String DETAILS = "DETAILS" ;
	String BN_MEMBER_UPCOMING_EVENT = "MemberUpcomingEventView";
	String BN_EVENT_LAP_TIME_RESULT = "EventLapTimeResultBean";
	String BN_EVENT_TYPE_LAP = "EventTypeLapMasterBean";
	String BN_EVENT_RESULT = "EventResultBean";
	String TR_MEMBER_RECENT_VIEW_BN = "MemberRecentEventView";
	String TR_EVENT_RESULT_BN = "EventResultBean";
	String START_DATE_ = "START_DATE_";
	String START_TIME_ = "START_TIME_";
	String START_TIME = "START_TIME";
	String CD_TIME_BN = "TimeCodeBean";
	String REG_DATE = "REG_DATE";
	String BN_COUNTRY = "CountryBean";
	String Country = "Country";
	String ZipCode = "ZipCode";
	String State = "State";
	String City ="City";
	String Street = "Street";
	String EmergencyCntNo = "EmergencyCntNo";
	String PersonalBestLink = "PersonalBestLink";
	String AssignedUserName ="AssignedUserName";
	String CompanyName = "CompanyName";
	String OtherOrganization = "OtherOrganization";
	String hdnCreatedon_ = "hdnCreatedon_";
	String BN_EVENT_TYPE_TIME ="EventTypeTimeMasterBean"; 
	String BN_EVENT_TYPE_REGISTRATION = "EventTypeRegistrationBean";
	String hdnTimeID_ = "hdnTimeID_";
	String TotalDistance_ = "TotalDistance_";
	String hdnCreatedon = "hdnCreatedon";
	String hdnUpdatedBy = "hdnUpdatedBy";
	String hdnUpdatedOn = "hdnUpdatedOn";
	String END_DATE = "END_DATE";
	String txtEventTypeName_ = "txtEventTypeName_";
	String txtCertificateType_ = "txtCertificateType_";
	String txtDateTime_ = "txtDateTime_";
	String CD_CERTIFICATE_TYPE_BN = "CertificateTypeBean";
	String hdnActivityCounter_ = "hdnActivityCounter_";
	String hdnMenuCounter = "hdnMenuCounter";
	String selRoleType_ = "selRoleType_";
	String selMember = "selMember";
	String txtRole_ = "txtRole_";
	String selRoleID ="selRoleID";
	String BN_ROLE_MASTER = "RoleBean";
	String txtIMAGE_DESCRIPTION ="txtIMAGE_DESCRIPTION";
	String txtIMAGE_DESCRIPTION_ = "txtIMAGE_DESCRIPTION_";
	String txtIMAGE_BIB_NUM = "txtIMAGE_BIB_NUM";
	String txtIMAGE_BIB_NUM_ = "txtIMAGE_BIB_NUM_";
	String DISPLAY_IMAGE = "DISPLAY_IMAGE";
	String EventTypeStartTime = "EventTypeStartTime";
	String selCertificate = "selCertificate";
	String EventTypeName = "EventTypeName";
	String EventPlaceName = "EventPlaceName";
	String START_DATE = "START_DATE";
	String EventName ="EventName";
	String sign_in = "sign_in";
	String rememberme = "rememberme";
	String Action = "Action";
	String selEventName = "selEventName";
	String selEventName2 = "selEventName2";
	String BN_EVENT_MASTER = "EventMasterBean";
	String selEventType = "selEventType";
	String selEventTypeName = "selEventTypeName";
	String BN_EVENT_TYPE_MASTER = "EventTypeMasterBean";
	String BN_EVENT_MEMBER_LIST_VIEW = "EventMemberListViewBean";
	String find_event_type = "find_event_type";
	String Event_Name_Session = "Event_Name_Session";
	String Event_Type_Name_Session = "Event_Type_Name_Session";
	String event_type_session = "event_type_session";
	String BN_EVENT_PHOTO_UPLOAD = "EventPhotoUploadBean";
	String ImagePathName_= "ImagePathName_";
	String ImageBIBNoDescription_= "ImageBIBNoDescription_";
	String FirstName = "FirstName";
	String LastName = "LastName";
	String  CD_GENDER_BN = "MyBosGender";
	String DOB = "DOB";
	String CD_BLOOD_GROUP_BN = "BloodGroupBean";
	String selBloodGroup = "selBloodGroup";
	String Occupation = "Occupation";
	String secondaryNumber = "secondaryNumber";
	String primaryNumber = "primaryNumber";
	String emailID = "emailID";
	String UserName = "UserName";
	String password = "password";
	String confirmPassword ="confirmPassword";
	String selSecQuestion = "selSecQuestion";
	String CD_SECURITY_QUESTION_BN = "SecurityQuestionBean";
	String SecAnswer = "SecAnswer";
	String txtValidEmail = "txtValidEmail";
	String BN_USER_MASTER_BEAN="UserMasterBean";
	String BN_MAIL_VARIABLES_BEAN="MailVariableCodeBean";
	
	
	
	
	
	String propUpload_="propUpload_";
	String txtName = "txtName";
	String txtCHIP = "txtCHIP";
	String txtGUN = "txtGUN";
	String txtPACE = "txtPACE";
	String txtTimeTaken = "txtTimeTaken";
	String txtTimeTaken_ = "txtTimeTaken_";
	String txtAge = "txtAge";
	String selEmpStatus = "selEmpStatus";
	String hdnID = "hdnID";
	String ID = "ID";
	String strStatus ="strStatus";
	String txtDeptId = "longDept";
	String txtManagerID= "lngManagerID";
	String selEmpID = "selEmpID";
	String txtDOJ="DOJ";
	String txtManager="manager";
	String txtDepartment = "department";
	String selEducationalInstitute = "selEducationalInstitute";
	String chkIsActive = "chkIsActive";
	
	String hdnTestTimeHour = "hdnTestTimeHour";
	String hdnTestTimeMinutes = "hdnTestTimeMinutes";
	String hdnDesignSheet_ = "hdnDesignSheet_";
	
	String selTestID = "selTestID";
	String selQuestionID = "selQuestionID";
	
	String hdnCompanyLogo_ ="hdnCompanyLogo_";
	String hdnNumberOfQuestions = "hdnNumberOfQuestions";
	String hdnIsQuestionGenerated = "hdnIsQuestionGenerated";
	
	String hdnOption_1 = "hdnOption_1";
	String hdnOption_2 = "hdnOption_2";
	String hdnOption_3 = "hdnOption_3";
	String hdnOption_4 = "hdnOption_4";
	String hdnChoosedOption = "hdnChoosedOption";
	
	String strPageName = "strPageName";
	
	String strSelfTestName = "strSelfTestName";
	
	
	String BN_PROPERTY_DOCUMENT_VIEW = "PropertyDocumentMasterView";
	String BN_DOCUMENT_MASTER = "DocumentMasterBean";
	String txtNumOfQuestion = "txtNumOfQuestion";
	String txtIsTimeDependent = "txtIsTimeDependent";
	String txtTestTimeHour = "txtTestTimeHour";
	String txtTestTimeMinutes = "txtTestTimeMinutes";
	String txtMinMArks = "txtMinMArks";
	String txtFirstClassMarks ="txtFirstClassMarks";
	String txtDistinctionMarks = "txtDistinctionMarks";
	String txtNegetiveMarks= "txtNegetiveMarks";
	String txtMarksPerQuestion = "txtMarksPerQuestion";
	String txtTotalMarks="txtTotalMarks";
	
	
	
	
	String txtNumber = "txtNumber";
	String txtEmail = "txtEmail";
	String selState = "selState";
	String selDistrict = "selDistrict";
	String txtQuantity = "txtQuantity";
	String txtRemarks = "txtRemarks";
	
	
	String txtLoginDateStart = "txtLoginDateStart";
	String txtLoginDateEnd = "txtLoginDateEnd";
	String txtLastName = "txtLastName";
	String txtFirstName = "txtFirstName";
	String txtMail = "txtMail";
	String txtContactNumber = "txtContactNumber";
	String txtContactPerson = "txtContactPerson";
	String txtWebsite = "txtWebsite";
	String hdnLogoURL = "hdnLogoURL";
	String txaComments = "txaComments";
	String txaAddress = "txaAddress";
	String selCity = "selCity";
	String txtContactInfoFilter = "txtContactInfoFilter";
	String selPropertyTypeFilter = "selPropertyTypeFilter";
	String rdlGender = "rdlGender";
	String selUserRole = "selUserRole";
	String txtUserName = "txtUserName";
	String txtConfirmPassword = "txtConfirmPassword";
	String txtPassword = "txtPassword";
	String txtPasswordHint = "txtPasswordHint";
	String selBuilder = "selBuilder";
	String selLocality = "selLocality";
	String txtLocalityOthers ="txtLocalityOthers";
	String selPropertyType = "selPropertyType";
	String selLocalityFilter  = "selLocalityFilter";
	String selLoanStatus = "selLoanStatus";
	String selBank = "selBank";
	String selExecutive = "selExecutive";
	String selUserRoleFilter = "selUserRoleFilter";
	String selProduct_ = "selProduct_";
	String selProduct = "selProduct";
	
	String selLoanType = "selLoanType";
	String selPossessionStatus = "selPossessionStatus";
	String txtPriceLow = "txtPriceLow";
	String txtPriceHigh = "txtPriceHigh";
	String txtPrice = "txtPrice";
	String txtCityOthers = "txtCityOthers";
	String hdnProductName = "hdnProductName";
	String hdnAgentName = "hdnAgentName";
	String hdnAgentNumber = "hdnAgentName";
	String hdnStateName = "hdnStateName";
	String hdnDistrictName = "hdnDistrictName";
	
	String chkHomePageDiaplay = "chkHomePageDiaplay";
	String txaProjectDetails = "txaProjectDetails";
	String txtLandMark = "txtLandMark";
	String flLocalityMap = "flLocalityMap";
	String flProjectBluePrint = "flProjectBluePrint";
	String flMainImage = "flMainImage";
	String flImage1 = "flImage1";
	String flImage2 = "flImage2";
	String flImage3 = "flImage3";
	String txtTag = "txtTag";
	String txtSize = "txtSize";
	String txtListSize = "txtListSize";
	
	String txtDisplayOrder = "txtDisplayOrder";
	String selHomePageDisplayType = "selHomePageDisplayType";
	String chkHighlightDisplay = "chkHighlightDisplay";
	
	String txaMailContent = "txaMailContent";
	String txtMailSubject = "txtMailSubject";
	String flToMailIDExcel = "flToMailIDExcel";
	String txtFromMailID = "txtFromMailID";
	
	String hdnCreatedBy_ = "hdnCreatedBy_";
	String hdnCreatedOn_ = "hdnCreatedOn_";
	
	String hdnRowCounter = "hdnRowCounter";
	String hdnUserAction = "hdnUserAction";
	String hdnSelectedID = "hdnSelectedID";
	String hdnSelectedItemID = "hdnSelectedItemID";
	String hdnCreatedBy = "hdnCreatedBy";
	String hdnCreatedOn = "hdnCreatedOn";
	String hdnApprovalStatus = "hdnApprovalStatus";
	String hdnApprovedBy = "hdnApprovedBy";
	String hdnReminderSent = "hdnReminderSent";
	String hdnAjaxSubmit = "hdnAjaxSubmit";
	String hdnCurrentPage = "hdnCurrentPage";
	String hdnReviewCurrentPage = "hdnReviewCurrentPage";
	String hdnSkippedCurrentPage = "hdnSkippedCurrentPage";
	String hdnPageEdited = "hdnPageEdited";
	String hdnActivationCode = "hdnActivationCode";
	String hdnEmpNumber = "hdnEmpNumber";
	String hdnEmpID = "hdnEmpID";
	String hdnRefNumber = "hdnRefNumber";
	String hdnRefID = "hdnRefID";
	
	String txtDOB = "txtDOB";
	
	
	String txtGenFilter = "txtGenFilter";	
	String txtMobFilter = "txtMobFilter";	
	String txtPlaceFilter = "txtPlaceFilter";	
	
	
	String txtCategoryFilter ="txtCategoryFilter";
	String txtBibNoFilter= "txtBibNoFilter";
	String txtNameFilter = "txtNameFilter";	
	String selComplexityFilter = "selComplexityFilter";	
	String txtAgentNameFilter = "txtAgentNameFilter";
	String selCityFilter = "selCityFilter";
	String txtAgentNumberFilter = "txtAgentNumberFilter";
	String selStatusFilter ="selStatusFilter";
	String txtAgentEmailFilter = "txtAgentEmailFilter";
	String selStateFilter = "selStateFilter";
	String selEducationalInstituteFilter = "selEducationalInstituteFilter";
	
	String CD_PROPERTY_TYPE_BN = "PropertyTypeBean";
	
	
	//QuestionPop
	
	String txtMobileNoFilter="txtMobileNoFilter";
	String txtEmailIDFilter="txtEmailIDFilter";
	String selRoleFilter="selRoleFilter";
	String selChapterIDFilter="selChapterIDFilter";
	String txaQuestionFilter="txaQuestionFilter";
	String txaAnswerFilter="txaAnswerFilter";
	String txtStartDateFilter="txtStartDateFilter";
	String txtEndDateFilter="txtEndDateFilter";
	String chkActiveFilter="chkActiveFilter";
	String selStartDate_="selStartDate_";
	String selEndDate_="selEndDate_";
	
	
	String txtLoginDate = "txtLoginDate";
	String txtCustomerName = "txtCustomerName";
	String txtAppliedAmount = "txtAppliedAmount";
	String txtSanctionedAmount = "txtSanctionedAmount";
	String txtProposedROI = "txtProposedROI";
	String txtApplicableROI = "txtApplicableROI";
	String txtProposedTenure = "txtProposedTenure";
	String txtApplicableTenure = "txtApplicableTenure";
	String txtProcessingFee = "txtProcessingFee";
	String txtProcessingPeriod = "txtProcessingPeriod";
	String txtPreClosureCharges = "txtPreClosureCharges";
	String txtEMIAmount = "txtEMIAmount";
	String txtChequeLeaves = "txtChequeLeaves";
	String rdlPartPaymentAllowed = "rdlPartPaymentAllowed";
	String txaPartPaymentCondition = "txaPartPaymentCondition";
	String txtLoanAccountNumber = "txtLoanAccountNumber";
	String txtCompanyName = "txtCompanyName";
	String txaRejectionReason = "txaRejectionReason";
	
	String txtApplicableTenureFilter = "txtApplicableTenureFilter";
	String txtAppliedAmountFilter = "txtAppliedAmountFilter";
	String txtSanctionedAmountFilter = "txtSanctionedAmountFilter";
	
	String SEL_ID = "SEL_ID";
	String UN_SEL_ID = "UN_SEL_ID";
	String IS_AJAX_SUBMIT = "IS_AJAX_SUBMIT";
	String USER_ACTION = "USER_ACTION";
	String ROW_COUNTER = "ROW_COUNTER";
	String CURRENT_PAGE = "CURRENT_PAGE";
	String REVIEW_CURRENT_PAGE = "REVIEW_CURRENT_PAGE";
	String SKIPPED_CURRENT_PAGE = "SKIPPED_CURRENT_PAGE";
	String UNRECORDED_CURRENT_PAGE = "UNRECORDED_CURRENT_PAGE";
	String PAGE_TYPE = "PAGE_TYPE";
	
	String chkMain = "chkMain";
	String chkChild_ = "chkChild_";
	
	String hdnID_ =  "hdnID_";
	String txtName_ = "txtName_";
	String selState_ = "selState_";
	String txtAgentName_ = "txtAgentName_";
	String txtAgentNumber_ = "txtAgentNumber_";
	String txtAgentEmail_ = "txtAgentEmail_";
	String selEducationalInstitute_ = "selEducationalInstitute_";
	
	//QuestionPop
	
		String txaAddress_="txaAddress_";
		String txtMobileNo_="txtMobileNo_";
		String txtEmail_="txtEmail_";
		String selRole_="selRole_";
		String txtDOJ_="txtDOJ_";
		String txtUserName_="txtUserName_";
		String txtPassword_="txtPassword_";
		String selSecurityQuestions_="selSecurityQuestions_";
		String txtSecurityAnswer_="txtSecurityAnswer_";
		String selChapter_="selChapter_";
		String txaQuestion_="txaQuestion_";
		String txaAnswer_="txaAnswer_";
		String selAnswer_="selAnswer_";
		String txtStartDate_="txtStartDate_";
		String txtEndDate_="txtEndDate_";
		String chkIsActive_="chkIsActive_";
	    String hdnUpdatedBy_="hdnUpdatedBy_";
	    String hdnUpdatedOn_="hdnUpdatedOn_";
	
	String txtDesc_ = "txtDesc_";
	String txtContactNum_	= "txtContactNum_";
	String txtMail_	= "txtMail_";
	String txtPrefix_ = "txtPrefix_";
	String txtPIN_ = "txtPIN_";
	String selCity_ = "selCity_";
	String selLoanType_ = "selLoanType_";
	String selLoanTypeFilter = "selLoanTypeFilter";
		
	String selPropertyType_ = "selPropertyType_";
	String txtBHK_ = "txtBHK_";
	String txtBathRooms_ = "txtBathRooms_";
	String txtSize_ = "txtSize_";
	String txtPrice_ = "txtPrice_";
	String flFloorPlanURL_ = "flFloorPlanURL_";
	String hdnItemID_ = "hdnItemID_";
	String hdnFloorPlanURL_ = "hdnFloorPlanURL_";
	String hdnBuilderLogoURL = "hdnBuilderLogoURL";
	
	String hdnProjectBluePrintURL = "hdnProjectBluePrintURL";
	String hdnProjectLocationURL = "hdnProjectLocationURL";
	String hdnProjectMainImageURL = "hdnProjectMainImageURL";
	String hdnProjectImage2URL = "hdnProjectImage2URL";
	String hdnProjectImage3URL = "hdnProjectImage3URL";
	String hdnProjectImage1URL = "hdnProjectImage1URL";
	
	
	String hdnPropertyMainImageURL = "hdnPropertyMainImageURL";
	String hdnPropertyImage2URL = "hdnPropertyImage2URL";
	String hdnPropertyImage3URL = "hdnPropertyImage3URL";
	String hdnPropertyImage1URL = "hdnPropertyImage1URL";
	
	String selBudgetLow = "selBudgetLow";
	String selBudgetHigh = "selBudgetHigh";
	String selBedRooms = "selBedRooms";
	
	String selBathRooms = "selBathRooms";
	String txtStartDate = "txtStartDate";
	String txaBannerText = "txaBannerText";
	String txtEndDate = "txtEndDate";
	
	String txtPostedBy ="txtPostedBy";
    String txtPropertyNumber ="txtPropertyNumber";
    String txtPropertyNumberFilter= "txtPropertyNumberFilter";
	String txtApprovalDate ="txtApprovalDate";
	
	String hdnFilePath = "hdnFilePath";
	
	String hdnServiceImageURL="hdnServiceImageURL";
}